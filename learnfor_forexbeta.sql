-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 23, 2019 at 09:46 PM
-- Server version: 5.6.44-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `learnfor_forexbeta`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `delay` int(11) NOT NULL DEFAULT '5',
  `active` int(11) NOT NULL DEFAULT '1',
  `stop` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `callback_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `call_to_action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `admin`, `title`, `content`, `delay`, `active`, `stop`, `callback_url`, `call_to_action`, `created_at`, `updated_at`) VALUES
(5, 25, 'Welcome!', 'Welcome!\r\nWe are one of the most transparent FOREX SIGNAL service providers in the world today. Please, feel absolutely free to browse all contents and utilize our live chat window located at the bottom right corner of this page. If you have doubts, questions or contributions, contact us immediately.\r\nWelcome again!', 3, 1, '12/15/2018', NULL, NULL, '2018-09-02 23:56:49', '2018-09-02 23:56:49');

-- --------------------------------------------------------

--
-- Table structure for table `broadcast_schedule`
--

CREATE TABLE `broadcast_schedule` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `time` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `broadcast_schedule`
--

INSERT INTO `broadcast_schedule` (`id`, `admin_id`, `time`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 25, '05/21/2019 9:45 PM', NULL, '2019-05-21 01:27:58', '2019-05-21 01:27:58'),
(2, 1, '0000-00-00 00:00:00.000000', NULL, '2017-07-12 21:50:44', '2017-07-12 21:50:44'),
(3, 1, '0000-00-00 00:00:00.000000', NULL, '2017-07-12 21:51:17', '2017-07-12 21:51:17'),
(4, 1, '0000-00-00 00:00:00.000000', NULL, '2017-07-13 08:23:00', '2017-07-13 08:23:00'),
(5, 2, '0000-00-00 00:00:00.000000', NULL, '2017-07-14 11:54:22', '2017-07-14 11:54:22'),
(6, 2, '0000-00-00 00:00:00.000000', NULL, '2017-07-14 11:56:44', '2017-07-14 11:56:44');

-- --------------------------------------------------------

--
-- Table structure for table `channel`
--

CREATE TABLE `channel` (
  `id` int(10) UNSIGNED NOT NULL,
  `channel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'youtube',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `channel`
--

INSERT INTO `channel` (`id`, `channel`, `created_at`, `updated_at`) VALUES
(1, 'youtube', '2017-07-16 08:02:59', '2018-01-13 16:07:46');

-- --------------------------------------------------------

--
-- Table structure for table `equity`
--

CREATE TABLE `equity` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `equity`
--

INSERT INTO `equity` (`id`, `name`, `email`, `equity`, `created_at`, `updated_at`) VALUES
(1, 'Nick Hanson', 'nicholashanson@hotmail.co.uk', '47000', '2017-09-12 00:22:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `investors`
--

CREATE TABLE `investors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `broker_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt4_account_number` bigint(30) NOT NULL,
  `trading_account_balance` double(8,2) NOT NULL DEFAULT '0.00',
  `mt4_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `plan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `investors`
--

INSERT INTO `investors` (`id`, `name`, `email`, `broker_name`, `mt4_account_number`, `trading_account_balance`, `mt4_password`, `active`, `deleted_at`, `created_at`, `updated_at`, `plan`) VALUES
(1, 'Jacob Ashibi', 'jacshib@gmail.com', 'Hot Forex', 55555, 5676.00, '11111', 0, NULL, '2017-06-29 19:44:28', '2017-07-22 06:15:52', NULL),
(2, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'Hot Forex', 64444444, 66767.00, '1111', 0, NULL, '2017-06-29 20:44:32', '2017-07-22 06:15:55', NULL),
(3, 'Jacob Ashibi', 'learnforexlivesignals@gmail.com', 'Hot Forex', 56566, 458777.00, '1111', 0, NULL, '2017-06-30 06:06:15', '2017-07-22 06:15:59', NULL),
(4, 'John B.', 'fashib1@gmail.com', 'FX PRO', 3786748, 5600.00, 'rrrrrr', 0, NULL, '2017-06-30 15:56:47', '2017-07-22 06:16:01', NULL),
(5, 'Djimon Honsou', 'dele@findworka.com', 'Wall Street', 3231837021, 10990.00, 'secret', 0, NULL, '2017-06-30 23:27:00', '2017-07-21 23:15:16', NULL),
(6, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'Hot Forex', 454556, 56776.00, '1111', 0, NULL, '2017-07-01 07:29:52', '2017-07-22 06:16:06', NULL),
(7, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'Hot Forex', 354554, 55444.00, '1111', 0, NULL, '2017-07-01 07:30:51', '2017-07-22 06:16:08', NULL),
(8, 'Jacob Ashibi', 'fdssss@gmail.com', 'Hot Forex', 22222, 7000.00, '1111', 0, NULL, '2017-07-25 17:33:24', '2017-07-25 17:38:47', NULL),
(9, 'Essam El Hosseiny', 'eehosseiny@yahoo.com', 'FXCM', 99029365, 10000.00, 'ehkz7005', 0, NULL, '2017-07-26 00:56:08', '2017-07-26 02:58:31', NULL),
(10, 'Essam El Hosseiny', 'eehosseiny@yahoo.com', 'FXCM', 99029365, 10000.00, 'ehkz7005', 0, NULL, '2017-07-26 01:07:27', '2017-12-27 18:59:22', NULL),
(11, 'Deepa V', 'deepav6433@gmail.com', 'Hantec Markets', 56341807, 5000.00, 'power123$', 0, NULL, '2017-08-01 21:03:04', '2017-12-27 18:59:26', NULL),
(12, 'Katarina Kovacova', 'katarinagb@yahoo.com', 'IG Group Limited', 72304, 9999.00, 'USPech2017', 1, NULL, '2017-08-31 01:17:20', NULL, NULL),
(13, 'Nick Hanson', 'nicholashanson@hotmail.co.uk', 'ETX Capital London', 197381, 47000.00, 'hanson27', 0, NULL, '2017-09-12 00:21:27', '2018-02-03 21:49:18', NULL),
(14, 'Mr Lee Healy', 'lfhealy@hotmail.com', 'Alpari', 5210133, 5002.00, 'Annabelle1', 1, NULL, '2017-09-28 11:57:17', NULL, NULL),
(15, 'John Harris', 'jharris@gmail.com', 'Forex', 1546258, 500000.00, 'Sammy1964', 0, NULL, '2017-10-14 13:53:47', '2017-10-14 14:24:26', NULL),
(16, 'Princewill Samuel', 'prinzllsamuel@gmail.com', 'Princewill\'s Brookers', 12345, 5000.00, 'password', 0, NULL, '2018-01-07 22:58:11', '2018-01-22 11:59:21', NULL),
(17, 'James Riggle', 'jriggle854@gmail.com', 'Forex.com', 12293199, 5000.00, 'Smarttrading00', 0, NULL, '2018-01-08 17:57:49', '2018-06-22 16:48:03', NULL),
(18, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'XYZ', 1111, 11111.00, '1111', 0, NULL, '2018-01-18 22:48:05', '2018-01-19 20:41:35', NULL),
(19, 'joshua', 'theamazinceo@gmail.com', 'jsksksksoss`', 65456789098, 88889.00, 'Josh250198', 0, NULL, '2018-01-19 04:10:57', '2018-01-22 11:59:13', NULL),
(20, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'XYZ', 1111, 11111.00, '1111', 0, NULL, '2018-01-19 20:42:10', '2018-01-21 13:22:49', NULL),
(21, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'XYZ', 1111, 11111.00, '1111', 0, NULL, '2018-01-19 22:53:44', '2018-01-21 13:22:58', NULL),
(22, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'XYZ', 1111, 11111.00, '1111', 0, NULL, '2018-01-21 13:21:53', '2018-01-21 13:22:54', NULL),
(23, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'XYZ', 22222, 54434.00, '1111', 0, NULL, '2018-01-21 17:08:24', '2018-01-22 11:59:08', NULL),
(24, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'Big Man', 1234556, 50000.00, '111111', 0, NULL, '2018-01-21 23:58:26', '2018-01-22 11:59:03', NULL),
(25, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'Big Man', 1234556, 50000.00, '111111', 0, NULL, '2018-01-22 00:05:01', '2018-01-22 11:59:00', NULL),
(26, 'Inim Andrew', 'grapheneee@gmail.com', 'Jason', 23343434, 5000.00, 'andrew', 0, NULL, '2018-01-22 00:14:02', '2018-01-22 11:58:56', NULL),
(27, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'XYZ', 22222, 5000.00, '1111', 0, NULL, '2018-01-22 00:38:39', '2018-01-22 11:58:43', NULL),
(28, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'XYZ', 11111, 5886.00, '1111', 0, NULL, '2018-01-22 11:51:49', '2018-01-22 11:58:38', NULL),
(29, 'Inim Andrew', 'grapheneee@gmail.com', 'Jason', 123456, 20000.00, 'polinium', 0, NULL, '2018-01-22 16:39:26', '2018-01-31 16:07:46', NULL),
(30, 'Joshua Firima', 'zekofilmlimited@gmail.com', 'fffffff', 11111111111, 43434.00, '111111', 0, NULL, '2018-01-23 21:10:51', '2018-01-31 16:07:40', NULL),
(31, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'XYZ', 434333, 5000.00, '1111', 0, NULL, '2018-01-23 21:11:54', '2018-01-31 16:07:36', NULL),
(32, 'danny andy', 'graph@gmail.com', 'jason', 123456, 30000.00, '1234', 0, NULL, '2018-02-01 00:35:47', '2018-02-03 21:49:10', NULL),
(33, 'test test', 'testaccount@xyrz.com', 'jason', 1234567, 50000.00, '123456', 0, NULL, '2018-02-03 19:50:08', '2018-02-03 21:48:41', NULL),
(34, 'test', 'test@pp.com', 'tested', 123456, 7000.00, '12345', 0, NULL, '2018-02-04 13:24:30', '2018-02-04 14:05:38', NULL),
(35, 'hossein tayefehyehesami', 'ho.hesami@gmail.com', 'ROBOFOREX', 2914135, 5000.00, 'hesam1234', 0, NULL, '2018-02-04 14:42:22', '2018-09-17 22:18:35', NULL),
(36, 'Test', 'testaccount@xyz.com', 'XYZ', 133322, 34322.00, '1111', 0, NULL, '2018-02-04 18:02:46', '2018-02-04 18:04:06', NULL),
(37, 'hendrik', 'chaihendrik@gmail.com', 'ic market', 1000029470, 5000.00, 'eejj61', 1, NULL, '2018-02-05 18:42:32', NULL, NULL),
(38, 'Rolands', 'veinbergsr@gmail.com', 'CitadeleFXtrader', 1568402087, 10000.00, 't4cqohr', 0, NULL, '2018-02-06 22:14:35', '2018-02-23 03:29:16', NULL),
(39, 'asdf', 'msdaf@asd.com', 'dsf', 12323, 100000.00, '12345', 0, NULL, '2018-02-11 12:55:41', '2018-02-12 00:54:39', NULL),
(40, 'Test', 'testaccount@xyz.com', 'XYZ', 545544, 50000.00, '1111', 0, NULL, '2018-02-12 00:52:11', '2018-02-12 00:54:33', NULL),
(41, 'Ehtesham Bokhari', 'ehtesham.bokhari@yahoo.com', 'OneFinancialMarkets', 1266111, 5000.00, 'trading', 0, NULL, '2018-02-20 15:07:52', '2018-03-13 13:49:32', NULL),
(42, 'Test', 'cleanjaybarbers@gmail.com', 'Test', 111111, 30002.00, '1111', 0, NULL, '2018-02-21 15:05:33', '2018-02-23 03:29:04', NULL),
(43, 'test', 'tested@gmail.com', 'test', 12345, 20000.00, '12345', 0, NULL, '2018-02-21 15:14:43', '2018-02-23 03:28:59', NULL),
(44, 'jasdip samra', 'japsamra0@gmail.com', 'fxcm', 89009638, 5048.08, 'Mrsamra1', 1, NULL, '2018-02-27 23:56:39', '2018-02-28 14:39:18', NULL),
(45, 'bahador pazoki', 'bahadorpazoky@gmail.com', 'roboforex', 2919127, 5000.00, 'bahador1359', 0, NULL, '2018-03-03 11:08:35', '2018-09-17 22:18:45', NULL),
(46, 'Ehtesham Bokhari', 'ehtesham.bokhari@yahoo.com', 'OneFinancialMarkets', 1247413, 5000.00, 'managed409', 1, NULL, '2018-03-12 08:24:36', NULL, NULL),
(47, 'Joao Carlos Teixeira', 'joaocarlosbhbr@gmail.com', 'FXPrimus', 930098, 5000.00, 'JCT=jct$7', 0, NULL, '2018-03-15 16:32:50', '2018-06-22 17:13:02', NULL),
(48, 'Meenakshi Bagga', 'baggam30@gmail.com', 'Oanda', 9962750, 5000.00, 'peace2000', 1, NULL, '2018-03-22 04:58:08', NULL, NULL),
(49, 'Yih-An Hsueh', 'orchid1642@gmail.com', 'Global Prime', 985711, 5000.00, 'Jpmhvn031', 0, NULL, '2018-04-01 20:03:11', '2018-06-22 17:12:43', NULL),
(50, 'Liton Das', 'lintush@gmail.com', 'Orbex', 16750, 5000.00, 'VSvh8g5', 1, NULL, '2018-04-20 09:54:37', NULL, NULL),
(51, 'Mohammadjavad  Hamedparvaneh', 'usa.ecommerce@yahoo.com', 'HFMarketsSV-Live Server 4', 35049450, 5000.00, '9v&vuxZ4', 1, NULL, '2018-05-14 21:03:43', NULL, NULL),
(52, 'test', 'test@gmail.com', 'test', 1234561, 20000.00, '1234', 0, NULL, '2018-06-07 22:19:38', '2018-06-22 17:11:38', NULL),
(53, 'test', 'test@gmail.com', 'test', 1234567890, 30000.00, '12345', 0, NULL, '2018-07-01 18:05:51', '2018-08-06 18:43:30', NULL),
(54, 'G M Saydur Rahman', 'balakabd2008@gmail.com', 'Pepperstone', 323367, 5000.00, 'yx9tEhwe', 0, NULL, '2018-08-06 17:55:23', '2018-12-31 17:05:23', NULL),
(55, 'Jacob Ashibi', 'cleanjaybarbers@gmail.com', 'fgfddd', 23322222, 5000.00, '1111', 0, NULL, '2018-08-31 14:25:21', '2018-08-31 14:26:01', NULL),
(56, 'Nguyen Hung', 'hungvannguyen@gmail.com', 'FxPro', 9181905, 5000.00, 'fxpro2018$', 0, NULL, '2018-09-11 10:33:50', '2019-01-28 22:44:02', NULL),
(57, 'test', 'test@gmail.com', 'test', 12345, 50000.00, '1234', 0, NULL, '2018-09-23 16:13:58', '2018-10-26 12:47:17', NULL),
(58, 'test', 'test1@gmail.com', 'test', 12345, 30000.00, '1234', 0, NULL, '2018-09-23 17:02:47', '2018-10-26 12:47:13', NULL),
(59, 'test', 'test@gmail.com', 'test', 12345678, 30000.00, '1234', 0, NULL, '2018-10-14 17:13:23', '2018-10-26 12:47:10', NULL),
(60, 'Yangyang', 'selina@sorosedu.com', 'EopoeroScience-live', 202928, 5000.00, 'eopoero@123', 0, NULL, '2019-01-16 13:00:10', '2019-01-28 22:43:52', NULL),
(61, 'Jacob Ashibi', 'jacshib@gmail.com', 'iuikplokjik', 111111, 5500.00, '1111', 0, NULL, '2019-01-17 16:04:47', '2019-01-17 16:05:47', NULL),
(62, '王红', 'Selina@sorosedu.com', 'EOPOERO', 202915, 5000.00, 'eopoero@123', 0, NULL, '2019-01-18 11:52:36', '2019-01-28 22:43:47', NULL),
(63, 'Malghalara shah', 'shahmalghalara@gmail.com', 'onefinancialmarkets', 1213465, 5000.00, 'R7iaLMcL', 1, NULL, '2019-01-28 19:08:21', NULL, NULL),
(64, 'rongnan hu', '190998818@qq.com', 'EopoeroScience-live', 201840, 5000.00, 'eop@0703', 0, NULL, '2019-02-21 14:45:29', '2019-02-21 15:01:24', NULL),
(65, 'rongnan hu', '190998818@qq.com', 'EopoeroScience-live', 201840, 5000.00, 'eop@0703', 0, NULL, '2019-02-21 14:52:41', '2019-02-21 15:01:30', NULL),
(66, 'rongnan hu', '190998818@qq.com', 'EopoeroScience-live', 201840, 5000.00, 'eop@0703', 1, NULL, '2019-02-21 14:54:19', NULL, NULL),
(67, 'Jim Wisbey', 'jim@wiznetwork.co.uk', 'GKFX', 12534, 5000.00, 'Taro19!', 0, NULL, '2019-02-25 11:55:39', '2019-03-05 23:08:25', NULL),
(68, 'Jim Wisbey', 'jim@wiznetwork.co.uk', 'Pepperstone UK', 10009892, 5000.00, 'Taro19!', 0, NULL, '2019-03-04 15:16:54', '2019-03-05 23:08:21', NULL),
(69, 'James Wisbey', 'jim@wiznetwork.co.uk', 'Pepperstone UK', 10009892, 5000.00, 'Taro19!', 0, NULL, '2019-03-04 15:26:45', '2019-03-05 23:08:14', NULL),
(70, 'James Wisbey', 'jim@wiznetwork.co.uk', 'Pepperstone UK', 10009892, 5000.00, 'Taro19!', 0, NULL, '2019-03-04 15:29:25', '2019-03-05 23:08:11', NULL),
(71, 'James Wisbey', 'jim@wiznetwork.co.uk', 'Pepperstone UK', 10009892, 5000.00, 'Taro19!', 0, NULL, '2019-03-05 14:56:32', '2019-03-05 23:08:06', NULL),
(72, 'James Wisbey', 'jim@wiznetwork.co.uk', 'Pepperstone UK', 10009892, 5000.00, 'Taro19!', 1, NULL, '2019-03-05 16:07:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `line_copier_ea`
--

CREATE TABLE `line_copier_ea` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `line_copier_ea`
--

INSERT INTO `line_copier_ea` (`id`, `name`, `email`, `active`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Olamide Agbabiaka', 'yusuftunde001@yahoo.com', 0, NULL, '2017-06-30 23:17:22', '2017-07-22 06:13:28'),
(2, 'Jacob Ashibi', 'jacshib@gmail.com', 0, NULL, '2017-07-01 07:16:11', '2017-07-22 06:13:36'),
(3, 'Anthonyus Kuswanto', 'newbiz888fx@gmail.com', 0, NULL, '2017-07-23 22:33:02', '2017-07-24 00:03:30'),
(4, 'Anthonyus Kuswanto', 'newbiz888fx@gmail.com', 0, NULL, '2017-07-23 22:59:40', '2017-07-29 19:32:42'),
(5, 'Aleksandar Petković', 'alex69575@gmail.com', 0, NULL, '2017-07-24 12:37:04', '2017-07-24 20:34:32'),
(6, 'Aleksandar Petković', 'alex69575@gmail.com', 0, NULL, '2017-07-24 17:27:00', '2017-07-24 20:34:11'),
(7, 'scott', 'scott@learnforexlive.com', 0, NULL, '2017-07-24 21:23:28', '2017-07-28 15:05:42'),
(8, 'Rajath', 'crystalsforex@gmail.com', 0, NULL, '2017-07-24 21:25:33', '2017-07-29 19:32:38'),
(9, 'Al Chin', 'nowview@microinsight.com', 0, NULL, '2017-08-01 20:12:30', '2017-08-01 21:51:45'),
(10, 'Essam El Hosseiny', 'eehosseiny@yahoo.com', 0, NULL, '2017-08-10 15:37:35', '2017-08-10 16:08:38'),
(11, 'Okoro, Emmanuel Chigozie', 'gozieclick@gmail.com', 0, NULL, '2017-08-12 01:40:10', '2017-08-13 00:28:21'),
(12, 'Pejman Yadavar', 'pyadavar@yahoo.com', 0, NULL, '2017-08-13 07:22:25', '2017-08-18 00:57:08'),
(13, 'lahiru', 'kgppriyadarshana@gmail.com', 0, NULL, '2017-08-13 13:29:59', '2017-08-18 00:57:04'),
(14, 'Veli-Pekka Poutanen', 'vp.poutanen@gmail.com', 0, NULL, '2017-08-16 17:33:00', '2017-08-18 00:56:56'),
(15, 'Veli-Pekka Poutanen', 'vp.poutanen@gmail.com', 0, NULL, '2017-08-17 16:52:18', '2017-08-17 18:28:23'),
(16, 'Pejman Yadavar', 'pyadavar@yahoo.com', 0, NULL, '2017-08-18 00:06:01', '2017-08-18 00:57:01'),
(17, 'Md mojammel Haque Chy', 'hamidchowdhury2013@gmail.com', 0, NULL, '2017-08-19 23:38:45', '2017-08-20 18:49:23'),
(18, 'ogwo', 'wisdom4u2004@yahoo.com', 1, NULL, '2017-08-21 23:23:52', '2017-08-21 23:23:52'),
(19, 'Guy Maurice Donato', 'g_donat@yahoo.com', 1, NULL, '2017-08-31 11:02:52', '2017-08-31 11:02:52'),
(20, 'vamsidhar', 'webcomms@gmail.com', 1, NULL, '2017-09-03 17:14:34', '2017-09-03 17:14:34'),
(21, 'Jim Brown', 'jbrown999@gmail.com', 1, NULL, '2017-09-03 22:58:46', '2017-09-03 22:58:46'),
(22, 'Jim', 'jbrown999@gmail.com', 0, NULL, '2017-09-04 01:16:52', '2017-09-04 02:46:30'),
(23, 'alex', 'karamokotoma@gmail.com', 1, NULL, '2017-09-05 01:25:17', '2017-09-05 01:25:17'),
(24, 'TERRY MCREYNOLDS', 'tjmcreynolds@yahoo.com', 1, NULL, '2017-09-07 00:18:55', '2017-09-07 00:18:55'),
(25, 'Paul Peene', 'p.peene@solcon.nl', 1, NULL, '2017-09-11 10:32:08', '2017-09-11 10:32:08'),
(26, 'Steve Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-09-12 03:38:31', '2017-10-13 11:09:31'),
(27, 'jamie richardson', 'zincdox@rocketmail.com', 1, NULL, '2017-09-12 11:20:47', '2017-09-12 11:20:47'),
(28, 'Jeraman Guab', 'guab.jeraman@gmail.com', 1, NULL, '2017-09-12 17:24:50', '2017-09-12 17:24:50'),
(29, 'scott', 'scott@learnforexlive.com', 0, NULL, '2017-09-12 17:58:44', '2017-10-13 11:10:19'),
(30, 'Jacob', 'Jjjjd@hjdjd', 0, NULL, '2017-09-12 20:31:12', '2017-10-13 11:10:26'),
(31, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-09-13 10:01:20', '2017-10-13 11:09:52'),
(32, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-09-13 10:02:06', '2017-10-13 11:08:55'),
(33, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-09-13 10:02:48', '2017-10-13 11:09:00'),
(34, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-09-13 10:04:30', '2017-10-13 11:08:48'),
(35, 'Jose Zuniga', 'jaza138@yahoo.com', 1, NULL, '2017-09-13 10:14:32', '2017-09-13 10:14:32'),
(36, 'Vahid', 'daghighi.vahid@googlemail.com', 1, NULL, '2017-09-14 02:27:43', '2017-09-14 02:27:43'),
(37, 'Vahid', 'daghighi.vahid@googlemail.com', 0, NULL, '2017-09-14 02:28:01', '2017-10-13 11:10:45'),
(38, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-09-14 03:44:52', '2017-10-13 11:08:34'),
(39, 'md Mojammel Haque Chy', 'hamidchowdhury2013@gmail.com', 1, NULL, '2017-09-14 21:31:20', '2017-09-14 21:31:20'),
(40, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-09-15 04:28:28', '2017-10-13 11:09:05'),
(41, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-09-16 05:27:46', '2017-10-13 11:08:40'),
(42, 'Alastair Forres', 'aforres@gmail.com', 1, NULL, '2017-09-17 11:38:48', '2017-09-17 11:38:48'),
(43, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-09-18 08:49:07', '2017-10-13 11:10:06'),
(44, 'hurongnan', '190998818@qq.com', 1, NULL, '2017-09-18 11:48:04', '2017-09-18 11:48:04'),
(45, 'Md mojammel Haque Chowdhury', 'seorated@gmail.com', 1, NULL, '2017-09-18 23:13:11', '2017-09-18 23:13:11'),
(46, 'SOBANKE OLALEKAN', 'sobankeolalekan@yahoo.co.uk', 1, NULL, '2017-09-19 00:16:29', '2017-09-19 00:16:29'),
(47, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-09-19 01:55:55', '2017-10-13 11:10:01'),
(48, 'Muhibbullah Shiddique', 'bgmeaman@gmail.com', 1, NULL, '2017-09-19 09:20:10', '2017-09-19 09:20:10'),
(49, 'hurongnan', '190998818@qq.com', 0, NULL, '2017-09-19 13:27:05', '2017-10-13 11:08:13'),
(50, 'hurongnan', '190998818@qq.com', 0, NULL, '2017-09-21 14:10:09', '2017-10-13 11:07:38'),
(51, 'JJjjkdj', 'fdd@gmail.com', 0, NULL, '2017-09-24 12:48:09', '2017-10-13 11:07:30'),
(52, 'Sharmaarke Maxamed Cali', 'Sharmaarke.ma@gmail.com', 1, NULL, '2017-09-25 03:33:15', '2017-09-25 03:33:15'),
(53, 'Sharmaarke Maxamed Cali', 'Sharmaarke.ma@gmail.com', 0, NULL, '2017-09-25 03:33:33', '2017-10-13 11:07:24'),
(54, 'DSGD BHH', 'WFD@FGRGR', 0, NULL, '2017-09-25 14:29:09', '2017-10-13 11:06:54'),
(55, 'Rich Briley', 'chaplain_rick@hotmail.com', 1, NULL, '2017-09-25 20:20:13', '2017-09-25 20:20:13'),
(56, 'jfjkffgg', 'dgdhd@jjhff', 0, NULL, '2017-09-26 19:42:04', '2017-10-13 11:06:45'),
(57, 'freyeyry', 'werryturt@gg', 0, NULL, '2017-09-29 22:18:55', '2017-10-13 11:07:17'),
(58, 'DIEGO', 'brescia40@gmail.com', 0, NULL, '2017-10-01 19:23:12', '2017-10-13 11:06:39'),
(59, 'DIEGO', 'brescia40@gmail.com', 1, NULL, '2017-10-01 19:23:14', '2017-10-01 19:23:14'),
(60, 'Diego Barbera', 'brescia40@gmail.com', 0, NULL, '2017-10-01 22:18:09', '2017-10-13 11:07:11'),
(61, 'DIEGO', 'brescia40@gmail.com', 0, NULL, '2017-10-02 06:08:03', '2017-10-13 11:07:00'),
(62, 'Mohammed Aldossary', 'dammame@gmail.com', 1, NULL, '2017-10-02 09:53:47', '2017-10-02 09:53:47'),
(63, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 0, NULL, '2017-10-02 16:14:32', '2017-10-13 11:07:58'),
(64, 'hurongnan', '190998818@qq.com', 0, NULL, '2017-10-07 17:18:33', '2017-10-13 11:11:59'),
(65, 'Michael', 'mikechuakh@yahoo.com', 0, NULL, '2017-10-10 09:44:47', '2017-10-13 11:07:46'),
(66, 'Michael Chua', 'mikechuakh@yahoo.com', 1, NULL, '2017-10-10 09:45:41', '2017-10-10 09:45:41'),
(67, 'Stephen Gilbertson', 'stevegilbertson.au@gmail.com', 1, NULL, '2017-10-12 04:13:23', '2017-10-12 04:13:23'),
(68, 'Hyjd sghdkf', 'jsfgfhf@yejf.com', 0, NULL, '2017-10-13 10:57:07', '2017-10-13 11:06:24'),
(69, 'Steve Jones', 'stevejonesaz@yahoo.com', 1, NULL, '2017-10-14 13:49:26', '2017-10-14 13:49:26'),
(70, 'Veli Poutanen', 'vp.poutanen@gmail.com', 1, NULL, '2017-10-14 15:37:31', '2017-10-14 15:37:31'),
(71, 'Richard Anderson', 'andersonrichardd@yahoo.com', 1, NULL, '2017-10-18 14:25:49', '2017-10-18 14:25:49'),
(72, 'Jacob Ashibi', 'jacshib@gmail.com', 1, NULL, '2018-01-14 14:03:50', '2018-01-14 14:03:50');

-- --------------------------------------------------------

--
-- Table structure for table `live_videos`
--

CREATE TABLE `live_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `live_videos`
--

INSERT INTO `live_videos` (`id`, `admin_id`, `title`, `url`, `active`, `created_at`, `updated_at`) VALUES
(1, 25, 'SAMPLE VIDEO', 'https://youtu.be/NXU7sO3_Iao', 1, '2017-12-28 14:35:03', '2017-12-28 14:35:03');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(30, '2014_10_12_000000_create_users_table', 1),
(31, '2014_10_12_100000_create_password_resets_table', 1),
(32, '2017_06_03_090406_create_subscriptions_table', 1),
(33, '2017_06_03_091010_create_plans_table', 1),
(34, '2017_06_07_180751_create_messages_table', 1),
(35, '2017_06_17_113844_create_promos_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `completed` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `active` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `email`, `completed`, `active`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Jacob Ashibi', 'jacshib@gmail.com', 'yes', 1, NULL, NULL, '2017-07-22 06:14:58'),
(2, 'Tunde Yusuf', 'yusuphtunday@yahoo.com', 'yes', 1, NULL, '2017-06-30 23:20:44', '2017-07-21 23:04:43'),
(3, 'Rajath Rajath', 'Crystalsforex@gmail.com', 'yes', 1, NULL, '2017-07-25 17:11:02', '2017-07-26 12:29:12'),
(4, 'TERRY', 'tjmcreynolds@yahoo.com', 'yes', 1, NULL, '2017-07-26 03:01:26', '2017-07-26 12:36:08'),
(5, 'ali shahrivari', 'alishus@yahoo.com', 'yes', 1, NULL, '2017-07-26 03:44:13', '2017-07-26 12:36:12'),
(6, 'Mark', 'byattonline@gmail.com', 'yes', 1, NULL, '2017-07-29 20:29:45', '2017-07-29 21:48:45'),
(7, 'TERRY', 'tjmcreynolds@yahoo.com', 'yes', 1, NULL, '2017-08-03 02:18:35', '2017-08-03 09:27:47'),
(8, 'El Hosseiny', 'eehosseiny@yahoo.com', 'yes', 1, NULL, '2017-08-03 19:43:40', '2017-08-04 01:46:04'),
(9, 'mehrdad', 'mehrdad.shakery@gmail.com', 'yes', 1, NULL, '2017-08-05 14:27:09', '2017-08-05 17:43:46'),
(10, 'Charles Shi', 'Charles.shi7@gmail.com', 'yes', 1, NULL, '2017-08-06 13:56:03', '2017-08-06 14:52:36'),
(11, 'mehrdad', 'mehrdad.shakery@gmail.com', 'yes', 1, NULL, '2017-08-12 17:57:11', '2017-08-13 00:28:03'),
(12, 'Veli-Pekka Poutanen', 'vp.poutanen@gmail.com', 'yes', 1, NULL, '2017-08-16 23:38:28', '2017-08-17 01:22:48'),
(13, 'Steven Miller', 'stevenmiller33@hotmail.com', 'yes', 1, NULL, '2017-08-17 11:53:28', '2017-08-17 12:17:13'),
(14, 'Pejman Yadavar', 'pyadavar@yahoo.com', 'yes', 1, NULL, '2017-08-18 00:04:33', '2017-08-18 00:56:14'),
(15, 'Katarina', 'katarinagb@yahoo.com', 'yes', 1, NULL, '2017-08-18 02:29:58', '2017-08-18 09:01:12'),
(16, 'Bryan Alvin', 'bryanalvinlim@gmail.com', 'yes', 1, NULL, '2017-08-21 12:01:10', '2017-08-21 12:25:06'),
(17, 'wasfi', 'wcheaito@hotmail.com', 'yes', 1, NULL, '2017-08-22 20:14:51', '2017-08-22 21:29:47'),
(18, 'Ryan', 'Ryan_Rolle@Bellsouth.net', 'yes', 1, NULL, '2017-09-03 23:05:10', '2017-09-04 02:32:30'),
(19, 'Ryan', 'Ryan_Rolle@Bellsouth.net', 'yes', 1, NULL, '2017-09-04 23:28:34', '2017-09-05 10:49:28'),
(20, 'Joe Barbieto', 'jbroboform@gmail.com', 'yes', 1, NULL, '2017-09-05 06:25:15', '2017-09-05 10:49:21'),
(21, 'Nicholas Hanson', 'nicholashanson@hotmail.co.uk', 'yes', 1, NULL, '2017-09-12 09:12:44', '2017-09-12 11:26:33'),
(22, 'Misteli Stefan', 'st.misteli@bluewin.ch', 'yes', 1, NULL, '2017-09-12 11:05:06', '2017-09-12 11:25:17'),
(23, 'jamie', 'humbugle123@gmail.com', 'yes', 1, NULL, '2017-09-12 11:21:13', '2017-09-12 11:26:29'),
(24, 'Robert Reeves', '373hhc@gmail.com', 'yes', 1, NULL, '2017-09-12 16:21:58', '2017-09-12 18:56:25'),
(25, 'Anthony De Luca', 'tuca4.2016@gmail.com', 'yes', 1, NULL, '2017-09-14 05:04:33', '2017-09-14 10:10:48'),
(26, 'Robin Pike', 'robinpike423@gmail.com', 'yes', 1, NULL, '2017-09-14 09:39:23', '2017-09-14 10:10:45'),
(27, 'Steve Gilbertson', 'stevegilbertson.au@gmail.com', 'yes', 1, NULL, '2017-09-14 16:51:52', '2017-09-14 23:36:56'),
(28, 'Muhibbullah Shiddique', 'newblogtoday@gmail.com', 'yes', 1, NULL, '2017-09-16 16:48:06', '2017-09-17 21:58:41'),
(29, 'hurongnan', '190998818@qq.com', 'yes', 1, NULL, '2017-09-18 11:46:45', '2017-09-18 12:10:50'),
(30, 'Jorge Alberto Scotta', 'jorge.scotta@gmail.com', 'yes', 1, NULL, '2017-09-19 04:30:39', '2017-09-19 12:36:14'),
(31, 'Muhibbullah Shiddique', 'newblogtoday@gmail.com', 'yes', 1, NULL, '2017-09-19 09:54:55', '2017-09-19 12:36:08'),
(32, 'Mohammed Aldossary', 'dammame@gmail.com', 'yes', 1, NULL, '2017-09-19 13:48:04', '2017-09-19 16:10:04'),
(33, 'Sandra Puschnig', 'sandrajean222@yahoo.com', 'yes', 1, NULL, '2017-09-20 20:20:32', '2017-09-20 20:44:51'),
(34, 'Mohammed Aldossary', 'dammame@gmail.com', 'yes', 1, NULL, '2017-09-21 06:24:46', '2017-09-21 08:38:50'),
(35, 'Md mojammel Haque Chowdhury', 'hamidchowdhury2013@gmail.com', 'yes', 1, NULL, '2017-10-01 18:17:40', '2017-10-02 15:20:44'),
(36, 'Diego', 'brescia40@gmail.com', 'yes', 1, NULL, '2017-10-02 06:06:32', '2017-10-02 15:20:04'),
(37, 'Peter', 'pbrownewb@gmail.com', 'yes', 1, NULL, '2017-10-04 23:50:47', '2017-10-05 10:56:59'),
(38, 'Abimbola', 'bimbooyewo@yahoo.com', 'yes', 1, NULL, '2017-10-10 03:42:31', '2017-10-10 13:01:21'),
(39, 'Kenneth Mitchell', 'kenneth.mitchell66@yahoo.com', 'yes', 1, NULL, '2017-10-10 11:25:56', '2017-10-10 13:01:03'),
(40, 'Margaret Fadoju', 'mfadoju@gmail.com', 'yes', 1, NULL, '2017-10-11 21:43:24', '2017-10-11 22:50:03'),
(41, 'Paul Peene', 'p.peene@solcon.nl', 'yes', 1, NULL, '2017-10-11 23:13:47', '2017-10-12 00:55:19'),
(42, 'G M Saydur Rahman', 'balakabd2008@gmail.com', 'yes', 1, NULL, '2017-10-13 09:47:07', '2017-10-13 11:05:44'),
(43, 'mehrdad', 'mehrdad.shakery@gmail.com', 'yes', 1, NULL, '2017-10-13 13:37:22', '2017-10-13 13:41:44'),
(44, 'N\'guessan Richard', 'senateurr@gmail.com', 'yes', 1, NULL, '2017-10-14 21:13:39', '2017-10-15 00:09:04'),
(45, 'Michael Villondo', 'myke_villondo@yahoo.com', 'yes', 1, NULL, '2017-10-31 09:57:54', '2017-10-31 11:09:03'),
(46, 'JOHN LUGALE', 'J.LUGALE@UPCMAIL.NL', 'yes', 1, NULL, '2017-11-26 02:21:58', '2017-11-26 20:25:30'),
(47, 'Srdjan Perkic', 'srdjanperkic@gmail.com', 'yes', 1, NULL, '2017-11-30 15:20:34', '2017-11-30 19:22:03'),
(48, 'Jeff', 'jeffery.chen2010@gmail.com', 'yes', 1, NULL, '2017-12-02 12:36:29', '2017-12-02 17:27:25'),
(49, 'Eric Sackey', 'ricky123sack@gmail.com', 'yes', 1, NULL, '2017-12-12 03:33:31', '2017-12-12 14:47:03');

-- --------------------------------------------------------

--
-- Table structure for table `password_requests`
--

CREATE TABLE `password_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_requests`
--

INSERT INTO `password_requests` (`id`, `user_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2017-07-22 06:22:02', '2017-07-22 06:22:02'),
(2, 1, 1, '2017-07-22 11:04:03', '2017-07-22 11:04:03');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `title`, `description`, `url`, `validity`, `days`, `price`, `active`, `created_at`, `updated_at`) VALUES
(1, 'trial', '', 'https://safecart.com/fxknights/live/trial', '30 Days', 30, 10, 1, NULL, NULL),
(2, 'silver', '', 'https://safecart.com/fxknights/live/mnth', 'Month', 31, 50, 1, NULL, NULL),
(3, 'gold', '', 'https://safecart.com/fxknights/live/3mnth', '3 Months', 93, 120, 1, NULL, NULL),
(4, 'platinum', '', 'https://safecart.com/fxknights/live/6mnth', '6 Months', 186, 249, 1, NULL, NULL),
(5, 'platinum+', '', 'https://safecart.com/fxknights/live/12mnth', 'Year', 365, 420, 1, NULL, NULL),
(6, 'Life - Time', NULL, 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=XWKPV4CFSDHKW', 'Life Time ', 730, 1500, 1, NULL, NULL),
(7, 'Managed Account', NULL, NULL, NULL, NULL, 250, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `promos`
--

CREATE TABLE `promos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `days` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `promos`
--

INSERT INTO `promos` (`id`, `title`, `days`, `active`, `created_at`, `updated_at`) VALUES
(1, '2 weeks free live trading room access', 14, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `signals`
--

CREATE TABLE `signals` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `setup_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stop_loss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `high_impact_news_ahead` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `risk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `currency_pair` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `signals`
--

INSERT INTO `signals` (`id`, `admin_id`, `setup_type`, `entry`, `stop_loss`, `target_1`, `target_2`, `target_3`, `target_4`, `high_impact_news_ahead`, `quality`, `duration`, `risk`, `image`, `update`, `active`, `deleted_at`, `created_at`, `updated_at`, `currency_pair`) VALUES
(1, 13, 'Fake Breakout', 'Sell Stop Order @ 87.43', '88.67 (-124 pips)', 'first weekly candle close = 50%. Move Stop Loss to break-even.', 'Close the remaining 50%.', 'Nil', 'Nil', 'No interest rate. No monetary policy statement.', 'High', '2 Weeks', '3% Maximum of account balance', 'trade_signal_1LyU5oKxlahHaUn.jpg', 'Failed to trigger. Setup was violated and Sell stop order deleted!', 1, NULL, '2017-07-24 04:56:28', '2017-07-25 17:06:35', 'AUD/JPY (D1)'),
(2, 25, 'D1 3SMA Setup.', 'SELL @ 0.9924', '1.0071 (-145 pips)', '0.9769 ( Cashout 50% - Move Stop Loss to Entry)', '0.9708 (Cashout 15%)', '0.96514 (Cashout 20%)', '0.9621 ( Cashout remaining 15%)', 'No interest rate. No monetary policy statement.', 'Medium', 'Keep until targets are hit.', '2% of account balance', 'trade_signal_kwlUAUudyqJ2AQy.jpg', '1.) This trade will be closed at break even for strong technical reasons as analysed! 2.) Trade is now closed at break-even.', 1, NULL, '2017-07-24 19:31:19', '2017-07-26 00:48:03', 'AUD/CAD (D1)'),
(3, 25, 'Market Flow', '1.0667', '1.0640 (-26 pips Approx.)', '1.0699 (Cashout 50% and Move Stop Loss to break-even)', '1.0714', '1.0729', 'Nil', 'NO', 'Low', '8 hours Maximum', '2% of account balance', 'trade_signal_09En8AvGWkLBWic.jpg', '1.) First Target has been hit and SL trailed to break-even. 2.) Remaining portion has been closed for lack of price momentum.', 1, NULL, '2017-07-25 11:00:04', '2017-07-26 03:21:22', 'AUD/NZD (H1)'),
(4, 25, 'Aggressive Range Trading', '145.79', '146.47 (-70pips Approx.)', '144.98 (Cashout 50% and move SL to break-even).', '144.14 (Cashout remaining position).', 'Nil', 'Nil', 'No', 'Medium', '1 day.', '2% of account balance', 'trade_signal_lb02hOr1hShOVm8.jpg', '1) Stop Loss was hit! Trade ended in a loss.', 1, NULL, '2017-07-26 23:12:04', '2017-07-27 14:40:45', 'GBP/JPY (H4)'),
(5, 25, 'Technical failure off key area.', '0.9954', '0.9879 (-76 Pips Approx.)', '1.0021 ( Cashout 50% - Move Stop Loss to Entry)', '1.0082 (Remaining 50%). Trade should be closed.', 'Nil', 'Nil', 'No', 'Medium', '2 days', '2% of account balance', 'trade_signal_Hh61P4EPxJD0yQz.jpg', '1.) 50% of profit cashed out @ +30pips for dangerous pullback.  2.) Stop loss returned to original location .3.) 20% of profit cashed out @ +50pips and SL moved to BE. 4.) Trade closed @ BE!', 1, NULL, '2017-07-27 00:45:43', '2017-07-28 14:51:54', 'AUD/CAD (H1) - BUY / LONG Trade'),
(6, 25, 'Breakout - Pullback - Bounce.', '1.6383', '1.6544 (-166 pips Approx.)', '1.6277 (50%) Move Stop Loss to Break-even.', '1.6184 (Remaining 50%). Trade should be closed.', 'Nil', 'Nil', 'No', 'High', '2 Days.', '3% Maximum of account balance', 'trade_signal_8MspJzEgE6rUBuJ.jpg', 'Stop loss was hit!', 1, NULL, '2017-07-27 01:06:52', '2017-08-01 14:04:56', 'GBP/AUD (D1) - Avoid taking correlated pairs. At best, split your risk!'),
(11, 25, 'Market Flow', '1.7472', '1.7514 (-42 Pips Approx.)', '1.7436 (Cashout 50% of profit)', '1.7404 (Cashout 30% of profit and move SL to BE)', '1.7359 (Cashout remaining 20% of profit)', 'Nil', 'No', 'Medium', '7 Hours', '2% of account balance', 'trade_signal_MOlgoVDKnxWnOVZ.jpg', '1.) 50% of profit cashed out and SL move to BE as price hesitates around the 1.47500 round number area. 2.) Remaining portion hit at break-even. Trade closed!', 1, NULL, '2017-07-27 17:47:18', '2017-07-27 22:03:43', 'GBP/NZD (H1) - SELL'),
(13, 25, 'Aggressive TC', '145.58', '146.05 (-48 Pips Approx)', '145.09 (50%) Move Stop Loss to Break-even.', '144.47 (Cashout 30% of profit)', '144.06 (Close remaining portion of the trade)', 'Nil', 'No', 'Low', 'Until Targets are hit.', 'Maximum of 2% of account balance.', 'trade_signal_Df4flOezNVQ2db7.jpg', '1.) Cashout 50% of profit @ +25pips. Do not move SL to break-even. Keep the remaining portion of the trade till next week. 2.) Stop Loss Moved to break-even. 3.) Remaining portion hit @ break-even.', 1, NULL, '2017-07-28 13:57:51', '2017-07-31 20:14:38', 'GBP/JPY - SELL / SHORT'),
(14, 25, 'LOB', '1.2685', '1.2707 (-22 pips)', '1.2673 (Cashout 15%. Do not move SL to BE).', '1.2651 (Cashout 50%. Move SL to BE)', '1.2639 (Close the trade).', 'Nil', 'No', 'High', '9 hours', '2% of account balance', 'trade_signal_b1iP74DFvMLJ2kt.jpg', 'Stop loss was hit!', 1, NULL, '2017-07-31 11:44:42', '2017-07-31 12:04:03', 'GBP/CHF - SELL/SHORT'),
(15, 25, 'H4 BPC', '88.14', '88.50 (-40 Pips Approx)', '87.89 (Cashout 50%. Move Stop Loss to break-even)', '87.65 (Cashout 30%)', '87.29 (close the trade).', 'Nil', 'YES. 15 hours away.', 'Medium', '13 hours', '2% of account balance', 'trade_signal_dgn7dYJtS4EJXZO.jpg', '1.) Trade was close at break-even for lack of price momentum.', 1, NULL, '2017-07-31 16:53:48', '2017-07-31 20:13:53', 'AUD/JPY (H4)'),
(16, 25, 'Trend Collapse (TC)', '82.77', '83.06 (-30Pips Approx).', '82.56 (Cashout 50% of profit)', '82.36 (Cashout 30% of profit and move Stop Loss to breakeven)', '82.16 (Close the trade)', 'Nil', 'YES. 17 hours, 45 minutes away.', 'Medium', '16 Hours', '2% of account balance', 'trade_signal_bfXEdj5vSRE366T.jpg', '1.) TP1 hit @ +19pips.2.) Remaining portion closed for too much of price hesitation.', 1, NULL, '2017-08-01 08:43:01', '2017-08-01 16:27:34', 'NZD/JPY - SELL/SHORT'),
(17, 25, 'Market Flow', '1.0673', '1.0661', '1.0695 (Cashout 50% and move SL to BE)', '1.0705 (Cashout 30%)', '1.0715 (Close the trade)', 'Nil', 'No', 'Medium', '10 Hours', '2% of account balance', 'trade_signal_ix9RRPCmr6RzXuH.jpg', 'Stop loss was hit!', 1, NULL, '2017-08-01 11:29:24', '2017-08-01 17:47:04', 'AUD/NZD - BUY/LONG'),
(18, 25, 'Breakout - Congestion - Continuation Pattern.', '1.0720', '1.0694 (-28pips Approx)', '1.0744 (Cashout 50% and move Stop Loss to break-even)', '1.0769 (Close the trade).', 'Nil', 'Nil', 'No', 'Low', '8 hours', '1% of account balance', 'trade_signal_DaLJDzyQ9clp77x.jpg', '1.) 50% of profit cashed out. 2.) Trade close for too much of price hesitation.', 1, NULL, '2017-08-02 11:56:40', '2017-08-02 16:58:44', 'AUD/NZD - BUY/LONG'),
(20, 13, 'Fake Breakout', '1.6500', '1.6654 (Approx. -154 Pips)', '1.6389 (Cashout 50% of profit and move Stop Loss to break-even)', '1.6305 (Close remaining 50% of the trade)', 'Nil', 'Nil', 'No', 'High', '2 days', '3% Maximum of account balance', 'trade_signal_9sd9RYyADluomYK.jpg', '1.) 50% of profit cashed out and stop Loss moved to break-even. 2.) Last 50% was hit at break-even. The trade is now closed!', 1, NULL, '2017-08-04 00:59:41', '2017-08-07 13:37:38', 'GBP/AUD: D1 (SELL STOP/SHORT TRADE)'),
(21, 25, 'Breakout - Congestion - Continuation (BCC) Pattern.', '7.8499', '7.9774 (-1274 pips Approx.)', '1st weekly candle close (Cash out 50% of profit and move your Stop Loss to break-even).', '2nd weekly candle close (Cash our remaining 50% of profit. Close the trade).', 'Nil', 'Nil', 'No', 'High', '2 Weeks', '3% Maximum of account balance', 'trade_signal_ez6KPvi5cQa9TuF.jpg', '1.) Keep pending sell stop order until next week. 2.) Sell Stop order deleted for reasons of a potential reversal.', 1, NULL, '2017-08-07 01:02:35', '2017-08-14 09:02:45', 'USD/NOK - W1: SELL STOP.'),
(22, 25, 'Reversal', '0.9046', '0.9120 (-73 Pips Approx.)', 'first daily candle close = 50%. Move Stop Loss to break-even.', '2nd daily candle close (Cash our remaining 50% of profit. Close the trade).', 'Nil', 'Nil', 'No', 'Low', '2 days', '2% of account balance', 'trade_signal_frmpf0CYQlu031o.jpg', 'Trade Close for a scratch due to sign of market confusion in D1.', 1, NULL, '2017-08-09 00:56:40', '2017-08-10 01:15:07', 'EUR/GBP - SELL/SHORT'),
(23, 13, 'Reversal', '0.9703', '0.9777 (-74 Pips)', 'first daily candle close = Cashout 50%. Move Stop Loss to break-even.', '2nd daily candle close (Cash our remaining 50% of profit. Close the trade).', 'Nil', 'Nil', 'No', 'Medium', '2 days', '2% of account balance', 'trade_signal_4SNItZLrju9g37b.jpg', '1) 50% of profit cashed out @ +60 pips. Stop Loss moved to break-even. 2.) Remaining portion now closed @ +68 Pips.', 1, NULL, '2017-08-09 01:16:03', '2017-08-10 21:04:55', 'USD/CHF - SELL STOP'),
(24, 25, 'Fake Breakout', '86.29', '85.40 (-88 Pips Approx.)', 'first daily candle close = 50%. Move Stop Loss to break-even.', '2nd daily candle close (Cash our remaining 50% of profit. Close the trade).', 'Nil', 'Nil', 'YES (AUD: Monetary Policy Meeting Minutes - 38 hours away).', 'Medium', '2 days', '2% of account balance', 'trade_signal_GOWFlFombpFUsQD.jpg', '1.) Set TP to break-even for lack of performance and (AUD) monetary Policy Meeting Minutes release ahead. 2.) Trade has now been closed at break-even.', 1, NULL, '2017-08-14 00:36:52', '2017-08-15 11:06:18', 'AUD/JPY - BUY STOP'),
(26, 13, 'Trend Collapse (TC)', '0.7622', '0.7708 (-88 Pips Approx.)', '0.7534 (Cashout 50% and move Stop Loss to break-even.', '0.7449 (Cashout remaining 50%. Close the trade).', 'Nil', 'Nil', 'No', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_IsTXz33kWehsaj8.jpg', '1.) 50% cashed out @ +24 Pips. Stop Loss moved to break-even. 2.) Remaining portion is now closed for too much of price hesitation.', 1, NULL, '2017-08-16 00:57:01', '2017-08-18 11:02:31', 'CAD/CHF - SELL/SHORT'),
(27, 25, 'Trend Collapse (TC)', '129.64', '130.60 (-98 Pips Approx.)', '128.86 (Cashout 50% of profit).', '128.14 (Cashout 30% of profit and move stop loss to break-even)', '126.96 (Close the trade)', 'Nil', 'No', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_PR2IKXaCxBFDw3f.jpg', '1.) TP1 hit @ +79Pips. 2.) TP2 hit @ +150 Pips. Stop loss moved to break-even. 3.) Remaining Portion is now closed.', 1, NULL, '2017-08-17 00:56:30', '2017-08-21 20:05:53', 'EUR/JPY - SELL/SHORT'),
(28, 25, 'Breakout - Full swing Reversal.', '0.7928', '0.7787 (-141 pips Approx.)', '0.8005 (Cashout 30% of profit).', '0.8065 (Cashout 20% of profit & move SL to BE).', '0.8139 (Close the remaining 50% of the trade).', 'Nil', 'No', 'Medium', '2 Weeks', '2% of account balance', 'trade_signal_BwUiXT1bevGwzyf.jpg', 'Trade close @+30 pips profit.', 1, NULL, '2017-08-21 00:46:59', '2017-08-29 15:32:05', 'AUD/USD - BUY/LONG'),
(29, 25, 'London Open Breakout (LOB)', '1.2459', '1.2433 (-25 pips Approx.)', '1.2482 (Cashout 50% of profit)', '1.2495 (Cashout 20% and move SL to BE)', '1.2515 (Close the remaining 30% of the trade)', 'Nil', 'No', 'High', '8 hours', '3% Maximum of account balance', 'trade_signal_sF0vvRH8A9bctdX.jpg', 'SL was hit.', 1, NULL, '2017-08-21 13:13:52', '2017-08-21 16:38:29', 'GBP/CHF - BUY STOP'),
(30, 25, '3SMA TREND RIDING STRATEY', '1.2441', '1.24194 (-22 Pips Approx.)', '1.2473 (Cashout 50% of profit and move SL to BE).', '1.2495 (Cashout 30% of profit)', '1.2515 (Close the remaining 20% of the trade)', 'Nil', 'No', 'High', '8 hours Maximum', '3% Maximum of account balance', 'trade_signal_MGTBS1sXg79iUWn.jpg', 'SL was hit for the 2nd time!', 1, NULL, '2017-08-21 16:57:22', '2017-08-21 18:00:42', 'GBP/CHF - BUY/LONG'),
(31, 25, 'Fake Breakout', '1.6257', '1.6162 (-94 Pips Approx.)', '1.6382 (Cashout 50% of profit and move stop loss to break-even)', '1.6438 (Cashout the remaining 50% of profit)', 'Nil', 'Nil', 'No', 'Medium', '2 days', '2% of account balance', 'trade_signal_dBxk2itqaFS8MS3.jpg', 'Closed at break-even.', 1, NULL, '2017-08-23 05:09:16', '2017-08-24 11:28:16', 'GBP/AUD - BUY STOP ORDER'),
(32, 25, 'UNSPECIFIED', '0.9919', '0.9849 (Approx. -70pips).', 'TP1 = 0.9965 (Cashout 50% and move SL to BE).', 'TP2 = 1.0010 (Close the remaining portion of the trade).', 'Nil', 'Nil', 'No', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_skGQXfE3ByAxRx7.jpg', 'Trade closed @ +17Pips.', 1, NULL, '2017-08-24 01:20:25', '2017-08-28 19:40:15', 'AUD/CAD - BUY/LONG'),
(33, 25, 'UNSPECIFIED', '128.67', 'STOP LOSS = 129.52 (Approx. -82pips).', 'TP1 = 127.65 (Cashout 50% and move SL to BE).', 'TP2 = 127.01 (Close the remaining portion of the trade).', 'Nil', 'Nil', 'No', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_KcMCOy0IUaJG0u2.jpg', 'Stop loss was hit!', 1, NULL, '2017-08-24 01:32:19', '2017-08-25 14:46:41', 'EUR/JPY - SELL/SHORT'),
(34, 25, 'UNSPECIFIED', '0.7189', '0.7071 (Approx. -117 Pips)', '0.7258 ( Cashout 50% of profit - Move Stop Loss to Entry + spread)', '0.7313 (Cashout 30% of profit)', '0.7384 (Close the trade).', 'Nil', 'YES. 3 hours away. But impact might not change anything.', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_eO5Fl3W01vOSFvX.jpg', 'Order has been deleted!', 1, NULL, '2017-08-24 12:48:36', '2017-08-28 15:52:17', 'NZD/USD - BUY LIMIT ORDER'),
(35, 25, 'UNSPECIFIED', '87.57', '88.23 (Approx. -65Pips )', '87.12 (Cashout 50% of profit)', '86.70 (Cashout 20% of profit and move stop loss to entry + spread cost)', '86.13 (Cashout 15% of profit)', '85.59 (Close the remaining 15% of profit).', 'No', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_damQ0NzdRHoMHxT.jpg', 'Closed for a small profit.', 1, NULL, '2017-08-24 13:03:35', '2017-08-28 15:52:38', 'CAD/JPY - SELL LIMIT ORDER'),
(36, 25, 'Fake Breakout', '1.2358', '1.2260 (Approx. -95 pips)', '1.2425 (Cashout 50% of profit and move SL to BE).', '1.2472 (Close the remaining 50% of profit). Trade should be closed.', 'Nil', 'Nil', 'NO!', 'High', '2 Days.', '3% Maximum of account balance', 'trade_signal_KyYEW6TrD1K8ra0.jpg', '1.) TP1 hit @ +103 Pips. SL has been moved to BE. 2.) TP2 Cashed out @ +77 Pips. Trade is now closed.', 1, NULL, '2017-08-30 00:25:10', '2017-09-01 11:51:12', 'GBP/CHF - BUY STOP'),
(37, 25, 'Fake Breakout', '109.96', '108..81 (Approx. -115 pips)', '110.96 (Cashout 50% of profit and move SL to BE)', '111.76 (Close the remaining 50% of profit). Trade should be closed.', 'Nil', 'Nil', 'YES. 16 hours away.', 'High', '2 Days.', '2% of account balance', 'trade_signal_CmMLhJemKVNrvBO.jpg', '1.) TP1 cashed out @ +46 pips. 2.) TP2 cashed out @ +50 pips. Trade closed.', 1, NULL, '2017-08-30 00:38:40', '2017-09-01 11:49:08', 'USD/JPY - BUY STOP ORDER'),
(38, 25, 'Fake Breakout', '1.4957', '1.5039 (Approx. -78 pips)', '1.4910 (Cashout 50% and Move Stop Loss to break-even)', '1.4869 (Close the remaining 50% of profit). Trade should be closed.', 'Nil', 'Nil', 'NO!', 'Medium', '2 Days.', '2% of account balance', 'trade_signal_geo2IFkHzYvW4pc.jpg', 'SL was hit.', 1, NULL, '2017-08-30 00:55:13', '2017-08-30 19:59:36', 'EUR/CAD - SELL STOP ORDER'),
(39, 25, 'Breakout - Pullback - Continuation Pattern', '0.7985', '0.7900 (Approx. -86 pips)', '0.8051 (Cashout 50% and Move Stop Loss to break-even)', '0.8138 (Cashout remaining 50%. Close the trade).', 'Nil', 'Nil', 'YES. 16 hours away.', 'High', 'Until Targets are hit.', '2% of account balance', 'trade_signal_HhKx6u4VA0AgQ8M.jpg', 'SL was hit.', 1, NULL, '2017-08-30 01:05:22', '2017-08-30 19:59:21', 'AUD/USD - BUY STOP ORDER'),
(40, 13, 'Breakout - Pullback - Bounce', '1.8055', '1.7950 (Approx. -109 Pips)', 'Cashout 50% of profit as 1st daily candle close and move SL to BE.', 'Cashout the remaining 50% of profit.', 'Nil', 'Nil', 'No.', 'High', '2 days Maximum.', '3% of account account equity.', 'trade_signal_p4Rh9T0bfni1pD6.jpg', '1.) 50% of profit cashed out @ +59Pips. SL moved to BE. 2.) SL returned to 1.7915 to run remaining 50% as a BPC. 3.) SL was hit on remaining 50%.', 1, NULL, '2017-09-06 00:53:53', '2017-09-10 13:46:13', 'GBP/NZD - BUY STOP ORDER'),
(41, 25, 'Breakout - Pullback - Continuation Pattern', '1.8129', '1.7915 (Approx. -217 pips).', '1.8322 (Cashout 50% of profit and move SL to BE).', '1.8513 (Cashout 30% of profit).', '1.8635 (Close the trade).', 'Nil', 'No.', 'High', 'Until Targets are hit.', '2% of account balance', 'trade_signal_kB41UMJ7JcvYkXb.jpg', 'SL was hit.', 1, NULL, '2017-09-07 10:04:34', '2017-09-10 13:45:42', 'GBP/NZD : BUY STOP ORDER'),
(42, 13, 'Fake Breakout', '0.8037', '0.8094 (Approx. -59 Pips)', 'Cashout 50% of profit as 1st daily candle close and move SL to BE.', 'Cashout the remaining 50% of profit.', 'Nil', 'Nil', 'Not influential.', 'High', '2 days Maximum.', '2% of account account equity.', 'trade_signal_4mPVAT1pOSNLIhQ.jpg', '1.) Cashout a significant portion of the trade @ +11 pips and move SL to BE. 2.) Remaining portion was hit @ BE.', 1, NULL, '2017-09-10 13:24:59', '2017-09-12 13:26:45', 'AUD/USD - SELL STOP ORDER'),
(43, 25, 'Breakout - Pullback - Continuation Pattern', '1.8094', '1.7878 (Approx. -219 pips).', '1.8248 (Cashout 30% of profit.', '1.8543 (Cashout 33% of profit and move SL to BE).', '1,8826 ( Close the remaining portion of the trade).', 'Nil', 'YES. 3 days 12 hours away.', 'High', 'Until Targets are hit.', '2% of account balance', 'trade_signal_j2YLuppEBbKYtrU.jpg', '1.) TP1 was hit @ +119 Pips. SL has been moved to BE. 2.) Remaining portion of the trade has been closed @ +144 pips.', 1, NULL, '2017-09-10 13:51:43', '2017-09-13 12:41:52', 'GBP/NZD : BUY LIMIT ORDER'),
(44, 25, 'Reversal Formation.', '87.09', '88.02 (Approx. - 94 Pips).', '85.95 (Cashout 50% of profit and move SL to BE).', '84.95 (Cashout 30% of profit).', '83.81 (Close the trade).', 'Nil', 'No', 'High', 'Until Targets are hit.', '2% of account balance', 'trade_signal_xeQW023FBfDsyHj.jpg', 'SL was hit!', 1, NULL, '2017-09-10 14:06:50', '2017-09-14 23:40:15', 'AUD/JPY - SELL LIMIT ORDER'),
(48, 13, 'Reversal', '110.01', '110.82 (Approx. 83 pips)', 'Cashout 50% of profit as 1st daily candle close and move SL to BE.', 'Cashout the remaining 50% of profit as the 2nd daily candle close.', 'Nil', 'Nil', 'Not influential.', 'High', '2 days Maximum.', '3% of account account equity.', 'trade_signal_OWZpMHDXFTkfAQ1.jpg', 'SL was hit!', 1, NULL, '2017-09-15 00:55:12', '2017-09-15 13:34:13', 'USD/JPY - SELL STOP'),
(49, 13, 'Market Flow', '0.7720', '0.7708 (Approx. 12 Pips)', '0.7742 (Cashout 50% and Move stop Loss to break-even).', '0.7748 (Cashout 30% of profit)', '0.7758 (Close the trade).', 'Nil', 'No', 'Medium', '8 hours Maximum', '1% of Account equity', 'trade_signal_8dIpSdiNtuGIIMj.jpg', '1.) A small portion of profit has been cashed out (+11 Pips) on the appearance of magic dot against our best interest. 2.) TP2 hit @ +27Pips. 3.) TP3 hit @ +38 Pips.', 1, NULL, '2017-09-20 12:58:08', '2017-09-20 17:53:24', 'AUD/CHF - BUY/LONG'),
(51, 13, 'Market Flow', '88.79', '89.08 (Approx. 28 pips)', '88.54 (Cashout 50% of profit)', '88.36 (Cashout 30% of profit and move SL to BE)', '88.22 (Close the trade)', 'Nil', 'No.', 'High', '8 hours Maximum', '2% of account account equity.', 'trade_signal_sf6TZO9dvzR6Sre.jpg', 'SL was hit.', 1, NULL, '2017-09-22 10:45:07', '2017-09-22 11:33:36', 'AUD/JPY - SELL / SHORT'),
(55, 13, 'D1 3SMA', '1.2346', '1.2537 (Approx. -194 Pips)', '1.2106 (Cashout 50% and Move stop Loss to break-even).', 'Cashout 20% of profit @ Proj. band 1.', 'Cashout 15% of profit @ Proj. band 2.', 'Cashout 15% of profit @ Proj. band 3', 'Not influential.', 'High', 'Until targets are hit.', '2% of account account equity.', 'trade_signal_ofm2sripDHybSc8.jpg', '...trade is in progress.', 1, NULL, '2017-09-27 00:53:10', '2017-09-30 00:02:20', 'USD/CAD - SELL/SHORT'),
(56, 13, 'Reversal', '0.9693', '0.9747 (Approx. 55pips)', 'Cashout 50% of profit as 1st daily candle close and move SL to BE.', 'Cashout the remaining 50% of profit as the 2nd daily candle close.', 'Nil', 'Nil', 'No.', 'High', '2 days Maximum.', '2% of account account equity.', 'trade_signal_fTiv2qcAebnzkvU.jpg', '1.) 50% of profit cashed out @ +10Pips. SL maintained. 2.) Stopped out on remaining portion.', 1, NULL, '2017-09-29 01:05:44', '2017-10-05 13:14:20', 'USD/CHF - SELL STOP'),
(58, 13, 'Fake Breakout', '0.7843', '0.7799 (Approx. 43 Pips)', 'Cashout 50% of profit as 1st daily candle close and move SL to BE.', 'Cashout the remaining 50% of profit as the 2nd daily candle close.', 'Nil', 'Nil', 'No.', 'High', '2 days Maximum.', '2% of account account equity.', 'trade_signal_w16K37IcUTqG9SS.jpg', '1.) 50% cashed out @ +19 Pips. Stop Loss moved to break-even. 2.) Remaining portion hit at BE. THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-10-04 00:53:14', '2017-11-10 22:31:10', 'AUD/USD - BUY STOP ORDER'),
(59, 13, 'Reversal', '1.1744', '1.1692 (Approx. 55pips)', 'Cashout 50% of profit as 1st daily candle close and move SL to BE.', 'Cashout the remaining 50% of profit as the 2nd daily candle close.', 'Nil', 'Nil', 'No.', 'Medium', '2 days Maximum.', '2% of account account equity.', 'trade_signal_2lQuq79C7xHZMWN.jpg', '1.) 50% cashed out @ +32 Pips. Stop Loss moved to break-even. 2.) Remaining 50% of profit closed @ +69 pips. Trade closed. THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-10-09 00:37:02', '2017-11-10 22:30:55', 'EUR/USD - BUY STOP ORDER'),
(62, 13, 'Reversal', '113.59', '114.50 (Approx. 93pips)', 'Cashout 50% of profit as 1st daily candle close and move SL to BE.', 'Cashout the remaining 50% of profit as the 2nd daily candle close.', 'Nil', 'Nil', 'YES: Tuesday & Wednesday.', 'High', '2 days Maximum.', '2% of account account equity.', 'trade_signal_zGhHjeIXpP3MZRr.jpg', '1.) TP1 cashed out @ +44 pips. SL moved to break-even. 2.) Remaining portion cashed out @ +40 pips. THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-10-29 14:32:11', '2017-11-10 22:31:54', 'USD/JPY - SELL STOP'),
(63, 25, 'Aggressive Trade signal (New Trend formation).', '149.26', '150.98 (Approx. 177 pips).', '147.27 (Cashout 50% of profit and move SL to BE).', '146.19 (Cash our remaining 50% of profit. Close the trade).', 'Nil', 'Nil', 'YES. GBP- Bank Rates on Thursday.', 'Medium', 'Until Targets are hit.', '1% of account balance', 'trade_signal_iLc3QKKxMlNOKwn.jpg', 'Stopped out! THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-10-29 15:07:30', '2017-11-10 22:30:39', 'GBP/JPY - SELL/SHORT ENTRY'),
(64, 25, 'Aggressive Trade signal (New Trend formation).', '0.7654', '0.7730 (Approx. 74 Pips)', '0.7572 (Cashout 50% of profit and move SL to BE).', '0.7520 (Cashout 30% of profit).', '0.7488 (Cashout 20% of profit. Close the trade).', 'Nil', 'No.', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_slaouMR60VQZB7u.jpg', '1.) Stop loss now moved to break-even position. 2.) Trade closed @ +19 pips due to too much market hesitation. THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-10-29 15:22:07', '2017-11-10 22:30:19', 'AUD/CHF - SELL/SHORT'),
(69, 13, 'Fake Breakout', '114.01', '114.44 (Approx. 46 Pips)', 'First daily candle close = Cash-out 50% of profit. Move Stop Loss to break-even.', '2nd daily candle close (Cash our remaining 50% of profit. Close the trade).', 'Nil', 'Nil', 'No', 'High', '2 days', '2% of account balance', 'trade_signal_5cNxRLsOnsFp5zY.jpg', '1.) 50% of profit cashed out @ +48 pips. Stop loss now moved to break-even position. 2.) Remaining portion closed @ + 30 pips of profit. THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-11-07 02:53:33', '2017-11-10 22:29:57', 'USD/JPY - SELL LIMIT ORDER'),
(71, 25, 'Reversal', '1.1611', '1.1562 (Approx. 49 pips).', 'First daily candle close = Cash-out 50% of profit. Move Stop Loss to break-even.', '2nd daily candle close (Cash our remaining 50% of profit. Close the trade).', 'Nil', 'Nil', 'No', 'Medium', '2 days', '2% of account balance', 'trade_signal_M5OesAuUOSKIcsE.jpg', '1.) 50% of profit has now been cashed out @ +32pips and SL moved to break-even. 2.) Remaining portion has now been closed @ +52 pips. THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-11-09 13:02:29', '2017-11-10 22:44:51', 'EUR/USD - BUY'),
(72, 25, 'D1 3SMA', '1.6829', '1.6568 (Approx. -264 pips).', '1.7178 : Cash-out 50% of profit. Move Stop Loss to break-even.', 'Cashout 15% at ATR Projection 1.', 'Cashout 15% at ATR Projection 2.', 'Cashout 20% at ATR Projection 3.', 'No', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_Owf7RlDZm1WSWfh.jpg', '1.) 50% of profit cashed out @ +218 pips. SL moved to break-even ahead of several fundamental speeches.2.) A 30% has been cashed out @ +335 pips profit. 3.) Remaining portion close @ +327 pips profit. THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-11-10 16:14:01', '2017-11-15 21:53:07', 'EUR/NZD - BUY STOP ORDER'),
(74, 25, 'Aggressive Trade signal', '1.1667', '1.1592 (Approx. 68 Pips)', '1.1723 (Cashout 50% of profit and move SL to BE).', 'You will be prompted. Keep Open!', 'You will be prompted. Keep Open!', 'You will be prompted. Keep Open!', 'No', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_VG1Wnx0q16xhTIu.jpg', '1.) 50% of profit cashed out @ +51 pips. SL moved to break-even. TOO LATE. DO NOT JOIN.', 1, NULL, '2017-11-12 22:10:39', '2017-11-14 14:43:17', 'EUR/USD - BUY'),
(75, 13, 'Fake Breakout', '1.6722', '1.6583 (Approx. 138 Pips)', 'First daily candle close = Cash-out 50% of profit. Move Stop Loss to break-even.', '2nd daily candle close (Cash our remaining 50% of profit. Close the trade).', 'Nil', 'Nil', 'No', 'Medium', '2 days', '2% of account balance', 'trade_signal_OsU9uxyHQJ9glEp.jpg', 'Manually closed for a -52 pips loss for strong technical reasons. THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-11-14 04:29:58', '2017-11-14 16:52:25', 'GBP/CAD - BUY STOP ORDER'),
(77, 25, 'Aggressive Trade signal', '113.60', '114.82 (Approx. 122 Pips)', '112.32 = Cash-out 50% of profit. Move Stop Loss to break-even.', '111.75 (Close 30% of profit).', '110.58 (Close the trade).', 'Nil', 'No', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_yiBlELW54BWzvgm.jpg', '1.) 50% of profit cashed out @ +72 pips profit. Stop loss now moved to break-even. 2.) 30% cashed out @ +154 pips profit . TOO LATE. DO NOT JOIN.', 1, NULL, '2017-11-14 16:26:26', '2017-11-17 22:42:18', 'USD/JPY - SELL/SHORT'),
(78, 25, 'Aggressive Trade signal', '1.9071', '1.9501 (Approx. 246 Pips)', '1.8810 = Cash-out 50% of profit. Move Stop Loss to break-even.', '1.8720 (Close 30% of profit).', '1.8529 (Close the trade).', 'Nil', 'No', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_qPIxYzC1MN5K9ns.jpg', 'Stopped out! THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-11-14 18:12:49', '2017-11-17 22:42:48', 'GBP/NZD - SELL/SHORT'),
(79, 25, '3SMA', '1.2981', '1.3008 (Approx. 27 Pips)', '1.2955 (Cash-out 40% of profit)', '15% - H1 3SMA Projection band 1 (SL to BE)', '15% - H1 3SMA Projection band 2', '30% -  H41 3SMA Projection band 3.', 'No', 'Medium', 'Until Targets are hit.', '2% of account balance', 'trade_signal_NjWIIR4vBvCk7UE.jpg', 'Stopped out! THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-11-15 18:50:30', '2017-11-15 21:09:28', 'GBP/CHF - SELL /SHORT'),
(81, 25, 'Trend Collapse', '0.9878', '0.9953 (Approx. 77 Pips)', '0.9807 (Cashout 50% of profit and Move stop Loss to break-even).', '0.9767 (Close 30% of profit).', '0.9717 (Cashout 20% of profit. Close the trade).', 'Nil', 'No', 'Medium', 'Until targets are hit.', '2% of account account equity.', 'trade_signal_GEAgoGInWX6LbZE.jpg', '1.) 50% of profit cashed out @ +72 Pips. SL moved to break-even. 2.) 30% cashed out @ +94 pips. 3.) 20% closed out @ +72 pips. THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-11-20 02:56:17', '2017-11-28 00:23:15', 'USD/CHF - SELL/CHORT'),
(83, 25, 'Aggressive Trade signal', '1.2765', '1.2839 (Approx. 73 Pips)', '1.2674 (Cashout 50% and Move stop Loss to break-even).', '1.2636 (Close 30% of profit).', '1.2601 (Cashout 20% of profit. Close the trade).', 'Nil', 'No', 'Medium', 'Until targets are hit.', '2% of account account equity.', 'trade_signal_1KEAQheS69xcEbe.jpg', '1.) 50% cashed out @ +88 Pips. Stop Loss moved to break-even. TOO LATE. DO NOT JOIN.', 1, NULL, '2017-11-20 03:12:29', '2017-11-23 16:14:29', 'USD/CAD - SELL/SHORT'),
(90, 25, 'Fake Breakout', '151.16', '152.98 (Approx. 189 pips)', 'First daily candle close = Cash-out 50% of profit. Move Stop Loss to break-even.', '2nd daily candle close (Cash out remaining 50% of profit. Close the trade).', 'Nil', 'Nil', 'No', 'High', '2 days Maximum.', '2% - 3% of account account equity.', 'trade_signal_N4pJGgfMkQnjyNO.jpg', '1.) 50% of profit cashed out @ +61 pips. SL moved to break-even. 2.) Remaining 50% cashed out @ +97 pips. THIS TRADE IS NOW CLOSED!', 1, NULL, '2017-12-05 01:49:21', '2017-12-07 02:09:33', 'GBP/JPY - SELL STOP ORDER');

-- --------------------------------------------------------

--
-- Table structure for table `signal_counter`
--

CREATE TABLE `signal_counter` (
  `id` int(10) UNSIGNED NOT NULL,
  `count` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `signal_counter`
--

INSERT INTO `signal_counter` (`id`, `count`, `created_at`, `updated_at`) VALUES
(1, 58, '2017-07-27 19:28:38', '2018-01-21 17:03:37');

-- --------------------------------------------------------

--
-- Table structure for table `signal_dislikes`
--

CREATE TABLE `signal_dislikes` (
  `id` int(10) UNSIGNED NOT NULL,
  `signal_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `signal_dislikes`
--

INSERT INTO `signal_dislikes` (`id`, `signal_id`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 3, 37, NULL, '2017-07-25 18:02:51', '2017-07-25 18:02:51'),
(2, 3, 23, NULL, '2017-07-26 16:37:55', '2017-07-26 16:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `signal_likes`
--

CREATE TABLE `signal_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `signal_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `signal_likes`
--

INSERT INTO `signal_likes` (`id`, `signal_id`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 25, NULL, '2017-07-24 11:24:51', '2017-07-24 11:24:51'),
(2, 1, 37, NULL, '2017-07-24 13:20:18', '2017-07-24 13:20:18'),
(3, 1, 32, NULL, '2017-07-24 15:45:02', '2017-07-24 15:45:02'),
(4, 1, 24, NULL, '2017-07-24 17:05:50', '2017-07-24 17:05:50'),
(5, 1, 46, NULL, '2017-07-24 18:49:43', '2017-07-24 18:49:43'),
(6, 2, 46, NULL, '2017-07-24 19:33:09', '2017-07-24 19:33:09'),
(7, 2, 13, NULL, '2017-07-24 19:39:52', '2017-07-24 19:39:52'),
(8, 1, 13, NULL, '2017-07-24 19:39:55', '2017-07-24 19:39:55'),
(9, 2, 25, NULL, '2017-07-24 19:41:06', '2017-07-24 19:41:06'),
(10, 1, 40, NULL, '2017-07-25 04:18:42', '2017-07-25 04:18:42'),
(11, 2, 40, NULL, '2017-07-25 04:18:46', '2017-07-25 04:18:46'),
(12, 3, 25, NULL, '2017-07-25 11:03:59', '2017-07-25 11:03:59'),
(13, 3, 13, NULL, '2017-07-25 13:04:59', '2017-07-25 13:04:59'),
(14, 3, 41, NULL, '2017-07-25 17:43:56', '2017-07-25 17:43:56'),
(15, 2, 37, NULL, '2017-07-25 18:01:01', '2017-07-25 18:01:01'),
(16, 3, 37, NULL, '2017-07-25 18:02:54', '2017-07-25 18:02:54'),
(17, 3, 56, NULL, '2017-07-26 03:41:21', '2017-07-26 03:41:21'),
(18, 1, 56, NULL, '2017-07-26 03:43:19', '2017-07-26 03:43:19'),
(19, 4, 56, NULL, '2017-07-26 23:40:16', '2017-07-26 23:40:16'),
(20, 5, 37, NULL, '2017-07-27 00:57:37', '2017-07-27 00:57:37'),
(21, 4, 37, NULL, '2017-07-27 00:57:40', '2017-07-27 00:57:40'),
(22, 6, 56, NULL, '2017-07-27 01:15:38', '2017-07-27 01:15:38'),
(23, 6, 25, NULL, '2017-07-27 09:55:27', '2017-07-27 09:55:27'),
(24, 5, 25, NULL, '2017-07-27 09:55:30', '2017-07-27 09:55:30'),
(25, 4, 25, NULL, '2017-07-27 09:55:35', '2017-07-27 09:55:35'),
(26, 5, 56, NULL, '2017-07-27 10:43:21', '2017-07-27 10:43:21'),
(27, 5, 41, NULL, '2017-07-27 12:36:56', '2017-07-27 12:36:56'),
(28, 5, 28, NULL, '2017-07-27 13:09:14', '2017-07-27 13:09:14'),
(29, 10, 37, NULL, '2017-07-27 17:24:05', '2017-07-27 17:24:05'),
(30, 11, 37, NULL, '2017-07-27 17:53:52', '2017-07-27 17:53:52'),
(31, 11, 25, NULL, '2017-07-27 18:07:31', '2017-07-27 18:07:31'),
(32, 11, 56, NULL, '2017-07-27 18:33:35', '2017-07-27 18:33:35'),
(33, 11, 13, NULL, '2017-07-27 18:51:52', '2017-07-27 18:51:52'),
(34, 6, 13, NULL, '2017-07-27 18:51:58', '2017-07-27 18:51:58'),
(35, 5, 13, NULL, '2017-07-27 18:52:00', '2017-07-27 18:52:00'),
(36, 4, 13, NULL, '2017-07-27 18:52:03', '2017-07-27 18:52:03'),
(37, 13, 38, NULL, '2017-07-28 14:42:10', '2017-07-28 14:42:10'),
(38, 13, 56, NULL, '2017-07-28 14:44:55', '2017-07-28 14:44:55'),
(39, 13, 46, NULL, '2017-07-28 15:48:12', '2017-07-28 15:48:12'),
(40, 11, 46, NULL, '2017-07-28 15:48:20', '2017-07-28 15:48:20'),
(41, 13, 25, NULL, '2017-07-28 17:04:47', '2017-07-28 17:04:47'),
(42, 13, 13, NULL, '2017-07-28 18:18:49', '2017-07-28 18:18:49'),
(43, 11, 64, NULL, '2017-07-29 10:21:21', '2017-07-29 10:21:21'),
(44, 5, 43, NULL, '2017-07-31 10:18:46', '2017-07-31 10:18:46'),
(45, 6, 43, NULL, '2017-07-31 10:18:54', '2017-07-31 10:18:54'),
(46, 3, 43, NULL, '2017-07-31 10:19:00', '2017-07-31 10:19:00'),
(47, 4, 43, NULL, '2017-07-31 10:19:04', '2017-07-31 10:19:04'),
(48, 1, 43, NULL, '2017-07-31 10:19:11', '2017-07-31 10:19:11'),
(49, 2, 43, NULL, '2017-07-31 10:19:12', '2017-07-31 10:19:12'),
(50, 14, 25, NULL, '2017-07-31 11:47:35', '2017-07-31 11:47:35'),
(51, 14, 56, NULL, '2017-07-31 11:50:01', '2017-07-31 11:50:01'),
(52, 14, 37, NULL, '2017-07-31 11:52:55', '2017-07-31 11:52:55'),
(53, 6, 65, NULL, '2017-07-31 18:07:52', '2017-07-31 18:07:52'),
(54, 15, 25, NULL, '2017-07-31 20:16:11', '2017-07-31 20:16:11'),
(55, 15, 13, NULL, '2017-07-31 21:36:04', '2017-07-31 21:36:04'),
(56, 16, 25, NULL, '2017-08-01 09:18:15', '2017-08-01 09:18:15'),
(57, 17, 35, NULL, '2017-08-03 22:52:45', '2017-08-03 22:52:45'),
(58, 18, 35, NULL, '2017-08-03 22:52:47', '2017-08-03 22:52:47'),
(59, 20, 77, NULL, '2017-08-04 15:02:47', '2017-08-04 15:02:47'),
(60, 20, 56, NULL, '2017-08-05 01:14:37', '2017-08-05 01:14:37'),
(61, 20, 25, NULL, '2017-08-05 13:23:06', '2017-08-05 13:23:06'),
(62, 18, 25, NULL, '2017-08-05 13:23:08', '2017-08-05 13:23:08'),
(63, 17, 25, NULL, '2017-08-05 13:23:10', '2017-08-05 13:23:10'),
(64, 20, 41, NULL, '2017-08-07 01:40:28', '2017-08-07 01:40:28'),
(65, 21, 77, NULL, '2017-08-07 12:11:20', '2017-08-07 12:11:20'),
(66, 20, 85, NULL, '2017-08-07 16:04:54', '2017-08-07 16:04:54'),
(67, 20, 35, NULL, '2017-08-08 12:52:08', '2017-08-08 12:52:08'),
(68, 23, 35, NULL, '2017-08-09 09:00:59', '2017-08-09 09:00:59'),
(70, 22, 25, NULL, '2017-08-09 11:56:40', '2017-08-09 11:56:40'),
(73, 23, 41, NULL, '2017-08-09 17:20:20', '2017-08-09 17:20:20'),
(74, 23, 46, NULL, '2017-08-09 18:57:53', '2017-08-09 18:57:53'),
(75, 22, 46, NULL, '2017-08-09 18:57:58', '2017-08-09 18:57:58'),
(76, 23, 88, NULL, '2017-08-10 01:33:46', '2017-08-10 01:33:46'),
(79, 21, 1, NULL, '2017-08-13 18:26:43', '2017-08-13 18:26:43'),
(82, 24, 113, NULL, '2017-08-14 07:24:53', '2017-08-14 07:24:53'),
(85, 23, 25, NULL, '2017-08-14 11:09:45', '2017-08-14 11:09:45'),
(86, 24, 25, NULL, '2017-08-14 11:10:03', '2017-08-14 11:10:03'),
(88, 24, 41, NULL, '2017-08-14 14:45:53', '2017-08-14 14:45:53'),
(89, 24, 109, NULL, '2017-08-15 07:18:02', '2017-08-15 07:18:02'),
(91, 24, 76, NULL, '2017-08-16 22:51:06', '2017-08-16 22:51:06'),
(92, 27, 35, NULL, '2017-08-17 10:05:55', '2017-08-17 10:05:55'),
(93, 27, 131, NULL, '2017-08-17 15:47:29', '2017-08-17 15:47:29'),
(94, 27, 94, NULL, '2017-08-17 17:20:26', '2017-08-17 17:20:26'),
(95, 27, 25, NULL, '2017-08-17 23:25:55', '2017-08-17 23:25:55'),
(96, 26, 25, NULL, '2017-08-17 23:25:56', '2017-08-17 23:25:56'),
(97, 26, 131, NULL, '2017-08-18 02:59:50', '2017-08-18 02:59:50'),
(98, 27, 109, NULL, '2017-08-18 07:18:31', '2017-08-18 07:18:31'),
(99, 27, 13, NULL, '2017-08-18 09:29:50', '2017-08-18 09:29:50'),
(100, 26, 13, NULL, '2017-08-18 09:29:51', '2017-08-18 09:29:51'),
(101, 24, 13, NULL, '2017-08-18 09:29:55', '2017-08-18 09:29:55'),
(102, 23, 13, NULL, '2017-08-18 09:29:58', '2017-08-18 09:29:58'),
(103, 22, 13, NULL, '2017-08-18 09:30:05', '2017-08-18 09:30:05'),
(105, 21, 13, NULL, '2017-08-18 09:30:13', '2017-08-18 09:30:13'),
(106, 27, 113, NULL, '2017-08-18 10:08:12', '2017-08-18 10:08:12'),
(107, 26, 88, NULL, '2017-08-18 13:01:22', '2017-08-18 13:01:22'),
(109, 36, 25, NULL, '2017-09-01 13:37:46', '2017-09-01 13:37:46'),
(111, 37, 13, NULL, '2017-09-01 13:38:01', '2017-09-01 13:38:01'),
(112, 36, 13, NULL, '2017-09-01 13:38:02', '2017-09-01 13:38:02'),
(113, 40, 96, NULL, '2017-09-06 23:48:20', '2017-09-06 23:48:20'),
(114, 40, 25, NULL, '2017-09-10 13:46:33', '2017-09-10 13:46:33'),
(115, 37, 25, NULL, '2017-09-10 13:46:41', '2017-09-10 13:46:41'),
(116, 44, 43, NULL, '2017-09-12 09:34:00', '2017-09-12 09:34:00'),
(117, 43, 43, NULL, '2017-09-12 09:34:07', '2017-09-12 09:34:07'),
(118, 40, 248, NULL, '2017-09-12 13:08:52', '2017-09-12 13:08:52'),
(119, 48, 43, NULL, '2017-09-15 01:04:03', '2017-09-15 01:04:03'),
(120, 47, 43, NULL, '2017-09-15 01:04:06', '2017-09-15 01:04:06'),
(121, 48, 292, NULL, '2017-09-19 22:19:20', '2017-09-19 22:19:20'),
(122, 49, 234, NULL, '2017-09-20 13:50:59', '2017-09-20 13:50:59'),
(123, 49, 298, NULL, '2017-09-20 18:41:12', '2017-09-20 18:41:12'),
(124, 49, 301, NULL, '2017-09-20 18:54:12', '2017-09-20 18:54:12'),
(125, 48, 301, NULL, '2017-09-20 18:54:16', '2017-09-20 18:54:16'),
(126, 44, 301, NULL, '2017-09-20 18:54:26', '2017-09-20 18:54:26'),
(128, 51, 43, NULL, '2017-09-22 11:04:20', '2017-09-22 11:04:20');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `plan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `activated_on` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deactivated_on` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `plan_id`, `user_id`, `active`, `activated_on`, `deactivated_on`, `created_at`, `updated_at`) VALUES
(1, 2, 8, 0, '2017-07-05', '2017-08-05', '2017-07-05 22:35:22', '2017-07-05 22:35:22'),
(2, 1, 1, 0, '2017-06-29', '2017-07-29', '2017-06-29 17:03:08', '2017-06-29 17:03:08'),
(3, 6, 4, 0, '2017-06-30', '2017-07-31', '2017-06-30 01:30:22', '2017-06-30 01:30:22'),
(4, 6, 5, 0, '2017-06-30', '2017-07-31', '2017-06-30 13:52:50', '2017-06-30 13:52:50'),
(5, 6, 6, 0, '2017-06-30', '2017-07-31', '2017-06-30 14:28:58', '2017-06-30 14:28:58'),
(6, 6, 7, 0, '2017-06-30', '2017-07-31', '2017-06-30 15:54:17', '2017-06-30 15:54:17'),
(7, 4, 2, 0, '2017-07-08', '2018-07-08', '2017-07-08 16:22:06', '2017-07-08 16:22:06'),
(8, 1, 10, 0, '2017-07-13', '2018-07-13', '2017-07-13 05:30:50', '2017-07-13 05:30:50'),
(9, 2, 11, 0, '2017-07-14', '2017-08-14', '2017-07-14 12:39:25', '2017-07-14 12:39:25'),
(10, 1, 17, 0, '2017-07-22', '2017-08-21', '2017-07-23 02:43:17', '2017-07-23 02:43:17'),
(11, 1, 19, 0, '2017-07-23', '2017-08-22', '2017-07-23 09:30:11', '2017-07-23 09:30:11'),
(12, 1, 21, 0, '2017-07-23', '2017-08-22', '2017-07-23 16:08:54', '2017-07-23 16:08:54'),
(13, 1, 27, 0, '2017-07-23', '2017-08-22', '2017-07-23 22:03:35', '2017-07-23 22:03:35'),
(14, 1, 28, 0, '2017-07-23', '2017-08-22', '2017-07-24 01:10:42', '2017-09-01 14:27:36'),
(15, 1, 29, 0, '2017-07-23', '2017-08-22', '2017-07-24 00:24:12', '2017-07-24 00:24:12'),
(16, 1, 32, 0, '2017-07-23', '2017-08-22', '2017-07-24 00:56:04', '2017-08-23 06:56:43'),
(17, 5, 33, 0, '2018-07-30', '2019-07-30', '2018-07-30 19:51:24', '2018-07-30 19:51:24'),
(18, 1, 34, 0, '2017-07-23', '2017-08-22', '2017-07-24 01:40:54', '2017-07-24 01:40:54'),
(19, 1, 44, 0, '2017-07-24', '2017-08-23', '2017-07-24 11:50:20', '2017-07-24 11:50:20'),
(20, 2, 49, 0, '2017-07-24', '2017-08-23', '2017-07-25 00:48:14', '2017-07-25 00:48:14'),
(21, 2, 55, 0, '2017-07-25', '2017-08-25', '2017-07-26 03:04:32', '2017-07-26 03:04:32'),
(22, 1, 56, 0, '2017-07-25', '2017-08-24', '2017-07-26 03:27:41', '2017-07-26 03:31:19'),
(23, 1, 58, 0, '2017-07-26', '2017-08-25', '2017-07-27 01:41:17', '2017-08-30 02:07:39'),
(24, 1, 59, 0, '2017-07-27', '2017-08-26', '2017-07-27 11:21:43', '2017-07-27 11:21:43'),
(25, 1, 61, 0, '2017-07-28', '2017-08-27', '2017-07-28 13:56:24', '2017-07-28 13:58:11'),
(26, 3, 62, 0, '2017-08-30', '2017-12-01', '2017-08-30 10:18:57', '2017-08-30 10:18:57'),
(27, 1, 63, 0, '2017-07-29', '2017-08-29', '2017-07-29 09:54:51', '2017-07-29 09:54:51'),
(28, 1, 64, 0, '2017-07-29', '2017-08-28', '2017-07-29 09:59:24', '2017-07-29 10:02:06'),
(29, 1, 65, 0, '2017-07-29', '2017-08-28', '2017-07-29 15:38:07', '2017-07-29 15:39:57'),
(30, 1, 68, 0, '2017-07-29', '2017-08-28', '2017-07-29 20:31:21', '2017-07-29 21:32:31'),
(31, 1, 72, 0, '2017-07-30', '2017-08-29', '2017-07-30 09:55:15', '2017-09-01 00:41:46'),
(32, 1, 22, 0, '2017-07-30', '2017-08-29', '2017-07-30 12:51:38', '2017-08-30 10:20:16'),
(33, 2, 74, 0, '2017-07-31', '2017-08-31', '2017-07-31 16:20:18', '2017-07-31 16:20:18'),
(34, 2, 77, 0, '2017-11-01', '2017-12-02', '2017-11-01 12:41:41', '2017-11-01 13:15:13'),
(35, 5, 76, 1, '2017-08-09', '2018-08-09', '2017-08-09 16:16:05', '2017-08-09 16:16:05'),
(36, 3, 78, 1, '2019-05-07', '2019-08-08', '2019-05-06 10:23:56', '2019-05-07 10:18:55'),
(37, 5, 80, 0, '2017-09-08', '2017-10-08', '2017-09-08 08:00:12', '2017-09-08 08:00:12'),
(38, 1, 83, 0, '2017-08-11', '2017-09-10', '2017-08-11 06:28:53', '2017-09-11 04:06:48'),
(39, 1, 85, 0, '2017-08-06', '2017-09-05', '2017-08-06 13:41:56', '2017-09-07 10:05:25'),
(40, 1, 87, 0, '2017-08-06', '2017-09-05', '2017-08-07 02:24:41', '2017-09-07 16:13:16'),
(41, 3, 88, 0, '2017-09-10', '2017-10-10', '2017-09-10 11:52:11', '2017-09-10 12:07:59'),
(42, 2, 91, 0, '2017-08-07', '2017-09-07', '2017-08-08 01:49:16', '2017-08-08 01:49:16'),
(43, 1, 92, 0, '2017-08-08', '2017-09-07', '2017-08-08 06:36:33', '2017-08-08 06:36:33'),
(44, 2, 38, 0, '2017-08-10', '2017-09-10', '2017-08-10 15:26:48', '2017-08-10 15:26:48'),
(45, 2, 96, 0, '2017-12-04', '2018-01-04', '2017-12-04 11:00:14', '2017-12-04 11:01:27'),
(46, 4, 94, 0, '2017-08-17', '2017-09-16', '2017-08-18 02:28:54', '2017-09-18 18:57:26'),
(47, 1, 128, 0, '2017-08-14', '2017-09-13', '2017-08-14 22:04:28', '2017-08-14 22:04:28'),
(48, 3, 129, 0, '2019-03-18', '2019-04-18', '2019-03-18 18:27:32', '2019-03-18 18:27:32'),
(49, 1, 130, 0, '2017-08-15', '2017-09-14', '2017-08-15 22:56:47', '2017-08-15 22:56:47'),
(50, 2, 131, 0, '2019-01-24', '2019-02-23', '2019-01-25 04:05:30', '2019-01-25 04:05:30'),
(51, 1, 132, 0, '2017-08-16', '2017-09-15', '2017-08-16 08:55:49', '2017-08-16 09:40:31'),
(52, 1, 133, 0, '2017-08-16', '2017-09-15', '2017-08-16 19:58:35', '2017-08-16 19:58:35'),
(53, 1, 125, 0, '2017-08-31', '2018-08-31', '2017-08-31 09:05:44', '2017-08-31 09:05:44'),
(54, 1, 136, 0, '2017-08-19', '2017-09-18', '2017-08-19 10:16:27', '2017-08-19 10:16:27'),
(55, 1, 137, 0, '2017-08-19', '2017-09-18', '2017-08-19 10:34:54', '2017-08-19 10:34:54'),
(56, 1, 139, 0, '2017-08-19', '2017-09-18', '2017-08-19 13:44:10', '2017-08-19 13:44:10'),
(57, 3, 140, 0, '2017-08-19', '2017-11-20', '2017-08-19 13:55:02', '2017-08-19 13:55:02'),
(58, 1, 143, 0, '2017-08-19', '2017-09-18', '2017-08-19 20:36:32', '2017-08-19 20:36:32'),
(59, 3, 25, 0, '2019-05-02', '2020-05-01', '2019-05-02 11:47:43', '2019-05-02 11:47:43'),
(60, 1, 144, 0, '2017-08-19', '2017-09-18', '2017-08-20 01:46:30', '2017-08-20 01:46:30'),
(61, 3, 145, 0, '2017-08-19', '2017-11-20', '2017-08-20 02:05:01', '2017-08-20 02:05:01'),
(62, 1, 146, 0, '2017-08-20', '2017-09-19', '2017-08-20 14:55:32', '2017-08-20 14:55:32'),
(63, 1, 147, 0, '2017-08-20', '2017-09-19', '2017-08-20 19:03:45', '2017-08-20 19:03:45'),
(64, 1, 149, 0, '2017-08-20', '2017-09-19', '2017-08-21 02:31:20', '2017-08-21 05:25:51'),
(65, 1, 150, 0, '2017-08-21', '2017-09-20', '2017-08-21 11:13:53', '2017-08-21 11:16:58'),
(66, 1, 151, 0, '2017-08-21', '2017-09-20', '2017-08-21 11:46:06', '2017-08-21 11:46:06'),
(67, 1, 153, 0, '2017-08-23', '2017-09-22', '2017-08-23 09:23:18', '2017-08-23 09:23:18'),
(68, 1, 156, 0, '2017-08-23', '2017-09-22', '2017-08-23 20:30:27', '2017-08-23 20:30:27'),
(69, 5, 36, 1, '2018-09-11', '2019-09-11', '2018-09-12 01:01:26', '2018-09-12 01:01:26'),
(70, 1, 157, 0, '2017-08-24', '2017-09-23', '2017-08-24 19:51:59', '2017-08-24 19:56:30'),
(71, 1, 159, 0, '2017-08-26', '2017-09-25', '2017-08-26 14:48:25', '2017-09-26 12:05:48'),
(72, 3, 161, 0, '2017-09-02', '2017-10-02', '2017-09-02 21:21:41', '2017-09-02 21:21:41'),
(73, 5, 46, 1, '2017-08-27', '2018-08-27', '2017-08-27 14:45:58', '2017-08-27 14:48:16'),
(74, 3, 162, 0, '2018-02-10', '2018-05-14', '2018-02-10 23:41:20', '2018-02-10 23:41:20'),
(75, 6, 163, 0, '2018-09-17', '2018-10-18', '2018-09-17 23:50:37', '2018-09-17 23:50:37'),
(76, 2, 24, 1, '2018-09-19', '2018-10-20', '2018-09-19 12:18:59', '2018-09-19 12:19:45'),
(77, 1, 165, 0, '2017-08-28', '2017-09-27', '2017-08-28 05:24:50', '2017-08-28 09:16:05'),
(78, 1, 168, 0, '2017-08-29', '2017-09-28', '2017-08-29 17:04:06', '2017-08-29 17:06:34'),
(79, 2, 171, 0, '2017-08-30', '2017-09-29', '2017-08-30 16:01:28', '2017-08-30 16:12:20'),
(80, 1, 172, 0, '2017-08-30', '2017-09-29', '2017-08-30 16:28:08', '2017-08-30 16:28:37'),
(81, 5, 173, 0, '2017-08-30', '2018-08-30', '2017-08-30 17:17:50', '2017-08-30 17:17:50'),
(82, 1, 175, 0, '2017-08-31', '2017-09-30', '2017-08-31 11:22:01', '2017-08-31 11:25:34'),
(83, 1, 166, 0, '2017-08-31', '2017-09-30', '2017-08-31 15:24:43', '2017-08-31 15:55:33'),
(84, 1, 176, 0, '2017-08-31', '2017-09-30', '2017-09-01 00:49:45', '2017-09-01 03:22:28'),
(85, 1, 177, 0, '2017-08-31', '2017-09-30', '2017-09-01 03:26:02', '2017-10-01 16:30:41'),
(86, 1, 178, 0, '2017-09-01', '2017-10-01', '2017-09-01 12:39:10', '2017-10-02 13:18:11'),
(87, 2, 179, 0, '2017-09-01', '2017-10-02', '2017-09-01 18:28:11', '2017-09-01 18:28:11'),
(88, 2, 180, 0, '2018-02-05', '2018-03-08', '2018-02-05 19:06:28', '2018-02-05 19:21:45'),
(89, 1, 181, 0, '2017-09-02', '2017-10-02', '2017-09-02 15:23:24', '2017-09-02 15:23:24'),
(90, 1, 67, 0, '2017-09-02', '2017-10-02', '2017-09-03 01:30:30', '2017-09-03 01:30:30'),
(91, 1, 186, 0, '2017-09-03', '2017-10-03', '2017-09-03 17:20:42', '2017-09-03 17:20:42'),
(92, 1, 188, 0, '2017-09-03', '2017-10-03', '2017-09-03 19:54:12', '2017-11-27 10:02:51'),
(93, 2, 191, 0, '2018-07-10', '2018-08-10', '2018-07-10 10:19:53', '2018-07-10 10:19:53'),
(94, 1, 190, 0, '2017-09-04', '2017-10-04', '2017-09-04 23:03:51', '2017-09-04 23:03:51'),
(95, 1, 192, 0, '2017-09-05', '2017-10-05', '2017-09-05 13:33:27', '2017-09-05 14:11:28'),
(96, 1, 193, 0, '2017-09-05', '2017-10-05', '2017-09-05 22:27:04', '2017-09-05 22:27:04'),
(97, 3, 195, 1, '2019-05-20', '2019-08-21', '2019-05-20 23:34:20', '2019-05-21 01:30:25'),
(98, 2, 199, 0, '2017-09-07', '2017-10-07', '2017-09-07 17:31:46', '2017-09-07 17:31:46'),
(99, 5, 201, 0, '2017-09-08', '2018-03-13', '2017-09-08 19:02:13', '2017-09-08 19:02:13'),
(100, 1, 202, 0, '2017-09-09', '2017-10-09', '2017-09-09 19:05:01', '2017-09-09 21:04:28'),
(101, 5, 204, 0, '2017-09-10', '2017-10-10', '2017-09-10 12:17:40', '2017-09-10 12:17:40'),
(102, 3, 205, 0, '2017-09-10', '2017-12-12', '2017-09-10 12:25:01', '2017-12-14 13:09:55'),
(103, 1, 207, 0, '2017-09-25', '2018-09-25', '2017-09-25 12:53:50', '2017-09-25 12:53:50'),
(104, 1, 208, 0, '2017-09-11', '2017-10-11', '2017-09-11 10:18:44', '2017-09-11 10:18:44'),
(105, 1, 210, 0, '2017-09-11', '2017-10-11', '2017-09-11 17:17:15', '2017-09-11 17:22:15'),
(106, 5, 211, 0, '2017-09-11', '2018-03-16', '2017-09-11 21:10:21', '2017-09-11 21:10:21'),
(107, 5, 212, 0, '2017-09-11', '2018-09-11', '2017-09-11 22:05:41', '2017-09-11 22:05:41'),
(108, 3, 213, 0, '2017-09-11', '2017-12-13', '2017-09-11 22:38:27', '2017-09-11 22:38:27'),
(109, 1, 215, 0, '2017-09-11', '2017-10-11', '2017-09-11 23:23:08', '2017-09-12 01:05:45'),
(110, 4, 219, 0, '2017-09-19', '2017-10-19', '2017-09-19 23:20:09', '2017-09-19 23:20:09'),
(111, 1, 220, 0, '2017-09-11', '2017-10-11', '2017-09-12 00:19:43', '2017-10-12 15:46:43'),
(112, 2, 221, 0, '2017-09-11', '2017-10-11', '2017-09-12 00:40:53', '2017-09-12 01:47:34'),
(113, 1, 222, 0, '2017-09-11', '2017-10-11', '2017-09-12 01:13:47', '2017-09-12 01:16:30'),
(114, 1, 223, 0, '2017-09-11', '2017-10-11', '2017-09-12 01:16:58', '2017-09-12 01:22:57'),
(115, 1, 224, 0, '2017-09-11', '2017-10-11', '2017-09-12 01:25:21', '2017-09-12 01:30:05'),
(116, 1, 226, 0, '2017-09-11', '2017-10-11', '2017-09-12 01:49:15', '2017-09-12 04:15:45'),
(117, 3, 227, 1, '2019-03-05', '2019-06-06', '2019-03-06 00:05:24', '2019-03-06 00:14:56'),
(118, 1, 228, 0, '2017-09-11', '2017-10-11', '2017-09-12 02:13:12', '2017-09-12 02:15:57'),
(119, 1, 229, 0, '2017-09-11', '2017-10-11', '2017-09-12 02:18:58', '2017-09-12 02:21:10'),
(120, 1, 230, 0, '2017-10-12', '2018-10-12', '2017-10-12 04:17:32', '2017-10-12 04:17:32'),
(121, 1, 231, 0, '2017-09-11', '2017-10-11', '2017-09-12 03:40:45', '2017-09-12 04:13:16'),
(122, 1, 233, 0, '2017-09-11', '2018-03-16', '2017-09-12 03:52:38', '2017-09-12 03:52:38'),
(123, 1, 234, 0, '2017-09-12', '2017-10-12', '2017-09-12 04:05:26', '2017-09-12 04:24:24'),
(124, 1, 235, 0, '2017-09-12', '2017-10-12', '2017-09-12 04:27:09', '2017-09-12 04:30:58'),
(125, 1, 236, 0, '2017-09-12', '2017-10-12', '2017-09-12 04:28:58', '2017-09-12 09:47:00'),
(126, 1, 237, 0, '2017-09-12', '2017-10-12', '2017-09-12 04:41:28', '2017-09-12 09:50:24'),
(127, 5, 238, 0, '2017-09-12', '2018-09-12', '2017-09-12 05:05:57', '2017-09-12 05:05:57'),
(128, 1, 239, 0, '2017-09-12', '2017-10-12', '2017-09-12 05:40:32', '2017-09-12 09:52:21'),
(129, 1, 240, 0, '2017-09-12', '2017-10-12', '2017-09-12 06:10:31', '2017-09-12 21:08:59'),
(130, 2, 241, 0, '2017-10-11', '2017-11-10', '2017-10-11 20:14:56', '2017-10-11 20:14:56'),
(131, 1, 242, 0, '2017-09-12', '2017-10-12', '2017-09-12 08:00:43', '2017-09-12 09:57:33'),
(132, 2, 244, 0, '2017-10-18', '2017-11-17', '2017-10-18 08:48:03', '2017-10-18 08:56:41'),
(133, 1, 245, 0, '2017-09-12', '2017-10-12', '2017-09-12 09:59:05', '2017-10-16 06:13:33'),
(134, 1, 246, 0, '2017-09-12', '2017-10-12', '2017-09-12 10:02:15', '2017-09-12 10:47:28'),
(135, 2, 247, 0, '2017-10-11', '2017-11-11', '2017-10-11 14:40:27', '2017-10-11 14:40:27'),
(136, 3, 248, 0, '2017-10-11', '2018-01-12', '2017-10-11 06:28:00', '2017-10-11 06:28:00'),
(137, 6, 250, 0, '2018-09-17', '2018-09-27', '2018-09-17 21:53:26', '2018-09-17 21:53:26'),
(138, 1, 251, 0, '2017-09-12', '2017-10-12', '2017-09-12 14:44:39', '2017-09-12 15:04:51'),
(139, 1, 252, 0, '2017-09-12', '2018-09-12', '2017-09-12 14:57:41', '2017-09-12 15:06:39'),
(140, 1, 253, 0, '2017-09-12', '2017-10-12', '2017-09-12 16:08:48', '2017-09-13 10:36:56'),
(141, 1, 254, 0, '2017-09-12', '2017-10-12', '2017-09-12 16:23:04', '2017-09-12 17:36:24'),
(142, 1, 255, 0, '2017-09-12', '2017-10-12', '2017-09-12 16:29:23', '2017-09-12 16:29:23'),
(143, 5, 258, 0, '2017-10-17', '2018-10-17', '2017-10-17 21:43:32', '2018-02-16 02:20:02'),
(144, 1, 259, 0, '2017-09-12', '2017-10-12', '2017-09-12 19:20:42', '2017-09-12 19:27:44'),
(145, 1, 260, 0, '2017-09-12', '2017-10-12', '2017-09-12 20:45:18', '2017-09-12 20:45:18'),
(146, 1, 261, 0, '2017-09-12', '2017-10-12', '2017-09-12 20:52:26', '2017-09-12 20:52:26'),
(147, 1, 262, 0, '2017-09-12', '2017-10-12', '2017-09-12 20:55:48', '2017-09-12 20:55:48'),
(148, 1, 232, 0, '2017-09-12', '2017-10-12', '2017-09-12 21:22:24', '2017-10-13 04:08:27'),
(149, 1, 264, 0, '2017-09-12', '2017-10-12', '2017-09-12 23:13:34', '2017-09-12 23:22:40'),
(150, 1, 265, 0, '2017-09-12', '2017-10-12', '2017-09-13 00:19:36', '2017-09-13 00:26:31'),
(151, 1, 266, 0, '2017-09-12', '2017-10-12', '2017-09-13 03:53:19', '2017-09-13 04:17:32'),
(152, 1, 267, 0, '2017-09-13', '2017-10-13', '2017-09-13 04:04:56', '2017-09-13 04:04:56'),
(153, 1, 268, 0, '2017-09-13', '2017-10-13', '2017-09-13 05:05:28', '2017-09-13 05:05:28'),
(154, 1, 269, 0, '2017-09-13', '2017-10-13', '2017-09-13 09:58:43', '2017-09-13 09:58:43'),
(155, 1, 271, 0, '2017-09-13', '2017-10-13', '2017-09-13 11:20:52', '2017-09-13 11:20:52'),
(156, 1, 273, 0, '2017-09-13', '2017-10-13', '2017-09-13 14:16:52', '2017-09-13 14:21:07'),
(157, 1, 274, 0, '2017-09-13', '2017-10-13', '2017-09-13 14:41:55', '2017-09-13 14:46:23'),
(158, 1, 275, 0, '2017-09-13', '2017-10-13', '2017-09-13 15:23:25', '2017-09-14 10:52:37'),
(159, 1, 276, 0, '2017-09-13', '2017-10-13', '2017-09-13 16:17:07', '2017-09-13 16:27:25'),
(160, 2, 277, 0, '2017-09-13', '2017-10-13', '2017-09-13 17:06:03', '2017-09-13 17:06:03'),
(161, 2, 279, 0, '2017-09-13', '2017-10-14', '2017-09-13 17:15:31', '2017-09-13 17:36:47'),
(162, 1, 280, 0, '2017-09-13', '2017-10-13', '2017-09-13 17:58:52', '2017-09-13 17:58:52'),
(163, 1, 281, 0, '2017-09-13', '2017-10-13', '2017-09-13 18:57:42', '2017-09-15 12:49:41'),
(164, 1, 282, 0, '2017-09-13', '2017-10-13', '2017-09-13 19:00:05', '2017-09-13 19:00:05'),
(165, 3, 283, 0, '2017-09-13', '2017-12-15', '2017-09-13 19:57:25', '2017-10-30 21:45:09'),
(166, 1, 284, 0, '2017-09-13', '2017-10-13', '2017-09-13 20:13:01', '2017-09-13 20:18:01'),
(167, 1, 285, 0, '2017-09-13', '2017-10-13', '2017-09-13 21:49:12', '2017-09-13 21:54:16'),
(168, 1, 286, 0, '2017-09-13', '2017-10-13', '2017-09-13 22:52:11', '2017-09-13 22:52:11'),
(169, 1, 287, 0, '2017-09-13', '2017-10-13', '2017-09-14 00:28:45', '2017-09-14 00:46:37'),
(170, 1, 289, 0, '2017-09-14', '2017-10-14', '2017-09-14 06:58:56', '2017-09-14 06:58:56'),
(171, 2, 290, 0, '2017-09-14', '2017-10-15', '2017-09-14 07:43:30', '2017-09-14 07:43:30'),
(172, 2, 256, 0, '2017-10-31', '2017-11-30', '2017-10-31 11:24:53', '2017-10-31 11:24:53'),
(173, 1, 291, 0, '2017-09-14', '2017-10-14', '2017-09-14 15:51:35', '2017-09-19 16:01:44'),
(174, 1, 293, 0, '2017-09-14', '2017-10-14', '2017-09-15 01:33:29', '2017-09-16 11:36:28'),
(175, 5, 294, 0, '2017-10-10', '2017-11-09', '2017-10-10 19:23:01', '2017-10-10 19:23:01'),
(176, 1, 292, 0, '2017-09-15', '2017-10-15', '2017-09-15 13:37:02', '2017-09-18 23:42:26'),
(177, 1, 296, 0, '2017-09-16', '2017-10-16', '2017-09-16 07:06:00', '2017-09-17 17:02:23'),
(178, 2, 297, 0, '2018-03-27', '2018-04-27', '2018-03-27 20:37:30', '2018-05-23 15:11:28'),
(179, 1, 298, 0, '2017-09-16', '2017-10-16', '2017-09-16 12:08:05', '2017-09-16 12:13:44'),
(180, 1, 299, 0, '2017-09-16', '2017-10-17', '2017-09-16 14:53:07', '2017-09-16 14:58:41'),
(181, 1, 301, 0, '2017-09-17', '2017-10-17', '2017-09-17 15:04:44', '2017-10-18 10:41:00'),
(182, 1, 303, 0, '2017-09-17', '2017-10-17', '2017-09-18 00:34:38', '2017-09-18 00:34:38'),
(183, 1, 304, 0, '2017-10-20', '2017-11-19', '2017-10-20 09:13:00', '2017-10-20 09:13:00'),
(184, 1, 305, 0, '2017-09-18', '2017-10-18', '2017-09-18 12:33:06', '2017-09-18 12:38:00'),
(185, 1, 306, 0, '2017-09-18', '2017-10-18', '2017-09-18 12:48:39', '2017-09-18 12:53:26'),
(186, 1, 307, 0, '2017-09-18', '2017-10-18', '2017-09-18 13:47:35', '2017-09-18 13:51:38'),
(187, 5, 308, 1, '2018-09-20', '2019-09-20', '2017-09-18 21:44:35', '2018-09-20 20:56:49'),
(188, 2, 312, 0, '2017-09-20', '2017-10-20', '2017-09-21 02:15:25', '2017-09-21 02:15:25'),
(189, 1, 313, 0, '2017-09-20', '2017-10-20', '2017-09-20 16:52:58', '2017-09-20 16:57:38'),
(190, 1, 314, 0, '2017-09-20', '2017-10-20', '2017-09-20 16:56:20', '2017-09-20 16:56:20'),
(191, 1, 315, 0, '2017-09-20', '2017-10-20', '2017-09-20 17:25:40', '2017-09-20 18:08:34'),
(192, 1, 316, 0, '2017-09-20', '2017-10-20', '2017-09-20 17:38:22', '2017-09-20 18:10:45'),
(193, 1, 317, 0, '2017-09-20', '2017-10-20', '2017-09-20 19:21:59', '2017-09-20 19:25:38'),
(194, 1, 318, 0, '2017-09-20', '2017-10-20', '2017-09-20 22:59:56', '2017-10-21 04:07:26'),
(195, 1, 319, 0, '2017-09-21', '2017-10-21', '2017-09-21 06:05:01', '2017-09-21 06:05:01'),
(196, 1, 321, 0, '2017-09-21', '2017-10-21', '2017-09-21 14:08:38', '2017-09-26 08:48:25'),
(197, 1, 322, 0, '2017-09-21', '2017-10-21', '2017-09-21 18:40:43', '2017-10-23 14:54:09'),
(198, 1, 324, 0, '2017-09-22', '2017-10-22', '2017-09-22 05:21:44', '2017-09-22 05:21:44'),
(199, 1, 325, 0, '2017-09-22', '2017-10-22', '2017-09-22 08:51:29', '2017-09-22 08:51:29'),
(200, 1, 326, 0, '2017-09-23', '2017-10-23', '2017-09-23 14:55:41', '2017-09-23 16:47:02'),
(201, 1, 327, 0, '2017-09-23', '2017-10-23', '2017-09-23 22:30:47', '2017-09-23 22:30:47'),
(202, 1, 329, 0, '2017-09-24', '2017-10-24', '2017-09-24 09:23:12', '2017-09-24 09:23:12'),
(203, 1, 331, 0, '2017-09-24', '2017-10-24', '2017-09-24 15:53:05', '2017-09-24 15:53:05'),
(204, 1, 332, 0, '2017-09-24', '2017-10-24', '2017-09-24 21:51:27', '2017-09-24 21:51:27'),
(205, 1, 333, 0, '2017-09-24', '2017-10-24', '2017-09-25 00:35:08', '2017-12-13 01:48:42'),
(206, 1, 334, 0, '2017-09-25', '2017-10-25', '2017-09-25 12:57:09', '2017-10-26 19:45:40'),
(207, 1, 336, 0, '2017-09-26', '2017-10-26', '2017-09-26 08:43:19', '2017-09-26 08:43:19'),
(208, 1, 337, 0, '2017-09-26', '2017-10-26', '2017-09-26 12:26:42', '2017-10-27 04:09:13'),
(209, 1, 338, 0, '2017-09-26', '2017-10-26', '2017-09-26 18:09:53', '2017-09-26 18:09:53'),
(210, 1, 339, 0, '2017-09-27', '2017-10-27', '2017-09-27 13:57:19', '2017-09-27 14:48:56'),
(211, 1, 340, 0, '2017-09-28', '2017-10-28', '2017-09-28 12:08:41', '2017-09-28 12:08:41'),
(212, 1, 341, 0, '2017-09-29', '2017-10-29', '2017-09-29 07:02:36', '2017-10-31 21:47:39'),
(213, 1, 343, 0, '2017-09-30', '2017-10-30', '2017-09-30 17:46:30', '2017-11-02 00:02:37'),
(214, 5, 344, 0, '2018-04-10', '2019-04-10', '2018-04-10 17:34:50', '2018-04-10 17:34:50'),
(215, 1, 345, 0, '2017-10-01', '2017-10-31', '2017-10-01 12:13:10', '2017-10-01 12:56:52'),
(216, 1, 347, 0, '2017-10-02', '2017-11-01', '2017-10-02 08:13:49', '2017-10-02 08:46:34'),
(217, 1, 349, 0, '2017-10-03', '2017-11-02', '2017-10-03 04:38:22', '2017-12-16 04:27:30'),
(218, 2, 350, 1, '2018-05-22', '2018-06-22', '2018-05-17 05:17:44', '2018-05-22 21:49:53'),
(219, 1, 351, 0, '2017-10-03', '2017-11-02', '2017-10-03 10:02:37', '2017-10-03 10:07:17'),
(220, 1, 352, 0, '2017-10-03', '2017-11-02', '2017-10-03 21:04:57', '2017-10-03 21:04:57'),
(221, 1, 353, 0, '2017-10-04', '2017-11-03', '2017-10-04 11:10:31', '2017-10-04 11:58:20'),
(222, 1, 354, 0, '2017-10-04', '2017-11-03', '2017-10-04 14:40:19', '2017-11-04 10:34:10'),
(223, 1, 357, 0, '2017-10-05', '2017-11-04', '2017-10-05 12:21:37', '2017-10-05 12:21:37'),
(224, 1, 359, 0, '2017-10-05', '2017-11-04', '2017-10-05 18:42:36', '2017-11-10 09:21:38'),
(225, 1, 360, 0, '2017-10-05', '2017-11-04', '2017-10-05 21:33:29', '2017-10-05 21:41:47'),
(226, 1, 361, 0, '2017-10-06', '2017-11-06', '2017-10-06 09:27:31', '2017-11-07 17:44:39'),
(227, 1, 362, 0, '2017-10-06', '2017-11-05', '2017-10-06 10:30:53', '2017-10-06 11:23:36'),
(228, 1, 363, 0, '2017-10-07', '2017-11-06', '2017-10-07 15:51:04', '2017-10-07 15:51:04'),
(229, 1, 364, 0, '2017-10-08', '2017-11-08', '2017-10-09 02:51:38', '2017-11-18 08:43:39'),
(230, 1, 365, 0, '2017-10-09', '2017-11-08', '2017-10-10 00:09:30', '2017-10-10 00:25:09'),
(231, 2, 366, 0, '2019-05-05', '2019-06-05', '2019-05-05 06:47:45', '2019-05-05 06:47:45'),
(232, 5, 367, 0, '2018-10-19', '2018-10-19', '2017-10-10 19:41:23', '2018-10-19 21:54:00'),
(233, 1, 368, 0, '2017-10-10', '2017-11-09', '2017-10-11 01:54:21', '2017-10-11 08:57:46'),
(234, 2, 370, 0, '2017-10-11', '2017-11-10', '2017-10-11 07:25:39', '2017-10-11 09:02:09'),
(235, 2, 371, 0, '2018-02-04', '2018-05-08', '2018-02-04 23:00:07', '2018-02-04 23:01:34'),
(236, 1, 372, 0, '2017-10-12', '2017-11-11', '2017-10-12 13:09:43', '2017-10-12 13:15:44'),
(237, 1, 375, 0, '2017-10-13', '2017-11-12', '2017-10-13 17:17:56', '2017-10-13 17:17:56'),
(238, 1, 376, 0, '2017-10-14', '2017-11-13', '2017-10-14 15:28:09', '2017-10-14 15:28:09'),
(239, 1, 377, 0, '2017-11-12', '2017-12-12', '2017-11-12 07:40:08', '2018-01-01 01:37:18'),
(240, 2, 378, 0, '2017-11-11', '2017-12-12', '2017-11-11 20:02:59', '2017-11-11 20:02:59'),
(241, 1, 379, 0, '2017-10-16', '2017-11-15', '2017-10-16 22:45:56', '2017-11-16 18:59:57'),
(242, 1, 381, 0, '2017-10-17', '2017-11-16', '2017-10-17 08:09:26', '2017-10-17 08:09:26'),
(243, 1, 382, 0, '2017-10-17', '2017-11-16', '2017-10-17 13:09:21', '2017-10-17 15:26:54'),
(244, 1, 383, 0, '2017-10-17', '2017-11-16', '2017-10-18 02:15:53', '2017-10-18 08:17:29'),
(245, 1, 384, 0, '2017-10-18', '2017-11-17', '2017-10-18 05:46:55', '2017-10-18 08:18:02'),
(246, 1, 385, 0, '2017-10-18', '2017-11-17', '2017-10-18 09:26:06', '2017-10-18 09:26:06'),
(247, 1, 386, 0, '2017-10-18', '2017-11-17', '2017-10-18 10:10:55', '2017-10-18 10:19:10'),
(248, 3, 388, 0, '2017-10-18', '2018-01-19', '2017-10-18 19:53:49', '2017-10-18 19:53:49'),
(249, 1, 392, 0, '2017-10-22', '2017-11-21', '2017-10-22 11:32:51', '2017-10-22 11:32:51'),
(250, 1, 393, 0, '2018-03-17', '2018-04-16', '2018-03-17 19:56:14', '2018-04-17 05:45:12'),
(251, 2, 398, 0, '2017-10-25', '2017-11-25', '2017-10-26 03:56:11', '2017-10-26 03:56:11'),
(252, 2, 400, 0, '2017-10-27', '2017-11-26', '2017-10-27 14:47:10', '2017-10-27 16:06:11'),
(253, 1, 401, 0, '2017-11-08', '2017-12-09', '2017-11-08 20:52:51', '2017-11-08 20:52:51'),
(254, 1, 402, 0, '2017-10-31', '2017-11-30', '2017-10-31 04:52:42', '2017-10-31 08:13:40'),
(255, 1, 330, 0, '2017-11-04', '2017-12-04', '2017-11-04 13:04:34', '2017-11-04 13:04:34'),
(256, 1, 406, 0, '2017-11-06', '2017-12-06', '2017-11-06 18:12:57', '2017-11-06 18:12:57'),
(257, 2, 407, 0, '2017-11-06', '2017-12-07', '2017-11-07 01:56:15', '2017-11-07 01:56:15'),
(258, 2, 409, 0, '2017-11-07', '2017-12-08', '2017-11-07 21:17:34', '2017-11-07 21:17:34'),
(259, 1, 410, 0, '2017-11-07', '2017-12-07', '2017-11-07 22:28:43', '2017-11-07 23:30:01'),
(260, 1, 411, 0, '2017-11-08', '2017-12-08', '2017-11-08 05:36:43', '2017-11-08 05:36:43'),
(261, 3, 412, 0, '2017-11-09', '2017-12-09', '2017-11-09 11:36:08', '2017-11-09 12:15:55'),
(262, 1, 413, 0, '2017-11-09', '2017-12-09', '2017-11-09 22:56:42', '2017-11-09 23:06:53'),
(263, 1, 414, 0, '2017-11-10', '2017-12-10', '2017-11-10 18:28:15', '2017-11-10 18:28:15'),
(264, 1, 416, 0, '2017-11-13', '2017-12-13', '2017-11-13 21:07:45', '2017-12-20 11:13:15'),
(265, 2, 418, 0, '2017-11-17', '2018-11-17', '2017-11-17 15:17:30', '2017-11-17 15:17:30'),
(266, 1, 419, 0, '2017-11-14', '2017-12-14', '2017-11-14 06:02:06', '2017-11-14 06:10:18'),
(267, 1, 374, 0, '2017-11-14', '2017-12-14', '2017-11-15 04:32:01', '2017-11-15 08:01:55'),
(268, 3, 428, 0, '2017-11-15', '2018-02-16', '2017-11-15 21:51:27', '2017-11-15 21:51:27'),
(269, 1, 430, 0, '2017-11-17', '2017-12-17', '2017-11-17 12:43:31', '2017-11-17 13:05:05'),
(270, 1, 433, 0, '2017-11-17', '2017-12-17', '2017-11-18 01:16:00', '2017-11-18 01:16:00'),
(271, 1, 436, 0, '2017-11-18', '2017-12-18', '2017-11-19 03:27:44', '2017-11-19 03:27:44'),
(272, 1, 437, 0, '2017-11-19', '2017-12-19', '2017-11-19 09:45:21', '2017-11-19 12:04:33'),
(273, 1, 441, 0, '2017-11-22', '2017-12-22', '2017-11-23 02:27:49', '2017-11-23 02:27:49'),
(274, 2, 442, 0, '2018-09-01', '2018-09-11', '2018-09-01 17:36:57', '2018-09-01 17:36:57'),
(275, 1, 443, 0, '2017-11-24', '2017-12-24', '2017-11-24 07:07:32', '2017-11-24 07:07:32'),
(276, 1, 439, 0, '2017-11-24', '2017-12-24', '2017-11-24 21:43:09', '2017-11-24 21:56:31'),
(277, 1, 444, 0, '2017-11-24', '2017-12-24', '2017-11-25 01:23:13', '2017-11-25 01:30:15'),
(278, 1, 446, 0, '2017-11-25', '2017-12-25', '2017-11-25 21:33:13', '2017-11-25 21:33:13'),
(279, 4, 447, 0, '2018-02-11', '2018-08-16', '2018-02-11 23:13:53', '2018-02-11 23:20:04'),
(280, 1, 448, 0, '2017-11-26', '2017-12-26', '2017-11-27 00:59:28', '2017-11-27 00:59:28'),
(281, 2, 450, 0, '2017-12-03', '2018-01-02', '2017-12-03 15:49:23', '2017-12-03 15:49:23'),
(282, 2, 452, 0, '2018-02-09', '2018-03-11', '2018-02-09 19:43:02', '2018-02-09 19:58:56'),
(283, 1, 454, 0, '2017-11-29', '2017-12-29', '2017-11-30 00:24:34', '2017-11-30 17:20:05'),
(284, 1, 455, 0, '2017-11-29', '2017-12-29', '2017-11-30 03:28:08', '2017-11-30 10:31:46'),
(285, 2, 456, 0, '2017-12-02', '2018-01-01', '2017-12-02 11:56:59', '2017-12-02 12:24:17'),
(286, 1, 457, 0, '2017-12-02', '2018-01-01', '2017-12-02 13:37:41', '2017-12-02 13:37:41'),
(287, 2, 458, 0, '2017-12-02', '2018-01-02', '2017-12-02 22:27:01', '2017-12-02 22:27:01'),
(288, 1, 459, 0, '2017-12-03', '2018-01-02', '2017-12-03 18:52:22', '2017-12-03 18:52:22'),
(289, 3, 461, 0, '2019-01-13', '2019-04-16', '2019-01-14 01:37:22', '2019-04-19 07:59:30'),
(290, 1, 463, 0, '2017-12-04', '2018-01-03', '2017-12-04 21:54:26', '2017-12-04 21:54:26'),
(291, 1, 464, 0, '2017-12-05', '2018-01-04', '2017-12-05 18:44:14', '2017-12-05 18:44:14'),
(292, 1, 465, 0, '2017-12-05', '2018-01-04', '2017-12-06 02:38:12', '2017-12-06 10:51:06'),
(293, 1, 469, 0, '2017-12-10', '2018-01-09', '2017-12-10 20:46:06', '2017-12-10 20:46:06'),
(294, 1, 471, 0, '2017-12-11', '2018-01-10', '2017-12-11 07:04:44', '2017-12-11 10:37:02'),
(295, 2, 472, 0, '2018-08-03', '2018-09-03', '2018-08-03 12:21:16', '2018-08-03 12:21:16'),
(296, 1, 473, 0, '2017-12-11', '2018-01-10', '2017-12-11 16:14:44', '2017-12-11 16:14:44'),
(297, 1, 475, 0, '2017-12-13', '2018-01-12', '2017-12-14 04:13:44', '2017-12-14 04:13:44'),
(298, 1, 476, 0, '2017-12-14', '2018-01-14', '2017-12-14 21:20:03', '2017-12-15 10:33:32'),
(299, 1, 478, 0, '2017-12-23', '2018-01-22', '2017-12-24 02:41:19', '2017-12-24 02:41:19'),
(300, 3, 479, 0, '2018-02-24', '2018-03-27', '2018-02-24 18:26:41', '2018-02-24 18:26:41'),
(301, 2, 480, 1, '2018-01-02', '2018-02-02', '2018-01-02 19:31:05', '2018-01-02 19:31:39'),
(302, 1, 481, 1, '2018-02-24 10:50:00', '2018-02-04', '2018-01-05 20:10:17', '2018-02-24 15:50:00'),
(303, 2, 438, 0, '2018-03-17', '2018-04-17', '2018-03-17 14:38:22', '2018-03-17 14:38:22'),
(304, 3, 482, 0, '2018-06-07', '2018-09-08', '2018-06-08 01:22:12', '2018-06-08 01:29:00'),
(305, 6, 13, 0, '2018-07-01', '2019-07-01', '2018-07-01 12:16:20', '2018-07-01 12:16:20'),
(306, 1, 483, 0, '2018-04-16', '2018-05-17', '2018-04-16 11:08:27', '2018-04-16 11:08:27'),
(307, 2, 489, 1, '2018-10-08', '2018-11-08', '2018-05-28 08:14:38', '2018-10-08 09:48:53'),
(308, 1, 490, 0, '2018-01-21', '2018-02-20', '2018-01-21 15:21:29', '2018-01-21 15:21:29'),
(309, 1, 492, 0, '2018-01-21', '2018-02-20', '2018-01-22 00:45:16', '2018-01-22 00:45:16'),
(310, 3, 493, 0, '2018-01-21', '2018-02-20', '2018-01-22 00:53:44', '2018-01-22 00:53:44'),
(311, 1, 495, 0, '2018-01-23', '2018-02-22', '2018-01-23 16:29:46', '2018-01-23 16:29:46'),
(312, 1, 496, 0, '2018-01-23', '2018-02-22', '2018-01-23 21:01:45', '2018-01-23 21:01:45'),
(313, 1, 497, 0, '2018-01-23', '2018-02-22', '2018-01-23 21:13:05', '2018-01-23 21:13:05'),
(314, 3, 498, 0, '2018-01-23', '2018-02-23', '2018-01-23 21:16:25', '2018-01-23 21:16:25'),
(315, 1, 500, 0, '2018-01-27', '2018-02-26', '2018-01-28 03:53:49', '2018-01-28 03:53:49'),
(316, 1, 501, 0, '2018-01-29', '2018-02-28', '2018-01-29 12:33:43', '2018-01-29 12:33:43'),
(317, 5, 503, 0, '2018-02-04', '2019-02-04', '2018-02-04 16:14:32', '2018-02-04 16:14:32'),
(318, 1, 505, 0, '2018-01-31', '2018-03-02', '2018-01-31 21:43:15', '2018-01-31 21:43:15'),
(319, 3, 506, 0, '2018-01-31', '2018-03-02', '2018-01-31 22:16:22', '2018-01-31 22:16:22'),
(320, 2, 507, 0, '2018-01-31', '2019-01-31', '2018-02-01 00:34:20', '2018-02-01 00:34:20'),
(321, 4, 508, 0, '2018-02-03', '2019-02-03', '2018-02-03 17:16:04', '2018-02-03 17:16:04'),
(322, 5, 509, 1, '2018-02-05', '2019-02-05', '2018-02-05 14:28:24', '2018-02-15 19:15:56'),
(323, 3, 510, 1, '2018-02-04', '2018-05-08', '2018-02-04 19:43:15', '2018-02-04 22:36:56'),
(324, 2, 512, 0, '2018-08-27', '2018-09-06', '2018-08-28 01:51:20', '2018-08-28 01:51:20'),
(325, 1, 513, 0, '2018-02-04', '2018-03-06', '2018-02-05 04:06:51', '2018-02-05 04:06:51'),
(326, 1, 515, 0, '2018-02-05', '2018-03-07', '2018-02-05 19:01:53', '2018-02-05 19:01:53'),
(327, 2, 517, 0, '2018-02-06', '2018-03-09', '2018-02-07 02:23:15', '2018-02-07 02:23:15'),
(328, 1, 355, 0, '2018-03-08', '2018-04-07', '2018-03-08 12:45:13', '2018-03-08 12:45:13'),
(329, 1, 521, 0, '2018-02-09', '2018-03-11', '2018-02-09 14:37:16', '2018-02-09 14:51:51'),
(330, 1, 523, 0, '2018-02-11', '2018-03-14', '2018-02-11 16:25:27', '2018-02-11 16:25:27'),
(331, 5, 524, 0, '2018-02-11', '2018-05-15', '2018-02-11 23:11:48', '2018-02-11 23:11:48'),
(332, 1, 525, 0, '2018-02-11', '2018-03-13', '2018-02-12 00:30:15', '2018-02-12 00:30:15'),
(333, 2, 526, 0, '2018-02-11', '2018-03-13', '2018-02-12 01:28:10', '2018-02-12 01:28:10'),
(334, 4, 527, 0, '2018-02-12', '2018-08-17', '2018-02-12 06:20:58', '2018-02-12 06:20:58'),
(335, 2, 528, 0, '2018-02-13', '2018-03-15', '2018-02-13 11:16:42', '2018-02-13 11:16:42'),
(336, 1, 529, 0, '2018-02-13', '2018-03-15', '2018-02-14 03:04:00', '2018-02-14 03:04:00'),
(337, 2, 530, 0, '2018-05-15', '2018-06-15', '2018-05-15 04:43:17', '2018-05-15 04:43:17'),
(338, 5, 348, 0, '2018-05-18', '2019-05-18', '2018-05-18 18:08:07', '2018-05-18 18:08:07'),
(339, 2, 532, 0, '2018-02-19', '2018-03-22', '2018-02-19 17:17:26', '2018-02-19 17:17:26'),
(340, 1, 535, 0, '2018-02-22', '2018-03-24', '2018-02-23 01:24:25', '2018-02-23 03:36:45'),
(341, 2, 536, 0, '2018-02-23', '2018-03-25', '2018-02-23 09:32:37', '2018-02-23 10:26:52'),
(342, 2, 537, 0, '2018-03-12', '2018-04-11', '2018-03-12 18:52:49', '2018-03-12 18:52:49'),
(343, 2, 539, 0, '2018-02-24', '2018-03-27', '2018-02-24 16:08:16', '2018-02-24 16:08:16'),
(344, 2, 540, 1, '2018-02-24', '2018-03-27', '2018-02-24 18:32:01', '2018-02-24 19:02:02'),
(345, 1, 542, 0, '2018-03-01', '2018-03-31', '2018-03-01 12:49:22', '2018-03-01 12:49:22'),
(346, 2, 543, 0, '2018-04-17', '2018-05-17', '2018-04-18 03:21:23', '2018-04-18 03:21:23'),
(347, 3, 544, 0, '2018-10-14', '2018-10-14', '2018-04-05 16:37:11', '2018-10-15 00:32:40'),
(348, 1, 545, 0, '2018-03-03', '2018-04-02', '2018-03-03 22:55:03', '2018-03-03 22:55:03'),
(349, 1, 546, 0, '2018-03-04', '2018-04-03', '2018-03-04 08:11:59', '2018-03-04 08:11:59'),
(350, 1, 548, 0, '2018-03-08', '2018-04-07', '2018-03-08 13:13:24', '2018-03-08 13:34:09'),
(351, 1, 550, 0, '2018-03-10', '2018-04-09', '2018-03-10 18:17:43', '2018-03-10 18:19:50'),
(352, 2, 552, 0, '2018-03-10', '2018-04-10', '2018-03-10 19:21:23', '2018-03-10 19:32:50'),
(353, 2, 553, 0, '2018-03-10', '2018-04-10', '2018-03-10 19:41:16', '2018-03-10 19:41:16'),
(354, 2, 556, 0, '2018-03-11', '2018-04-11', '2018-03-10 20:05:44', '2018-03-11 14:51:48'),
(355, 2, 557, 0, '2018-03-10', '2018-04-10', '2018-03-10 20:37:32', '2018-03-10 20:37:32'),
(356, 1, 558, 0, '2018-03-17', '2018-04-16', '2018-03-17 16:04:03', '2018-03-17 16:09:05'),
(357, 2, 561, 0, '2018-10-14', '2018-10-14', '2018-03-18 17:01:23', '2018-10-15 00:33:05'),
(358, 6, 564, 0, '2018-06-19', '2018-07-20', '2018-06-19 10:13:20', '2018-06-19 10:13:20'),
(359, 2, 566, 0, '2018-03-11', '2018-04-11', '2018-03-11 11:13:01', '2018-03-11 21:20:14'),
(360, 2, 565, 0, '2018-03-11', '2018-04-11', '2018-03-11 11:13:08', '2018-03-11 14:24:18'),
(361, 2, 568, 0, '2018-06-12', '2018-07-13', '2018-06-12 20:35:10', '2018-08-27 00:50:36'),
(362, 3, 570, 0, '2018-08-28', '2018-09-28', '2018-08-28 18:16:07', '2018-08-28 18:16:07'),
(363, 2, 571, 0, '2018-03-11', '2018-04-11', '2018-03-11 16:10:22', '2018-03-11 16:19:48'),
(364, 3, 572, 0, '2018-03-26', '2018-04-26', '2018-03-26 16:17:29', '2018-03-26 16:17:29'),
(365, 2, 554, 0, '2018-03-11', '2018-04-11', '2018-03-11 16:49:14', '2018-03-11 16:52:13'),
(366, 2, 573, 0, '2018-03-11', '2018-03-16', '2018-03-11 17:10:32', '2018-03-11 17:42:17'),
(367, 2, 574, 0, '2018-03-11', '2018-04-11', '2018-03-11 17:29:23', '2018-03-11 17:46:57'),
(368, 2, 575, 0, '2018-03-11', '2018-04-11', '2018-03-11 18:02:50', '2018-03-11 18:02:50'),
(369, 2, 576, 0, '2018-03-11', '2018-04-11', '2018-03-11 21:30:45', '2018-03-11 21:31:37'),
(370, 1, 577, 0, '2018-03-11', '2018-04-10', '2018-03-11 22:20:30', '2018-03-11 22:20:30'),
(371, 1, 579, 0, '2018-03-12', '2018-04-11', '2018-03-12 04:11:24', '2018-03-12 04:11:24'),
(372, 2, 580, 0, '2018-03-12', '2018-04-12', '2018-03-12 09:42:29', '2018-03-12 09:42:29'),
(373, 3, 582, 0, '2018-04-10', '2018-07-12', '2018-04-10 21:35:20', '2018-07-14 20:26:33'),
(374, 1, 583, 0, '2018-03-13', '2018-04-12', '2018-03-13 16:06:25', '2018-03-13 17:31:12'),
(375, 1, 586, 0, '2018-03-14', '2018-04-13', '2018-03-15 01:05:06', '2018-03-15 01:38:15'),
(376, 2, 589, 0, '2018-03-16', '2018-04-16', '2018-03-16 23:04:39', '2018-03-16 23:04:39'),
(377, 1, 591, 0, '2018-03-17', '2018-04-16', '2018-03-17 15:43:13', '2018-03-17 15:43:13'),
(378, 1, 593, 0, '2018-03-17', '2018-04-16', '2018-03-17 17:04:44', '2018-03-17 17:04:44'),
(379, 2, 594, 0, '2018-03-17', '2018-04-17', '2018-03-17 17:20:04', '2018-03-17 18:09:23'),
(380, 2, 596, 0, '2018-03-18', '2018-04-18', '2018-03-18 02:27:35', '2018-03-18 19:22:41'),
(381, 2, 597, 0, '2018-03-18', '2018-04-18', '2018-03-18 00:46:09', '2018-03-18 19:32:14'),
(382, 2, 578, 0, '2018-03-18', '2018-04-18', '2018-03-18 01:12:12', '2018-03-18 19:38:07'),
(383, 2, 599, 0, '2018-03-18', '2018-04-18', '2018-03-18 08:00:58', '2018-03-18 21:33:52'),
(384, 1, 600, 0, '2018-03-18', '2018-04-17', '2018-03-18 11:28:21', '2018-04-18 08:50:39'),
(385, 2, 601, 0, '2018-03-18', '2018-04-18', '2018-03-18 17:11:41', '2018-03-18 17:11:41'),
(386, 1, 602, 0, '2018-03-18', '2018-04-17', '2018-03-18 22:26:45', '2018-03-18 22:26:45'),
(387, 1, 563, 0, '2018-03-18', '2018-04-17', '2018-03-19 00:55:24', '2018-03-19 01:03:04'),
(388, 1, 604, 0, '2018-03-19', '2018-04-18', '2018-03-19 22:03:43', '2018-03-19 22:19:30'),
(389, 2, 605, 0, '2018-03-20', '2018-04-20', '2018-03-20 05:43:08', '2018-03-20 05:43:08'),
(390, 1, 607, 0, '2018-04-06', '2018-05-07', '2018-04-06 07:15:00', '2018-04-06 07:15:00'),
(391, 1, 608, 0, '2018-03-21', '2018-04-20', '2018-03-21 12:10:06', '2018-03-21 12:10:06'),
(392, 3, 609, 0, '2018-03-21', '2018-06-22', '2018-03-22 01:10:44', '2018-03-22 01:10:44'),
(393, 2, 610, 0, '2018-10-14', '2018-10-14', '2018-03-23 13:56:23', '2018-10-14 17:26:08'),
(394, 1, 611, 0, '2018-10-14', '2018-10-14', '2018-03-24 19:43:32', '2018-10-15 00:33:24'),
(395, 2, 614, 0, '2018-10-14', '2018-10-14', '2018-04-03 16:49:56', '2018-10-14 16:48:36'),
(396, 1, 616, 0, '2018-04-14', '2018-05-14', '2018-04-14 10:10:38', '2018-05-15 06:41:57'),
(397, 5, 617, 1, '2018-08-13', '2019-08-13', '2018-08-13 09:34:09', '2018-08-13 09:39:00'),
(398, 5, 620, 0, '2018-04-26', '2019-04-26', '2018-04-26 17:29:06', '2018-04-26 17:29:06'),
(399, 5, 621, 0, '2018-05-07', '2019-05-07', '2018-05-07 13:39:07', '2018-05-07 13:39:07'),
(400, 1, 622, 0, '2018-05-07', '2018-06-06', '2018-05-07 16:47:09', '2018-05-07 16:47:09'),
(401, 2, 623, 0, '2018-06-20', '2018-07-20', '2018-06-21 00:46:31', '2018-06-21 00:46:31'),
(402, 1, 624, 0, '2018-05-09', '2018-06-08', '2018-05-09 12:21:46', '2018-06-21 10:38:35'),
(403, 2, 625, 0, '2018-06-11', '2018-07-12', '2018-06-11 18:19:37', '2018-07-14 08:56:53'),
(404, 2, 626, 0, '2018-05-11', '2018-06-11', '2018-05-12 02:07:16', '2018-05-12 02:07:16'),
(405, 2, 629, 0, '2018-10-14', '2018-10-14', '2018-05-17 03:29:16', '2018-10-15 00:33:39'),
(406, 1, 630, 0, '2018-05-23', '2018-06-22', '2018-05-23 05:24:56', '2018-05-23 05:24:56'),
(407, 5, 632, 1, '2018-05-28', '2019-05-28', '2018-05-28 18:22:55', '2018-05-28 18:23:59'),
(408, 1, 633, 0, '2018-06-14', '2018-07-15', '2018-06-14 14:35:47', '2018-06-14 14:35:47'),
(409, 1, 634, 0, '2018-05-30', '2018-06-29', '2018-05-30 22:20:43', '2018-05-30 22:20:43'),
(410, 6, 637, 0, '2018-06-15', '2018-06-15', '2018-06-15 23:45:41', '2018-06-15 23:45:41'),
(411, 3, 639, 0, '2018-06-17', '2018-09-18', '2018-06-17 22:29:36', '2018-06-17 22:29:36'),
(412, 1, 640, 0, '2018-06-19', '2018-07-19', '2018-06-19 10:09:27', '2018-06-19 10:09:27'),
(413, 1, 642, 0, '2018-06-22', '2018-07-22', '2018-06-22 16:29:17', '2018-06-22 16:29:17'),
(414, 1, 643, 0, '2018-06-22', '2018-07-22', '2018-06-22 21:30:00', '2018-06-22 21:30:00'),
(415, 3, 645, 0, '2019-03-13', '2019-03-13', '2018-07-04 12:00:11', '2019-03-13 09:44:11'),
(416, 3, 646, 0, '2018-07-05', '2018-10-06', '2018-07-05 23:30:52', '2018-07-05 23:30:52'),
(417, 4, 648, 0, '2018-07-17', '2018-10-18', '2018-07-17 15:50:48', '2018-07-17 15:50:48'),
(418, 1, 651, 1, '2018-08-05', '2018-08-15', '2018-08-05 23:17:07', '2018-08-05 23:17:55'),
(419, 5, 652, 1, '2018-09-13', '2019-09-13', '2018-09-13 09:37:42', '2018-09-13 14:01:36'),
(420, 2, 653, 1, '2019-04-04', '2019-05-05', '2019-04-04 06:19:04', '2019-04-04 08:53:22'),
(421, 1, 654, 0, '2018-09-01', '2018-09-11', '2018-08-19 01:29:21', '2018-09-01 11:29:04'),
(422, 1, 655, 0, '2018-09-18', '2018-09-28', '2018-08-21 11:37:11', '2018-09-18 12:14:28'),
(423, 1, 656, 0, '2018-09-01', '2018-09-11', '2018-08-23 13:33:22', '2018-09-01 11:27:28'),
(424, 1, 657, 0, '2018-09-01', '2018-09-11', '2018-08-23 20:58:43', '2018-09-01 11:27:00'),
(425, 1, 658, 0, '2018-09-01', '2018-09-11', '2018-08-25 19:03:55', '2018-09-01 11:26:23'),
(426, 1, 660, 0, '2018-09-01', '2018-09-11', '2018-08-27 07:31:09', '2018-09-01 11:25:41'),
(427, 1, 661, 0, '2018-09-01', '2018-09-11', '2018-08-27 10:11:25', '2018-09-01 11:25:09'),
(428, 1, 662, 0, '2018-09-01', '2018-09-11', '2018-08-27 15:58:22', '2018-09-01 11:24:45'),
(429, 1, 663, 0, '2018-09-18', '2018-09-28', '2018-08-27 16:58:07', '2018-09-18 12:13:58'),
(430, 1, 664, 0, '2018-09-18', '2018-09-28', '2018-08-27 19:03:25', '2018-09-18 12:13:47'),
(431, 1, 665, 0, '2018-09-18', '2018-09-28', '2018-08-27 22:00:59', '2018-09-18 12:13:35'),
(432, 1, 666, 0, '2018-09-18', '2018-09-28', '2018-08-29 14:50:09', '2018-09-18 12:13:13'),
(433, 1, 667, 0, '2018-09-18', '2018-09-28', '2018-08-30 06:03:15', '2018-09-18 12:12:59'),
(434, 1, 668, 0, '2018-09-18', '2018-09-28', '2018-08-30 13:38:28', '2018-09-18 12:12:43'),
(435, 1, 669, 0, '2018-09-18', '2018-09-28', '2018-08-30 14:12:25', '2018-09-18 12:12:31'),
(436, 1, 670, 0, '2018-09-18', '2018-09-28', '2018-08-30 21:00:01', '2018-09-18 12:12:18'),
(437, 4, 671, 0, '2018-09-01', '2019-03-06', '2018-08-31 08:18:36', '2018-09-01 11:15:44'),
(438, 1, 672, 0, '2018-09-17', '2018-09-27', '2018-08-31 12:25:25', '2018-09-17 20:35:54'),
(439, 1, 674, 0, '2018-09-01', '2018-09-11', '2018-09-01 16:30:45', '2018-09-01 19:19:10'),
(440, 1, 675, 0, '2018-10-14', '2018-10-14', '2018-09-24 07:15:20', '2018-10-15 00:35:00'),
(441, 1, 677, 0, '2018-09-17', '2018-09-27', '2018-09-02 22:38:13', '2018-09-17 20:38:16'),
(442, 1, 678, 0, '2018-09-03', '2018-09-13', '2018-09-03 06:25:02', '2018-09-03 08:21:17'),
(443, 2, 679, 0, '2018-10-19', '2018-10-19', '2018-09-03 14:57:50', '2018-10-19 21:53:04'),
(444, 1, 680, 0, '2018-09-03', '2018-09-13', '2018-09-03 20:08:54', '2018-09-03 20:18:17'),
(445, 1, 555, 0, '2018-09-03', '2018-09-13', '2018-09-03 22:46:20', '2018-09-03 22:46:20'),
(446, 1, 681, 0, '2018-09-05', '2018-09-15', '2018-09-05 17:56:27', '2018-09-05 20:04:17'),
(447, 1, 682, 0, '2018-09-05', '2018-09-15', '2018-09-05 18:14:50', '2018-09-05 20:03:59'),
(448, 1, 683, 0, '2018-09-05', '2018-09-15', '2018-09-05 18:17:28', '2018-09-05 20:03:44'),
(449, 1, 684, 0, '2018-09-05', '2018-09-15', '2018-09-05 18:26:39', '2018-09-05 20:03:30'),
(450, 1, 685, 0, '2018-09-18', '2018-09-28', '2018-09-05 19:34:26', '2018-09-18 09:52:22'),
(451, 2, 686, 0, '2019-05-22', '2019-06-21', '2019-05-22 12:17:47', '2019-05-22 12:17:47'),
(452, 1, 689, 0, '2018-09-07', '2018-09-17', '2018-09-06 22:30:23', '2018-09-07 21:20:07'),
(453, 1, 690, 0, '2018-09-07', '2018-09-17', '2018-09-07 14:28:37', '2018-09-07 21:22:33'),
(454, 1, 691, 0, '2018-09-09', '2018-09-19', '2018-09-08 02:40:17', '2018-09-09 12:35:18'),
(455, 1, 692, 0, '2018-09-09', '2018-09-19', '2018-09-08 06:01:22', '2018-09-09 12:35:35'),
(456, 1, 693, 0, '2018-09-09', '2018-09-19', '2018-09-09 14:37:54', '2018-09-20 14:53:10'),
(457, 1, 694, 0, '2018-09-09', '2018-09-19', '2018-09-09 18:27:40', '2018-09-09 21:51:54'),
(458, 1, 695, 0, '2018-09-09', '2018-09-19', '2018-09-09 23:10:06', '2018-09-10 01:50:51'),
(459, 2, 696, 0, '2018-09-10', '2018-10-11', '2018-09-10 15:55:37', '2018-09-10 15:55:37'),
(460, 1, 673, 0, '2018-09-20', '2018-09-30', '2018-09-20 19:37:31', '2018-09-20 19:37:31'),
(461, 1, 659, 0, '2018-10-14', '2018-10-14', '2018-09-13 00:09:23', '2018-10-15 00:34:44'),
(462, 1, 697, 0, '2018-10-19', '2018-10-19', '2018-09-17 13:17:06', '2018-10-19 21:52:47'),
(463, 1, 698, 0, '2018-10-19', '2018-10-19', '2018-09-18 19:15:49', '2018-10-19 21:52:35'),
(464, 1, 585, 0, '2018-09-18', '2018-09-28', '2018-09-19 00:56:10', '2018-09-19 00:56:10'),
(465, 2, 699, 0, '2018-10-05', '2018-10-15', '2018-10-05 16:55:31', '2018-10-05 16:55:31'),
(466, 1, 702, 0, '2018-10-19', '2018-10-19', '2018-09-24 12:59:49', '2018-10-19 21:52:17'),
(467, 1, 703, 0, '2018-10-19', '2018-10-19', '2018-09-24 22:30:03', '2018-10-19 21:52:06'),
(468, 1, 705, 0, '2018-10-19', '2018-10-19', '2018-09-26 14:39:44', '2018-10-19 21:51:54'),
(469, 1, 706, 0, '2018-10-19', '2018-10-19', '2018-09-28 10:48:19', '2018-10-19 21:51:42'),
(470, 1, 707, 0, '2018-10-19', '2018-10-19', '2018-09-28 20:04:47', '2018-10-19 21:51:28'),
(471, 1, 709, 0, '2018-10-19', '2018-10-19', '2018-10-04 00:37:33', '2018-10-19 21:51:18'),
(472, 1, 710, 0, '2018-10-19', '2018-10-19', '2018-10-07 22:00:21', '2018-10-19 21:50:16'),
(473, 1, 711, 0, '2019-05-09', '2019-06-09', '2019-05-09 23:29:43', '2019-05-09 23:29:43'),
(474, 2, 712, 0, '2018-10-22', '2018-11-22', '2018-10-23 00:46:44', '2018-11-26 01:40:42'),
(475, 1, 713, 0, '2018-10-12', '2018-10-22', '2018-10-12 03:53:26', '2018-10-24 21:17:40'),
(476, 1, 714, 0, '2018-10-12', '2018-10-22', '2018-10-12 15:53:34', '2018-10-23 06:33:21'),
(477, 2, 715, 1, '2018-10-14', '2018-11-14', '2018-10-14 14:48:39', '2018-10-14 14:48:39'),
(478, 3, 716, 0, '2018-10-14', '2018-10-14', '2018-10-14 16:14:41', '2018-10-14 16:33:22'),
(479, 2, 718, 0, '2018-10-14', '2018-11-14', '2018-10-15 03:49:51', '2018-10-15 03:49:51'),
(480, 1, 719, 0, '2018-10-15', '2018-10-25', '2018-10-15 11:15:45', '2018-10-15 11:15:45'),
(481, 3, 721, 1, '2019-01-08', '2019-04-11', '2019-01-06 23:52:06', '2019-01-08 08:31:31'),
(482, 1, 724, 0, '2018-10-28', '2018-11-07', '2018-10-22 00:48:50', '2018-11-09 01:52:23'),
(483, 1, 725, 0, '2018-10-28', '2018-11-27', '2018-10-22 01:59:18', '2018-11-28 14:01:41'),
(484, 3, 727, 0, '2018-10-23', '2019-01-24', '2018-10-23 14:49:09', '2018-10-23 14:49:09'),
(485, 1, 729, 0, '2018-10-24', '2018-11-03', '2018-10-24 22:46:43', '2018-10-24 22:46:43'),
(486, 1, 730, 0, '2018-10-24', '2018-11-03', '2018-10-25 01:51:08', '2018-10-25 01:51:08'),
(487, 1, 731, 0, '2018-10-26', '2018-11-05', '2018-10-26 21:09:59', '2018-10-26 21:09:59'),
(488, 1, 733, 0, '2018-10-27', '2018-11-06', '2018-10-27 07:20:47', '2018-10-27 07:20:47'),
(489, 1, 734, 0, '2019-03-13', '2019-03-13', '2018-10-28 23:05:11', '2019-03-13 09:46:12'),
(490, 2, 736, 0, '2018-11-13', '2018-12-13', '2018-11-13 09:11:40', '2018-11-13 09:11:40'),
(491, 1, 738, 0, '2019-03-13', '2019-03-13', '2018-11-13 10:32:07', '2019-03-13 09:48:25'),
(492, 6, 739, 0, '2018-11-16', '2019-11-16', '2018-11-16 07:24:59', '2018-11-16 07:24:59'),
(493, 1, 740, 0, '2018-11-17', '2018-12-17', '2018-11-17 19:09:07', '2018-11-17 19:09:07'),
(494, 1, 741, 0, '2019-03-13', '2019-03-13', '2018-12-03 21:35:01', '2019-03-13 09:49:12'),
(495, 1, 743, 0, '2018-11-18', '2018-12-18', '2018-11-18 15:03:08', '2018-11-18 15:03:08'),
(496, 1, 745, 0, '2018-11-18', '2018-12-18', '2018-11-18 20:56:32', '2018-11-18 20:56:32'),
(497, 1, 746, 0, '2018-11-18', '2018-12-18', '2018-11-18 21:47:43', '2018-11-18 21:47:43'),
(498, 1, 747, 0, '2018-11-18', '2018-12-18', '2018-11-18 22:25:38', '2018-11-18 22:25:38'),
(499, 1, 748, 0, '2019-03-13', '2019-03-13', '2018-11-18 22:37:11', '2019-03-13 09:50:08'),
(500, 1, 749, 0, '2018-11-18', '2018-12-18', '2018-11-18 23:26:51', '2019-01-17 01:50:52'),
(501, 1, 754, 0, '2018-11-19', '2018-12-19', '2018-11-19 07:40:49', '2018-11-19 07:40:49'),
(502, 1, 755, 0, '2018-11-19', '2018-12-19', '2018-11-19 11:26:21', '2018-11-19 11:26:21'),
(503, 1, 758, 0, '2018-11-19', '2018-12-19', '2018-11-19 21:02:06', '2018-11-19 21:02:06'),
(504, 1, 759, 0, '2019-03-13', '2019-03-13', '2018-11-19 21:50:59', '2019-03-13 09:50:50'),
(505, 1, 762, 0, '2018-11-25', '2018-12-25', '2018-11-25 06:57:13', '2018-11-25 06:57:13'),
(506, 1, 764, 0, '2019-03-13', '2019-03-13', '2018-11-28 01:55:46', '2019-03-13 09:51:39'),
(507, 2, 767, 1, '2019-01-25', '2019-02-25', '2018-12-05 19:02:17', '2019-01-25 19:54:50'),
(508, 1, 768, 0, '2019-01-25', '2019-02-24', '2018-12-05 20:54:35', '2019-04-08 19:39:27'),
(509, 1, 769, 0, '2018-12-06', '2019-01-05', '2018-12-06 22:29:50', '2018-12-06 22:29:50'),
(510, 1, 772, 0, '2018-12-11', '2019-01-10', '2018-12-11 22:57:54', '2018-12-11 22:57:54'),
(511, 1, 774, 0, '2018-12-16', '2019-01-15', '2018-12-17 01:44:16', '2018-12-17 01:44:16'),
(512, 1, 775, 1, '2019-03-21', '2019-04-20', '2019-03-21 23:06:59', '2019-03-22 00:21:28'),
(513, 1, 779, 0, '2019-01-12', '2019-02-11', '2019-01-13 00:04:59', '2019-01-13 00:04:59'),
(514, 3, 756, 0, '2019-01-21', '2019-04-24', '2019-01-21 16:01:47', '2019-01-21 16:01:47'),
(515, 1, 784, 0, '2019-03-13', '2019-03-13', '2019-01-21 18:02:11', '2019-03-13 09:53:31'),
(516, 1, 786, 0, '2019-01-25', '2019-02-24', '2019-01-26 03:01:28', '2019-01-26 03:01:28'),
(517, 6, 787, 1, '2019-02-11', '2021-02-10', '2019-02-11 17:26:42', '2019-02-11 18:03:25'),
(518, 1, 788, 0, '2019-03-13', '2019-03-13', '2019-01-27 09:42:03', '2019-03-13 09:54:22'),
(519, 1, 789, 0, '2019-01-27', '2019-02-26', '2019-01-27 19:28:48', '2019-01-27 19:28:48'),
(520, 1, 790, 0, '2019-01-28', '2019-02-27', '2019-01-28 16:26:40', '2019-01-28 16:26:40'),
(521, 1, 792, 0, '2019-01-28', '2019-02-27', '2019-01-28 22:40:58', '2019-01-28 22:40:58'),
(522, 1, 798, 0, '2019-02-02', '2019-03-04', '2019-02-02 18:56:10', '2019-02-02 18:56:10'),
(523, 1, 800, 0, '2019-03-13', '2019-03-13', '2019-02-03 00:52:20', '2019-03-13 09:55:02'),
(524, 1, 801, 0, '2019-02-04', '2019-03-06', '2019-02-04 13:02:58', '2019-02-04 13:02:58'),
(525, 2, 802, 1, '2019-03-20', '2019-04-20', '2019-03-20 02:30:46', '2019-03-20 16:17:29'),
(526, 1, 803, 0, '2019-02-06', '2019-03-08', '2019-02-06 16:31:04', '2019-02-06 16:31:04'),
(527, 1, 804, 0, '2019-02-07', '2019-03-09', '2019-02-07 11:30:14', '2019-02-07 11:30:14'),
(528, 1, 806, 0, '2019-02-07', '2019-03-09', '2019-02-07 14:02:28', '2019-02-07 14:02:28'),
(529, 1, 807, 0, '2019-02-08', '2019-03-10', '2019-02-08 08:49:39', '2019-02-08 08:49:39'),
(530, 1, 808, 0, '2019-02-08', '2019-03-10', '2019-02-08 18:04:48', '2019-02-08 18:04:48'),
(531, 1, 809, 0, '2019-02-09', '2019-03-11', '2019-02-09 18:59:04', '2019-02-09 18:59:04'),
(532, 1, 812, 0, '2019-02-10', '2019-03-12', '2019-02-10 19:38:36', '2019-02-10 19:38:36'),
(533, 1, 814, 0, '2019-02-14', '2019-03-16', '2019-02-14 17:44:29', '2019-02-14 17:44:29'),
(534, 2, 818, 0, '2019-02-24', '2019-03-26', '2019-02-24 17:16:31', '2019-02-24 17:16:31'),
(535, 1, 823, 0, '2019-02-24', '2019-03-26', '2019-02-25 03:37:42', '2019-02-25 03:37:42'),
(536, 2, 832, 0, '2019-04-11', '2019-05-12', '2019-04-11 16:49:02', '2019-04-11 16:49:02'),
(537, 1, 833, 0, '2019-02-26', '2019-03-28', '2019-02-26 20:00:42', '2019-02-26 20:00:42'),
(538, 1, 837, 0, '2019-03-24', '2019-04-23', '2019-03-24 06:58:20', '2019-03-24 06:58:20'),
(539, 1, 839, 0, '2019-02-28', '2019-03-30', '2019-02-28 08:26:17', '2019-02-28 08:26:17'),
(540, 1, 840, 0, '2019-02-28', '2019-03-30', '2019-02-28 14:19:39', '2019-02-28 14:19:39'),
(541, 1, 841, 0, '2019-02-28', '2019-03-30', '2019-03-01 00:08:09', '2019-03-01 00:08:09'),
(542, 1, 842, 0, '2019-02-28', '2019-03-30', '2019-03-01 02:21:07', '2019-03-01 02:21:07'),
(543, 1, 844, 0, '2019-03-01', '2019-03-31', '2019-03-01 10:56:56', '2019-03-01 10:56:56'),
(544, 1, 846, 0, '2019-03-02', '2019-04-01', '2019-03-02 16:32:53', '2019-03-02 16:32:53'),
(545, 1, 847, 0, '2019-03-03', '2019-04-02', '2019-03-02 20:01:46', '2019-04-17 11:13:38'),
(546, 1, 848, 0, '2019-03-02', '2019-04-01', '2019-03-03 02:29:55', '2019-03-03 02:29:55'),
(547, 1, 849, 0, '2019-03-03', '2019-04-02', '2019-03-03 11:05:11', '2019-03-03 11:05:11'),
(548, 1, 853, 0, '2019-03-03', '2019-04-02', '2019-03-03 19:08:54', '2019-03-03 19:08:54'),
(549, 3, 859, 0, '2019-03-13', '2019-06-14', '2019-03-13 18:08:03', '2019-03-13 18:08:03'),
(550, 1, 860, 0, '2019-03-17', '2019-04-16', '2019-03-17 15:17:21', '2019-03-17 15:17:21'),
(551, 4, 861, 0, '2019-03-17', '2019-09-19', '2019-03-17 21:23:35', '2019-03-17 21:23:35'),
(552, 3, 855, 0, '2019-03-17', '2019-06-18', '2019-03-17 23:22:38', '2019-03-17 23:22:38'),
(553, 3, 592, 1, '2019-04-18', '2019-07-20', '2019-04-18 14:51:34', '2019-04-18 14:51:34'),
(554, 4, 866, 0, '2019-04-01', '2019-05-01', '2019-04-01 15:03:09', '2019-04-01 15:03:09'),
(555, 1, 868, 0, '2019-04-01', '2019-05-01', '2019-04-01 15:55:52', '2019-04-01 15:55:52');
INSERT INTO `subscriptions` (`id`, `plan_id`, `user_id`, `active`, `activated_on`, `deactivated_on`, `created_at`, `updated_at`) VALUES
(556, 1, 870, 0, '2019-04-01', '2019-05-01', '2019-04-01 17:11:21', '2019-04-01 17:11:21'),
(557, 1, 871, 0, '2019-04-01', '2019-05-01', '2019-04-01 17:21:18', '2019-04-01 17:21:18'),
(558, 1, 873, 0, '2019-04-01', '2019-05-01', '2019-04-01 17:51:16', '2019-04-01 17:51:16'),
(559, 1, 878, 0, '2019-04-01', '2019-05-01', '2019-04-01 21:56:52', '2019-04-01 21:56:52'),
(560, 1, 880, 0, '2019-04-01', '2019-05-01', '2019-04-01 22:56:11', '2019-04-01 22:56:11'),
(561, 1, 883, 0, '2019-04-01', '2019-05-01', '2019-04-02 01:30:17', '2019-04-02 01:30:17'),
(562, 1, 885, 0, '2019-04-02', '2019-05-02', '2019-04-02 07:19:16', '2019-04-02 07:19:16'),
(563, 1, 886, 0, '2019-04-02', '2019-05-02', '2019-04-02 08:07:33', '2019-04-02 08:07:33'),
(564, 1, 889, 0, '2019-04-02', '2019-05-02', '2019-04-02 09:34:59', '2019-04-02 09:34:59'),
(565, 2, 890, 0, '2019-04-02', '2019-05-03', '2019-04-02 11:27:15', '2019-04-02 11:27:15'),
(566, 1, 892, 0, '2019-04-02', '2019-05-02', '2019-04-02 14:30:43', '2019-04-02 14:30:43'),
(567, 1, 894, 0, '2019-04-02', '2019-05-02', '2019-04-02 14:45:32', '2019-04-02 14:45:32'),
(568, 1, 896, 0, '2019-04-02', '2019-05-02', '2019-04-02 17:18:23', '2019-04-02 17:18:23'),
(569, 1, 900, 0, '2019-04-02', '2019-05-02', '2019-04-02 21:21:09', '2019-04-02 21:21:09'),
(570, 1, 901, 1, '2019-04-02', '2019-05-02', '2019-04-02 21:57:48', '2019-04-02 22:17:11'),
(571, 1, 902, 0, '2019-04-02', '2019-05-02', '2019-04-02 21:59:34', '2019-04-02 21:59:34'),
(572, 1, 905, 0, '2019-04-02', '2019-05-02', '2019-04-03 00:49:11', '2019-04-03 00:49:11'),
(573, 1, 907, 0, '2019-04-02', '2019-05-02', '2019-04-03 03:37:24', '2019-04-03 03:37:24'),
(574, 1, 911, 0, '2019-04-03', '2019-05-03', '2019-04-03 14:37:24', '2019-04-03 14:37:24'),
(575, 1, 913, 0, '2019-04-03', '2019-05-03', '2019-04-03 17:39:36', '2019-04-03 17:39:36'),
(576, 1, 917, 0, '2019-04-03', '2019-05-03', '2019-04-03 23:26:22', '2019-04-03 23:26:22'),
(577, 1, 920, 0, '2019-04-03', '2019-05-03', '2019-04-04 01:23:33', '2019-04-04 01:23:33'),
(578, 1, 921, 0, '2019-04-04', '2019-05-04', '2019-04-04 06:52:29', '2019-04-04 06:52:29'),
(579, 1, 922, 0, '2019-04-04', '2019-05-04', '2019-04-04 07:14:29', '2019-04-04 07:14:29'),
(580, 1, 923, 0, '2019-04-04', '2019-05-04', '2019-04-04 07:28:46', '2019-04-04 07:28:46'),
(581, 1, 925, 0, '2019-04-04', '2019-05-04', '2019-04-04 09:11:44', '2019-04-04 09:11:44'),
(582, 1, 928, 0, '2019-04-04', '2019-05-04', '2019-04-04 11:11:03', '2019-04-04 11:11:03'),
(583, 1, 930, 1, '2019-04-04', '2019-05-04', '2019-04-04 12:02:33', '2019-04-04 12:31:36'),
(584, 1, 933, 0, '2019-04-04', '2019-05-04', '2019-04-04 14:22:13', '2019-04-04 14:22:13'),
(585, 1, 936, 0, '2019-04-04', '2019-05-04', '2019-04-04 15:38:01', '2019-04-04 15:38:01'),
(586, 1, 937, 0, '2019-04-04', '2019-05-04', '2019-04-04 16:24:27', '2019-04-04 16:24:27'),
(587, 1, 939, 0, '2019-04-04', '2019-05-04', '2019-04-04 18:55:04', '2019-04-04 18:55:04'),
(588, 1, 952, 0, '2019-04-05', '2019-05-05', '2019-04-05 09:49:58', '2019-04-05 09:49:58'),
(589, 1, 950, 0, '2019-04-05', '2019-05-05', '2019-04-05 11:41:59', '2019-04-05 11:41:59'),
(590, 2, 953, 0, '2019-04-05', '2019-05-06', '2019-04-05 14:17:39', '2019-04-05 14:17:39'),
(591, 1, 955, 0, '2019-04-05', '2019-05-05', '2019-04-05 16:17:45', '2019-04-05 16:17:45'),
(592, 1, 957, 0, '2019-04-05', '2019-05-05', '2019-04-05 18:37:15', '2019-04-05 18:37:15'),
(593, 1, 961, 0, '2019-04-05', '2019-05-05', '2019-04-05 22:29:15', '2019-04-05 22:29:15'),
(594, 2, 962, 0, '2019-04-05', '2019-05-06', '2019-04-05 23:36:12', '2019-04-05 23:36:12'),
(595, 1, 963, 0, '2019-04-05', '2019-05-05', '2019-04-06 00:14:41', '2019-04-06 00:14:41'),
(596, 1, 965, 0, '2019-04-06', '2019-05-06', '2019-04-06 08:18:16', '2019-04-06 08:18:16'),
(597, 1, 966, 0, '2019-04-06', '2019-05-06', '2019-04-06 08:21:00', '2019-04-06 08:21:00'),
(598, 1, 967, 0, '2019-04-06', '2019-05-06', '2019-04-06 08:56:22', '2019-04-06 08:56:22'),
(599, 1, 968, 0, '2019-04-06', '2019-05-06', '2019-04-06 11:40:18', '2019-04-06 11:40:18'),
(600, 1, 969, 0, '2019-04-06', '2019-05-06', '2019-04-06 15:02:04', '2019-04-06 15:02:04'),
(601, 2, 971, 0, '2019-04-06', '2019-05-07', '2019-04-06 19:04:59', '2019-04-06 19:04:59'),
(602, 1, 972, 0, '2019-04-06', '2019-05-06', '2019-04-06 19:10:56', '2019-04-06 19:10:56'),
(603, 1, 974, 0, '2019-04-06', '2019-05-06', '2019-04-06 19:54:42', '2019-04-06 19:54:42'),
(604, 1, 975, 1, '2019-04-07', '2019-05-07', '2019-04-06 23:35:29', '2019-04-08 00:36:07'),
(605, 1, 976, 0, '2019-04-06', '2019-05-06', '2019-04-06 23:44:03', '2019-04-06 23:44:03'),
(606, 1, 980, 0, '2019-04-07', '2019-05-07', '2019-04-07 07:42:55', '2019-04-07 07:42:55'),
(607, 1, 981, 0, '2019-04-07', '2019-05-07', '2019-04-07 08:20:16', '2019-04-07 08:20:16'),
(608, 1, 983, 0, '2019-04-07', '2019-05-07', '2019-04-07 08:58:32', '2019-04-07 08:58:32'),
(609, 1, 987, 0, '2019-04-07', '2019-05-07', '2019-04-07 18:25:15', '2019-04-07 18:25:15'),
(610, 1, 990, 1, '2019-04-07', '2019-05-07', '2019-04-07 23:50:09', '2019-04-08 00:37:16'),
(611, 2, 995, 0, '2019-04-09', '2019-10-12', '2019-04-09 22:33:01', '2019-04-09 22:33:01'),
(612, 1, 996, 1, '2019-04-15', '2019-05-15', '2019-04-15 17:09:32', '2019-04-16 00:38:12'),
(613, 1, 1001, 1, '2019-05-05', '2019-06-04', '2019-05-05 17:12:32', '2019-05-06 00:48:27'),
(614, 1, 1003, 0, '2019-05-06', '2019-06-05', '2019-05-06 21:55:47', '2019-05-06 21:55:47'),
(615, 3, 1004, 0, '2019-05-09', '2019-08-10', '2019-05-09 05:59:30', '2019-05-09 05:59:30'),
(616, 3, 1005, 0, '2019-05-13', '2019-08-14', '2019-05-13 13:33:29', '2019-05-13 13:33:29'),
(617, 2, 1006, 0, '2019-05-17', '2019-06-16', '2019-05-17 12:58:41', '2019-05-17 12:58:41'),
(618, 1, 1007, 1, '2019-05-19', '2019-06-18', '2019-05-19 14:34:14', '2019-05-19 20:06:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signal_option` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `verified` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_plan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `avatar`, `signal_option`, `phone`, `user_type`, `verified`, `active`, `remember_token`, `created_at`, `updated_at`, `role`, `notify`, `ip_address`, `first_plan`) VALUES
(13, 'Hector DeVille', 'support@learnforexlivesignals.com', '$2y$10$iqx.2nB2I4CPCX1AJWPCzOYNgfm8/Yk8q9GYwMl/dRI2ZBrndlm86', 'useravatar_vBBYdNN4Fv6xnz2rcyF534NZsqKm2S.jpg', 'Telegram', '08038990223', 2, 0, 1, 'Ce1oRzBWxqx0uo2xpzEdvoiAxjbNOfz9HspNkp0tja79fXiAjuvG8tDUp3Oj', '2017-07-18 22:42:16', '2019-05-12 15:53:00', 'admin', 0, '105.112.17.26, 141.101.69.137', 6),
(24, 'El Hosseiny', 'eehosseiny@yahoo.com', '$2y$10$pBd0A0okHqb0.04y0MsFZuPUU/kWHEfouzvWzUggoheuUxTu8JENK', NULL, 'Telegram', '+201222139900', 2, 0, 1, 'M4I5HjwGDAdqxc442lirOPNtKTomQWNBoaVmJMFIlrbqVS5hrCGdzx8vP1XA', '2017-07-23 16:31:37', '2018-12-17 02:10:55', NULL, 0, '197.161.156.242, 188.114.102.54', 2),
(25, 'Jacob Ashibi', 'jacshib@gmail.com', '$2y$10$eFPOD4cGgsHfmsJ.aEB8vOW7BAeRQLnCEdQi/8okR.wbtP2n.BDYO', 'useravatar_44tVtLf182SxUag49DaGYhQ0bJYf6n.jpg', 'Telegram', '08038990223', 1, 0, 1, 'LPpqZ6UJEEsbvso4eKhCcGjOpvkvyzsv9fBRzQ4PyEsFbhobHrWrZVrezbpx', '2017-07-23 16:56:54', '2019-05-21 01:31:03', 'admin', 0, '105.112.16.255, 141.101.99.169', 3),
(27, 'baktha', 'baktha.sg@gmail.com', '$2y$10$ojvrL06HOJ7exCIFHh0r1OTb8zKFsixPfQ2UlDdngZyWm02ibLubK', NULL, 'Telegram', '00971503751300', 1, 0, 1, 'ZonKGktRY4YOkm3m2uwNRjmYl4I4FmiyJCl4aVE33e19WK5cnIdA0TcjglWG', '2017-07-23 22:02:43', '2018-11-07 13:38:28', NULL, 0, '2.50.169.251, 162.158.111.153', 1),
(28, 'Sebastian Popa', 'spopa80@gmail.com', '$2y$10$/UgMW1PO/vFAUuPv4iw04uzBxhFOC7dQLnx/8CxpDQ5Hpc7Pm3lTu', NULL, 'Telegram', '447920400338', 1, 0, 1, 'AY6p45c1CJzeMFV7VEimpAblkcNWJ2pe2ib272OgqRe4sP3yZu80ZQVk8y1C', '2017-07-24 00:06:03', '2018-11-07 13:38:28', NULL, 0, NULL, 1),
(33, 'Scott', 'scott@learnforexlive.com', '$2y$10$42PxfrUoeHORdX5Q9d9zBOsFVMKKAPe5VwOvPcGhsiqXJtYMG/6Py', NULL, 'Telegram', '+39960119104', 1, 0, 1, 'RYtDksa3QXja70KqagYLj16sOFYHW7EUymq016TK0YK91o8EXtelC3lQfVvV', '2017-07-24 01:38:08', '2018-11-07 13:38:28', NULL, 0, '2602:306:3654:4210:a14a:4fef:3fb4:ad40, 162.158.186.20', 5),
(36, 'Philip Lodge', 'peelodge@aol.com', '$2y$10$rZEJaOLGwuKss8qD0C/BxOb7AYRBJsWe6/1jHCZ2LYQrNsjA5hXf2', NULL, 'Telegram', '+447503723869', 2, 0, 1, 'qZYJY4DXPTiQKhlzNG6DcRmTgVOqsRcX6agWh2VBkly14xRFVVr0ohASPUMi', '2017-07-24 02:48:08', '2019-01-15 17:07:15', NULL, 0, '51.7.145.156, 141.101.107.92', 5),
(43, 'Omolere Femi', 'fredotade@gmail.com', '$2y$10$LjlWGlvFe438bhbSOScHV.Cnyef8cuaqjPYIocU5YAO5fFK6YkQaO', NULL, 'Telegram', '08033191165', 2, 0, 1, 'lRmnlk50lw0whhVyxVJA50BfQXGcnEHsSZ3R5OxjBQoTH5640xznqRk1tMdC', '2017-07-24 10:40:08', '2018-11-07 13:38:28', NULL, 0, '41.190.3.167, 141.101.98.42', NULL),
(46, 'Rajath Rajath', 'Crystalsforex@gmail.com', '$2y$10$8CQCa1mbqhwEj9r.9Tx.cOS4MhSKAwcgPCjzH4rVqZgPuJtyZ7JSa', NULL, 'Telegram', '+919916553345', 2, 0, 1, 'hqUSh0rhcS7lRO6a9MfAVzM7BBrqdGwUpNKYGCJ0s8Dn4k5GhSRUAE4zQCVo', '2017-07-24 15:18:26', '2019-02-01 01:19:33', NULL, 0, '122.179.65.43, 162.158.50.66', 5),
(56, 'ali shahrivari', 'alishus@yahoo.com', '$2y$10$LMLSrBRb6Hzy5f.IJFc./evvNRlfQRj5CbszmJLt0AZbX3geJk5cW', NULL, 'Telegram', '8182623730', 1, 0, 1, 'XlQ40ffQVlEfeEzqgIKC5zFlMQUX8ZZfzWpgOjY87DFIf35sQEgh23dltMqc', '2017-07-26 03:27:22', '2018-11-07 13:38:28', NULL, 0, NULL, NULL),
(64, 'Ellen Koszyk', 'elkoszyk@gmail.com', '$2y$10$HuTpcUYNxwojMUx1HjxoCOnVEGJsFoImtJCOn00ZrgOscWHG5RIFu', NULL, 'Telegram', '702-418-9023', 1, 0, 1, 'QecKU7zk7DFpj5EadrcCkhYg6XoLWsKDyHZkWeqLveoaiwTTOrNbp5dysoQL', '2017-07-29 09:58:26', '2018-11-07 13:38:28', NULL, 0, NULL, NULL),
(65, 'Mark', 'byattonline@gmail.com', '$2y$10$c4NcqjkIhl7ov6TCkRw1w.w9rwCC6jJNWOXHp9A5TS8u9UYOeXUJW', NULL, 'Telegram', '07829717605', 1, 0, 1, '8NkxvTa7sFLxRl6qQ7ndsww2z69atbfUutT8ziZEGFHlqQuh10tADiKwNsaV', '2017-07-29 15:38:07', '2018-11-07 13:38:28', NULL, 0, NULL, 1),
(76, 'Al Chin', 'nowview@microinsight.com', '$2y$10$8o3dTa/tbe0RYnUspK55wehumiN4IDW5AXS/kfqKBeKwhkmEFCGDi', NULL, 'Telegram', '3215050490', 1, 0, 1, 'UaxhPJNDcQSCWSfKbwAKjUcIeeXoWL7fOoj9xbPfZvfi8ufBaKej4HWORyr7', '2017-08-01 03:19:26', '2018-11-07 13:38:28', NULL, 0, '208.68.97.235, 162.158.122.68', 5),
(78, 'mehrdad', 'mehrdad.shakery@gmail.com', '$2y$10$CSkYwOEwiR0VQznV818LVuSbUisbxSu0cZADHyIS5hFDfHYdoyGsO', NULL, 'Telegram', '+33782030792', 1, 0, 1, 'MxxgCjKpHv8wEO8S6VFg8kh1DF30o4P85BVpechCPsfRo0UxktrIMC4HfVBp', '2017-08-01 20:22:40', '2019-05-09 10:12:42', NULL, 0, '213.161.94.33, 162.158.154.99', 3),
(80, 'JOHN', 'johnaadegoke@gmail.com', '$2y$10$FlkTAqStbVR3xhuLVIt4Je6tgjBh6ZPg2Bfz3Hm4H6UqKLpH./qwm', NULL, 'Telegram', '+234 8037900942', 1, 0, 1, '0HFV7g16HO9YhEYROkFB4N7b1bGPDWPhzQoBKo3uYIqzEIvYKWSOmn5R9wC6', '2017-08-03 06:36:30', '2018-11-07 13:38:28', NULL, 0, '105.112.29.10, 141.101.99.103', 1),
(83, 'Andrew', 'thepoptartfiend@gmail.com', '$2y$10$WpAl3k7GnA8RC8BzblLM5e6WO5k25uL3gLIQ8j9SEkn8M6EvjM5kK', NULL, 'Telegram', '+61433579258', 1, 0, 1, 'taT1n5WlfmAbDmdG1n8Kp0BMuIjrov0SLP2DzJCCDq0Fgyipujmt9elNsSAc', '2017-08-04 18:00:40', '2018-11-07 13:38:28', NULL, 0, NULL, 1),
(85, 'Charles Shi', 'Charles.shi7@gmail.com', '$2y$10$TvLbGJay.Cu21JWIvNQpLuysBSqNWBZ5/EPcqg5eAA2r9Ebq5rvrm', NULL, 'Telegram', '(+61)401145711', 1, 0, 1, '8pq2KCm0IlQ7DMjCC9JqTUtWqB46wiB8ChW8ayOd314cVVrL6iVPOjjVChPp', '2017-08-06 13:41:39', '2018-11-07 13:38:28', NULL, 0, NULL, 1),
(127, 'Good', 'HJJSHDJ@JDKDK.COM', '$2y$10$un3HCee14K7lqZxWm4eMmuje7TzbpuqInHxEhCRbmCAqqH31Eibuu', NULL, 'Telegram', '3356565', 1, 0, 0, '7ZRI8FPHSRPMqjsguxz2NhoQwf6XKqTsaaLKHHKUPyMuFIqU9MOJB6d5U6t0', '2017-08-14 12:54:40', '2017-08-14 12:55:34', NULL, 0, NULL, NULL),
(129, 'Veli-Pekka Poutanen', 'vp.poutanen@gmail.com', '$2y$10$5Deu1h1DOGniBiUyNDo4bejAFp03hDiXxmq3t6ZO7f0gqMLL/jx7u', NULL, 'Telegram', '+358504117345', 1, 0, 1, 'tj0xPHkyKnAPyjiXxYLKGtGImTiNaonTPCpTEkDRLgYH04uE58scI49g5GY1', '2017-08-15 17:02:24', '2019-03-18 18:29:39', NULL, 0, '84.34.73.1, 162.158.238.135', 3),
(131, 'Steven Miller', 'stevenmiller33@hotmail.com', '$2y$10$r3E7ll7srMxTbQhfQhfWAO87MnQoJo8mQAjenShj00g6ut5JNil2S', NULL, 'Telegram', '07708283209', 1, 0, 1, 'TjMyMRAljbQoRg8dIzFEec9E0KuyWMCpMTeB8WFCJFjKegXBx8NJitwX2DAr', '2017-08-15 22:58:11', '2019-05-13 11:56:14', NULL, 0, '2a02:c7d:c2d:cc00:316f:215e:7587:6c94, 162.158.155.52', 2),
(132, 'Hari', 'harykrishna2007@gmail.com', '$2y$10$oNLEWTAYb.XB6vlCP6qQ/OJSQgs3TmIIjbJjfXTTWBO2lOtL4QH.K', NULL, 'Telegram', '+919895451426', 1, 0, 1, 'FUCXYmIcRo0FgXcaOpGNSjH8E63dtwZPndcR4WOzMGSPe9xkcGffauT84xek', '2017-08-16 08:54:59', '2018-11-07 13:38:28', NULL, 0, NULL, NULL),
(149, 'Kandace Wilson', 'successismychoice@hotmail.com', '$2y$10$Y26yLs8spQiR8k1RU3ziR.w3JwCb9JGVGKgtXqnJKbeRP8jMMN2bi', NULL, 'Telegram', '8647879857', 1, 0, 1, 'tOpSY8qpPEB0xMpC5uWhqm0cM1Ov7sbAkJXjCwvmuloolXxRwpa1tCK2Rezs', '2017-08-20 23:31:07', '2018-11-07 13:38:28', NULL, 0, '2602:306:3189:af00:44b:382:e54a:26bb, 108.162.238.175', 1),
(157, 'Nick Wright', 'nicholaswright2010@hotmail.com', '$2y$10$QDVjdMr8tuqMt55gqeDu4.xexFgaPUKjHctPSzbD9kt2znzBGDfEu', NULL, 'Telegram', '+61 459478752', 1, 0, 1, 'mQfDE3WRWI45hfutTTn0DXtDXoHAszyAcrQEafJTVg7cdUelTE9GbhhTBQIJ', '2017-08-24 19:51:59', '2018-11-07 13:38:28', NULL, 0, '115.70.250.99, 172.68.2.42', 1),
(159, 'Enudo Prince', 'richprince1333@gmail.com', '$2y$10$G8D3QlySjHl9WmJ731yEvuXN0Tt55oSCnpkBcbQYfykubQCGA4HT.', NULL, 'Telegram', '+2347032095181', 1, 0, 1, 'bHrJMESB90pMAIcbQ9jrucEEjKiQTvBqPsU4ONPoUeVauuBrCYq2HPsAMM39', '2017-08-26 14:45:01', '2018-11-07 13:38:28', NULL, 0, '197.210.24.81, 141.101.104.235', 1),
(162, 'dimi bozhilov', 'dibojjilov@abv.bg', '$2y$10$RC81r6x1AArS5T9So75gI.SVXCDfrNheHENguuPfCVF60fB5yYTBe', NULL, 'Telegram', '359888806960', 1, 0, 1, 'LfkwXZbfspRmMBTXhOOi7kTuKFjbzhD2Xzn6NooPLzvadVVCrOzVu7dBlneN', '2017-08-27 16:35:05', '2018-11-07 13:38:28', NULL, 0, '85.118.84.30, 162.158.92.48', 3),
(172, 'KenLO', 'cystyle1983@gmail.com', '$2y$10$UVtsAx7RJjOME8pm1pdOheqndhNQehoAI/xE8Cokq5DWiMn6i9R1q', NULL, 'Telegram', '85292868660', 1, 0, 1, '7er2Fqg5iz6PdmdzOReQYO9zR6JI8bsxlKQSVJKw6a2wUgJksltvPp9CLjNU', '2017-08-30 16:28:02', '2018-11-07 13:38:28', NULL, 0, '58.153.110.167, 172.68.254.164', 1),
(186, 'vamsidhar', 'webcomms@gmail.com', '$2y$10$9Ldxm6KqkhRj3N3sbZXOTeCoudPI3CcH5zpLZqMGZR9UbvB9VzeEK', NULL, 'Telegram', '917760776097', 1, 0, 1, NULL, '2017-09-03 17:20:42', '2018-11-07 13:38:28', NULL, 0, '43.247.158.3, 162.158.50.132', 1),
(188, 'Ryan', 'Ryan_Rolle@Bellsouth.net', '$2y$10$WoL/3ahGRF/sTHH2w1hYYOnSx56OtcgQq.fEdkNnMT2Y0xkbQWdKe', NULL, 'Telegram', '9546500671', 1, 0, 1, 'RuDMGjCwJdS8FPldsnrRRqaoWJo9IAihh5gp9NnxmNljN6dsHTPCCJ3TfsJD', '2017-09-03 19:54:12', '2018-11-07 13:38:28', NULL, 0, '104.14.147.29, 162.158.122.128', 1),
(191, 'alex', 'karamokotoma@gmail.com', '$2y$10$Upe9iCgOTqQNmW3NGdQ6kOfZmhnJdjsojEqDVZXU3vaQDHwLvj0Ki', NULL, 'Telegram', '0034631686068', 1, 0, 1, '3VtrTnONhtaJcE5k4FzJAMtUjzpPI2aogeBRrfvQEeIaeeo5AwVpRIl7bBRD', '2017-09-04 20:25:56', '2018-11-07 13:38:28', NULL, 0, '83.40.3.250, 188.114.111.31', 2),
(192, 'Robert Kovacs', 'kovapuka@aol.com', '$2y$10$cE7e9.30NCVHfH/fdgQ9DO95klH5265eUGE/cpCa3DlNdZM0waFoO', NULL, 'Telegram', '+1-646-299-3638', 1, 0, 1, 'LCGo0SF6lGcwChqjw5tDoIkcxE4yvbJPMC1VPod1lRk8aM9GJk1Cz2YQYD4v', '2017-09-05 13:33:27', '2018-11-07 13:38:28', NULL, 0, '74.72.250.43, 162.158.62.239', 1),
(193, 'Jonathan dela Torre', 'delatorre_jonathan@yahoo.com', '$2y$10$RbN/wsCszZIO1n5VvM.I4.wYQCBTSGl3mRcpHBVoIeagtPd0MDbkO', NULL, 'Telegram', '(+966)547848063', 1, 0, 1, NULL, '2017-09-05 22:24:48', '2018-11-07 13:38:28', NULL, 0, '178.80.43.167, 162.158.155.28', NULL),
(195, 'Terry McReynolds', 'tjmcreynolds@yahoo.com', '$2y$10$wUWbygAdhUARHOGAKrw2Y.5d0GeAEN726ajdYGiimag5bfcjymPPO', NULL, 'Telegram', '(+623)9104745', 1, 0, 1, 'cwN3QAIfx3P4tr8wn0bQzLjAUK6iYRESwb3e63N3ZFyhOxqincnhORLqpb8D', '2017-09-07 00:30:44', '2019-05-15 03:09:35', NULL, 0, '70.176.158.40, 162.158.142.96', 3),
(199, 'md Mojammel Haque Chy', 'hamidchowdhury2013@gmail.com', '$2y$10$d2nqNtBXrl1ymAXW0VAZB.Pv8Rjf9ua20MM7jK2yapZ8psSZSFMT2', NULL, 'Telegram', '+8801610106060', 1, 0, 1, NULL, '2017-09-07 17:02:04', '2018-11-07 13:38:28', NULL, 0, '180.234.47.53, 162.158.167.98', 1),
(201, 'Jim', 'jfinsker@gmail.com', '$2y$10$xfvyzFLIWhT2Ss2C8imPq.LkKItNUfGe7IIGyxNV0/5xOfEL94FRy', NULL, 'Telegram', '854-784-5623', 1, 0, 1, NULL, '2017-09-08 19:01:42', '2018-11-07 13:38:28', NULL, 0, '99.101.68.33, 108.162.238.205', 5),
(202, 'danisa mpofu', 'mpofu35@gmail.com', '$2y$10$tG6gXzTR2s0HrPWhlRGWhOPQ6Rw7ikl8ll9EnBAaebQ995lEloE0W', NULL, 'Telegram', '07886349767', 1, 0, 1, 'llmTwlyiOQoYaRqjTJoFVKKOG8zC902xFAxyRkQX7jxOYFOrEeyCyy4BUlzl', '2017-09-09 19:03:08', '2018-11-07 13:38:28', NULL, 0, '86.27.155.186, 141.101.107.182', 1),
(205, 'Nicki mcadie', 'airdon2011@hotmail.com', '$2y$10$uNiuMa92ZJ2Q.YtAyGzER.3eOQuB9QCCH25eG/NV4p3SVhkzn79Ui', NULL, 'Telegram', '(+61)419199942', 1, 0, 1, 'sTkqulJOYJLEG1gbtHoOjzBP4P7NThGjYw2ZiUPkRwSIYxVnhV0B2JMJyph7', '2017-09-10 12:25:01', '2018-11-07 13:38:28', NULL, 0, '120.145.149.122, 172.68.47.164', 3),
(208, 'Peter', 'thepoptartfiend@protonmail.com', '$2y$10$r28VIEWiOLrm88JLV.j9.OkCpws13Hi5rITZ23/.wgrojCb8oN72e', NULL, 'Telegram', '+6204553456732', 1, 0, 1, NULL, '2017-09-11 10:18:40', '2018-11-07 13:38:28', NULL, 0, '27.32.20.42, 172.68.253.25', NULL),
(211, 'stan', 'stan@cox-international.com', '$2y$10$PBLFDkPMzbnOYz0jyMpcxejHdKgTw0YIsVnDJMwURyNY7m3DACKu.', NULL, 'Telegram', '715-484-7856', 1, 0, 1, NULL, '2017-09-11 21:09:27', '2018-11-07 13:38:28', NULL, 0, '2602:306:3654:4210:a14a:4fef:3fb4:ad40, 162.158.186.20', 5),
(215, 'Nicholas Hanson', 'nicholashanson@hotmail.co.uk', '$2y$10$jSIQTEA3uTC4Esz0dltZT.GYXb/tmoHmmzEJy.OgDOly48kCj7Qgm', NULL, 'Telegram', '001832571497', 1, 0, 1, 'FNj44MmLpssyVWuqMVHzmiFzLbt6JLyepYXmt70Xbhx4c8xXM02IB1fENmi8', '2017-09-11 23:22:20', '2018-11-07 13:38:28', NULL, 0, '2a02:c7d:608c:4600:923:f643:cdf8:13d8, 141.101.99.109', 1),
(219, 'manzoor', 'manzoor11@aol.com', '$2y$10$.iMm8/lY/i1rD.7mgGDCjuYK9ln9cVHZA0Ey3A//l3RsYfKt8U2W6', NULL, 'Telegram', '(044)7872971894', 1, 0, 1, 'pe5iFHix9Iz8XYcgURnN2VlYpZflNnNkjVhj3koD8j2R5qflnGMSBhjWLa76', '2017-09-12 00:16:59', '2018-11-07 13:38:28', NULL, 0, '81.100.101.31, 162.158.154.111', 4),
(221, 'Robert Reeves', '373hhc@gmail.com', '$2y$10$V3Lcja2.pdLWhnFQhSL40uPvNk0tvYaZLRH.X3r65lc6nGvfkQFLq', NULL, 'Telegram', '828-467-8136', 1, 0, 1, 'OhtVhtGiyd538tst5IRnp3f3iny98jMZobpQ8ON9jbiRxTpRN416YFYgYcnj', '2017-09-12 00:21:56', '2018-11-07 13:38:28', NULL, 0, '2600:6c5e:407f:e676:d9f0:896d:d30:956f, 108.162.238.211', 2),
(223, 'bill miltenburg', 'billmilt@hotmail.com', '$2y$10$jOJsDniv/CpUe9mo0LFOBO.AcEaRQ7hNuB6buXP9Tnws3pyICJFbe', NULL, 'Telegram', '5196328307', 1, 0, 1, NULL, '2017-09-12 01:16:10', '2018-11-07 13:38:28', NULL, 0, '192.40.241.137, 162.158.126.12', 1),
(227, 'Jose Zuniga', 'jaza138@yahoo.com', '$2y$10$TQf79FZ3.8Lg8fmCAqN0FuDjR0SdJlosqIoMK3nAK8CRQ6dggcMty', NULL, 'Telegram', '6193663336', 1, 0, 1, '355yUFakRLcVIB0noKY7yjgpGRfMeCAojLrJ5ucz54ZWcCaASE4PJx76amPB', '2017-09-12 01:59:22', '2019-05-15 00:51:53', NULL, 0, '68.8.141.10, 172.68.230.84', 3),
(230, 'Steve Gilbertson', 'stevegilbertson.au@gmail.com', '$2y$10$NAeGRkS9bgGh1SJ1eoSW2edlWF4Qwgq1q/Nrm8pMjScLCnAL/4G6q', NULL, 'Telegram', '(+61)0432650259', 1, 0, 1, 'gWy9PuR143hQ8QIAu3GeOyKtIVu3WzcunPbmIx0i8lrkQPY8WMCoKMNVBYCe', '2017-09-12 02:54:04', '2018-11-07 13:38:28', NULL, 0, '116.240.53.115, 108.162.249.12', 1),
(231, 'Ikhalfani', 'inkuumba@mail.com', '$2y$10$RXqjo15.coi.L9Dpy2HXre/xaSluIxOItt3SHdGVb7PznbIUbu1Bi', NULL, 'Telegram', '111111111111', 1, 0, 1, NULL, '2017-09-12 03:40:21', '2018-11-07 13:38:28', NULL, 0, '174.110.170.124, 108.162.238.133', 1),
(242, 'Ronaldo Lim', 'ronnie.limph@gmail.com', '$2y$10$hbNgx2WRJhXcjQa5lzyQT.ch3hIIFGMoCh.k7oqwefib89A4liD3e', NULL, 'Telegram', '09953024325', 1, 0, 1, 'vlwRyhiTnejvCjRfujcC1BX03Tm2efKW6l1eZdNj6sPvakstutLnlx1orMaJ', '2017-09-12 07:59:32', '2018-11-07 13:38:28', NULL, 0, '112.205.218.130, 162.158.167.98', 1),
(246, 'milan bera', 'mbera_ii@yahoo.com', '$2y$10$hnBykwC12QBUeSkjiPbSqeQzv.eZTK31CjSP.No0vM9TWR6vOSarq', NULL, 'Telegram', '(1)2059089281', 1, 0, 1, 'o3HyJZyJW10oOqIKm0gvRCz1DBuFRTu1imNFWr6FbVOnHdN4WMOvSSGkVkxM', '2017-09-12 10:01:45', '2018-11-07 13:38:28', NULL, 0, '216.170.64.50, 108.162.237.30', 1),
(252, 'Jong-su Kim', 'paypalage@yahoo.ca', '$2y$10$lG5N/dd03Vn1mQT8gc5z1.nYDkQnxWdHFN5LpN9tW7oL.jrXDMnSC', NULL, 'Telegram', '+446627828282', 1, 0, 1, 'LWpaXhEj3R1lBQ9HSpSYWxSMi6818trwLfLGaVvgF4X2dJ0Jf5B8RA7Z2Dpj', '2017-09-12 14:56:40', '2018-11-07 13:38:28', NULL, 0, '121.126.211.79, 172.68.254.32', 1),
(258, 'Rajesh', 'rajesh.mokeri@gmail.com', '$2y$10$ZWUReUjlR/Ehqo9pNQgHYuqjW.2Dsv2RosZjApgBdZe23jLyxuFzu', NULL, 'Telegram', '(+1)3032148967', 1, 0, 1, 'bknygQUre5vnb4YflFn7Qf6J0VRLMOlciuiNpn4WEzJkQzuryXk6Nflh5PVU', '2017-09-12 18:55:11', '2018-11-07 13:38:28', NULL, 0, '108.171.132.189, 108.162.221.30', 5),
(260, 'Peng Xia', 'xpno_001@qq.com', '$2y$10$XRx/bk/6EuJATn8irP8XROApicyR4.rwFMAe1AZmnNS7hpvwtlrB.', NULL, 'Telegram', '+8613611551135', 1, 0, 1, 'slOkxn6PmJS67xC34IcbMDBNiPQgGqS9vaVHv5FJg7EaJj0ynUAyx2WNEYrW', '2017-09-12 20:44:41', '2018-12-09 14:39:14', NULL, 0, '117.151.20.62, 172.68.254.32', 1),
(261, 'md mojammel haque', 'mdmojammedhaquechy@gmail.com', '$2y$10$41UubUwazeGzCCPcj/CQqOyHbfsvkW2z9QPjQtXXZHZJgpfwV82rK', NULL, 'Telegram', '+8801552438440', 1, 0, 1, NULL, '2017-09-12 20:51:25', '2018-11-07 13:38:28', NULL, 0, '180.234.34.127, 162.158.50.30', NULL),
(264, 'ron barel', 'ronbarel1960@gmail.com', '$2y$10$.KRcDOyNhxjA4jz24A/XF.16D9eVKiYTbuIF4lh/iSHlNkW6gn9Sa', NULL, 'Telegram', '972542548119', 1, 0, 1, '1VIXSWBKAkk9WlINapgMVLuY7like1m7E76RQ842D65rJYcYkqmFIfGplW5A', '2017-09-12 23:13:24', '2018-11-07 13:38:28', NULL, 0, '77.138.14.24, 162.158.91.145', 1),
(274, 'Ben D\'limi', 'lin3359ben@bigpond.com', '$2y$10$QLQs1zbDoQMRgVezsDD4mOI4ISZs6S3wqnfV.wch7uLTiYUllDLvi', NULL, 'Telegram', '610403021862', 1, 0, 1, 'yHDGusc4nniBwtNf8ND85FgkRe1vfZetjES4QHkS3L3Ehf9GNaSrwo7MDjeq', '2017-09-13 14:41:34', '2019-04-02 08:53:40', NULL, 0, '2001:8003:88cb:7400:5da:45b:54ab:cce5, 172.68.2.78', 1),
(277, 'Dan', 'ddaley@outlook.com', '$2y$10$N4QyXagycH.gCQnmkKpo1OiNPKO/615eNPqEiBHKoGwmzuoSBGx1m', NULL, 'Telegram', '7819836555', 1, 0, 1, '3M8tHFjh6Znsq1Mb8VGYTweTqahKPQyCqAIAC2iFZeYxrVIs6UqEPnc5A2sE', '2017-09-13 17:02:49', '2018-11-07 13:38:28', NULL, 0, '100.0.117.126, 162.158.62.47', 2),
(281, 'Pooja', 'prsaini@gmail.com', '$2y$10$jG/8NmN2wubswg.u/RtDUeFufQemsHSzqIKzcTcm2usEIO2DSs0Om', NULL, 'Telegram', '8398900031', 1, 0, 1, 'I7dynn2LburTrSAgrC1gbgWr1QmWbK178v4T2nDeL7LeTX369DoKXxL86VCE', '2017-09-13 18:57:41', '2018-11-07 13:38:28', NULL, 0, '2405:205:338f:7a3a:6c70:e81e:4575:443, 162.158.165.114', 1),
(285, 'Sandra Puschnig', 'sandrajean222@yahoo.com', '$2y$10$UCbbE2Za3KaW0ZqCCUnmpuI7rSLI5cFZhmOIwy5IbfUstErrxrqJ2', NULL, 'Telegram', '17154160944', 1, 0, 1, 'rw6EOfYcBUgj13ehvUszuqXHQCRKgTIFkbnaoI9o3vbqpp92r3OPwlqhNAMg', '2017-09-13 21:45:19', '2018-11-07 13:38:28', NULL, 0, '96.37.121.199, 162.158.214.66', 1),
(291, 'Dawid', 'dawidc83@gmail.com', '$2y$10$QnwQnz.Ue2OiahaNchcmj.0rWZCj2Uueghbvx9y5cLxYuzxw9P0gW', NULL, 'Telegram', '+27 72 386 1741', 1, 0, 1, 'Vv110mAhPqwZsgJrc6xlObWenUeUOYHRHwzvUNDAZF97Pa0gz1FrRNJ90Bqi', '2017-09-14 15:51:35', '2018-11-07 13:38:28', NULL, 0, '196.215.111.198, 197.234.242.6', 1),
(293, 'Jorge Alberto Scotta', 'jorge.scotta@gmail.com', '$2y$10$oIMl07jJD8Isg5oMYSweS.f.e3qmO6WgzP8itS0IJZ2sI4TzVnH0G', NULL, 'Telegram', '+5551997672296', 1, 0, 1, 'wxjG1pRdyCKYyvvmO2WVXMVO42SNwvwUoRphu0cK6rb5rpTAPTNoFc1sl1wE', '2017-09-15 01:32:51', '2018-11-07 13:38:28', NULL, 0, '201.37.163.48, 172.68.26.223', 1),
(306, 'Erayne Stuart', 'erayne.stuart01@gmail.com', '$2y$10$jw1AobBd1DdOLOEKR5Fww.QpKjyvxC8G6p0xjVIgK5BQ0ni.xn5Eu', NULL, 'Telegram', '+61403277862', 1, 0, 1, 'cnQnqhQfEYTKmJjvtXJHiklVgiS3yd5E9cPIiKOWMAO5gbzs8JG4OTITskfe', '2017-09-18 12:48:27', '2018-11-07 13:38:28', NULL, 0, '2001:8003:6502:f800:8dfe:b3f0:974c:6529, 108.162.249.18', 1),
(307, 'Miles Stuart', 'finsolutions@bigpond.com', '$2y$10$Z3S5I1u7EL94qH4BnJy.z.kDnfPVEYL5GSNudrTm6DKTPdCrdRPAO', NULL, 'Telegram', '+61416325621', 1, 0, 1, 'WtJbOmofD1c4fWWyS1Y4e8CVgkf7eGp3pCu5TNZxomtWnxYQzFNXu1T39dvN', '2017-09-18 12:48:29', '2018-11-07 13:38:28', NULL, 0, '2001:8003:6502:f800:714a:5c4:12b2:e3f6, 108.162.249.84', 1),
(308, 'Katarina', 'katarinagb@yahoo.com', '$2y$10$Pp4AQJ0PuN8hDFQP1TEc8uxFm5UnXP0w3bqhJQcZbiwgyD.42BzIW', NULL, 'Telegram', '00421907600642', 1, 0, 1, 'VBMGocrhk2TjItP6FBYHiITyWXMuoBVG0Q07WTXBWfabaxeiy4FAUClYXky0', '2017-09-18 21:44:21', '2018-11-07 13:38:28', NULL, 0, '95.103.161.102, 172.68.50.186', 5),
(312, 'Chandra', 'jinkacs@gmail.com', '$2y$10$0QEZugDvSTek3BTRJgSejeIGdNNhs17gs2Djb1AVqzPMi6LWB5/XG', NULL, 'Telegram', '3106346290', 1, 0, 1, '5LzfH0vCurjxO3rws3KLq6AuUjMGAMbaiybSTT3Bmoee0z03mRU7yRJyRofC', '2017-09-20 09:54:54', '2018-11-07 13:38:28', NULL, 0, '108.185.125.100, 173.245.48.215', 2),
(315, 'Jeremy Burton', 'jjmburton@googlemail.com', '$2y$10$8bZkB5dNbUkx4lOeKE4nxeJId1366UanBO5oRA/a7voYcUXxS7EzO', NULL, 'Telegram', '+447554 444705', 1, 0, 1, 'lvYYObvb1pmFooHGg5UsUI4ZjaMHdcFy4YFol9Pfz8bmobEwBL2No8XIpxzx', '2017-09-20 17:25:27', '2018-11-07 13:38:28', NULL, 0, '2.7.23.18, 141.101.99.91', 1),
(325, 'Syed bilal yousaf', 'bilalcomsats@gmail.com', '$2y$10$mDsPkP0JVKPavkj3S.2EzuoQHfnjqSHMbB2qFHp3QxMcrbuvrECm6', NULL, 'Telegram', '923404994906', 1, 0, 1, 'YDyJampnm5561N3oAL5dgYF34xzecbwq0SZVsnmT6iLeSjWdMaA7PCo1b20M', '2017-09-22 08:51:29', '2018-11-07 13:38:28', NULL, 0, '103.255.4.246, 162.158.165.186', 1),
(330, 'Jackson', 'kija1985@gmail.com', '$2y$10$hbPf9bO9EBzzi6ddbr2oK.yoJEgqlBmv01A3rLzsopSFX5nz.7idm', NULL, 'Telegram', '+255755783615', 1, 0, 1, 'v2mfr58TdNzE4MOAheMC9EPOSkYuzsR6DIAD3VyPuTUaYsWP8ENnkGmisQrD', '2017-09-24 12:06:29', '2018-11-07 13:38:28', NULL, 0, '196.33.137.69, 197.234.242.66', 1),
(331, 'abbeyforex', 'mankind6202@yahoo.com', '$2y$10$Y1TXlQ8f8jETR3LPpPo/XOvpr4VCMV.J033nP6VTrHSJQoMh3HkIO', NULL, 'Telegram', '+2348035018559', 1, 0, 1, 'u0sE7UzKuoWD4ZmGdokZR8nMD6H7BdWlXIsQLpmqHQnQmVOaWP6uX1sQHNJ0', '2017-09-24 15:53:05', '2018-11-07 13:38:28', NULL, 0, '197.210.227.23, 162.158.111.195', 1),
(366, 'Babur', 'babur_shaikh@yahoo.com', '$2y$10$Hj/YmWxVBUsn758zURt8LuxQKu.b0gVz7T3fehx3iyLNFt4oxx1H6', NULL, 'Telegram', '1234556775', 1, 0, 1, 'FdnWmRdxJqKThsDtsVbutlJBh3lxDRihgJK6kFND07KPczo13ZgTG5EIh71C', '2017-10-10 11:51:32', '2019-05-05 06:45:53', NULL, 0, '220.245.76.62, 162.158.179.30', 1),
(367, 'Yuricia Vinkawaty', 'yuricia@gmail.com', '$2y$10$VoRa23A2/0z26bXU0wGSxuS0HwknneQN3rchwVXvE779IStBRLbh6', NULL, 'Telegram', '+6591175677', 1, 0, 1, '0lzIbcV4WS7NJat11YMvHAYG9EBNvulSTUyzqcTGoWuTuShoBnDMl9mfVL4u', '2017-10-10 19:41:17', '2018-11-07 13:38:28', NULL, 0, '49.213.19.145, 172.68.144.69', 5),
(461, 'Aleksandar Petkovic', 'alex69575@gmail.com', '$2y$10$2Y2xgrp7gvAMnCZp2ZDuFeaTvbVSxvQpLbEK0h14ifpqaaE.fBPy.', NULL, 'Telegram', '+38163466926', 1, 0, 1, 'DlZGIz7LqUxnwPWbbtbSw3rNrCMheKAT5t38hdw8kHtxsFM9VDizjyVe4i6q', '2017-12-04 13:54:35', '2019-04-19 07:59:30', NULL, 0, '109.245.141.42, 172.68.154.90', 3),
(509, 'James', 'jamesgreenwood888@gmail.com', '$2y$10$no1vUf7P5s4EwEoZR0NejOErECQoph/cLHecJVttG66ElPdMdSMWq', NULL, 'Telegram', '+33769329373', 1, 0, 1, 'EqoGada6Zph7uNNVDpxtAEbkGuSOUSPAboEn4qSvbI5rivXrSOgzIhLXSivy', '2018-02-04 16:11:47', '2018-11-23 01:42:43', NULL, 0, '2a01:cb1d:83b6:1e00:2171:2e1f:f9c3:3a9b, 162.158.22.108', 5),
(519, 'Zuhu', 'fjdjrudu@gmail.com', '$2y$10$fimBpBPUYSgR7.C44vJJzuxBD.VXZbvDb8nVQ2mxzCFkn9KPKDuE2', NULL, 'Telegram', '91927477347', 1, 0, 1, NULL, '2018-02-08 10:53:33', '2018-11-07 13:38:28', NULL, 0, '171.48.113.92, 162.158.154.177', NULL),
(531, 'Bryon Seiler', 'binary.bryon17@gmail.com', '$2y$10$cxeKT2gk3QntZTuDjrVjHegRx5xLRsbD9alGyIZD1d9ejfhIqk.iu', NULL, 'Telegram', '405-413-2666', 1, 0, 1, NULL, '2018-02-15 03:09:12', '2018-11-07 13:38:28', NULL, 0, '156.110.24.142, 108.162.216.216', NULL),
(533, 'Yaliwe', 'ysoko2@gmail.com', '$2y$10$21ocN.7TFlUkm2S1GHBv0uTEvWbeihFEToxOdu/rFU27TCT3D2MfW', NULL, 'Telegram', '+27784771754', 1, 0, 1, NULL, '2018-02-19 21:03:45', '2018-11-07 13:38:28', NULL, 0, '105.228.192.186, 172.68.186.78', NULL),
(534, 'fabian', 'ffabian845@gmail.com', '$2y$10$YmjEo.9yBmnK3wze3Eq7FeZJ4nCxCDEAs.fICUJk8RhBUgeINY2/q', NULL, 'Telegram', '03495823485', 1, 0, 1, NULL, '2018-02-19 21:24:05', '2018-11-07 13:38:28', NULL, 0, '79.218.139.110, 162.158.91.169', NULL),
(538, 'mark', 'marcusmgs@tiscali.co.uk', '$2y$10$J5vwJ0d99uVNi5Ldv.eTUumE6WUPKJmWjhkS0rSIdHuKqOEeh.r9C', NULL, 'Telegram', '07983567553', 1, 0, 1, NULL, '2018-02-23 22:10:48', '2018-11-07 13:38:28', NULL, 0, '88.109.18.40, 162.158.154.63', NULL),
(541, 'Bassey Eboh', 'Ebohbassey@yahoo.com', '$2y$10$jY7QylRJ3wTcYF7sIR82leOa1n0n3cKd.SDdbwDy2K0Lv0t3EkI0W', NULL, 'Telegram', '+2348055672229', 1, 0, 1, NULL, '2018-02-26 12:10:08', '2018-11-07 13:38:28', NULL, 0, '197.211.63.157, 141.101.99.241', NULL),
(544, 'francois', 'francois.sarron@wanadoo.fr', '$2y$10$C/2XWfa3o8ejvsUsYqVwsuW35fLLaU/AC7BfG3rdlkxXL7cbejI4C', NULL, 'Telegram', '+33680950608', 1, 0, 1, 'pHEdK2HZaQqaGTIB3mGbexQdyGTplsRZImmfOCud5AsIBfr8znCd6jHAuyQq', '2018-03-03 16:28:50', '2018-11-07 13:38:28', NULL, 0, '78.245.106.8, 141.101.69.227', 3),
(547, 'M.M. ASHRAF', 'MAQSOOD3@HOTMAIL.COM', '$2y$10$hu6srvrsmS58cIiaV.EyGe2t.ut0fvnofDk//eyz0hefOEfWnVRf2', NULL, 'Telegram', '+923368118302', 1, 0, 1, NULL, '2018-03-04 17:20:53', '2018-11-07 13:38:28', NULL, 0, '39.50.134.200, 172.68.63.13', NULL),
(549, 'syed zain bin qasim', 'ssyedzains@gmail.com', '$2y$10$zy4Dl8PbD7sU4X7H.CVcIutE3ETEDDobSjV6q9F/at/BR8P88E8JK', NULL, 'Telegram', '+923335108538', 1, 0, 1, NULL, '2018-03-08 18:07:20', '2018-11-07 13:38:28', NULL, 0, '39.32.156.27, 172.68.63.7', NULL),
(551, 'Yuseng limarta', 'yusenglimarta@gmail.com', '$2y$10$5VO4dPBWv2BiGkRTDPnzF.CSvyaxXr2pyf7JkGkj0ZGxJFDZOB9N2', NULL, 'Telegram', '+628122013818', 1, 0, 1, NULL, '2018-03-10 19:10:09', '2018-11-07 13:38:28', NULL, 0, '111.94.27.197, 172.69.33.127', NULL),
(559, 'Ganesamoorthy Ramasamy', 'ganesa.ram21@gmail.com', '$2y$10$uBV3lylvK1Gb2H6AwyhiDu5nXU7f9wEuprMmSy7jVRQRu8k3Dk4yO', NULL, 'Telegram', '9791163788', 1, 0, 1, NULL, '2018-03-10 21:39:48', '2018-11-07 13:38:28', NULL, 0, '42.111.142.76, 162.158.154.15', NULL),
(560, 'UDIT PRATAP KUJUR', 'upkpratap@gmail.com', '$2y$10$zXEIHuW7voS00xSyq6lQuO/FAhFitKdfsspZ3r6PpTMxKaVWC8iaO', NULL, 'Telegram', '8895765929', 1, 0, 1, NULL, '2018-03-10 22:09:44', '2018-11-07 13:38:28', NULL, 0, '103.87.89.6, 141.101.99.25', NULL),
(561, 'Nirupam Roy', 'nirupam2222@gmail.com', '$2y$10$NDU7qBYvaCg0DrCjieab8.nb.tOJ/gAcwmPl0suji2kt1Rbb1fEm2', NULL, 'Telegram', '+8801925677541', 1, 0, 1, 'JR8hsqbNK4cT9keTB0N8qdbhbzq8OiMTSb2UyDgHt3cnPfCM1BKSXUjc2guL', '2018-03-10 22:43:38', '2019-05-23 20:01:57', NULL, 0, '27.147.190.242, 188.114.103.43', 2),
(562, 'David Hall', 'djtradex@gmail.com', '$2y$10$McUKkJ0sID7JlaIWlgIqseEGGZ7zTR0aOEZq3x2iV.V3ily9Br.I2', NULL, 'Telegram', '+1 202 304 0038', 1, 0, 1, NULL, '2018-03-11 00:50:27', '2018-11-07 13:38:28', NULL, 0, '2600:8806:2400:69c0:ec9c:a214:5c37:a03f, 162.158.63.108', NULL),
(567, 'obadiah', 'mkuria849@gmail.com', '$2y$10$sn5i56G/IyFWh/H5rSylleG25paB/XOqx85JffCbiwO.1bQQtvl9S', NULL, 'Telegram', '+254727557755', 1, 0, 1, NULL, '2018-03-11 11:38:33', '2018-11-07 13:38:28', NULL, 0, '196.105.58.203, 162.158.42.6', NULL),
(569, 'Pier Dimitrov', 'pezihong@abv.bg', '$2y$10$6G/Ia211m4FCaIyypw.g6elkAqEybiB4kcCUloiKmL5o7H78LgPjW', NULL, 'Telegram', '+359 888 707637', 1, 0, 1, NULL, '2018-03-11 12:46:21', '2018-11-07 13:38:28', NULL, 0, '92.247.57.37, 162.158.210.48', NULL),
(581, 'Eric Ogbole', 'eric_ogbole@yahoo.com', '$2y$10$yTw7T1ZTAyGFnM401/KnweA0KvspCseUlWTUyxYmvqL8N517j2jeS', NULL, 'Telegram', '+2348036706311', 1, 0, 1, 'msySfCJi2neqdgFwvdxrs8ZwPJ4fJtzDASLzvFyBiBbYYoZHi4bkcCCoL3t1', '2018-03-12 12:30:31', '2018-11-07 13:38:28', NULL, 0, '197.211.63.72, 141.101.99.187', NULL),
(584, 'Rian Douma', '35M015@gmail.com', '$2y$10$F936aSy00wwJDLi4kMiwWOD1twXkoiC0N.mJJx1k0BzhQzx7/Bj22', NULL, 'Telegram', '624782772', 1, 0, 1, NULL, '2018-03-13 16:10:48', '2018-11-07 13:38:28', NULL, 0, '2a01:e34:eee1:66e0:91e8:835f:5cca:b77, 141.101.88.192', NULL),
(587, 'RAFAEL GARCIA LENDINEZ', 'garlenmotor@hotmail.com', '$2y$10$ti43SkBdrGcodJHcNpXD/u71lFJPrsV7okqIbCGOs8R.qiqsbBHLa', NULL, 'Telegram', '(+34)692243214', 1, 0, 1, NULL, '2018-03-15 16:36:03', '2018-11-07 13:38:28', NULL, 0, '217.217.154.199, 172.68.94.210', NULL),
(588, 'Swanzie Evans', 'devanjo@email.com', '$2y$10$yJNrUI/g5kKnAEPQ5hIFOueYXrE.JTz4ORN2RoO61yPKLiY.qDl2i', NULL, 'Telegram', '+254721803466', 1, 0, 1, 'VQaS8edFvVrYnqdzblJGg4wsQsJDuuyUl0zQWB8o1VBExW6wX1w83dHKVy7z', '2018-03-16 19:20:25', '2018-11-07 13:38:28', NULL, 0, '105.52.212.62, 162.158.42.12', NULL),
(590, 'Rony Buck', 'bogra_rony@yahoo.com', '$2y$10$J5Er1rxkI1FF5tdo4LAtaunhSYeUQk42DdJaeymOZUha5/CbvkYd6', NULL, 'Telegram', '+8801711874467', 1, 0, 1, NULL, '2018-03-17 14:41:12', '2018-11-07 13:38:28', NULL, 0, '103.93.90.242, 141.101.107.38', NULL),
(592, 'Anthonyus Kuswanto', 'newbiz888fx@gmail.com', '$2y$10$nuLAlrIbIC6IgZDXn6j.wOatzHwmYGFYvm0Cmd6FzYSexMi.n2yt6', NULL, 'Telegram', '+62818957168', 1, 0, 1, 'TjPEu0RVYfJcT0DP8ciz8xS1cVjvXAe1dElcpUKVwPyfEQGi4JIqa4Jn8N0J', '2018-03-17 16:40:38', '2019-05-12 20:41:17', NULL, 0, '112.215.200.243, 172.68.253.67', 3),
(595, 'Jannie', 'dippenaarj@hotmail.co.uk', '$2y$10$R4dkuTl6egfJ31en9rtB6u4SYDTAnweFxuUPpl12AW.hpIsTjOmt2', NULL, 'Telegram', '07875003349', 1, 0, 1, NULL, '2018-03-17 19:27:59', '2018-11-07 13:38:28', NULL, 0, '188.29.165.175, 141.101.107.26', NULL),
(598, 'Srijon Sarker', 'srijonsarker@gmail.com', '$2y$10$xm4qfJH0MKQF9IkiLRf2Vu5BI.h8a61f8UcjEp10uEo8klHwlt1uu', NULL, 'Telegram', '8801717133971', 1, 0, 1, NULL, '2018-03-18 00:46:25', '2018-11-07 13:38:28', NULL, 0, '103.229.83.210, 172.68.254.237', NULL),
(603, 'Manasseh Yohanna', 'manascares10@gmail.com', '$2y$10$FGQ4MAXG4k/dopgDHKalj.3..n3Dn1U3sE2dDUQc3LuJrGXu97waG', NULL, 'Telegram', '+2348063750025', 1, 0, 1, NULL, '2018-03-18 23:42:08', '2018-11-07 13:38:28', NULL, 0, '197.211.56.126,141.0.13.16, 141.101.105.158', NULL),
(606, 'YIH-AN HSUEH', 'orchid1642@gmail.com', '$2y$10$DKglSd8whGE9GVAYl5lPduRVL93bWOlSu0qVnwXJvLUpsvfkFgMEO', NULL, 'Telegram', '(+56) 984961278', 1, 0, 1, 'phltJvIDCuXgebxDAbOWPFfWB8doCjC2tBd0Fu5YdieR91uQ81Kum046PHMF', '2018-03-20 22:57:04', '2018-11-07 13:38:28', NULL, 0, '191.112.194.184, 141.101.102.84', NULL),
(610, 'Ngozi onyejiaka', 'victoria.onyejiaka@yahoo.com', '$2y$10$c2ucQ1sy7lHnscx.3aF6ZOk..8hRai8KD/v1D65hACHihXZdPpH/2', NULL, 'Telegram', '2348063338962', 1, 0, 1, 'Bc58RaZmteF4x3gJJTANoAseetJM1IlCwSG3UplHby5glmKsYRtsGmluBxNj', '2018-03-23 00:11:24', '2018-11-07 13:38:28', NULL, 0, '105.112.106.25, 141.101.99.109', 2),
(611, 'flavio cidonio', 'f.cidonio@gmail.com', '$2y$10$w/JhPrSAbSUBvQH2QXQlHeTf02BOuH.LzmLDONuz1x0GxDs4JOADi', NULL, 'Telegram', '(+39)3479066046', 1, 0, 1, '9mc3sVJsnut89ALL4vDaymn5EsqEpfK9u4exaUUfBUlh5zwr5bIigumFIN7W', '2018-03-24 19:43:23', '2018-11-07 13:38:28', NULL, 0, '188.152.174.60, 188.114.103.61', 1),
(612, 'rajesh', 'sagar13rahul@gmail.com', '$2y$10$uhya8o.rgxdC0kZD7DUk5.saN40D92nwkq2BD6BeCN/mI8PAY/c56', NULL, 'Telegram', '+971506462757', 1, 0, 1, NULL, '2018-03-26 03:07:53', '2018-11-07 13:38:28', NULL, 0, '109.177.207.175, 141.101.99.187', NULL),
(613, 'sha', 'crazykista@gmail.com', '$2y$10$lLuum9I.7kWRk7QuwpCzF.dGIW1AP81BI2PttbApVylHu3ONhw4oS', NULL, 'Telegram', '+918754256678', 1, 0, 1, NULL, '2018-03-26 22:25:21', '2018-11-07 13:38:28', NULL, 0, '2405:204:7103:7c7f:1b28:d5da:5a1c:567d, 172.69.134.239', NULL),
(614, 'Sahed Khan', 'mahmud2008@yahoo.com', '$2y$10$rB12XoZMA3oXegfjVKyYD.EBM7cjfEHEinuACjcdshBn0XWgDkBk.', NULL, 'Telegram', '01712517366', 1, 0, 1, 'O9LEbKXvAZU9D9bG6NZXU6o8ri8ycjgB4dKNNZo9vTAM8s6JubFZZMcxx2PF', '2018-04-03 16:32:38', '2018-11-07 13:38:28', NULL, 0, '103.101.253.1, 162.158.179.246', 2),
(615, 'Elber Cervantes López', 'elberacl@gmail.com', '$2y$10$2jU.tdPV/rVVUCTxZC.UCeGxpwDxmiTFllErR/rBXAmvdwKV0.YJ2', NULL, 'Telegram', '3215891272', 1, 0, 1, NULL, '2018-04-08 19:20:48', '2018-11-07 13:38:28', NULL, 0, '191.102.222.158, 162.158.122.62', NULL),
(617, 'Rowela', 'jasheweng@gmail.com', '$2y$10$3I3oASfaglfUbmQ535TJ2u..iM/JQBKsyTSzW3sWGra8UGFUGwjwu', NULL, 'Telegram', '(+63)9103425423', 1, 0, 1, 'wCB6PN7Y93gb7Zv9AaitI3zbUdUgos8ozv65MSH6NkSlIdCOfWAoteRE8djK', '2018-04-15 19:54:12', '2019-05-20 08:56:20', NULL, 0, '167.99.79.1, 162.158.118.18', 5),
(618, 'Don', 'cornerstoneanalysis.emp@gmail.com', '$2y$10$BJn6JqJ3QfMyvWIk3F1/4u9vHgxp//d76Rhna/SOwDOPul.4fojla', NULL, 'Telegram', '+2347082376112', 1, 0, 1, NULL, '2018-04-16 18:34:07', '2018-11-07 13:38:28', NULL, 0, '105.112.35.158, 162.158.154.231', NULL),
(619, 'Rohaizad', 'tawakallah24434@gmail.com', '$2y$10$nhWfhPcaKf/t6TbyXFX9jOFBxl9Tla53Z5GOk2Nu5hAcX6tnfL3aC', NULL, 'Telegram', '(+65)84531284', 1, 0, 1, NULL, '2018-04-19 10:21:30', '2018-11-07 13:38:28', NULL, 0, '111.65.39.53, 172.68.254.110', NULL),
(620, 'Scott', 'scotts.ops2@gmail.com', '$2y$10$FeFSa3X2OsdMDvSKg2HnWu8BGN2DSP7pAnZKTgWL0UCufcQugypVO', NULL, 'Telegram', '+1 458-458-5658', 1, 0, 1, NULL, '2018-04-26 17:28:45', '2018-11-07 13:38:28', NULL, 0, '99.101.68.33, 108.162.238.79', NULL),
(627, 'Renee', 'blackhawkrene@gmail.com', '$2y$10$0UGGPRMJPyxPqHbVrA0p7OjuGDY0Lmc3Uy9nSmPw17WObNFJQHCKq', NULL, 'Telegram', '0535082486', 1, 0, 1, NULL, '2018-05-13 18:48:53', '2018-11-07 13:38:28', NULL, 0, '5.41.94.220, 172.69.54.65', NULL),
(628, 'bando', 'bandolerfx@gmail.com', '$2y$10$Stw0HRG5SARFGPz.akwiyOQoAHB1TSjnttm3IomL2RJjV1TjYUpfm', NULL, 'Telegram', '34615250754', 1, 0, 1, NULL, '2018-05-14 01:32:36', '2018-11-07 13:38:28', NULL, 0, '62.57.81.93, 172.68.94.222', NULL),
(629, 'Joao', 'joaocarlosbhbr@gmail.com', '$2y$10$ex4bVMKIjzHU8vdlnH2RF.jREj1Akv46yHrJeqXEBt/.AQP0koqjq', NULL, 'Telegram', '+5531984113738', 1, 0, 1, 'BvwEPxIGpE8m0t91NaqY1sctW4wHaXt3efcG8v9a9Z0v65AGo7OI6MrQUQDi', '2018-05-17 03:27:20', '2018-11-07 13:38:28', NULL, 0, '201.80.104.60, 172.68.26.193', 2),
(631, 'Olawepo Oluwole', 'olawepo97@yahoo.com', '$2y$10$eAXsEhDSJiDDNZLPKCHItOwM3qx8GLrFkCSzDVLTiIJe00l4Q9Sga', NULL, 'Telegram', '23480485447', 1, 0, 1, NULL, '2018-05-23 12:40:53', '2018-11-07 13:38:28', NULL, 0, '41.58.249.111, 172.68.42.18', NULL),
(632, 'VINAYA MATHEWS', 'vinsmile@gmail.com', '$2y$10$iLDOGqhiQ/k2pD7DN.pUpOV4wj9EpjFfYNWqHD6U5BjxxyL6WPolG', NULL, 'Telegram', '+16475254526', 1, 0, 1, 'Hx5s5nn2iTQ4s9WcCNIsar7Y1CvJ8b8YjRlXPzKjeNkwWvLnfnMkqrA8YEUu', '2018-05-28 18:22:40', '2019-04-06 17:42:51', NULL, 0, '142.240.254.9, 108.162.241.156', 5),
(635, 'sandra lee suan ying', 'sandrayg86@hotmail.com', '$2y$10$1bEJ8B9qYuUnsL.rqpJdqeVNUbaJtej02KnOYI4JaYD1iy3Yuvw6W', NULL, 'Telegram', '+60174078716', 1, 0, 1, 'MUoE6Zb7LTxlTt9gHZCxLVHQMV3Y8l1yb2gnBhWLd0sJcKozOPc78emaY1Ld', '2018-06-08 22:49:50', '2018-11-07 13:38:28', NULL, 0, '123.136.107.190, 172.69.134.161', NULL),
(636, 'Brian Jones', 'brian@globalteleconsl.com', '$2y$10$/q0DN2vnlSf8U6TUT.KQk.HStjmh4vc7CSQIy/b6aKqCBf.yR8TxW', NULL, 'Telegram', '07899860104', 1, 0, 1, NULL, '2018-06-15 17:40:45', '2018-11-07 13:38:28', NULL, 0, '2.97.153.18, 162.158.154.75', NULL),
(638, 'Daryl shute', 'daryl.shute1@gmail.com', '$2y$10$svQojQ1fS8Ty4/ARqGMTfO61Y6koMHM7x4BmUoM0bXoYlQMLzL2mG', NULL, 'Telegram', '+61416040885', 1, 0, 1, NULL, '2018-06-16 06:36:51', '2018-11-07 13:38:28', NULL, 0, '49.180.157.171, 172.68.146.76', NULL),
(641, 'Peter barnard', 'peterjbarn@hotmail.com', '$2y$10$jdAbLQr6cPWFPy3s4gPbs.eee.DWFEySzibpU6nYSSqLnELhSW16G', NULL, 'Telegram', '6407519306', 1, 0, 1, NULL, '2018-06-22 06:29:40', '2018-11-07 13:38:28', NULL, 0, '120.21.35.50, 162.158.2.132', NULL),
(644, 'Jason Y Gabato', 'jasongabato29@gmail.com', '$2y$10$ugN7X.IF8ixNLcU3/NaeMeGeipuHTKhGNuICoSONgxQKSOjgMMUWu', NULL, 'Telegram', '(+63)9489902415', 1, 0, 1, NULL, '2018-06-24 10:32:39', '2018-11-07 13:38:28', NULL, 0, '49.146.0.20, 172.68.142.163', NULL),
(645, 'Avraham Cohen', 'achiyac@gmail.com', '$2y$10$rz7syG53E9qVp7Ft1AnBDOkOV.JWN5thnnXBbCtmgCcxWgvqqFRJW', NULL, 'Telegram', '972-50-6203576', 1, 0, 1, 'lkrTxnwjXwUswCN9fIdALhVx4CazUPl0MKneWaiPM3FkSxGlIdOYF9j1pE97', '2018-07-04 11:29:42', '2018-11-07 13:38:28', NULL, 0, '212.25.84.200, 172.69.130.12', 3),
(647, 'James', 'jamesdgreatfx@gmail.com', '$2y$10$ZOiMQurN0p9vg3hpHWhDM.EiQyVQaVZqs/ABkFD5F3G4S5ioistim', NULL, 'Telegram', '+2348039131947', 1, 0, 1, NULL, '2018-07-13 00:17:59', '2018-11-07 13:38:28', NULL, 0, '105.112.36.48, 162.158.154.165', NULL),
(649, 'sizwe', 'mtskhoza@gmail.com', '$2y$10$Zzocw7FEi.YwrSm3924uk.CvUj7q6IhoWBMK0zSKD.gL7EqsgtLUe', NULL, 'Telegram', '+27645326245', 1, 0, 1, NULL, '2018-07-23 14:39:21', '2018-11-07 13:38:28', NULL, 0, '197.89.42.92, 172.69.114.48', NULL),
(650, 'Donald King', 'donaldking247@gmail.com', '$2y$10$RASqI9fccTUET4JeF6j0W.MwIk2BppTsy8J4dSouNDq1.GwyMERkm', NULL, 'Telegram', '+447446898007', 1, 0, 1, NULL, '2018-07-30 19:51:39', '2018-11-07 13:38:28', NULL, 0, '82.129.117.146, 141.101.99.91', NULL),
(652, 'Gheorghe Romac', 'gzromac84@gmail.com', '$2y$10$CPYpEOijUKbcfLveKhFdFOOvJFWb8Uj0abdpWFS1v78ryliIygtQm', NULL, 'Telegram', '+61473042949', 1, 0, 1, 'ZdsGsme2vKCe17bw49dbDfTQqh66h7lmBxWb7mb5SEHP0CCCZYj7NMSdehnw', '2018-08-16 12:47:53', '2018-12-07 09:43:14', NULL, 0, '123.243.223.210, 162.158.178.233', 5),
(653, 'Saleh Almihaimeed', 'saleh_almi@hotmail.com', '$2y$10$pYQc/jyuK2xHXGovQqXXXeL1KI1qkLFWZ9QlBccdQN/ntz3UW5efS', NULL, 'Telegram', '+19025492421', 1, 0, 1, 'Rf8ULOkrS8apMF7fNxh48nDsTpTLjCtx6q68iXVwLcXsJzWHuaFSgl4IvIg7', '2018-08-17 07:46:49', '2019-02-26 10:30:08', NULL, 0, '156.57.249.162, 162.158.63.102', 2),
(654, 'Barnabas Umelo', 'umeloba@yahoo.com', '$2y$10$e2txlGY2cEnoipNsKft80Ocw0TxxkTg5zA7A5ioMSQ1cMVhX/FVqy', NULL, 'Telegram', '+2348036024619', 1, 0, 1, NULL, '2018-08-19 01:28:28', '2018-11-07 13:38:28', NULL, 0, '41.190.3.244, 141.101.107.68', NULL),
(655, 'Asad', 'anisfx19@gmail.com', '$2y$10$E3YeZxH6Jl0PKTXhDVjG0OupFxNQM4zd1IPeYbMiSn1tYZPQd15wy', NULL, 'Telegram', '+916266571382', 1, 0, 1, 'zFSaaOeH5IvO3ac4dUIsnBhjIn80oGEzKwkouPEDdC5BHaZXmddu5aN5MuYK', '2018-08-21 11:36:52', '2018-11-07 13:38:28', NULL, 0, '2405:204:e68e:d060:d987:237b:72eb:fce, 162.158.22.222', 1),
(656, 'miller xie', 'miller@theaspaces.com', '$2y$10$DX18.CExCBDCOln3yXy6m.zxGqQHWYMozY8.mMS5tN7./RItVubVC', NULL, 'Telegram', '13316990406', 1, 0, 1, NULL, '2018-08-23 13:32:28', '2018-11-07 13:38:28', NULL, 0, '61.216.166.153, 162.158.58.197', NULL),
(657, 'Ian', 'sroberts_1975@outlook.com', '$2y$10$mSm9M/3w0uRSZUReHIMYdOG3vPvbmchQEC55LNNKWh1zG8u2P3yxW', NULL, 'Telegram', '07474050626', 1, 0, 1, 'rZ86GfOL6PS3LeUZncCcfOjXMW1KJ4YHJIdb6Z6Hv8VgEMWLYjQuLwTaPtKS', '2018-08-23 20:58:35', '2018-11-13 03:37:07', NULL, 0, '81.102.109.237, 162.158.34.60', 1),
(658, 'Nkemerwa Kairuki', 'nkemerwa2018@gmail.com', '$2y$10$Vzg.lZAertxj0h14YAnz0uV14x9Xwewdg57Dtem16K/nsmfMc.1NO', NULL, 'Telegram', '+255758310314', 1, 0, 1, NULL, '2018-08-25 19:03:15', '2018-11-07 13:38:28', NULL, 0, '41.222.180.61, 197.234.242.24', NULL),
(659, 'Ogunmoroti Ebidapo', 'ayomaurice@gmail.com', '$2y$10$a1BKIM21/yRlmvog8g3UQeXD89XExdzVI4h5GCN5sifkmHZmTgMTG', NULL, 'Telegram', '+2348032386905', 1, 0, 1, 'pSnW7nm1NaFKJJmR10homFIPUxCkV5tWfAujRlMFk43JZ1uAAmqpoS58Tri8', '2018-08-25 22:57:06', '2018-11-07 13:38:28', NULL, 0, '105.112.35.217, 141.101.98.126', 1),
(660, 'Ulugbek', 'ulugbekmamatqulovuz@gmail.com', '$2y$10$MINPBOT2YyG5nVllBwXQXucjwTI7D4Qpfd8xZVMldSpH0aW3tjxRG', NULL, 'Telegram', '+998903208876', 1, 0, 1, NULL, '2018-08-27 07:30:28', '2018-11-07 13:38:28', NULL, 0, '37.110.215.225, 141.101.104.187', NULL),
(661, 'Mohammad Farid Uddin', 'mohammadfariduddin38@gmail.com', '$2y$10$it0ULD/akwRQV2vVoLDCI.K9T8qpSmK9KXEiL4vhIsVqV2p3GQB7W', NULL, 'Telegram', '8801915220180', 1, 0, 1, 'paExyVOR7xHTQneyTDcLiIBnkb53izzg2HAu0OzdgZr4kDpJSU5g7CqkuIId', '2018-08-27 10:10:53', '2019-01-21 09:40:58', NULL, 0, '119.10.175.115, 162.158.165.18', 1),
(662, 'Samuel', 'samuebong79@gmail.com', '$2y$10$8hFogDD7dfA2QO6S.rrZveSOx5/QqTVV9whuD90QqXtETa.E05GB.', NULL, 'Telegram', '+375297187616', 1, 0, 1, NULL, '2018-08-27 15:57:19', '2018-11-07 13:38:28', NULL, 0, '37.212.117.11, 172.68.245.21', NULL),
(663, 'Baljit', 'pandher_bill@hotmail.com', '$2y$10$J4TBsgAfruTPN4I1xRq89uT4rPl3BkSDNJj9zVjFDGxYkjPcZntxq', NULL, 'Telegram', '4169532857', 1, 0, 1, NULL, '2018-08-27 16:57:50', '2018-11-07 13:38:28', NULL, 0, '2605:8d80:601:c5a8:4dad:d47c:90e8:424d, 108.162.241.180', NULL),
(664, 'MD Sani', 'sanivs9@fmail.com', '$2y$10$GLVXLVv0r8rYSZKVYn4HO.qfJIPlWUWOzeCq179lmIOwW7lnA0QUS', NULL, 'Telegram', '+8801724636224', 1, 0, 1, NULL, '2018-08-27 19:01:01', '2018-11-07 13:38:28', NULL, 0, '119.30.32.26, 162.158.165.18', NULL),
(665, 'chowong', 'c.haowei@yahoo.com', '$2y$10$YMQdDBRp/kJHXmxdifuCWuTsQ1rUuqDo0PZe..BX.QgPmCgbyqa9y', NULL, 'Telegram', '(+60)187863512', 1, 0, 1, NULL, '2018-08-27 20:21:37', '2018-11-07 13:38:28', NULL, 0, '115.135.202.223, 162.158.167.86', 1),
(666, 'Manoj', 'manoj.randika15@gmail.com', '$2y$10$y41X4hQzR55cbsSOE.QzRe10HTjRQkpBfuiylxL0NsSFOWFsXPpEa', NULL, 'Telegram', '+94769131018', 1, 0, 1, NULL, '2018-08-29 14:49:32', '2018-11-07 13:38:28', NULL, 0, '175.157.70.153, 172.68.202.18', NULL),
(667, 'Pratik', 'pratikkalra38@gmail.com', '$2y$10$EwLvb9ORYjgnTYtWgHFWtOWfdVHsKRtxs8hGMmucUHNxNbL.CBhty', NULL, 'Telegram', '08602785725', 1, 0, 1, NULL, '2018-08-30 06:02:56', '2018-11-07 13:38:28', NULL, 0, '2401:4900:16d4:d92:f600:d343:705d:e083, 162.158.50.114', 1),
(668, 'Tony', 'tonykago@gmail.com', '$2y$10$wIZFeAnT.uJEiCbpkThKYujzNrqeqS39.jfXnE4o8bRpZCmpLHINO', NULL, 'Telegram', '+254735305464', 1, 0, 1, NULL, '2018-08-30 13:38:19', '2018-11-07 13:38:28', NULL, 0, '41.76.172.3, 162.158.42.24', NULL),
(669, 'Khurshid Alam', 'designengineer1977@gmail.com', '$2y$10$HnSH4MYGpa89OQX0yrL66uHdylnCiEFpdZGL8iKd/XIK27aln4Wy6', NULL, 'Telegram', '00923310303569', 1, 0, 1, 'TIxtdTcZt74p49RO3hLFt23E9E2aemWvnK1WAksn8sJ2iL6UZo58y5e7cTGx', '2018-08-30 14:11:48', '2018-11-07 13:38:28', NULL, 0, '103.255.7.19, 162.158.167.44', 1),
(670, 'ABDUL RAHMAN', 'amansime@gmail.com', '$2y$10$zX2EB7/Vz93ZMYX2vdv6EO8AaHb.fo4DCqq159z3xA9GlsgbKouGe', NULL, 'Telegram', '+60133333452', 1, 0, 1, 'pvVAuXPdaG6nQWKHO9jLWVVFueICNz91fLkz4CMvJA67Z2AXUbMWUtybMc5M', '2018-08-30 20:59:42', '2018-11-07 13:38:28', NULL, 0, '203.82.79.239, 162.158.178.197', 1),
(672, 'Emmanuel Agbekey', 'mr.agbekey@gmail.com', '$2y$10$Tfcs0V.9HXdOIDTWmxbGH.qrJBFs/ZfPvyaCtwqpPvMxf2hw78ZsK', NULL, 'Telegram', '+233233111333', 1, 0, 1, NULL, '2018-08-31 12:24:34', '2018-11-07 13:38:28', NULL, 0, '41.204.44.87, 162.158.155.226', 1),
(673, 'Petar Kostov', 'petur.kostov@gmail.com', '$2y$10$ifD5sVIjaEp./WxyDDzLE.S0ajUVZcCiyq./CS117ADeWyqz2S3ii', NULL, 'Telegram', '07744006024', 1, 0, 1, 'imw1WADoh9cuS5Ob0ahwBHxoV4UL5SldzolnmFd9HWj6lCqrCFI7cUb4bXHg', '2018-08-31 13:13:21', '2018-11-07 13:38:28', NULL, 0, '81.159.140.216, 162.158.155.160', NULL),
(674, 'John Cole', 'surferxix@yahoo.com', '$2y$10$/Z8OBHagP899znefoCButezTRQjgoU/o7tNHT3WnS2zx1wHwvFLJG', NULL, 'Telegram', '00447930523182', 1, 0, 1, 'UzYtkcmAkP1dGchxVvn09hdIWERqmacxTIvTIoMzw2X5igqhXIroP9VPIARF', '2018-09-01 16:30:39', '2018-11-07 13:38:28', NULL, 0, '88.111.79.114, 162.158.154.15', 1),
(675, 'Tham Vasudevan', 'thamsep19@gmail.com', '$2y$10$UKwM1djGYLeKwPVIyhabh.xnza3Sco0h8UKmHDZ5ef70y71uvxeXu', NULL, 'Telegram', '14166605252', 1, 0, 1, 'PIrdyND8n4209xdET0nkQ4ImHmVpioqEWtzw10yelEhUurbIlSFpMc2hdDrb', '2018-09-01 19:44:58', '2018-11-07 13:38:28', NULL, 0, '2607:fea8:c60:51d:4972:ef3e:a113:1bc, 108.162.241.180', 2),
(676, 'victor edet', 'preciousvictor2003@yahoo.com', '$2y$10$1tE89tynn2JSZRzQ8EXSyeFs1Em1xXhjGsTUtN2u4kGYiP0UWw9z.', NULL, 'Telegram', '+27732825929', 1, 0, 1, NULL, '2018-09-02 19:36:53', '2018-11-07 13:38:28', NULL, 0, '105.225.94.237, 197.234.242.30', NULL),
(677, 'sajid', 'msajidilyas@gmail.com', '$2y$10$K4WKtCsqPZ3D8hb4hMH7BOQR43A4hjmumG5/N7lJYqy1msOdsBVLK', NULL, 'Telegram', '92 0348611259', 1, 0, 1, 'On2U1iZxo41rDz8Km9h8Ij0EEyOP39NAfrMJ2dLi9wOJSPHMJp2H3D4oPak9', '2018-09-02 22:38:02', '2018-11-07 13:38:28', NULL, 0, '110.39.174.106, 162.158.166.115', 1),
(678, 'John', 'jthomasann@gmail.com', '$2y$10$vbkfjcVIOQf8w0X0QRJjp.GMl/k9m5Kj.3eZCUUp9jCENazxtVrXW', NULL, 'Telegram', '6479881637', 1, 0, 1, NULL, '2018-09-03 06:24:50', '2018-11-07 13:38:28', NULL, 0, '2607:fea8:e1f:fb3d:c0d4:54b7:fb2c:6a70, 108.162.241.132', 1),
(679, 'Anis', 'ladly_asad@rediffmail.com', '$2y$10$6vEyMaMGO.R9A7y5DKkxxOqr0B6XKuJcNZMnM94e40Z2MM0yivq0K', NULL, 'Telegram', '7354268308', 1, 0, 1, NULL, '2018-09-03 14:57:24', '2018-11-07 13:38:28', NULL, 0, '2405:204:e50b:839f:c088:d142:1b34:1c5, 162.158.22.132', NULL),
(680, 'Shittu', 'shitta.kola@yahoo.com', '$2y$10$//mmlng53ezi6dWeNhAnGOaZDnw1VBGC/s3XS3ThiUSXpU3nHMir.', NULL, 'Telegram', '08026251776', 1, 0, 1, 'oIZiTcOW3w3vyZG91oCkfzHLACDGHw19emIgPDChcHgJ3HiFh8Uro4YFO4Bs', '2018-09-03 20:08:37', '2018-11-07 13:38:28', NULL, 0, '105.112.23.166, 162.158.154.99', 1),
(681, 'kunten', 'kuntenfx@gmail.com', '$2y$10$CTHgPGwnDgDXsUh9td3bq.cgUgWT.5H8ZutXBFLmVKZDKQQsojE/2', NULL, 'Telegram', '+855885678899', 1, 0, 1, NULL, '2018-09-05 17:56:18', '2018-11-07 13:38:28', NULL, 0, '36.37.185.67, 172.69.134.71', NULL),
(682, 'xuxianyang', '380364998@qq.com', '$2y$10$TWBCAC7ucoXTosjw69J0YuR9KVOCQHhykU4Y3/xOR9IXrS5RUQHrW', NULL, 'Telegram', '+8615005876907', 1, 0, 1, NULL, '2018-09-05 18:14:45', '2018-11-07 13:38:28', NULL, 0, '112.10.208.99, 162.158.167.236', NULL),
(683, 'Jabed', 'mjavedh@yahoo.com', '$2y$10$M0zsmFglSdmaMc/hIqeuDOVawPN/ACJBbi6WxwlrXSWolkrGIYvqa', NULL, 'Telegram', '+8801711229800', 1, 0, 1, 'oWRsHUcbysrKomtZU4xcEn256UToZePAUG4WuiA0R9Ig3JB6QxvgrKlONkyo', '2018-09-05 18:17:03', '2018-11-07 13:38:28', NULL, 0, '59.152.97.118, 162.158.167.26', 1),
(684, 'Rashedul', 'rashedulgazipur123@gmail.com', '$2y$10$jYzMCeOUVFp5ow0OQlhpQe1Lp20uEXdDncjUFEWlxFCtaVIZhmgAG', NULL, 'Telegram', '+8801681902802', 1, 0, 1, NULL, '2018-09-05 18:26:06', '2018-11-07 13:38:28', NULL, 0, '45.127.51.178, 162.158.166.115', NULL),
(685, 'emma', 'bobjolly01@gmail.com', '$2y$10$uZcMP3V.35mtRepG.n.SsuHM6RvTEEfdOWmwtGYfYMs88yhD9SVVi', NULL, 'Telegram', '+2347032677308', 1, 0, 1, 'HyUBubT4P7OhLwJHrTIIWmxNeKGCFmxlK1Sm1O2szp6v3ZdYZfSfJ1ihbxNx', '2018-09-05 19:33:59', '2018-11-07 13:38:28', NULL, 0, '105.112.121.92, 162.158.155.226', 1),
(686, 'Md Maksudur Rahman', 'mrkfordnet@gmail.com', '$2y$10$8u3gDm.sZ7PRmU5d.Z4uje/aqiGBICThbDTodvJietq46GStpmeiC', NULL, 'Telegram', '+8801756414829', 1, 0, 1, 'TPSqoaHnJRBeLqgg8qrNqQX5SLUGNXUtQnbB1thPiFxuDSrxsgmll4XZv4Yz', '2018-09-05 21:00:44', '2019-05-22 12:17:35', NULL, 0, '37.111.205.94, 162.158.165.54', 1),
(687, 'Thoun Kat', 'fourmerge@hotmail.com', '$2y$10$9g7H6rAMV1d0AbR4.dN/..rbuWyLK6rafDuygUDdcWEq99yQ/3o1O', NULL, 'Telegram', '66816714363', 1, 0, 1, NULL, '2018-09-05 22:33:01', '2018-11-07 13:38:28', NULL, 0, '180.183.147.137, 172.68.6.18', NULL),
(688, 'Ulrich', 'bigstackbully@hotmail.com', '$2y$10$1LwXwg4FdORXxNSWufDX5.djbuIvnmLV7gvh5TptMu6xlj.T47YS6', NULL, 'Telegram', '(+49)1639837110', 1, 0, 1, NULL, '2018-09-06 17:02:37', '2018-11-07 13:38:28', NULL, 0, '217.234.84.83, 172.68.50.192', NULL),
(689, 'Brown Ubom', 'revbrownubom@gmail.com', '$2y$10$5Ge2jPkbu5lMtKRadYXuNOCk86Yis/rMA.iL4PsGhIkDBuJKWsvvy', NULL, 'Telegram', '+2348037135543', 1, 0, 1, 'whHR60xVxI95hy1G6tgT6EL5oWXm3sCNGycdZTlC0S3UtPlO0e36Neegaukt', '2018-09-06 22:29:24', '2018-11-07 13:38:28', NULL, 0, '41.190.30.220, 162.158.154.81', 1),
(690, 'Ang Yin Chong', 'yinchong.ang@gmail.com', '$2y$10$lU0NJEmUN6aUNr2Kjb1lc.FcK.MoSyd0MjWxiNwcT97y2tcQfnAgO', NULL, 'Telegram', '(+65)97127003', 1, 0, 1, NULL, '2018-09-07 14:27:18', '2018-11-07 13:38:28', NULL, 0, '118.201.94.2, 172.68.146.142', 1),
(691, 'Marty', 'martymane@gmail.com', '$2y$10$TpGQ9DECzehLMLsKwjKocujQ6DLedEQHc7bB57yhzc0Cy.Ns7c0w6', NULL, 'Telegram', '00353860449127', 1, 0, 1, '1pNd7gC3iBuUEST2NruAoQRsFcp987BbL96LUdtw0IXipR5GFCtmsxx1b4HJ', '2018-09-08 02:40:06', '2019-03-04 23:42:38', NULL, 0, '109.255.62.213, 162.158.38.234', 1),
(692, 'Abubakar', 'abubakar.bajwah@gmail.com', '$2y$10$igIklOF6o9H.kJ8CPnnRcuy..iL8.S6Ml6Rs74SPMI3XAOtklwBQW', NULL, 'Telegram', '13477127039', 1, 0, 1, NULL, '2018-09-08 06:01:14', '2018-11-07 13:38:28', NULL, 0, '2607:fb90:2465:77ff:6cd6:5f39:4dfc:d1d0, 162.158.62.209', 1),
(693, 'Redgy Pypops', 'redgy.pypops@gmail.com', '$2y$10$tYeRg0EAlZXQb8bforRSzecueLwF6Fz/NDg8NgPhriu93TCxDuxyK', NULL, 'Telegram', '+32491919280', 1, 0, 1, 'ucBTkLZ8TcBoy2DOrWv8rvEI7auNQOGKrMhj9L5O1UJvhXBGvXKwyp6wPUs2', '2018-09-09 14:36:26', '2018-11-07 13:38:28', NULL, 0, '84.195.104.175, 162.158.234.6', 1),
(694, 'Greg', 'pikegreg@yahoo.ca', '$2y$10$uadGNJUDKOF0osFbv6HnaurS9OZsoi5dcBbcX00m4EloPS0KhelA2', NULL, 'Telegram', '7808044451', 1, 0, 1, 'Xp3NvgYNiaA5keCq4zfcYvvkUtN29Lt3SnFJugyk0WS1dXoXrubD3aaB1gSs', '2018-09-09 18:27:22', '2019-03-09 21:23:09', NULL, 0, '194.59.251.26, 162.158.62.23', 1),
(695, 'Tiamiyu Samuel', 'tiamiyusamuel@protonmail.com', '$2y$10$AT7LAwwmhoGihfOhvXRmw.URXWVwuL92ApaYIk/HZlvUvm8aDgViq', NULL, 'Telegram', '+2348057015253', 1, 0, 1, 'DPesoGGdXdMP3QHWBJTT4YGxs3T0NffMjf1VmHYLOH5ViFrDW2HifHoRMgIj', '2018-09-09 23:09:33', '2018-11-07 13:38:28', NULL, 0, '197.210.64.22, 172.69.54.53', 1),
(696, 'oud', 'oud_you@outlook.com', '$2y$10$ykAQDONJCiVQI2mV./UuxeTDyhZZnF62a4FkaivdZbbxCMfrMS0bi', NULL, 'Telegram', '+60173121767', 1, 0, 1, 'jh4lsm2tIwkq2JsxvabcDZ0X3thgPMnNzX64yPWMhCCjSCvyLqKJvq7FzRwQ', '2018-09-10 15:10:52', '2018-11-07 13:38:28', NULL, 0, '118.100.23.47, 162.158.166.121', 2),
(697, 'Arnold', 'arnold.marowa@gmail.com', '$2y$10$C1QqdoR5196MSW8uW0yLaeNC8THDTcGUVIvBeHvdjOaJXkgrIW3.S', NULL, 'Telegram', '+263773221973', 1, 0, 1, 'AlIcmZp4YNSzgJ3wpRfktriCG5LdUgM5xGUUYIEsKmua01xNvYTMtfc4eXwd', '2018-09-17 13:16:46', '2018-11-07 13:38:28', NULL, 0, '197.221.254.234, 197.234.242.24', 1),
(698, 'Pius ngira', 'ngira3@gmail.com', '$2y$10$l5K6IZ7JCJwSeB8qzHyNIOcTWuRMcOPMewgwIkA40mZkEPEx60jke', NULL, 'Telegram', '(+255)765534048', 1, 0, 1, NULL, '2018-09-18 19:14:47', '2018-11-07 13:38:28', NULL, 0, '41.222.179.189, 197.234.242.66', NULL),
(699, 'Ismail Ismail', 'Ismata8989@gmail.com', '$2y$10$vj2jMWZ2dd3yFrGZTGToVex6hMKmmTSDLpAdl90mdPyGLdYpDL5eW', NULL, 'Telegram', '(+45)91628924', 1, 0, 1, 'YMQ27kdj5FaxgLiPR6CtNDImpN2Y0DIIITMk3QjzHdzTjIzbqKaj2uCKCSCM', '2018-09-19 03:23:24', '2018-11-07 13:38:28', NULL, 0, '185.13.106.106, 162.158.155.22', 2),
(700, 'Umair', 'uaimpex257@gmail.com', '$2y$10$r0gDIVdRx8WlkUJG4GOu9Ok3G.R8JMsOvCb4QZV2v6V9jZxlcQpqi', NULL, 'Telegram', '00923457778794', 1, 0, 1, NULL, '2018-09-19 15:57:00', '2018-11-07 13:38:28', NULL, 0, '103.255.5.57, 162.158.167.104', NULL),
(702, 'Walera', 'valerijsablin55@gmail.com', '$2y$10$Zio7VLLjygtMi1Q4Sak18.8zhjr.8HvKOvnxnaPOcUp4FO7rJWcT2', NULL, 'Telegram', '+79370250334', 1, 0, 1, 'isfHqinp8uJTOE9ZMqFLkJk61H0KNigEiuSebEAecrQh4RX9nyW0kDeKM1m2', '2018-09-24 12:59:39', '2018-11-07 13:38:28', NULL, 0, '95.84.15.109, 162.158.93.71', 1),
(703, 'Robert Matthews', 'matthewsjacquese@yahoo.com', '$2y$10$wVSgxSQVkKOp/YrrocUPW.VCiOCOXXAr7sKovmg4VAyUy.7OAmopC', NULL, 'Telegram', '2564178866', 1, 0, 1, NULL, '2018-09-24 22:29:58', '2018-11-07 13:38:28', NULL, 0, '2600:1700:5fb0:e550:fd72:ee65:ab33:a613, 108.162.237.168', NULL),
(704, 'Alex', 'idruid021@gmail.com', '$2y$10$ImoFYQuzaElyoyQpFKrpcu.wokqA9MSQBOTxl.etd8Mq88vBs8PGu', NULL, 'Telegram', '+381695597911', 1, 0, 1, NULL, '2018-09-26 01:27:04', '2018-11-07 13:38:28', NULL, 0, '188.2.102.119, 172.68.154.6', NULL),
(705, 'Antonio', 'afernd@hotmail.com', '$2y$10$JgJsx2q.JfYaJTr8eeNvYuVU0ZKduQZiaVfnvagYlH/gRoqomIag2', NULL, 'Telegram', '352325812154', 1, 0, 1, 'ZN6FRgAiyUG2wWVRrPi5M59MVimozOBCtM8vF14kf9PWV0e6FW5Ma09esHrm', '2018-09-26 14:39:41', '2018-11-07 13:38:28', NULL, 0, '89.109.65.31, 162.158.154.81', 1),
(706, 'Irfan amir', 'ranairfan620620@gmail.com', '$2y$10$M2MYDpLbUy3xPRzZijw7MOWJqamhFkEryccpkK/bx25lpOrnAMs3a', NULL, 'Telegram', '00923132330280', 1, 0, 1, NULL, '2018-09-28 10:48:08', '2018-11-07 13:38:28', NULL, 0, '119.160.118.39, 162.158.167.26', NULL);
INSERT INTO `users` (`id`, `name`, `email`, `password`, `avatar`, `signal_option`, `phone`, `user_type`, `verified`, `active`, `remember_token`, `created_at`, `updated_at`, `role`, `notify`, `ip_address`, `first_plan`) VALUES
(707, 'Bassam Daoud', 'bdaoud@ymail.com', '$2y$10$WacGmRLk7m1JilUcUPjrh.pDA2HhCQv6TyTjM/ttebQv/mn9omy6S', NULL, 'Telegram', '+971503477210', 1, 0, 1, NULL, '2018-09-28 20:04:15', '2018-11-07 13:38:28', NULL, 0, '94.204.160.129, 162.158.158.47', NULL),
(708, 'sabah', 'sabahsardar2022@gmail.com', '$2y$10$O30jjN9hEgEC1klWD7r6OO5M7/LuNygPzmUM4wSDrKR4jMov5F4Py', NULL, 'Telegram', '9647504910198', 1, 0, 1, NULL, '2018-09-29 23:46:14', '2018-11-07 13:38:28', NULL, 0, '185.106.28.121, 162.158.92.18', NULL),
(709, 'Sunny', 'nombusosunnymtambo@gmail.com', '$2y$10$5enETnDOyuERzE6uXuvxL.AVGf9GVfK/svpe0AWRuxq3rtp1uFsVi', NULL, 'Telegram', '0671716415', 1, 0, 1, NULL, '2018-10-04 00:37:10', '2018-11-07 13:38:28', NULL, 0, '165.144.148.164, 172.68.186.12', 1),
(710, 'tt', 'thelact@telkom.co.za', '$2y$10$b8XM2ppJkQ2EtycMCnRBqeJgERxAiRPACvjSJEbEpUXaqAeMeu2E2', NULL, 'Telegram', '(+27)814012269', 1, 0, 1, NULL, '2018-10-07 21:59:40', '2018-11-07 13:38:28', NULL, 0, '102.250.125.233, 197.234.242.180', NULL),
(711, 'oyourou', 'oleroc12@gmail.com', '$2y$10$bxafzqaChJIvptAyAstj2eLoX/q4gr350oP1xQQoyHpxDggQ9gnku', NULL, 'Telegram', '+22509804505', 1, 0, 1, '15OR8yXDnKYONkeHpxbAZjrFF3rexsslIJzKItqtuwTWjUdalaDkhEbg2K1d', '2018-10-08 15:49:49', '2019-05-09 23:29:31', NULL, 0, '154.68.5.111, 172.68.94.108', 2),
(712, 'Chinedu D Ilobinso', 'cilobinso@gmail.com', '$2y$10$J4bRHRacX5Ilr1MGGY44Y.YmrMR0ytE0/9m5Nb4pDblGh6s5.gzUC', NULL, 'Telegram', '+14437507816', 1, 0, 1, 'fThRnV1Ak92ZOZKUUGYK4QAb4goOACSYeDlGMPLkeZzGbmkOvGFuq4l2hdcs', '2018-10-09 20:57:58', '2019-01-24 04:37:23', NULL, 0, '2601:43:8200:b7a0:74d9:9025:4cd0:358a, 162.158.79.103', 2),
(713, 'Carolus Mekhaeil', 'carolus1997@live.nl', '$2y$10$qjuiik4fvYg.q67HkonO0.8NAaarWrCbSlsU5yGGlLjyOCwlgaEVe', NULL, 'Telegram', '0031634930969', 1, 0, 1, '7JhJ8XcumBtpgJ3TjrxWYmuL31rjd3k7LhBqe12IDUDNAkOMgZyTIl3ZrE4c', '2018-10-12 03:53:21', '2018-11-07 13:38:28', NULL, 0, '2a01:388:353:350:0:0:1:76, 162.158.155.226', 1),
(714, 'bigbull', 'saienxia@163.com', '$2y$10$Cupiw6CHtaUwlfXfJaIzCOgKa16SK1Kz5uYIUacwLV1rjL7KeqOq.', NULL, 'Telegram', '+8617326039167', 1, 0, 1, 'ZWfUdiKLpDaZe0NOXoOPZKWWZHDgipB9kV27IN9UQX267ktvAKnnAebb42Zq', '2018-10-12 15:53:17', '2019-02-21 06:37:28', NULL, 0, '43.249.28.230, 172.69.134.233', 1),
(717, 'Oluwaseun', 'sessuy@gmail.com', '$2y$10$u1JfnUBxW9iCDCHuVt7gQuvInxYbQ.S8AZcUP9jlcvFsP0eDWSQk2', NULL, 'Telegram', '07031172459', 1, 0, 1, NULL, '2018-10-15 03:14:26', '2018-11-07 13:38:28', NULL, 0, '197.210.226.33, 141.101.77.124', NULL),
(718, 'Steven Nguyen', 'hungvannguyen@gmail.com', '$2y$10$GWJTk3cI56YkPrZmflOzgOBXYC2GEs8bn3oCQCG008oLfQlT.MtFW', NULL, 'Telegram', '(+084)912919686', 1, 0, 1, NULL, '2018-10-15 03:48:48', '2018-11-07 13:38:28', NULL, 0, '117.1.164.211, 162.158.178.35', NULL),
(719, 'Allent', 'jjallent@126.com', '$2y$10$Y.1RhIXsldcu9r.QOYwSKOCES0TmdkQW5ENgIhYqG2QvR.4RnRWhu', NULL, 'Telegram', '13911813019', 1, 0, 1, NULL, '2018-10-15 11:13:00', '2018-11-07 13:38:28', NULL, 0, '183.129.47.41, 108.162.215.12', NULL),
(720, 'Jody', 'jodywbrown@hotmail.com', '$2y$10$.ApH9sCGE7c/LIT08W3.WeFny5OXnn0o4rYVbPBueRC55ARKvHNL2', NULL, 'Telegram', '2504707317', 1, 0, 1, NULL, '2018-10-15 19:29:32', '2018-11-07 13:38:28', NULL, 0, '2001:569:f80a:2f00:6900:6a1b:a362:121e, 108.162.246.67', NULL),
(721, 'Cameron van Rooyen', 'camimnot@gmail.com', '$2y$10$lvGNJcey9/WzHBmz5dJhZeQBHsSBzD1.YCp9A2qo/Bg2T1ut4RqZK', NULL, 'Telegram', '0727633011', 1, 0, 1, 'P9nVWhPW7hS4XmhIEOK1deaUNvSAMcqKDgC6rdfAglwkkQ1vugFAez0jkrWD', '2018-10-17 15:47:39', '2019-02-17 01:56:28', NULL, 0, '197.245.55.70, 197.234.242.120', 3),
(722, 'Hassnain Raza Alvi', 'alvirockstar2015@gmail.com', '$2y$10$.vo9eO7VLcPXQalyPAfgBOeHBiPavjh2hKT2rjDpbz9Bm7v9Rwl7W', NULL, 'Telegram', '+92333575838', 1, 0, 1, NULL, '2018-10-17 21:00:15', '2018-11-07 13:38:28', NULL, 0, '101.50.94.8, 162.158.167.122', NULL),
(723, 'John Butrovich', 'j2b2y2k@hotmail.com', '$2y$10$AWJgPD5TPog2xg9UAEedju5VOpdFoOgPOyGbvhgbZ4yxxCFUJ7HGu', NULL, 'Telegram', '928-793-2471', 1, 0, 1, NULL, '2018-10-20 08:59:28', '2018-11-07 13:38:28', NULL, 0, '107.161.81.66, 172.68.47.62', NULL),
(724, 'Manu Manninen', 'kulkuri.x@gmail.com', '$2y$10$Aw61BYsEpwaiVlj1KLlbCuOzGtCypm05LHagfV6/iPwQzIiLNzi3y', NULL, 'Telegram', '+358504303007', 1, 0, 1, 'Jqvi8PyxUwzAEbBjc4Xi0o9War8JPh2t9smD6tDIriPWcyDFcbQVs7uun9Vo', '2018-10-22 00:48:44', '2018-11-09 01:52:28', NULL, 0, '85.76.35.130, 162.158.238.30', 1),
(725, 'Anish Ahmad', 'dr_anis2008@yahoo.com', '$2y$10$YIw0ZMCg4kGWYesbZE5mKuwav5Ol/Qw1oTSQNuH2kwtdkr/p3gTPa', NULL, 'Telegram', '9981445011', 1, 0, 1, 'HsEiJcK02uCsfn7X76P74Pc6EdjmsEtHb8j5flWDjwW1kmHcCCZa4yYaAkXu', '2018-10-22 01:38:32', '2018-11-30 10:06:34', NULL, 0, '2405:204:e182:2ffc:b54b:a1a:f8a:c561, 162.158.154.147', 1),
(726, 'ibrahim el fadili', 'ibrahim2170@hotmail.com', '$2y$10$rIhabv4iJEcvumI5IUNEgucnhNxhEC4NUFQQ3uUbojsESm6VPaV86', NULL, 'Telegram', '(+32)0485168879', 1, 0, 1, NULL, '2018-10-22 23:45:35', '2018-11-07 13:38:28', NULL, 0, '213.219.163.156, 172.69.54.197', NULL),
(727, 'Paul Peene', 'p.peene@solcon.nl', '$2y$10$5y9W/7tQ8hr10vSTCFQcne8eXxkI/5RnHViEEMH8KwJHpwkhumh5S', NULL, 'Telegram', '31629779544', 1, 0, 1, NULL, '2018-10-23 14:41:18', '2018-11-07 13:38:28', NULL, 0, '2a02:120:23a3:3260:a0fc:1578:83cb:9839, 141.101.77.34', 3),
(728, 'Mangasar', 'mangasar22121983@gmail.com', '$2y$10$sAnO1HzSv09kDaqJ.hsZCeakcx0nKSiZ6qF47ox7zWfBe/rfhI3kq', NULL, 'Telegram', '+37495362015', 1, 0, 1, 'XR0Nf5EtQk94GNI0R6cpAzqESnDN5vHvnQwgexocSeb2yRyrJrZVD9vpvyD9', '2018-10-24 16:39:56', '2018-11-07 13:38:28', NULL, 0, '46.70.197.203, 162.158.89.141', NULL),
(729, 'Saeid ghassemzadeh', 'saied_ghasemzadeh@yahoo.com', '$2y$10$4X.msGjjMz9FfEEGpI1ShOPgEy5WyiWXO3cPQkzM5cQYZVApkKvM6', NULL, 'Telegram', '00989135457995', 1, 0, 1, NULL, '2018-10-24 22:45:53', '2018-11-07 13:38:28', NULL, 0, '5.215.75.39, 162.158.89.15', NULL),
(730, 'Jinan Mohamed Shameem', 'jinan809@gmail.com', '$2y$10$hlX2hsQcdDRUko.DUbYNlOuGAra5ZIYRN695N.SitRVJcLWYA50.W', NULL, 'Telegram', '(960)9540929', 1, 0, 1, 'JUgz1v06Bj27IJoqEIffCkRWW0QlGWp4IbcHEPAL9aOCeCzM910Af9KnMoyD', '2018-10-25 01:33:52', '2018-11-07 13:38:28', NULL, 0, '27.114.165.234, 172.68.144.207', 1),
(731, 'John Stracey', 'jstracey1971@gmail.com', '$2y$10$GQqSd7pPzcagwgkKvyDiGO.b4Tw.FdWNba5ggDHLv79lEV2sjoCyC', NULL, 'Telegram', '07911742212', 1, 0, 1, NULL, '2018-10-26 21:06:45', '2018-11-07 13:38:28', NULL, 0, '91.190.163.207, 162.158.154.81', NULL),
(732, 'Muhammad Aamir Iqbal', 'pakistanforex1610@gmail.com', '$2y$10$sGU7bLxw5y18VF61KPcT2.Y3cZdEWcJEbSGDlGfgS1g8THwZBdH6a', NULL, 'Telegram', '+923156839318', 1, 0, 1, 'w9iGEDlzHyyV41ba99SBNkSe0OdtU078SvCSiWhAeBmbudV4bhpIeBxx8L3g', '2018-10-27 05:44:08', '2018-11-07 13:38:28', NULL, 0, '39.34.233.239, 172.69.135.12', NULL),
(733, 'KroetSokkhoeurn', 'sokkhoeurn365@gmail.com', '$2y$10$EbiU8RkxPkRtYESxG.xSIe3nIQwDI07nhuvewRXOmK52yly90zr72', NULL, 'Telegram', '+855965405070', 1, 0, 1, NULL, '2018-10-27 07:20:26', '2018-11-07 13:38:28', NULL, 0, '117.20.112.106, 172.69.82.6', NULL),
(734, 'Joe', 'support@jaywilliam.com', '$2y$10$WE.hQ4zVYubjxQ0J92m33ucDvr636zdBoJXwctcqwX7vb/FdspryK', NULL, 'Telegram', '7033471523', 1, 0, 1, '6oiFekur5ERS2Z0EY20tIDY8MywQyF2DXYKVrpAiS4QTDLpPWsuc66q4xV2T', '2018-10-28 23:04:59', '2018-11-07 13:38:28', NULL, 0, '192.152.152.58, 162.158.186.62', 1),
(735, 'Ehtesham', 'ehtesham.bokhari@gmail.com', '$2y$10$.UhA2YsZmPJoyolj.EKXV.YCZRNBcaGxVtV6eINbaPpW6PQ1wK0cy', NULL, 'Telegram', '+923149000058', 2, 0, 1, 'LGlEouGpdf3irjNwASTy1S0yYUxm84dPPnfSGvpFVb3HzvHCeMFSNg0nyr8j', '2018-11-02 16:12:35', '2018-11-07 13:38:28', NULL, 0, '103.255.7.9, 172.69.135.192', NULL),
(736, 'Sarada Swain', 'spswain@gmail.com', '$2y$10$G9CLHP1FQ4ktDPdEMTST3OhR9iUsheJE7n3.jtfztwHcIckiMpBVm', NULL, 'Telegram', '+1 7064644845', 1, 0, 1, NULL, '2018-11-07 17:02:52', '2018-11-13 09:11:08', NULL, 0, '67.141.255.105, 162.158.78.36', 1),
(737, 'Md Adnan Hossain Akand', 'dibos69@yahoo.com', '$2y$10$BxmE6hJo/iS6xldK4GWYB.kPdijCXjL2VQFrgukkIDAvwdnCoyzIO', NULL, 'Telegram', '+88 01673369573', 1, 0, 1, NULL, '2018-11-08 23:11:30', NULL, NULL, 0, '103.104.217.25, 172.69.135.48', NULL),
(738, 'Alex Wells', 'ellexxwellss@gmail.com', '$2y$10$TvbJC6VWbIotpdLo8kB.x.1VpBRyQXqevbehwImwZaMbHWPisgfo.', NULL, 'Telegram', '(+91)7226930073', 1, 0, 1, 'bdy38LU6Fc8akUq7Vj0CyBOAW7oQciuqgYilsPhyY34TMik9QRW4qG10lhiV', '2018-11-13 10:31:49', '2018-11-14 12:32:24', NULL, 0, '123.201.225.139, 172.69.134.191', 1),
(739, 'Masum', 'irinparvinsumi18@gmail.com', '$2y$10$D23kzaKCnGk/5JS3u5dfb.JVt5cVukmDrr4NGfHFOs2pKGPNrAIBe', NULL, 'Telegram', '+8801646837499', 1, 0, 1, NULL, '2018-11-16 07:20:58', NULL, NULL, 0, '27.147.224.228, 172.68.146.148', NULL),
(740, 'Makara', 'Makarasam19@gmail.com', '$2y$10$FDVskMCqvluybcPx7XzvOuU0.No8pJXpBLALLXSc7zj3zeczunh6m', NULL, 'Telegram', '+855967065620', 1, 0, 1, '9aVPQzsvrc85EEXc8iF2blxHIRiTElqCfAQxoiXoeCzt3nDcy24rjEZGCZsv', '2018-11-17 19:08:30', '2019-03-18 07:01:20', NULL, 0, '203.144.67.159, 172.69.135.72', 1),
(741, 'faheem', 'kazoea@hotmail.com', '$2y$10$.IJpp/uSqFLtocdDmEjzju65cKSVZrcz8u2lUpKEN2MmhmUoKZ1S6', NULL, 'Telegram', '00966563560068', 1, 0, 1, 'BXfVreKEvNg25nD06kUU0manXDZOzUX4iwZ4prLUm8dC38txpZFd5tV7gWO9', '2018-11-18 05:02:57', '2018-12-03 23:28:08', NULL, 0, '31.166.100.8, 172.69.54.149', 1),
(742, 'Jomar Vergara', 'jomarvergara0808@yahoo.com.ph', '$2y$10$i65AETxa3Ymk8iIHtU/GW..raZEd6PhNhooQYIX8NKivLQJay5LRy', NULL, 'Telegram', '(+63)9150190159', 1, 0, 1, NULL, '2018-11-18 10:32:09', NULL, NULL, 0, '49.146.13.105, 172.68.141.162', NULL),
(743, 'Shafi Hidig', 'hidig991@gmail.com', '$2y$10$lnzxEKJP20P8NRh70aNjXuw/gW1XGY.CclWE3oAJzjFw6HGzZSa4O', NULL, 'Telegram', '+252615043051', 1, 0, 1, NULL, '2018-11-18 15:02:34', NULL, NULL, 0, '41.78.74.27, 197.234.242.36', NULL),
(744, 'roy', 'vickeryroy@gmail.com', '$2y$10$lvS9GGl.LIZFBBPqPAgL7uSJM2T7HDXg6KuvaTA6Q1pQPilcL/Nqu', NULL, 'Telegram', '0035699206368', 1, 0, 1, NULL, '2018-11-18 17:20:27', NULL, NULL, 0, '77.71.129.158, 162.158.150.108', NULL),
(745, 'Md Abdullah Al Momin', 'aalmomin4@gmail.com', '$2y$10$c485kiHZ8q6jF.dvrDAsiuIMaZ3a.p.UHoFnTlDVvKkKKV04VyVb.', NULL, 'Telegram', '+8801827717876', 1, 0, 1, NULL, '2018-11-18 20:54:33', NULL, NULL, 0, '103.242.23.188, 172.69.135.72', NULL),
(746, 'Kabungo Bwalya', 'kbbwalya725@gmail.com', '$2y$10$PyEl2yIOVOqgORscjx.9UOyGHBP60IQkPLRkC.UJ8VxdoA4KKHetm', NULL, 'Telegram', '+260974622556', 1, 0, 1, NULL, '2018-11-18 21:46:31', NULL, NULL, 0, '41.223.117.71, 197.234.242.78', NULL),
(747, 'Obuenwe Stanley', 'stanleyedesiri@gmail.com', '$2y$10$gjRRkKxaO.JhG7sWO7Cvd.kPLmM8nEcq6p8je.j5pZwr8QMwN5INC', NULL, 'Telegram', '+2348065193030', 1, 0, 1, NULL, '2018-11-18 22:25:28', NULL, NULL, 0, '197.210.55.40, 162.158.111.57', NULL),
(748, 'THEIN AUNG WIN', 'tawin66@gmail.com', '$2y$10$B9OwAiVXnLecIlCBghbZzur26T0fDZO5LfP0vM1iUd/.1NPItCdqC', NULL, 'Telegram', '09960872465', 1, 0, 1, 'WsTVNmHgAWIRUPjz9kXPOcRyVz5MOqGlq6YOacAOBhO7G4AlFHYmcCwnKZbj', '2018-11-18 22:36:34', '2018-11-19 00:20:25', NULL, 0, '79.140.194.252, 162.158.38.42', 1),
(749, 'Mohammed Aldossary', 'dammame@gmail.com', '$2y$10$xBJQOdnOSlpjwDyCwYI.keBHzTMjcEUCxHSzUMq.R8noGxHfhFrfu', NULL, 'Telegram', '+966505805023', 1, 0, 1, 'JsrgpntyajMrKk8wz7QBRGNPrxbDIXDNzSb8FhP2O9v9v40pZnFpP6hMWoU1', '2018-11-18 23:26:04', '2019-01-17 01:50:52', NULL, 0, '81.24.156.186, 172.68.146.220', 1),
(750, 'Yussif Zakaria', 'yussifzak72@gmail.com', '$2y$10$/OYhBNnH6.4IU5BxJ2UPvuGaHxzRQnXqa20hK3FfVGQDI9hvVJHDG', NULL, 'Telegram', '+233545477499', 1, 0, 1, NULL, '2018-11-19 01:16:52', NULL, NULL, 0, '154.160.2.200, 141.101.76.114', NULL),
(751, 'Abubakar iddrisu', 'aiddrisu496@gmail.com', '$2y$10$iwUoGXFqnBn9JiwBygbUbeD/whtndgWP9S5k.0FxHCRMj53QaEJF.', NULL, 'Telegram', '+233242557794', 1, 0, 1, NULL, '2018-11-19 02:39:14', NULL, NULL, 0, '154.160.1.34, 141.101.76.84', NULL),
(752, 'Saman', 'Sarodrigo1963@gmail.com', '$2y$10$MWmYWD7dhpOgRR29GsCj0uBDkCtP2ckyfNBf84BBo.9v0v8ZtRYsO', NULL, 'Telegram', '0094705712380', 1, 0, 1, NULL, '2018-11-19 06:20:39', NULL, NULL, 0, '112.135.5.140, 172.68.202.12', NULL),
(753, 'osama abdikhalif hashi', 'osamakhaliif123@gmail.com', '$2y$10$gj8MNRWY6JxES4aINM11r.FNw13TufCwUbvrWHyXxNCIIJ1PXiQUe', NULL, 'Telegram', '+(252)615357565', 1, 0, 1, NULL, '2018-11-19 06:26:31', NULL, NULL, 0, '197.220.84.6, 197.234.242.6', NULL),
(754, 'Asim Ali', 'asimali14mechanical@gmail.com', '$2y$10$Dsag44/9IqdXzHg0dCayTeuBGtu92d8iilqcuTeExOBvHHl07AGv2', NULL, 'Telegram', '+923056700269', 1, 0, 1, NULL, '2018-11-19 07:40:37', '2018-11-19 07:41:11', NULL, 0, '119.160.103.184, 172.68.144.81', 1),
(755, 'Abdulbaqi Rahimi', 'abdulbaqirahimi941@gmail.com', '$2y$10$XjLCU/W6k8J2Mw66DQijNOrYTdJil8pV/LWmaJgwfKIpb7Q4S1A1y', NULL, 'Telegram', '0093700983184', 1, 0, 1, NULL, '2018-11-19 11:24:11', NULL, NULL, 0, '121.100.49.15, 172.69.134.215', NULL),
(756, 'Mohsin Ali', 'mohsin8876@gmail.com', '$2y$10$qTL4oVlk6JoMxyvQmz1kHe3FfS1upoXqAsOywFBEZTbs5qOAjvMye', NULL, 'Telegram', '03335320164', 1, 0, 1, 'eJek1fcbNhBPzJDKod2R0ylJ7FfsyHwz36CGd3FnrIXgFK5iMMD0TU9hVfVI', '2018-11-19 14:14:38', NULL, NULL, 0, '39.32.187.201, 162.158.150.108', NULL),
(757, 'BEH TONG YU', 'behtongyu@yahoo.com', '$2y$10$5reEJDqms1Z73vbou3MMmO9XKIjagzVTYA.76KATmBTQAMfQRx1ti', NULL, 'Telegram', '60192818649', 1, 0, 1, NULL, '2018-11-19 19:13:25', NULL, NULL, 0, '175.142.95.157, 172.69.134.215', NULL),
(758, 'Abdulaziz Mohamud Abdi', 'Abdulazizmcc@gmail.com', '$2y$10$CEgEetlH6SuIX6CGbV1xDex1IxN/Md6FVWEvWVo22g8v6iHL5FIde', NULL, 'Telegram', '+252613718088', 1, 0, 1, NULL, '2018-11-19 20:56:33', NULL, NULL, 0, '41.78.74.30, 197.234.242.114', NULL),
(759, 'Nirasha khatoon', 'nureshakhatoon98@gmail.com', '$2y$10$OhlOLSfHuXSpNh4Ixoh7TevZZiPbUEs0LORv6FLnj8LI0UxjNLQZe', NULL, 'Telegram', '91 6266571382', 1, 0, 1, NULL, '2018-11-19 21:50:45', NULL, NULL, 0, '2405:204:e608:3a4c:cc67:4ee8:e6ca:8bd0, 108.162.229.198', NULL),
(760, 'Andries', 'andriesbasson12@gmail.com', '$2y$10$d7UAxbvZ/qtJqErRkz200OckPzLOUkTc5QdPyWY8gC9hbPTZcPQsK', NULL, 'Telegram', '0812090982', 1, 0, 1, NULL, '2018-11-20 02:15:43', NULL, NULL, 0, '197.188.166.79, 141.101.88.78', NULL),
(761, 'Siam Newaz', 'siam.mt11@gmail.com', '$2y$10$vhafGby0WbsggC49CzNZfe77jp3d392xcGS2ePD8R5AIR67h3QwQO', NULL, 'Telegram', '+8801874782595', 1, 0, 1, NULL, '2018-11-21 22:02:57', NULL, NULL, 0, '103.200.94.142, 162.158.166.127', NULL),
(762, 'siwane nems', 'siwanems@gmail.com', '$2y$10$S1zGdGsFTJZRONkq.JPtH.lva79abTz.jUJyFoZZbE.enDtc8RZvi', NULL, 'Telegram', '+212652148785', 1, 0, 1, NULL, '2018-11-25 06:56:39', NULL, NULL, 0, '196.65.192.239, 141.101.88.78', NULL),
(763, 'morteza', 'mortezavahdat@gmail.com', '$2y$10$U/TuDZ6umIpH6LmzvbMp6uDU03s/d6iVCojKWU/Gb29NEfoCA1arC', NULL, 'Telegram', '+989354708120', 1, 0, 1, 'vsBYlGcbBcHzfgInzfRrVmI40KtlIupo6dWAFXaTrXJrnZKuD3FkjqOTfLtI', '2018-11-25 15:15:50', NULL, NULL, 0, '109.202.101.46, 141.101.77.172', NULL),
(764, 'Brian M Birch', 'brianmbirch@yahoo.com', '$2y$10$6ofihv9tJPq6eg3m8PqAyOO1RCrON4RwqjrXx5/ONVtBYQMbNZG62', NULL, 'Telegram', '0415189584', 1, 0, 1, '5lYRP7D1yMUx3Fi5hOfkMQSCvAKUAnGeNZD7rTrNZIYRWEQ53TXR2cS3mvxM', '2018-11-28 01:55:30', '2018-12-02 07:22:02', NULL, 0, '211.26.237.35, 162.158.2.24', 1),
(765, 'peter anet', 'gerbtekh1@hotmail.co.uk', '$2y$10$agxJUn/dqg.gbDaZvQoEUOYErmMu6ui8y4VE6tONDxtizszKZGTeG', NULL, 'Telegram', '07584724359', 1, 0, 1, NULL, '2018-11-29 09:23:22', NULL, NULL, 0, '82.14.150.130, 162.158.38.54', NULL),
(766, 'Darryl', 'darrylwhiteman@gmai.com', '$2y$10$jev2dX50CAa4wT1KNg8JFOigHq.YFE4Q9SnUX3vBhw5pcP8koGetO', NULL, 'Telegram', '+27 745865717', 1, 0, 1, NULL, '2018-11-30 02:34:07', NULL, NULL, 0, '197.89.113.105, 197.234.242.78', NULL),
(767, 'qaiser ali shah', 'shahqaiserali@gmail.com', '$2y$10$DIVp/QudeE/04gc0YOxwSOo62bxTB4VymAPPoCn5dmaiZI/uA9dVa', NULL, 'Telegram', '+923339251412', 1, 0, 1, 'ANL5DYkMfChFeyhFhOH2dNNJYWsWjnxBHioSe8fVvqNVfY3UGN65PokD7lXO', '2018-12-02 00:48:21', '2018-12-05 13:15:20', NULL, 0, '182.185.79.45, 141.101.88.12', 1),
(768, 'Malghalara', 'shahmalghalara@gmail.com', '$2y$10$GG3CQQlsStdl1AMlYf79iuHN1vMYbyCJwWZRtwmSlMQxc3GoONtju', NULL, 'Telegram', '00923139698881', 1, 0, 1, '8DpH9FFYBuR5G41LHH237At0TG379Qx4hHGUCRCqAO4r96OvaFPe78wAwXpT', '2018-12-05 20:54:15', '2019-04-08 19:39:27', NULL, 0, '165.227.14.195, 172.68.133.14', 1),
(769, 'Duch Meanheng', 'duchheng117@gmail.com', '$2y$10$LEUvjlXvJbk4oib9PXVNveWlY0ldJsB9F994Y05jhurIyct8EMV6W', NULL, 'Telegram', '(+855)85657888', 1, 0, 1, NULL, '2018-12-06 22:28:18', NULL, NULL, 0, '175.100.20.205, 172.69.134.167', NULL),
(770, 'James Richter', 'jimr4266@gmail.com', '$2y$10$ghxh4proUDv21iuxXF5RPuae6EoGeXquuitTKgbzsy8WugxEvHmXK', NULL, 'Telegram', '6038437105', 1, 0, 1, NULL, '2018-12-07 09:46:02', NULL, NULL, 0, '65.96.176.109, 108.162.219.54', NULL),
(771, 'Chandra maulana', 'candramaulanasidik@gmail.com', '$2y$10$nqsTMJdvmIIg0fVPZChDyun1AM8ogs5n8kFTms0/To4grYwL62XEm', NULL, 'Telegram', '+6282111158542', 1, 0, 1, NULL, '2018-12-10 03:53:20', NULL, NULL, 0, '125.165.67.222, 162.158.166.241', NULL),
(772, 'jazeem jazz', 'jasfk3@gmail.com', '$2y$10$FHWAdtz/RwFBRwDdNR1WAuBZvKDts0D7/6ErODB/.BjnFJchvQh2u', NULL, 'Telegram', '8089990545', 1, 0, 1, 'WQ47GeCLbuM9fbMWjcvuCXZ4XU7P2khttMV2V2WvWt1lVvn3PMnNeiASRUW5', '2018-12-11 22:54:43', '2018-12-11 22:57:15', NULL, 0, '27.61.50.237, 172.69.135.180', 1),
(773, 'khalid mestaoui', 'khalid.mestaoui@gmail.com', '$2y$10$v3wGIG90vA5OkPGfngptKejvYL/tb8qStA9LrTXAF24EpUD7LJfRG', NULL, 'Telegram', '+212629862330', 1, 0, 1, NULL, '2018-12-15 18:53:36', NULL, NULL, 0, '41.141.227.31, 141.101.88.252', NULL),
(774, 'Donovan', 'donaven.davids@gmail.com', '$2y$10$kNpBztokGo9xa6rlA4an8exb8DkTPnenzP8Sdd/Ms43gJUbTleExm', NULL, 'Telegram', '+26412097080', 1, 0, 1, NULL, '2018-12-17 01:43:55', NULL, NULL, 0, '105.232.19.8, 197.234.242.6', NULL),
(775, 'Makonde Makhado', 'makondemakhado@gmail.com', '$2y$10$MjB7umqau5DVMGC5E0vsIO/yfyc5AhkKlyrH/nNbv7aMkbqsW/Tl.', NULL, 'Telegram', '+27832924606', 1, 0, 1, 'A02JbdLdVpQckXC1InUSX5ABpoTAA519T4QsHdsljlMLY3jOuw5ASxnlR2Ez', '2018-12-25 16:27:09', '2019-04-08 09:43:51', NULL, 0, '41.113.0.2, 197.234.242.114', 1),
(776, 'Mouad', 'mouadfellah@gmail.com', '$2y$10$fUxBJDgXHWrI9zED5ywz..R0rNLOz9eBe2YVCwkFfUpuCpT9Xf.mS', NULL, 'Telegram', '+212670728787', 1, 0, 1, NULL, '2019-01-03 03:44:55', NULL, NULL, 0, '105.154.164.174, 172.69.226.13', NULL),
(777, 'Isiaka Abdulmumin', 'ibrahimisiaka4@gmail.com', '$2y$10$eQxxsGCEzCeB9q7VAwQUCuibAKyygcWNnypoKJy8z86mzp3x/d/VO', NULL, 'Telegram', '+2348066947566', 1, 0, 1, 'uMZmH3o0f5GBCTwfujvtBkp2yz6z8WllEMRD3qCaBqMasBkBLntNUVMbMu4R', '2019-01-10 23:14:33', NULL, NULL, 0, '41.58.125.192, 197.234.242.144', NULL),
(778, 'Ahzam Kazmi', 'ahzamkazmi@yahoo.com', '$2y$10$4fuOdiUcSC9XWQXUel2eYOHJGfGGRRJurYVQncWwCsCR.P4slGJvG', NULL, 'Telegram', '+923317780128', 1, 0, 1, NULL, '2019-01-12 02:30:24', NULL, NULL, 0, '39.36.246.22, 172.69.244.140', NULL),
(779, 'Moloy Kanta Debnath', 'debnathmoloy@yahoo.com', '$2y$10$ckWj87N55MV.qX7Uk1Q/xe8kcIpm9.RzTd.m4DEb0xRtHho183AMK', NULL, 'Telegram', '+8801912716171', 1, 0, 1, NULL, '2019-01-13 00:04:33', '2019-01-13 00:30:38', NULL, 0, '116.58.204.128, 162.158.7.7', 1),
(780, 'Selina', 'selina@sorosedu.com', '$2y$10$jnDFQ441cdO.jiF45Hdv6eOkXWrYtofQ83tUOXqw2Zy4U0COug9W.', NULL, 'Telegram', '+86 18222108862', 1, 0, 1, 'vRs7ZmzaTj1HENoOIc7sV0hCgTXalqmjm41Xh9UIJH8jNANlZoIKvJnwcxsN', '2019-01-16 12:14:44', NULL, NULL, 0, '115.192.76.132, 172.68.46.175', NULL),
(781, 'Mehmet', 'membank@hotmail.com', '$2y$10$Q/0DQLuuCMzXSFMvDLTudOn/GPtILHap.M1NJcWGA7WtEGzFzZjQW', NULL, 'Telegram', '+447956398616', 1, 0, 1, NULL, '2019-01-20 15:27:09', NULL, NULL, 0, '2a02:c7d:bbc:a400:90e3:fd63:9538:4e63, 162.158.89.159', NULL),
(782, 'Grzegorz', 'pol-trade@o2.pl', '$2y$10$k6t1mkLZOiMQ4x3Il11ZZe8uGjeMyn/aVJIXzF.QUY/gAmBxj1IsW', NULL, 'Telegram', '781937454', 1, 0, 1, NULL, '2019-01-20 23:42:33', NULL, NULL, 0, '5.172.255.12, 172.69.55.12', NULL),
(783, 'Fidelis Akhimien', 'fidharrison2020@gmail.com', '$2y$10$XN9HWlUSLj/DVWuQ/9JHcuEpysqKdC3mBjXt7InYVlXaNmYXG4yTC', NULL, 'Telegram', '08175594356', 1, 0, 1, NULL, '2019-01-21 16:18:17', NULL, NULL, 0, '197.211.59.178, 162.158.38.54', NULL),
(784, 'James O\'Donovan', '2twins@gmx.co.uk', '$2y$10$eJnzsnFpLZY4dzjkUh/8JOIGtfD8IVfM7Yswqz/6QR/qM4/vX95oW', NULL, 'Telegram', '7712776958', 1, 0, 1, 'DhNW7k7v9aeC5ZQ5PvyxdERq1D6SrZW7xkqy7IO6SvggiipBifxxThQl3ZMY', '2019-01-21 18:02:04', '2019-01-21 19:32:35', NULL, 0, '86.182.162.82, 172.69.55.12', 1),
(785, 'Abdullah al mamun', 'mamunasif18@yahoo.com', '$2y$10$ehyZIE/4DcPj//e9dvbC6.FUxUI//0YnNIkxLQM3R5B9Qk1zYlbwe', NULL, 'Telegram', '+8801833901254', 1, 0, 1, NULL, '2019-01-25 22:35:22', NULL, NULL, 0, '103.242.23.179, 141.101.69.53', NULL),
(786, 'Kanishka', 'omidkanishka@gmail.com', '$2y$10$6dExM.tG08WmmiPrMEg22ev4oTpudXFZf8xfZLxyRidglXaM6yEIy', NULL, 'Telegram', '+92780277803', 1, 0, 1, NULL, '2019-01-26 03:00:50', NULL, NULL, 0, '103.213.105.170, 172.69.54.227', NULL),
(787, 'jk', 'fongasd@qq.com', '$2y$10$mNXKpvj29YaEPWSq1xb4yewpA2mpJ.tqzNNr419n00TLE.9gqCc/C', NULL, 'Telegram', '123949124', 1, 0, 1, 'EBPWdpDeUmoJXh8JSLZQ1P1IvUCp9eNV69oGRptQJ6jtkiAm2uXlG1fSDCwj', '2019-01-26 05:18:50', '2019-03-05 22:49:21', NULL, 0, '121.32.167.179, 172.68.132.169', 6),
(788, 'chirag', 'chiragsono@gmail.com', '$2y$10$HvEYZkTDB.FlPXZQkUuSr.x10fUjc0OLRs4Mjdh/bcApGUBvzHl7i', NULL, 'Telegram', '(+61)430520540', 1, 0, 1, NULL, '2019-01-27 09:41:54', '2019-01-27 11:25:06', NULL, 0, '2405:6e00:1e46:3c00:c4a:a5be:324:21c3, 172.68.86.30', 1),
(789, 'khaled hanafi hamed', 'khaledhotlaw@yahoo.com', '$2y$10$wHwIoE48cZGHSToUaDPljOoLK0TbLF2rbxKuDNcJBIL8DpXzbbQ4O', NULL, 'Telegram', '01022220224', 1, 0, 1, NULL, '2019-01-27 19:28:28', NULL, NULL, 0, '196.221.95.141, 172.69.226.31', NULL),
(790, 'Okenwa James', 'lordamburs@gmail.com', '$2y$10$UKyynynUPJlUoiL/oCBPnOvdpowJiuO84XeXRhKjSW7ErcP9UBBGu', NULL, 'Telegram', '+234670854305', 1, 0, 1, NULL, '2019-01-28 16:25:12', '2019-01-28 16:29:19', NULL, 0, '129.0.76.90, 172.69.54.5', 1),
(791, 'Heng sook Tee', 'trifficimports@gmail.com', '$2y$10$R259WeXJINSEfYvFreaal.FeZSKnvo1LFPIXaKctw7L3XvnHyD6N6', NULL, 'Telegram', '412113561', 1, 0, 1, NULL, '2019-01-28 16:30:37', NULL, NULL, 0, '123.243.77.250, 103.22.200.236', NULL),
(792, 'Konstantin Enchev', 'enchev1981@gmail.com', '$2y$10$fI3TSZhzYaFoACH/zvMpBOM5OCwPn3xPxIh4B3pbtWioVsKKUQQBK', NULL, 'Telegram', '895747231', 1, 0, 1, NULL, '2019-01-28 21:30:25', '2019-01-29 00:17:24', NULL, 0, '79.100.116.106, 162.158.210.30', 1),
(793, 'Mike Mawira', 'mikemawira@gmail.com', '$2y$10$3Q6Ylgnu/XbTV9Mfwxe1YOVC1kJKhzpMKI/G0fPHuvgFVt0VuHkEW', NULL, 'Telegram', '+254772704284', 1, 0, 1, NULL, '2019-01-29 04:55:50', NULL, NULL, 0, '41.89.130.11, 162.158.111.21', NULL),
(794, 'rolando bueno', 'jhunbueno09@yahoo.com', '$2y$10$oBHRwDUr9DFGY3AEcDSMg.fO7WVFboGax4gyg4ES5WHCbQq0PB296', NULL, 'Telegram', '+971527220881', 1, 0, 1, NULL, '2019-02-02 12:16:49', NULL, NULL, 0, '109.177.229.125, 141.101.105.206', NULL),
(795, 'Andu', 'anduuyhe@gmail.com', '$2y$10$F.zTaQDd9B5F2h8N08S4PuJxWxcoguJXnJW0v0NRK8YsJMfvh5Jti', NULL, 'Telegram', '+2348039690909', 1, 0, 1, NULL, '2019-02-02 12:32:50', NULL, NULL, 0, '105.112.120.111, 172.69.14.30', NULL),
(796, 'Zawam Mansour', 'zwmagagvb@gmail.com', '$2y$10$2vPQb.7C8ro/BkOnul.wLepPJ3PKVOZM/AZVkR2LZgvAmh75hs6Cm', NULL, 'Telegram', '1021402225', 1, 0, 1, NULL, '2019-02-02 15:39:22', NULL, NULL, 0, '41.34.38.25, 172.68.62.150', NULL),
(797, 'David Tersoo', 'dt.techologies@hotmail.com', '$2y$10$z2mvG0vwp6udarbiDe8HNurs.NGej5ymKVaNTQYA3mSFzO6/nK1m2', NULL, 'Telegram', '+2348030823226', 1, 0, 1, NULL, '2019-02-02 17:57:33', NULL, NULL, 0, '105.112.121.54, 172.69.14.18', NULL),
(798, 'philippe guinard', 'flippies@hotmail.fr', '$2y$10$CvqmpOWDplmSgrmU0rwyAe78e8jZEsTuVnfG/uR2NeCCxE9veEkSS', NULL, 'Telegram', '+33783518573', 1, 0, 1, NULL, '2019-02-02 18:55:11', NULL, NULL, 0, '78.253.61.10, 141.101.88.84', NULL),
(799, 'August Philander', 'walterphilander61@gmail.com', '$2y$10$YD4p7Snj4u4idFwaddLy9Oh9qkqKfz32QrlbWEC3VKAIaTuLwPDXy', NULL, 'Telegram', '+264814072501', 1, 0, 1, NULL, '2019-02-02 23:16:38', NULL, NULL, 0, '105.232.255.31, 197.234.242.30', NULL),
(800, 'Bassem', 'velazoza@gmail.com', '$2y$10$FVacXTh9hsp3nYjxBqScSuAjNJeaq1n/7JXrMT7XOqEuZDG6J9HnW', NULL, 'Telegram', '+966549998524', 1, 0, 1, 'uRuP8KBzKp2tbKkX4h24vx6W7d9iJs82XFAy0XJqMe8lT2Nf6JgrekARHZeG', '2019-02-03 00:51:24', '2019-02-07 11:09:59', NULL, 0, '51.39.76.205, 172.69.126.30', 1),
(801, 'Bassem', 'bassem.gamal2010@gmail.com', '$2y$10$aGRIXxh7/PMsf6w2uQZkt.y1QTZho9nCv.2re346.Q11iZE0Nq7x2', NULL, 'Telegram', '+966567286304', 1, 0, 1, '2fv29KgmftAGRXUD5Gix7J11V7IwIu9tx8tXr2WTNsrQKxtrBVjaeG9i0vkk', '2019-02-04 13:02:50', NULL, NULL, 0, '51.39.113.94, 172.69.126.6', NULL),
(802, 'Peter Oghenebrume', 'brupet2003@gmail.com', '$2y$10$askHxu3H5KqMuRhGagBOeu6/Lp00Cby/cUtfH1cVp.QRv4vtwvCry', NULL, 'Telegram', '+2348076723873', 1, 0, 1, 'XPs75AZ11xMppFA3iZsXH0uGydQ3aLhm9QlUZjQBiHhMx1nJdXE8eyQtMiRm', '2019-02-04 16:41:05', '2019-03-20 02:29:58', NULL, 0, '185.174.156.50, 172.68.94.186', 1),
(803, 'Bassem Gamal', 'bassem.gamal@huta.com.sa', '$2y$10$qEyrGL4MIUXmnmlOCdBl2uG86bdsChn9zaS.qKSzRUym3KMKSytRO', NULL, 'Telegram', '+966507459463', 1, 0, 1, 'FPV6w058e8FOdWDBlz2IT1zb8IVI9agEnoUmfHAo3QugoVvgCCAmbb1We1vi', '2019-02-06 15:54:05', '2019-02-06 17:10:59', NULL, 0, '51.39.125.164, 172.69.126.30', 1),
(804, 'imesh maduvantha', 'imesh4you@gmail.com', '$2y$10$bbbKvWrB6bWIg5O66GAkeeAP2GInrgbZFWrrohOlP/czNgaVNywX2', NULL, 'Telegram', '0710311067', 1, 0, 1, NULL, '2019-02-07 11:30:07', NULL, NULL, 0, '112.134.17.169, 172.68.202.30', NULL),
(805, 'Tumelo Seboko', 'tmlseboko7@gmail.com', '$2y$10$UiY3uwlySI5pcLYo4I/SU.ZBIfUXYoNhKXmTDeZnvt9ngJqeGGjmS', NULL, 'Telegram', '+26772901551', 1, 0, 1, NULL, '2019-02-07 13:15:52', NULL, NULL, 0, '196.216.163.138, 197.234.242.174', NULL),
(806, 'Sarbinder Sehmi', 'samsehmi@msn.com', '$2y$10$9SQDwKoloSGwYG8Sq7XdfO3.3axAC6hYxRmZZcBu7er9X8b.KcxHO', NULL, 'Telegram', '+447795516945', 1, 0, 1, 'v07ev5EpFWWy17Wk1F0LyCO0ADKLTv7dDSnpP06KWTz1RJmfwglsNPXRcvuN', '2019-02-07 14:02:24', '2019-02-24 21:28:41', NULL, 0, '2a02:c7d:3b9f:b00:2013:fa94:8904:ae76, 172.69.54.5', 1),
(807, 'Daniel Schur', 'daniel.schur@gmx.de', '$2y$10$CmDURac9dyJIdK2kHLHi6umvYbdBP87KRDvD1fn9.DTxGqWRv77H.', NULL, 'Telegram', '+16193016641', 1, 0, 1, 'rC7XuoNXwnC8q1Th66MC8l17ADKHRkKiT6Wtfq0ZN7peF5Zo3ctWMIlRU1Db', '2019-02-08 08:49:33', '2019-02-12 08:31:44', NULL, 0, '66.75.46.30, 172.69.33.97', 1),
(808, 'Md Abdul Halip bin hj sulaiman', 'Halipsulaiman@gmail.com', '$2y$10$ZpFwQk1nf3nT7Ftgrjiq8.WoKqQXgGGfcGEkzum3HadKJky.8D4Um', NULL, 'Telegram', '+6738900261', 1, 0, 1, NULL, '2019-02-08 18:04:31', NULL, NULL, 0, '61.6.236.156, 108.162.226.31', NULL),
(809, 'Kusum Priyantha', 'kusumpriyantha7@gmail.com', '$2y$10$3ev7Jh6VCmxp5opkRddDse/yHuahITJ4TinkqQGFjbScUXGgYLkaO', NULL, 'Telegram', '0717069375', 1, 0, 1, NULL, '2019-02-09 18:58:11', '2019-02-09 19:02:31', NULL, 0, '112.134.16.49, 172.68.202.18', 1),
(810, 'john shepherd', 'mukorajohn2@gmail.com', '$2y$10$OR2u4bPX7SAzzHNaw6xqwuibpbeBvrBQbaZqSkvzhPcFSupaRmdgK', NULL, 'Telegram', '00267728366270', 1, 0, 1, NULL, '2019-02-09 21:34:11', NULL, NULL, 0, '41.138.78.20, 172.69.54.5', NULL),
(811, 'Shaymol Chowdhury', 'shaymolchk@gmail.com', '$2y$10$9Di5qvVSjD8vFP66MMm3EuLfm/eC1bE/juf9GgLCUVtOtJT0VZm9a', NULL, 'Telegram', '0881914031968', 1, 0, 1, NULL, '2019-02-10 00:23:54', NULL, NULL, 0, '123.108.246.150, 172.68.211.108', NULL),
(812, 'Mohamed isse salad', 'mohamedisse262@gmail.com', '$2y$10$hwPATvZ6ZLl3LjMQ05u4k.5GXaTR.bpmF2ZTVrJTUUgPQ5nDvDJGy', NULL, 'Telegram', '+252618239356', 1, 0, 1, NULL, '2019-02-10 19:37:24', NULL, NULL, 0, '197.220.84.7, 197.234.242.114', NULL),
(813, 'katiso molapo', 'mpho5494@gmail.com', '$2y$10$dX2yyy/Y6wmqQIa7/ZzC8upyfB6ypdFKIKkQTKRPsE/SeiQuS5Bxi', NULL, 'Telegram', '(+266)58666075', 1, 0, 1, NULL, '2019-02-11 20:50:47', NULL, NULL, 0, '129.232.73.224,107.167.104.109, 172.69.62.222', NULL),
(814, 'Tee Onip', 'vonipede@yahoo.com', '$2y$10$J/VBs7ZMaD0JiobFF/ddBe4tUp3V5XwDaVSkafbQ73HLvFch3H9uy', NULL, 'Telegram', '08072034347', 1, 0, 1, NULL, '2019-02-14 17:44:11', NULL, NULL, 0, '197.149.87.38, 172.69.14.18', NULL),
(815, 'Okenwa James', 'jamesoky20@yahoo.com', '$2y$10$RAPhO0lhEose5uIT/vaj8O2bJ7..va3SQrnYaXWpxm0zIgKrzev6m', NULL, 'Telegram', '+237670854305', 1, 0, 1, NULL, '2019-02-14 18:31:40', NULL, NULL, 0, '41.244.226.67, 141.101.76.12', NULL),
(816, 'Jim Wisbey', 'jim@wiznetwork.co.uk', '$2y$10$6jXu9rb8kYh2A9DbnVnMZe13/CBMn5IC5TDPyWD4vwQHoPodxUdBC', NULL, 'Telegram', '00447860262005', 1, 0, 1, 'jcXME6b6zh5mfm2uxajiJMlyD54pJ0LzbyhAVNfl2mml3WEXnpdB7XYHf3MT', '2019-02-21 10:46:17', NULL, NULL, 0, '171.100.11.82, 172.68.242.102', NULL),
(817, 'Samit singh', 'manisingha28@gmail.com', '$2y$10$pkPoWy1AtGwVil2emzISsOJ2YV7N082uvz5DQiVZb8WXDMwKLhq/2', NULL, 'Telegram', '+917309394951', 1, 0, 1, 'bhFb4tshQLtd1PUR3a3FFN4O2jArNRiShO9L61hWC8VzWBTAgx45k053lAaF', '2019-02-22 13:16:11', NULL, NULL, 0, '2405:205:c:5fa8:817e:5a3c:a938:af5e, 172.69.226.121', NULL),
(818, 'Zhang Xinjiang', 'forexlearner@163.com', '$2y$10$T57AWYNu8Qpl6AcaZOhr6OCLzqYEG8l16jRI/tj5XJ4PR2d4Hq.Gu', NULL, 'Telegram', '+8613568464117', 1, 0, 1, 'qPnMrx2P1v4BYQ8DjTCSOdc9xk8wSESNkEllFpytkZZGetBkA4CDgDx82gWQ', '2019-02-23 19:16:04', '2019-02-24 17:12:56', NULL, 0, '185.212.168.140, 162.158.34.126', 1),
(819, 'Essam Mohamed', 'esssammomd74@yahoo.com', '$2y$10$xI6k5irAzRt8cKg.12xFf.MDsSHKVgP1UpBAkCkWKcitDQZ2g1NNW', NULL, 'Telegram', '+201141490066', 1, 0, 1, NULL, '2019-02-24 15:21:50', NULL, NULL, 0, '197.192.13.68, 162.158.150.72', NULL),
(820, 'gloria', 'josephchimezie63@yahoo.com', '$2y$10$CuxKHbIwDyKnB9SPvVHxPOJPJQWz1Wl0d/5Gmp8YfMBJmP/jjMe6i', NULL, 'Telegram', '+2348035930682', 1, 0, 1, NULL, '2019-02-24 23:32:49', NULL, NULL, 0, '41.190.3.204, 172.69.14.30', NULL),
(821, 'Chiedu', 'ezeprecious@yahoo.com', '$2y$10$keea1.g/3lqj66r6tl69KePM1Wd/vLoxawszlnFBW2gboJ7uZhZJa', NULL, 'Telegram', '+2348100557967', 1, 0, 1, NULL, '2019-02-25 02:22:52', NULL, NULL, 0, '105.112.16.137, 172.69.14.12', NULL),
(822, 'Houssame Selmani', 'houssame.selmani@gmail.com', '$2y$10$/fpJo0ueTg6SI9HSO1Q7kOmmlxphv3ttww.kg/UOU656FF2vYiQRi', NULL, 'Telegram', '+212 633423312', 1, 0, 1, NULL, '2019-02-25 02:48:38', NULL, NULL, 0, '105.154.214.240, 162.158.150.28', NULL),
(823, 'Spiros griparis', 'sgryparis@gmail.com', '$2y$10$QdgHO7Cd5MSaCU69t7bk/eVOAM2qBgdczWklN8Gh7UIIqcu.bFqvW', NULL, 'Telegram', '+306977205478', 1, 0, 1, NULL, '2019-02-25 03:36:53', NULL, NULL, 0, '79.107.179.98, 172.68.63.37', NULL),
(824, 'Belaihou ayoub', 'ayoubbelaihou99@gmail.com', '$2y$10$Ue3J8hM77cIq628zWZDwPehsnsjD4C/H/hkpoXFm1/W0plcrOTLD6', NULL, 'Telegram', '+212637883691', 1, 0, 1, NULL, '2019-02-25 04:38:23', NULL, NULL, 0, '105.147.231.193, 162.158.150.72', NULL),
(825, 'Atiemie Gift', 'mofe518@gmail.com', '$2y$10$m7.1Lof27C5rC9XzmwXUO.mnzhqf.dbxQVXriUk.rxIECsvsYtXay', NULL, 'Telegram', '+2349095878059', 1, 0, 1, NULL, '2019-02-25 12:42:31', NULL, NULL, 0, '105.112.36.186, 172.69.14.30', NULL),
(826, 'jawad', 'jawadlahzil@gmail.com', '$2y$10$lycGOmor6HWAcH9mRx./HOtgZPvZtKp2CyeBpIpPmJtcZ1L6y.3gq', NULL, 'Telegram', '(+212)632170009', 1, 0, 1, NULL, '2019-02-25 13:25:46', NULL, NULL, 0, '102.98.120.10, 172.68.165.24', NULL),
(827, 'Kalema isaac', 'kalemaisaac2014@gmail.com', '$2y$10$UA9/7KpiIXa//Tp3XJlGf.7.WFt/oyzrbAgq4hZthmG6LxitlTQvW', NULL, 'Telegram', '+2560703224127', 1, 0, 1, NULL, '2019-02-25 13:53:04', NULL, NULL, 0, '154.225.83.126, 162.158.98.6', NULL),
(828, 'Chathushka', 'kevinmarsh411@gmail.com', '$2y$10$EWBd76DOfsILzMDyS/.fBeRd0tXsb6Hln4YK3Y9x536blbekPXtp.', NULL, 'Telegram', '+94716009570', 1, 0, 1, NULL, '2019-02-25 19:36:51', NULL, NULL, 0, '43.250.242.38, 172.68.146.58', NULL),
(829, 'Andrew Epal', 'andrewanthonyepal26@gmail.com', '$2y$10$k2DSDCODCgHmYvJ2d3hIseYn.LnhqUrPTB4.fJeaqRHlH5GSpZRK.', NULL, 'Telegram', '+63 9672270974', 1, 0, 1, NULL, '2019-02-25 21:17:09', NULL, NULL, 0, '110.54.149.217, 162.158.138.6', NULL),
(830, 'chinedu ikemelu', 'chineduboy1000@gmail.com', '$2y$10$pLShZUhJRovkcBnhegGaeOd5H.QQviaLfLuS6cHamaW1.4hibiXUO', NULL, 'Telegram', '2348173172033', 1, 0, 1, NULL, '2019-02-25 23:43:35', NULL, NULL, 0, '197.210.45.229,141.0.12.233, 141.101.77.250', NULL),
(831, 'Phextuz', 'Phextuzdpo@gmail.com', '$2y$10$uQ8UAIWySN2YoIKrO0AIA.DiXlqZtRfcbcro2Xvm/QSB/DZN1kO5u', NULL, 'Telegram', '+2348137950876', 1, 0, 1, NULL, '2019-02-26 05:46:08', NULL, NULL, 0, '197.210.226.202, 141.101.104.103', NULL),
(832, 'Elizabeth Wambui', 'lizchina25@gmail.com', '$2y$10$YTVDInF8wk6KRWWu.UkoB.Vkw3iRmnit4bGHdhjo.XmYLz2MPWjQW', NULL, 'Telegram', '+254707920149', 1, 0, 1, 'NOLyPlQG6ZnFDAOryXpExopnum5e17MpoyJT3GfjrV2bigovKZLyxLBbvLVO', '2019-02-26 11:21:20', '2019-04-11 16:41:28', NULL, 0, '41.90.54.165, 162.158.42.24', 1),
(833, 'Boaz Mkemwa', 'mkemwa31@gmail.com', '$2y$10$u9tzUJoZRdpn/LtKiZOYtOmVYwFW2TV/OgC1HJAQ6FDR27a4d7bh2', NULL, 'Telegram', '0684310833', 1, 0, 1, NULL, '2019-02-26 19:58:20', NULL, NULL, 0, '156.156.77.90, 162.158.42.38', NULL),
(834, 'Felix chiungo', 'fchiungo150@gmail.com', '$2y$10$1DfymSTOyfknn9v/3/2uYuBURdYMQQZXRPGRVH0pcbETlwAOdhq6C', NULL, 'Telegram', '+258826959767', 1, 0, 1, NULL, '2019-02-26 21:31:18', NULL, NULL, 0, '197.235.71.164, 197.234.242.30', NULL),
(835, 'Mouhcine moumen', 'mohcine.moumen@gmail.com', '$2y$10$uu0Jf9g7ngukXhVaE8k3du8r5pOpMQtptWufioz6jT0Ro.YL7477C', NULL, 'Telegram', '212687920316', 1, 0, 1, NULL, '2019-02-26 21:57:29', NULL, NULL, 0, '196.86.163.202, 162.158.150.42', NULL),
(836, 'Peter Moses', 'creativewealth48info@gmail.com', '$2y$10$k8vWNdy3pc/vIDUhbg5rUutoP71ABndZN5DOGACRWDN1hBqP8bEDW', NULL, 'Telegram', '+2347037543967', 1, 0, 1, NULL, '2019-02-27 02:27:09', NULL, NULL, 0, '197.210.44.23, 172.69.14.18', NULL),
(837, 'Agus Sutrisno', 'agussutrisno12345678@gmail.com', '$2y$10$pAbNj5WT8G6053/r0H7qseMjHRgk88/6OGfW.cLrBtol2vHm466qa', NULL, 'Telegram', '+6285235927401', 1, 0, 1, 'HHDSbLLPuEzIj0YulIPhWOUx6weXO8T9Qnp8o0wTscfeyaKNzZPuzdAS3uah', '2019-02-27 17:52:28', NULL, NULL, 0, '202.80.214.106, 162.158.166.7', NULL),
(838, 'Noellelion Ntambue', 'ntambuekatomponoel@gmail.com', '$2y$10$TFx1M3c3MmVol1U4.z1qXu3K9H48uNn0WN1nPiQR9Pm9X7D5s8prW', NULL, 'Telegram', '+233892928386', 1, 0, 1, NULL, '2019-02-28 00:55:50', NULL, NULL, 0, '169.159.212.10, 197.234.242.30', NULL),
(839, 'Mohamaed Aadan hassan', 'maxamedaadan1996@gmail.com', '$2y$10$x7BrYeffWJZRJ9XE1bmy9uMa0P9sCnr6v.B.4svSUgXKNT.WI87ra', NULL, 'Telegram', '00252615758897', 1, 0, 1, NULL, '2019-02-28 08:25:46', NULL, NULL, 0, '197.220.84.8, 197.234.242.30', NULL),
(840, 'Ashit Chakraborty', 'Ashit71bd@gmail.com', '$2y$10$K/oqgSJ0FXHLWT06doXOe.2mGOffYfUcuhJQ.nx3/lUmwwc4oEyiS', NULL, 'Telegram', '+8801784434144', 1, 0, 1, 'qMCI3PfUcKB71PmXP6nNDtWYP7JMlfVrBsOc7eGxC4vOshq0YapiUnGLkTYM', '2019-02-28 14:19:26', '2019-04-07 06:11:17', NULL, 0, '45.116.248.22, 162.158.166.79', 1),
(841, 'Amir bashir ahmed', 'amirfx18@gmail.com', '$2y$10$iBaKwev0inbnhHqZco84eeI0DE8kWX95WthUerx8jGyjRCphCpYZe', NULL, 'Telegram', '+252615952299', 1, 0, 1, NULL, '2019-03-01 00:07:17', NULL, NULL, 0, '41.78.72.13, 197.234.242.144', NULL),
(842, 'Ahmed Yusuf Said', 'dheereeye10@gmail.com', '$2y$10$ISYfIkhONb3oxjQjaGePHeoZzI.VS63QpAAaVSZedGY53slESRfD6', NULL, 'Telegram', '+252907799061', 1, 0, 1, NULL, '2019-03-01 02:20:26', NULL, NULL, 0, '41.223.109.50, 162.158.30.24', NULL),
(843, 'Pernel', 'nyrelfresh@gmail.com', '$2y$10$mNl2QMMuVdXlO6d1U7XhP.NbSi3nYJGkUIyZR9Wbpxn69M0XH6M5O', NULL, 'Telegram', '+221782569556', 1, 0, 1, NULL, '2019-03-01 06:57:18', NULL, NULL, 0, '154.124.197.7, 172.69.54.5', NULL),
(844, 'Hassan Ahmed Yusuf', 'hassanyu60@gmail.com', '$2y$10$BwmBbg/swoQwccD7tdby6.y.AOswHJn8Pe06CZb2l7TZIkKDQ9axu', NULL, 'Telegram', '00252618070790', 1, 0, 1, NULL, '2019-03-01 10:56:10', NULL, NULL, 0, '197.220.84.7, 197.234.242.174', NULL),
(845, 'Swarye', 'Swarye27@gmail.com', '$2y$10$EpjQyBE7m81pYxBV4GW9ju0mdJqmzrPDypr8XJLBrFCC/qsjXCf36', NULL, 'Telegram', '+959790986880', 1, 0, 1, NULL, '2019-03-02 08:35:03', NULL, NULL, 0, '1.47.228.78, 162.158.204.170', NULL),
(846, 'Suldaankafx', 'momerwali@gmail.com', '$2y$10$0or3Lknr3isCpdPZkXnAoeKt9Q/24VPMTW7ExWNoq8ew5YPRBwoQC', NULL, 'Telegram', '+252633014335', 1, 0, 1, NULL, '2019-03-02 16:31:12', NULL, NULL, 0, '154.115.243.3, 162.158.150.12', NULL),
(847, 'Samir Auwalu Dudu', 'samirdudugk@gmail.com', '$2y$10$nm/Px/P./XSNrGiPCEmGeO9f4Sf6HPTKDupX4jHAz1xUW.DqUYa0i', NULL, 'Telegram', '+2347035896905', 1, 0, 1, 'P0m1cT6TvoBJmHGfqm8pphZM7ujYfJutvbwhFyff6MJBAKW4nUntZE7qRY0G', '2019-03-02 19:59:44', '2019-04-17 11:13:38', NULL, 0, '41.203.72.148, 172.69.54.203', 1),
(848, 'Luo zulu', 'luo.zulu70@gmail.com', '$2y$10$3tBKLoLn9/SrIf00Yb7NlOutQBeCUXkk1yYcz2GaIHeG5uMliycPa', NULL, 'Telegram', '+260976717955', 1, 0, 1, NULL, '2019-03-03 02:29:16', NULL, NULL, 0, '102.149.222.121,107.167.109.159, 162.158.78.228', NULL),
(849, 'Damian mlongwa', 'mayangaemmsnuel49@gmail.com', '$2y$10$DHLelkZ6yQSKKdP6UfhVHOJpNlIoSFOwz7kLUsZ.lNFEvBL4h5Xeq', NULL, 'Telegram', '(+255)757788194', 1, 0, 1, NULL, '2019-03-03 11:03:55', NULL, NULL, 0, '41.59.81.184, 172.69.226.167', NULL),
(850, 'Matthew Ogwu', 'owojopraise@gmail.com', '$2y$10$RhSeGw1IDf2dHoqfbU78NOY/a0Me/CYgd0CweT5epca/3OsD87GTa', NULL, 'Telegram', '+2348069189486', 1, 0, 1, NULL, '2019-03-03 17:32:41', NULL, NULL, 0, '197.210.226.94, 141.101.77.100', NULL),
(851, 'Godfrey kaijage', 'snipertzkai@gmail.com', '$2y$10$qEmaf/ywwxy7YBumwxHdle7NMgjmL/LYIuI2186oX.L5eSFhcMNri', NULL, 'Telegram', '+25576120778', 1, 0, 1, NULL, '2019-03-03 18:14:06', NULL, NULL, 0, '41.222.180.165, 197.234.242.174', NULL),
(852, 'Mudassir', 'mudassirakram73@gmail.com', '$2y$10$2VSeQF5.T0H1OBwWH53N0u35wVV2XfJgv.Gl77USWYlE1C5RyprIG', NULL, 'Telegram', '(+92)3418428055', 1, 0, 1, NULL, '2019-03-03 18:17:23', NULL, NULL, 0, '103.7.78.177, 172.68.144.123', NULL),
(853, 'UBA CHRISTOPHER JUNIOR', 'ubatradingforex@gmail.com', '$2y$10$eOGn6UHx8oH2SsQV6EO5juOhUm1xz7Rh/03CZzIUQO.gdhIqMHnU.', NULL, 'Telegram', '+2348188920807', 1, 0, 1, NULL, '2019-03-03 19:07:28', NULL, NULL, 0, '197.210.44.214, 172.69.14.30', NULL),
(854, 'Likhabiso Phamotse', 'modulelaphamotse@gmail.com', '$2y$10$oawrWZWQ5b5lSto5IM7IB.UtZbY0JtgCXl3aonhRpwI4halDuFsmC', NULL, 'Telegram', '+27737766505', 1, 0, 1, NULL, '2019-03-03 19:26:16', NULL, NULL, 0, '197.254.188.62, 197.234.242.114', NULL),
(855, 'Akeem', 'akeem@alibert-group.com', '$2y$10$q1h8eEVQfsgqHQxQMrWENeXd.DxJuh.Y/V.631Y1Tb9D9lmCz6H2W', NULL, 'Telegram', '+23408160839642', 1, 0, 1, 'geNgZQHDocZfXPkdGeqEDMfCaGxG0e2qdonrSXPHc8I1GbPtrdkJz7pd3fSG', '2019-03-03 19:28:58', NULL, NULL, 0, '41.190.30.214, 172.69.14.30', NULL),
(856, 'BABIO Anasse', 'anassebabio@gmail.com', '$2y$10$ezOVHtqjbPd4rL4v2i.scOfl83EMILTa4S6YOx0VQvHHLYTS3.Z5m', NULL, 'Telegram', '+224625954218', 1, 0, 1, NULL, '2019-03-03 20:10:32', NULL, NULL, 0, '197.149.220.76, 172.69.226.49', NULL),
(857, 'Gilbert macarde', 'mmacardegilbert@gmail.com', '$2y$10$vUUN4Dah2f.IV0MC5GcgMuYCmVAnKb0Vuer5qnEB9oTM70CbnTA8m', NULL, 'Telegram', '+233243132314', 1, 0, 1, NULL, '2019-03-04 10:33:21', NULL, NULL, 0, '154.160.7.22, 172.69.54.5', NULL),
(858, 'ventsislav georgiev', 'vencislavgeorgiev89@abv.bg', '$2y$10$zfpoKwpjEnaXdWNR9Oufjel1jCvWU6jGH4eK.bnYNTNoMV2UxU8G.', NULL, 'Telegram', '0896255858', 1, 0, 1, NULL, '2019-03-07 01:59:25', NULL, NULL, 0, '94.156.198.197, 162.158.89.63', NULL),
(859, 'TEBOGO', 'juerltebogo909@gmail.com', '$2y$10$A75H8BDgzQghJedqcYebvu4EbEe4BYyuoIH9CpAjBXQEr.C5Q5E2u', NULL, 'Telegram', '+27722429458', 1, 0, 1, NULL, '2019-03-13 18:07:44', NULL, NULL, 0, '102.250.4.108, 197.234.242.174', NULL),
(860, 'Pheello Motale', 'pheellomotale@gmail.com', '$2y$10$Us0vlyu7shEzXaFr8YzTGOro1Up3o/sNYZiDdrt5fJMqznsDEBHJ6', NULL, 'Telegram', '+266 58906395', 1, 0, 1, '4p8gurueoy3kc4faq7i7DcLZA0K3H3Os1WbftKnNH24pBO8djq21Ld1fANNH', '2019-03-17 15:16:08', '2019-04-27 09:34:52', NULL, 0, '197.254.137.254, 197.234.242.174', 1),
(861, 'ADIL EL OMRI', 'abadnallh@gmail.com', '$2y$10$X4n6i3oxRt.jNSqH7Y/1ie6gcVDfDw6u4QeWvbY5PvKGD24vZfI86', NULL, 'Telegram', '+212770632981', 1, 0, 1, NULL, '2019-03-17 21:23:16', NULL, NULL, 0, '105.154.77.207, 162.158.150.132', NULL),
(862, 'Andrews owusu', 'kingnager@gmail.com', '$2y$10$ZmghApJfO0DMO8fOnkWpsuARSmG1x7cum9C8Y9Zxigk8R9UGHWSFm', NULL, 'Telegram', '+233240607282', 1, 0, 1, NULL, '2019-03-24 03:57:28', NULL, NULL, 0, '154.160.16.146, 141.101.77.250', NULL),
(863, 'Carmelo Lasso', 'carmelolasso@gmail.com', '$2y$10$WfvEhPJ5r98SoLEhY5Ig3OrXdtHTgdqeshSXN9A8tVzqeLJrckDFi', NULL, 'Telegram', '34644400134', 1, 0, 1, NULL, '2019-03-28 23:55:42', NULL, NULL, 0, '85.48.57.9, 108.162.229.114', NULL),
(864, 'Asif', 'amasif1989@gmail.com', '$2y$10$TZ10s9IN1GS3V6QOH3on1O99bW/2lWUzlL.zOHlxRiwcuE1D6Loyy', NULL, 'Telegram', '+8801647134854', 1, 0, 1, 'ufrEEdlYaPyGJlFGkCiEiLwNRYQACXbzhk0k4n33ikQHyg6BLiLLRjKtive7', '2019-03-31 22:07:10', NULL, NULL, 0, '103.242.23.188, 162.158.165.114', NULL),
(865, 'Adokpogeraud', 'Adokpogeraud@gmail.com', '$2y$10$v.EwAyUulagNSoCEqDBXVOLIQwcFATDNhkBCevEbAcyIttMqxspRq', NULL, 'Telegram', '22588183724', 1, 0, 1, NULL, '2019-04-01 14:43:20', NULL, NULL, 0, '154.234.241.193, 141.101.104.103', NULL),
(866, 'Hussein Ali Mohamed', 'xuskacalimaxamed@gmail.com', '$2y$10$Wyj/VJeo6vzfKrT3NwjDNebiDSN5PKImJbdmNG/9IC9CTMqsONaCC', NULL, 'Telegram', '+2520618674625', 1, 0, 1, NULL, '2019-04-01 15:01:54', NULL, NULL, 0, '41.78.72.133, 162.158.155.52', NULL),
(867, 'Moha', 'Mohabarre.199@gmail.com', '$2y$10$OCCU/dA15k9OshZatC6Yg..q2gUPVVnfbVaVXhQhKmJqIRpmQLB6W', NULL, 'Telegram', '252612335185', 1, 0, 1, NULL, '2019-04-01 15:12:30', NULL, NULL, 0, '197.220.84.7, 162.158.158.131', NULL),
(868, 'Safkat wasy siam', 'siamfx5622@gmail.com', '$2y$10$Pa0JmAINd5NzV0nwVHNXYex4.t5V7Zc3/27EYWCFKD3pOmCJxMNRq', NULL, 'Telegram', '01793631901', 1, 0, 1, NULL, '2019-04-01 15:55:39', NULL, NULL, 0, '119.30.35.136, 172.69.135.144', NULL),
(869, 'Shafie', 'Shafieawil905@gmail.com', '$2y$10$Qj4PaxEj3DNDXCyWPDwgYeMFwe5bZ6LYZABpuM..Ilq5MtGKPVFki', NULL, 'Telegram', '+905539452899', 1, 0, 1, NULL, '2019-04-01 16:55:49', NULL, NULL, 0, '78.165.133.184, 172.69.54.203', NULL),
(870, 'Tawfic Labaran', 'tawficlabaran@gmail.com', '$2y$10$NObhB9FN83sym.xQrLFhM.GZOQRKusBTOiYjsMWIQtj0NvBuaHinW', NULL, 'Telegram', '+233246436351', 1, 0, 1, NULL, '2019-04-01 17:09:50', NULL, NULL, 0, '41.66.202.113, 162.158.155.22', NULL),
(871, 'Hamid Hossain', 'hamidhossain1164222@gmail.com', '$2y$10$GcWk4PtP5rOW9ToDlMGBbu.XiEuUsyPENlEQGV3tNYl7rWsaI4mRi', NULL, 'Telegram', '01811182443', 1, 0, 1, NULL, '2019-04-01 17:20:04', NULL, NULL, 0, '103.242.23.187, 172.69.134.53', NULL),
(872, 'Mustapha Muhammad', 'treeplemy@gmail.com', '$2y$10$rP1wIiObugdQVs4sHJKuxOCM54PSdy79/cwHXHvRqxgNJehZn1Xne', NULL, 'Telegram', '+2348034204445', 1, 0, 1, NULL, '2019-04-01 17:35:35', NULL, NULL, 0, '105.112.121.78, 172.69.14.18', NULL),
(873, 'Herieli', 'herielikisanga1@gmail.com', '$2y$10$YGffdtxG3lylrA8q62vaIOlu8SUD8/ZVbSAg90L3HM8sICBnwVSse', NULL, 'Telegram', '+254706974584', 1, 0, 1, NULL, '2019-04-01 17:50:17', NULL, NULL, 0, '95.213.153.91, 172.68.246.130', NULL),
(874, 'Saad Farah', 'alitofarrahi2003@gmail.ma', '$2y$10$C2eyxPmu79VF.QmtcYEpDOAjDSYgo.ydYE0UXxQy2dSX9O77jVUQC', NULL, 'Telegram', '+212689479013', 1, 0, 1, NULL, '2019-04-01 19:06:20', NULL, NULL, 0, '102.79.219.173, 162.158.150.122', NULL),
(875, 'Sulaiman kamaldeen', 'sulaimankamaldeen1@gmail.com', '$2y$10$IKXvK0LO/WlOTqj14hR8Z.dWHLNlY07abGEGToQAbGtCzadIRy7J.', NULL, 'Telegram', '08061196889', 1, 0, 1, NULL, '2019-04-01 20:06:54', NULL, NULL, 0, '197.210.28.0, 172.69.14.12', NULL),
(876, 'Bruno Rehan', 'abrunorehan@gmail.com', '$2y$10$BWscjZSU8PSTWIHrfIQZHO3BHlDsYVnX3XAdbk/aHqTutCmuz.n3K', NULL, 'Telegram', '(+255)767124879', 1, 0, 1, NULL, '2019-04-01 20:12:15', NULL, NULL, 0, '41.59.81.38, 172.69.226.43', NULL),
(877, 'Jerry Johannes', 'jjhannes@gmail.com', '$2y$10$BqiBevCwZEP28MFCqMBwjeBuQ0wHUNZeC261Duf8IPkEiuJwRihc2', NULL, 'Telegram', '+264813810530', 1, 0, 1, NULL, '2019-04-01 21:33:07', NULL, NULL, 0, '105.232.120.62, 197.234.242.144', NULL),
(878, 'Said ali salad', 'scalisalaad22@gmail.com', '$2y$10$0Kti8sAWicj6UPTJyH8Yyeawiz23KwrkRdk2u1xKP2PG5mcsJOCwy', NULL, 'Telegram', '00252907688176', 1, 0, 1, NULL, '2019-04-01 21:56:06', NULL, NULL, 0, '41.223.109.55, 162.158.30.24', NULL),
(879, 'zakariye ahmed', 'zekiahmed893@gmail.com', '$2y$10$e5QvQAqEiONAvfNm.frhA.4/3WGvZt/t65nOS4Spac6GjsvcqG.XC', NULL, 'Telegram', '020932037001', 1, 0, 1, NULL, '2019-04-01 22:08:36', NULL, NULL, 0, '41.78.72.130, 141.101.98.240', NULL),
(880, 'Chereen', 'chereenhega96@gmail.com', '$2y$10$8pzXorT2/9njsjx9jVUbg.rFU3WjIs6n6J6qDrOhLqDUX0fiND5Qm', NULL, 'Telegram', '+264813675281', 1, 0, 1, NULL, '2019-04-01 22:55:22', NULL, NULL, 0, '105.232.91.24, 197.234.242.114', NULL),
(881, 'Eric Akomea', 'ericakomeah.ea@gmail.com', '$2y$10$JUIXZrG4GnbtOEtgBA5r1ez8RXiGjE7rMJHHd0hWUZMqiyv2Od.O6', NULL, 'Telegram', '+233 265271412', 1, 0, 1, NULL, '2019-04-01 23:18:56', NULL, NULL, 0, '41.215.172.224, 172.69.14.18', NULL),
(882, 'ERNEST UZOMAH', 'ernestuzomah@gmail.com', '$2y$10$hQkWDy.OdxxqaK03L/VA/OyoUpuQL5MQZkgzZFtTPZKcG/o6T2jHy', NULL, 'Telegram', '07087059037', 1, 0, 1, NULL, '2019-04-02 00:52:00', NULL, NULL, 0, '197.210.55.85, 172.69.55.12', NULL),
(883, 'Gillian chumong', 'darpips@gmail.com', '$2y$10$79lIhgYxZOMBPd/dlfDCaOSfubP5sEG/Bfo50Aqvdm4iyTH1i.Zp.', NULL, 'Telegram', '+255689660366', 1, 0, 1, NULL, '2019-04-02 01:29:52', NULL, NULL, 0, '41.59.81.83, 108.162.229.114', NULL),
(884, 'Chawapiwa Gaikai', 'asayameshack@gmail.com', '$2y$10$2wZRy29IFuw0Soj.MgktE.wHr2Peygkf0.tU6nUkxmR/3oUkz2qni', NULL, 'Telegram', '71395469', 1, 0, 1, NULL, '2019-04-02 02:48:18', NULL, NULL, 0, '41.79.137.46, 197.234.242.30', NULL),
(885, 'Mahad shire egale', 'egalle1114@gmail.com', '$2y$10$jmaVNr1hBgY5.FWiNHhY6.V2uYfmHbrlQe.C0HFK.wevJqw2wFzHG', NULL, 'Telegram', '+252616700001', 1, 0, 1, NULL, '2019-04-02 07:15:41', NULL, NULL, 0, '197.220.84.4, 141.101.98.168', NULL),
(886, 'Abdirizak Ali Adam', 'istiila9@gmail.com', '$2y$10$PrAdR09Tifr6mIxXtL0WCOdiTnPGzRA8lC0fzdksoIlPaJZFqpk96', NULL, 'Telegram', '+252615518392', 1, 0, 1, NULL, '2019-04-02 08:07:08', NULL, NULL, 0, '197.231.202.64, 141.101.107.212', NULL),
(887, 'Sixtus Mohau Nkopi', 'mnkopi@gmail.com', '$2y$10$GAdg2gRwcVO.jZH2Tc23v.EeDcL38DBz1u9AlFmE8MTQRlhhVdhBW', NULL, 'Telegram', '±26659033315', 1, 0, 1, NULL, '2019-04-02 08:43:12', NULL, NULL, 0, '197.254.184.199, 197.234.242.30', NULL),
(888, 'david kahambuee', 'kahambueedavid81@gmail.com', '$2y$10$kzDgdH3UM3/xGy93xoTqOOoCirpkwJFZt2VG757cL18H0HCgBxIDu', NULL, 'Telegram', '+264813601416', 1, 0, 1, NULL, '2019-04-02 09:01:42', NULL, NULL, 0, '105.232.255.100, 197.234.242.174', NULL),
(889, 'Methusela Welwel', 'chrisssmenan@gmail.com', '$2y$10$KdX1r2ynsYAPmZVIJZ4gk.gwsZpHSvAff0sPs3XUtahRaY8amEOlW', NULL, 'Telegram', '0652541030', 1, 0, 1, NULL, '2019-04-02 09:34:13', NULL, NULL, 0, '197.250.231.107, 197.234.242.30', NULL),
(890, 'Abel MUNONYARA', 'ablmun@yahoo.co.uk', '$2y$10$fEebvPEJcrpayl5IsiZ/uuvfdjoosGzovY0a0kHBfl/aXDsMx7nF6', NULL, 'Telegram', '(+44)7453984889', 1, 0, 1, 'P4oytKpcZL3KXiqw3Uqzs7lv1nB9bKXoi9XSDXLayguuNGQpeJey16q6c9M1', '2019-04-02 10:30:36', '2019-04-02 11:16:10', NULL, 0, '94.175.137.161, 162.158.154.105', 1),
(891, 'Amir Ahmed', 'imranzaki1000@gmail.com', '$2y$10$iucfbTqZ7NI2IFZgx8enaOoDGpe4ppe4DPaTi0H2YZC2CzCVFZE3.', NULL, 'Telegram', '+252618329655', 1, 0, 1, NULL, '2019-04-02 11:20:16', NULL, NULL, 0, '154.118.242.231, 162.158.154.105', NULL),
(892, 'Abdulaiabdulmalikdojia', 'abdulaiabdulmalik5@gmail.com', '$2y$10$BkOn.5zC5oOiKh48.7Q5cOnjvlFEvhkSo6bgNOPPNkgJh43iwK/MS', NULL, 'Telegram', '0548165888', 1, 0, 1, NULL, '2019-04-02 14:25:11', NULL, NULL, 0, '41.66.202.36, 162.158.155.178', NULL),
(893, 'Jamal', 'jamalperhof65@gmail.com', '$2y$10$FQqayFd33MejCfg2PPTG0Oq8lbV12nykArMmXXAt2oJNO/xju7jM6', NULL, 'Telegram', '+252634691949', 1, 0, 1, NULL, '2019-04-02 14:38:21', NULL, NULL, 0, '197.157.244.206, 172.68.63.61', NULL),
(894, 'mohamed muse', 'mkhadar735@gmail.com', '$2y$10$zK0pk1seLuvAZwauEX4QdOwSLdrHAQWEtYSAMt6eg8VcdBhxSV1bu', NULL, 'Telegram', '00252659340275', 1, 0, 1, NULL, '2019-04-02 14:45:21', NULL, NULL, 0, '154.115.243.3, 162.158.150.28', NULL),
(895, 'Nicephore Baudelaire BALOGOUN', 'balogounbaudelaire@gmail.com', '$2y$10$/TJKuVrsibDkvvqvagXSDuXRKC3rJQ8XbA3AgvwRKUZsQpXuCJaAe', NULL, 'Telegram', '+22961082494', 1, 0, 1, NULL, '2019-04-02 14:58:09', NULL, NULL, 0, '41.138.89.214, 172.68.94.222', NULL),
(896, 'Colman s mtui', 'colmanmtui96@gmail.com', '$2y$10$Ku5TPmaaiiwL/jsEd4Q/oOfl64zC223HiqoLEtnWRVUb4B356r0Le', NULL, 'Telegram', '+255693409410', 1, 0, 1, NULL, '2019-04-02 17:17:32', NULL, NULL, 0, '41.75.223.95, 162.158.42.50', NULL),
(897, 'Richard', 'chaplain_rick@hotmail.com', '$2y$10$NpuPTGm/l.I.zUcjXrdnZeroLBtoJUXwJO7iwSSD/egWsi8JXaGOe', NULL, 'Telegram', '19377338905', 1, 0, 1, NULL, '2019-04-02 17:46:07', NULL, NULL, 0, '75.185.210.49, 172.69.63.63', NULL),
(898, 'Richman', 'folawiyo2014@gmail.com', '$2y$10$NZt7oda/CuF5lTjZXHNiL.dvfkwUj0/R8XuzQGL8RUTF7ZrG0PSWe', NULL, 'Telegram', '08022216272', 1, 0, 1, NULL, '2019-04-02 19:33:39', NULL, NULL, 0, '105.112.21.208, 172.69.14.12', NULL),
(899, 'Posholi Maxwell Posholi', 'poshposholi@yahoo.com', '$2y$10$8x8y.Ryqo10yyEJ1Mk4gPO2WInosVzaUur7hB66b831o.GNNGS4ZG', NULL, 'Telegram', '+26659089148', 1, 0, 1, NULL, '2019-04-02 19:43:28', NULL, NULL, 0, '129.232.81.69, 197.234.242.144', NULL),
(900, 'Yousaf', 'yousaf.momand71@gmail.com', '$2y$10$h5rgbagHo3PDMM3atPiHl.9mDRnJB/64LFMI.Pd4uws94zmSl1yhO', NULL, 'Telegram', '0093707072141', 1, 0, 1, NULL, '2019-04-02 21:19:55', NULL, NULL, 0, '180.222.139.151, 172.69.55.174', NULL),
(901, 'Pheello Motale', 'kefuoeramphoko2018@gmail.com', '$2y$10$BpYNJIdA8KPiznGXLOJdhu9IZ4DYmcBsbtH.w/jNSiorCh.qR28i6', NULL, 'Telegram', '(+266)58906395', 1, 0, 1, NULL, '2019-04-02 21:56:53', NULL, NULL, 0, '129.232.85.75, 197.234.242.114', NULL);
INSERT INTO `users` (`id`, `name`, `email`, `password`, `avatar`, `signal_option`, `phone`, `user_type`, `verified`, `active`, `remember_token`, `created_at`, `updated_at`, `role`, `notify`, `ip_address`, `first_plan`) VALUES
(902, 'Ahmed mohamed', 'axmadm47@gmail.com', '$2y$10$Usym5xkA7h3i0CwuGSAKKux/2Qw7jucx8BJB35HVfZfGjFukGOXwK', NULL, 'Telegram', '(+252)618455347', 1, 0, 1, NULL, '2019-04-02 21:59:11', NULL, NULL, 0, '197.220.84.4, 162.158.155.244', NULL),
(903, 'Ahmed', 'axdfwert@gmail.com', '$2y$10$/4m6JUxzBg6HSt5Rccn2gONM6KG5mYu/sHtVg4porcchkDDlkLCQG', NULL, 'Telegram', '218921255126', 1, 0, 1, NULL, '2019-04-02 22:44:12', NULL, NULL, 0, '41.253.95.1, 172.68.63.37', NULL),
(904, 'Simson Shinene', 'shnesjob@gmail.com', '$2y$10$cwZCm/fEXKEkWWf0ffNQ6.GcxFPWcx5EcKKYVHdpfu9MYvN/yMvkm', NULL, 'Telegram', '+264812127056', 1, 0, 1, NULL, '2019-04-02 23:55:06', NULL, NULL, 0, '105.232.70.41, 197.234.242.30', NULL),
(905, 'Tokla Ping', 'toklaping@gmail.com', '$2y$10$rXS4eJ8AmmANqePBTEV7A.14aYcjgyb77hSwAg2BNdtcazcj2Jbpu', NULL, 'Telegram', '+85512296168', 1, 0, 1, NULL, '2019-04-03 00:49:02', '2019-04-03 00:50:30', NULL, 0, '117.20.115.162, 172.69.82.6', 1),
(906, 'Frank Chirisa', 'frankchirisa@gmail.com', '$2y$10$Gz6x/t70Rj3Wr9BVCRlsgORNmTZ3A7P3LwsfdZfLfAUZO0s45Swx2', NULL, 'Telegram', '+263713001240', 1, 0, 1, NULL, '2019-04-03 01:24:33', NULL, NULL, 0, '196.41.88.249,82.145.211.154, 141.101.104.85', NULL),
(907, 'Mahad Mohamed', 'Engmahad3@gmail.com', '$2y$10$uyjQtqgQKIxmRTdJ89/zAO1mRGLE7Zyo/5AvFZj9f55t4QYIgnvwe', NULL, 'Telegram', '+252634916190', 1, 0, 1, NULL, '2019-04-03 03:36:58', NULL, NULL, 0, '197.231.203.101, 172.68.62.36', NULL),
(908, 'rdde dissanayaka', 'rddedulan84@gmail.com', '$2y$10$/V1sy7PDHNSp75nwrbXDN.oBZN/T7oCaNfAM38lEHWvKejnUeS8F6', NULL, 'Telegram', '0717926515', 1, 0, 1, NULL, '2019-04-03 05:45:34', NULL, NULL, 0, '43.250.242.9, 141.101.88.240', NULL),
(909, 'Johannes', 'joamunyela@ymail.com', '$2y$10$sIXPyquM5kkehZ.SFZqQM.lmt1DAZR70JsbLoOd.Vn07E182CFGn.', NULL, 'Telegram', '+264812497156', 1, 0, 1, NULL, '2019-04-03 10:02:50', NULL, NULL, 0, '105.232.93.254, 197.234.242.114', NULL),
(910, 'adriano kelvio da rocha dantas', 'adrianokelvio@gmail.com', '$2y$10$PPfjykY0Utd4aInx6/xiue32ulVDw.zF6.tNdLxuPq.SCpgfFGsFm', NULL, 'Telegram', '+5565999154075', 1, 0, 1, 'UXhyxciW4VvsW4rKVrv4yLQtuaLWlrGGJQpwtef9Fz71UDqf56az237ZLHPc', '2019-04-03 11:12:47', NULL, NULL, 0, '177.200.188.218, 172.68.24.12', NULL),
(911, 'Nathan Williams', 'nathwills1978@gmail.com', '$2y$10$JSQ901Z2U2iYgfqA/mR8ueR5N.ugFt24wmnWeWJibja2tvaFCfxFu', NULL, 'Telegram', '+2348038115518', 1, 0, 1, NULL, '2019-04-03 14:36:23', NULL, NULL, 0, '66.206.121.12, 108.162.219.210', NULL),
(912, 'Liban abdi', 'libanabdi0.04@gmail.com', '$2y$10$vAa55GMO417gVmy9WWkTKOyIBO3xPkJicUjA5DGU47lRf.LiswUYq', NULL, 'Telegram', '+252618535914', 1, 0, 1, NULL, '2019-04-03 17:37:09', NULL, NULL, 0, '197.231.202.65, 162.158.155.154', NULL),
(913, 'bahi', 'bahihoggas1998@gmail.com', '$2y$10$SRMVSDU.fDUMRDAow.p7cOprBIvQxdxIVf6sgyPRSTCmUiwBHHZT2', NULL, 'Telegram', '+21368547895', 1, 0, 1, NULL, '2019-04-03 17:39:23', '2019-04-03 17:39:54', NULL, 0, '105.105.180.177, 162.158.150.132', 1),
(914, 'Hassan liban ahmed', 'hassantest556@gmail.com', '$2y$10$mtRmq/Wn3H3PdcdL2Z/.jumw6B0xlOCrI5GmuBDXWdczuWngMd9.O', NULL, 'Telegram', '0618454257', 1, 0, 1, NULL, '2019-04-03 21:50:39', NULL, NULL, 0, '197.220.84.3, 162.158.155.22', NULL),
(915, 'NP Mishra', 'npmishra01@gmail.com', '$2y$10$H/81Y/Pzxw7kLWVwKRKHie22SHrDyBD.d/dYp.300xAcQC5JRm4ia', NULL, 'Telegram', '+919938991388', 1, 0, 1, NULL, '2019-04-03 21:51:23', NULL, NULL, 0, '2401:4900:3131:34bb:cca5:da76:73d4:53c2, 162.158.155.178', NULL),
(916, 'gatherru stephen', 'sgatherru@gmail.com', '$2y$10$5Fc5Twu0DEklasyaw.rChORGrQ28x8SYumlokJ2fuoNi0ir8zg6ze', NULL, 'Telegram', '+254708074462', 1, 0, 1, NULL, '2019-04-03 22:37:53', NULL, NULL, 0, '105.55.37.120, 162.158.42.18', NULL),
(917, 'Mphoso Tomo', 'mphosotomo691@gmail.com', '$2y$10$30mqk8RDBJdH4BUvfvKO.uFJBWqZSp3Yy/dEmnQHIxYDMF3.3s5Xa', NULL, 'Telegram', '±26662337333', 1, 0, 1, NULL, '2019-04-03 23:21:26', NULL, NULL, 0, '129.232.80.228, 197.234.242.144', NULL),
(918, 'Frank muzimba', 'muzimbafrank@gmail.com', '$2y$10$BBcHoXBZR8xKzdw3KWgouOJDg2uRBulfKOOUen0I/Gzkdx.pUJeiq', NULL, 'Telegram', '+213553289416', 1, 0, 1, 'Q18Q9xstSQdQuBvU37RthhPwWXOcE5IowV9fUSI45hs3EYNlHOmpg8bWKtA0', '2019-04-04 00:59:10', NULL, NULL, 0, '154.121.251.187, 141.101.69.227', NULL),
(919, 'Motale Noosi', 'norzivic@gmail.com', '$2y$10$VbGNw7XeDQXrS.zXzYiujuVS72YmWa.mNr6vGhTkEDG1kxWiyGT96', NULL, 'Telegram', '(+266) 62217158', 1, 0, 1, NULL, '2019-04-04 01:12:38', NULL, NULL, 0, '129.232.93.46, 197.234.242.114', NULL),
(920, 'Mthokozisi Dlamini', 'andy.dlamini@gmail.com', '$2y$10$gDoRIoHJQAaMPbvHVUswXuRfDAL0lQjwthrRAo4OznOizCrUUL8Jq', NULL, 'Telegram', '+26876473600', 1, 0, 1, NULL, '2019-04-04 01:22:37', NULL, NULL, 0, '41.211.36.17, 197.234.242.114', NULL),
(921, 'Luddy Iskandar', 'Luddyiskandar001@gmail.com', '$2y$10$2kPwmoZF6PYXsC2PkBopdOka8iTCrPVZ5QgwAUNX6k3lUPVp2nAfi', NULL, 'Telegram', '+6282185555351', 1, 0, 1, NULL, '2019-04-04 06:51:46', NULL, NULL, 0, '140.213.71.208, 108.162.226.13', NULL),
(922, 'Yousuf Abbas', 'malik.group4462633@gmail.com', '$2y$10$CQj74qBezO24QCBGKwxGK.2EEoiSv1UPk/mELsPAAbwXcWgeei4za', NULL, 'Telegram', '+923004462633', 1, 0, 1, NULL, '2019-04-04 07:13:55', NULL, NULL, 0, '119.160.119.153, 162.158.118.216', NULL),
(923, 'Abdiaziz Mohamud', 'abdiazizm850@gmail.com', '$2y$10$ongLSnYUS5V7laY7ghClLewYyUzZT37wfUmuHK2OpAH6fPyGYRb56', NULL, 'Telegram', '+905428909549', 1, 0, 1, NULL, '2019-04-04 07:27:54', '2019-04-04 07:30:19', NULL, 0, '88.241.41.118, 141.101.104.85', 1),
(924, 'Guillermo Benito Rodríguez Lorio', 'gbrlthaboss@gmail.com', '$2y$10$7F7WLTOC4KOeKyRlMe3Or.FPuEx/E7XbBBW8/pcD/tedmkgMjsDd.', NULL, 'Telegram', '0050588979529', 1, 0, 1, NULL, '2019-04-04 07:40:31', NULL, NULL, 0, '186.77.196.234, 172.68.78.108', NULL),
(925, 'Abdirisak Mohamed Osman', 'abdirisak420@gmail.com', '$2y$10$OvGvnua38DunHwBrmPQoRO4vdFljgieQeyI2j5ezMn0Vx8Bj65Pd.', NULL, 'Telegram', '+252615538829', 1, 0, 1, NULL, '2019-04-04 09:11:02', NULL, NULL, 0, '41.78.74.28, 162.158.154.105', NULL),
(926, 'caydaruus', 'caydaruus20188@gmail.com', '$2y$10$Th9QV6iY1hY9gjWkDe6HeeruCwXHiKx2C8RCV7JRciKmm62PgCzmi', NULL, 'Telegram', '2520633218004', 1, 0, 1, NULL, '2019-04-04 10:38:00', NULL, NULL, 0, '197.157.244.206, 172.68.63.37', NULL),
(927, 'Sekhonyana sekhonyana', 'sekhonyanasekhonyana@gmail.com', '$2y$10$YeSyRxiHngdbfjDzT/zG6uBGkNtXTaXUFhtRaIcicoz7EcZ7cdXje', NULL, 'Telegram', '+26658678678', 1, 0, 1, NULL, '2019-04-04 11:00:43', NULL, NULL, 0, '197.254.141.89, 197.234.242.144', NULL),
(928, 'Tariq Saeed', 'tariqsaeed840@gmail.com', '$2y$10$hPd/iDRrZRLORFXNZ/nmDekhPQeRD7ecHqJ/87nLFt7iiLBfhaMma', NULL, 'Telegram', '03117071991', 1, 0, 1, NULL, '2019-04-04 11:10:30', NULL, NULL, 0, '103.255.4.10, 162.158.118.222', NULL),
(929, 'Blessing Seane', 'seaneblessing@yahoo.com', '$2y$10$SYaZNUoTdELRg.EWXTDK5uEmI3pLdWXvp/H4SeKgmMr335nPjRXxa', NULL, 'Telegram', '+26771694118', 1, 0, 1, NULL, '2019-04-04 11:25:23', NULL, NULL, 0, '41.190.245.7, 172.69.226.49', NULL),
(930, 'Ninshan Sivananthaselvam', 'ninshan91@gmail.com', '$2y$10$xgiTQaPBCqmSuDOdPhZqN.H9HvNVhNgnOlOCUtTXMrFfhYqqSD48q', NULL, 'Telegram', '+94774671799', 1, 0, 1, 'VgQ3wqYAzMEf8g2W38Nho95EEP8kroPDNoGVlOwdY4EZ7J5yZQI2S68X7QxA', '2019-04-04 12:01:34', '2019-04-04 13:20:04', NULL, 0, '175.157.52.242, 162.158.93.29', 1),
(931, 'linda', 'linda2stepsmkoko@gmail.com', '$2y$10$E/7gI.bWeKo2RgQBL.sfN.EhMxhhFM4CwwbFtGrZgt9Mf6CIvfVnu', NULL, 'Telegram', '+26876719601', 1, 0, 1, NULL, '2019-04-04 13:34:08', NULL, NULL, 0, '165.73.133.63, 197.234.242.114', NULL),
(932, 'Edgar', 'malemasseurzim@gmail.com', '$2y$10$WrdFk5PkQ7ZjstsQBpBfp.AuWerh0zOhLVrpz7I7fgaUvuFqbbqkO', NULL, 'Telegram', '+263776867535', 1, 0, 1, NULL, '2019-04-04 13:48:22', NULL, NULL, 0, '77.246.49.27,107.167.104.113, 172.68.65.140', NULL),
(933, 'Md al amin robin', 'alaminrobin3443@gmail.com', '$2y$10$njEcaz/Z7YLZVabY103S5Oe1IukVCc.C.3QyNTPtO0TwGrBqpKZgu', NULL, 'Telegram', '01710826236', 1, 0, 1, NULL, '2019-04-04 14:20:43', NULL, NULL, 0, '103.79.216.65, 103.22.200.158', NULL),
(934, 'Kaydear', 'kayolofinlade@yahoo.com', '$2y$10$Wyxj7cKx7n/T167mjqHeru0GaDeScOpKTR.BGtBr3hOQpRlfrfno6', NULL, 'Telegram', '07037226104', 1, 0, 1, NULL, '2019-04-04 14:26:00', NULL, NULL, 0, '197.210.45.93, 172.69.14.30', NULL),
(935, 'Amos', 'amos_idehen@yahoo.com', '$2y$10$84Q/fy4anR.stjFVrY3VG.ikP.eNKC6vGwhCIqEPOUFsTgI0LyxbS', NULL, 'Telegram', '+2348034340626', 1, 0, 1, NULL, '2019-04-04 15:02:07', NULL, NULL, 0, '41.203.78.49, 172.69.14.18', NULL),
(936, 'jamal', 'njoyakouotou9@gmail.com', '$2y$10$mXoCgVTBLFetQ1Cw6jH5rOfTc20gD038.0nEf4z/Wnb9tQ3dTzTjC', NULL, 'Telegram', '697909376', 1, 0, 1, NULL, '2019-04-04 15:37:38', NULL, NULL, 0, '41.202.207.3, 162.158.155.178', NULL),
(937, 'kunciil', 'Kunciil21@gmail.com', '$2y$10$rAKjMwstsn0fV6uQ2saD5.1p3sb9tAyW3FtdQo8Xz.t5PWuSJkGPy', NULL, 'Telegram', '+252615807766', 1, 0, 1, NULL, '2019-04-04 16:23:13', NULL, NULL, 0, '41.78.72.11, 162.158.158.131', NULL),
(938, 'AKINNIYI AKINWALE TAOHEED', 'takinniyi@yahoo.co.uk', '$2y$10$w8uHfGY5/k0JqmrRwMP3Mek7FNo6pFvBg6n7X0jG6Hbwg94DeOVdC', NULL, 'Telegram', '+2348035064345', 1, 0, 1, NULL, '2019-04-04 18:18:25', NULL, NULL, 0, '197.210.54.181, 172.69.55.174', NULL),
(939, 'Khan', 'Yousuf.brahvi2015@gmail.com', '$2y$10$95M6c3hRBMlCIhq19299feAi6Vebgttv0sdAUSrCCMgN4gpujsk8K', NULL, 'Telegram', '+923313935067', 1, 0, 1, NULL, '2019-04-04 18:53:44', '2019-04-04 18:55:24', NULL, 0, '45.116.232.32, 103.22.200.74', 1),
(940, 'Micheal Osas', 'michealosass6@gmail.com', '$2y$10$FuuwoD7GqYBHLHrrfNl0Qe4JpGDcyG8NsLUC9V6Y1B4tH9lfdGqWC', NULL, 'Telegram', '+13016783775', 1, 0, 1, NULL, '2019-04-04 19:20:13', NULL, NULL, 0, '197.234.221.216, 141.101.105.206', NULL),
(941, 'Ilyas mohamed mohamud', 'yaashcarab7@gmail.com', '$2y$10$02UdRizGbmnKqIvgOVpQ1e4s8n7tQ1aIjAnWRaXuFGG7H4xL7jA2G', NULL, 'Telegram', '+252616604418', 1, 0, 1, NULL, '2019-04-04 19:23:23', NULL, NULL, 0, '197.220.84.3, 141.101.98.240', NULL),
(942, 'Xasan mustafa maxamed', 'kulaal984@gmail.com', '$2y$10$pWoon9RkgrQ47AUVKwqeG.29/S5xMJ67V785fOBPje094GA3sg9bu', NULL, 'Telegram', '618476390', 1, 0, 1, NULL, '2019-04-04 20:55:36', NULL, NULL, 0, '41.78.72.13, 162.158.155.178', NULL),
(943, 'Martin Muendo', 'martinmuendo82@gmail.com', '$2y$10$27Mu27erfRO9IL6R076N2u5ZUKPvviNb25gaBu1spQy65jkGjCoVa', NULL, 'Telegram', '+254723012076', 1, 0, 1, NULL, '2019-04-04 21:56:00', NULL, NULL, 0, '105.56.70.107, 162.158.42.38', NULL),
(944, 'Htet W Y Lin', 'dr.htetwaiyanlin.yours@gmail.com', '$2y$10$IkzLrQtBb5oTEjueAPB2neLB4zTd2Sz4prADPm.YzcUBxLdE4f66u', NULL, 'Telegram', '959780312100', 1, 0, 1, 'KlM7NYl2SGGkK4x0mOP8tZJOuq6b3CxXdC49iiKVQPuQaTIiu6nEh8FBX86e', '2019-04-04 22:06:32', NULL, NULL, 0, '103.217.156.193, 103.22.200.158', NULL),
(945, 'Mothusi King Selume', 'smartserotoo@gmail.com', '$2y$10$dKnjmVsF2lQ8RRlYFWSaoObRub6rPPdNLd2g/jRe3j4/jxvNDBw52', NULL, 'Telegram', '+26774373346', 1, 0, 1, NULL, '2019-04-05 01:05:47', NULL, NULL, 0, '168.167.81.189, 197.234.242.30', NULL),
(946, 'Hans Fredrik Solbakken', 'hansfs@hotmail.com', '$2y$10$hU4mGAHKZ5t/JQwhMLZmDeTXUv4IsNXJZVWFF2WdakW7Rmy.knz72', NULL, 'Telegram', '(+47)46891238', 1, 0, 1, NULL, '2019-04-05 02:10:33', NULL, NULL, 0, '84.208.80.106, 172.68.182.60', NULL),
(947, 'KOUAME SERGE FLORENT N\'GORAN', 'yemiserge@gmail.com', '$2y$10$0G1ejVa9EVwYZE0yP/V1jeveCD7ikl83le0ECT/Tk/Sy7gHwf/v9a', NULL, 'Telegram', '+22542942497', 1, 0, 1, NULL, '2019-04-05 04:09:31', NULL, NULL, 0, '154.0.27.147, 172.68.94.54', NULL),
(948, 'Dominic Paul', 'omohdominic22@gmail.com', '$2y$10$4VSYoVrXdEB1yWWdp5Nn6OvIvaD07JUNqUpBB2M.KDW8Pt0cCEmv6', NULL, 'Telegram', '+2348103031224', 1, 0, 1, NULL, '2019-04-05 07:14:38', NULL, NULL, 0, '197.210.46.53, 172.69.14.18', NULL),
(949, 'Sayidali', 'qbn3434@gmail.com', '$2y$10$tseg9i3KSXqzwK1/9U.PP.cppEtE3VWAtVyuRahEdlgcS4P9eVOtq', NULL, 'Telegram', '+252615703434', 1, 0, 1, NULL, '2019-04-05 08:26:12', NULL, NULL, 0, '41.78.72.128, 162.158.154.105', NULL),
(950, 'Kay Sap Gautham', 'gauthamkaysap10@gmail.com', '$2y$10$9/RENeKKFCOY9YoLA8ICCOdYKng3FdPby.IdEOX9F/2IYiOKOH2/2', NULL, 'Telegram', '(+95)9786262993', 1, 0, 1, '98eFfq0BZFwRE1LhMGMQMQAi8o5x3hu2lsEMDafNBzUbZ6VXLOiC84izxFX3', '2019-04-05 08:29:45', NULL, NULL, 0, '103.116.12.250, 141.101.69.137', NULL),
(951, 'Living shamy', 'shamilivingstone4@gmail.com', '$2y$10$2Ses.jj8hzwSeBaFb5FhXup6upTrSyo9I28zKLv9mqq7Wp6YW2PaW', NULL, 'Telegram', '0677874967', 1, 0, 1, NULL, '2019-04-05 09:31:25', NULL, NULL, 0, '41.222.181.180, 197.234.242.114', NULL),
(952, 'farai', 'fraihchips@gmail.com', '$2y$10$IE9jDb1lNbODOSTFrTN.veeAfREGil7n9sCACb8RPpI5Gc1b1ksPm', NULL, 'Telegram', '+263779753520', 1, 0, 1, NULL, '2019-04-05 09:49:20', '2019-04-05 09:59:26', NULL, 0, '77.246.55.129, 197.234.242.114', 1),
(953, 'Fredrick kamwera', 'fandk.kamwera@gmail.com', '$2y$10$iGUi.0lb2TcsF1La3T9WvuZyGxHwKFPkSb/rCp1P5MJ2k6L9.TTjO', NULL, 'Telegram', '+265884380761', 1, 0, 1, NULL, '2019-04-05 14:15:34', NULL, NULL, 0, '41.75.222.116, 162.158.42.50', NULL),
(954, 'Adamolekun Modupe', 'modupelove7@gmail.com', '$2y$10$hv.sKWp/6rfKZ9AI834j0OVS.My77bhZnKp5E/Vjm3QODoQGGJbqa', NULL, 'Telegram', '+2347052721849', 1, 0, 1, NULL, '2019-04-05 15:05:11', NULL, NULL, 0, '105.112.98.233, 172.69.14.12', NULL),
(955, 'Motasim', 'motasimbkb9@gmail.com', '$2y$10$IfRu27rzLyFTOtnSatfoSO6PFMfST2mczMdhHzyNggq4NTBC3lq6u', NULL, 'Telegram', '01776797030', 1, 0, 1, NULL, '2019-04-05 16:17:40', NULL, NULL, 0, '27.147.170.114, 162.158.150.132', NULL),
(956, 'Germain AKAKPO', 'akakpogermain88@gmail.com', '$2y$10$I9uu8CViCTwFgE14y0fTDurSqCIc0mRJTXmqJCk0EwW4ZyJFfSjTa', NULL, 'Telegram', '+22899530096', 1, 0, 1, NULL, '2019-04-05 17:38:01', NULL, NULL, 0, '41.78.136.110, 141.101.105.254', NULL),
(957, 'AMEDJIKO ulrich', 'ulrich.amedjiko@gmail.com', '$2y$10$HmOM6w9m3dXFrjRjcZ5DNu8R4HPpfVUersFONcVatBoW2hB8.U0GK', NULL, 'Telegram', '+22966865859', 1, 0, 1, 'SKZeOKBVLupGXyksXe1xG7THPj7vLx9nN9WUf8RQ1WkQwpfLFwct0Q49JWAO', '2019-04-05 18:35:52', '2019-04-05 18:40:59', NULL, 0, '197.234.221.170, 141.101.76.12', 1),
(958, 'Kaizer', 'royenchosley@gmail.com', '$2y$10$atfHNiYb.RBYHt4.6ctUJukX61uQnZviLfEcaq8B8ADWLVHe91u2W', NULL, 'Telegram', '+27627676231', 1, 0, 1, NULL, '2019-04-05 18:47:02', NULL, NULL, 0, '105.4.6.154, 197.234.242.144', NULL),
(959, 'taher', 'titotata249@gmail.com', '$2y$10$fdEEDvwjx6ymFGn4VEkaY.8Fs9IzXQ1IRqFFUCGPXgKFXKYOx9RgW', NULL, 'Telegram', '+213672175599', 1, 0, 1, NULL, '2019-04-05 20:09:12', NULL, NULL, 0, '105.235.132.218, 172.69.226.49', NULL),
(960, 'Baraka donald', 'westond611@gmail.com', '$2y$10$t.PHP67bh3z2GbYzUCoCR.uIYCR6Wmh2WkzlXlwlS/FGy.XGF0MB6', NULL, 'Telegram', '+25543929208', 1, 0, 1, NULL, '2019-04-05 20:28:03', NULL, NULL, 0, '41.75.223.11, 162.158.42.38', NULL),
(961, 'Ibrahim khalil', 'ibrahim03241@gmail.com', '$2y$10$HFzYx6yn/NseTwYHAt3IX.aJPYE2yQOTHSNSGTPURX2e/yfIiPLC6', NULL, 'Telegram', '01883216178', 1, 0, 1, NULL, '2019-04-05 22:28:52', NULL, NULL, 0, '202.134.13.132, 162.158.155.178', NULL),
(962, 'Mehdi Dameri', 'mehdidameri@gmail.com', '$2y$10$ukpYp0.c1imXMXL6Jtcr1.DxpT.vgsgaow3wz/ofYS4TK0NYP50Vi', NULL, 'Telegram', '+989173135285', 1, 0, 1, NULL, '2019-04-05 23:35:23', NULL, NULL, 0, '51.68.172.61, 162.158.91.97', NULL),
(963, 'Muhammad Salim', 'mohammadsalimchinjni786@gmail.com', '$2y$10$iPkOSg/Y4Qy1Sz4kJo6xROPcs1E5zCGHORatQ4fbLbjVwl7iEgAfq', NULL, 'Telegram', '+923072246327', 1, 0, 1, NULL, '2019-04-06 00:13:45', NULL, NULL, 0, '167.99.130.213, 141.101.105.206', NULL),
(964, 'Enock', 'enoqtaffin@gmail.com', '$2y$10$F7iAAvk99yhJPS9zJOnzHOsGcejPNOjZvegax.c5jw293b0LkbONi', NULL, 'Telegram', '+263774064895', 1, 0, 1, NULL, '2019-04-06 04:27:34', NULL, NULL, 0, '77.246.52.52, 197.234.242.144', NULL),
(965, 'Aziz', 'azizr041@gmail.com', '$2y$10$RTXn5B51KyBKPqWIcu1Vm.k9zi3qGHryGAHgn1W3steHab5VCZXTO', NULL, 'Telegram', '+923343740816', 1, 0, 1, NULL, '2019-04-06 08:18:04', NULL, NULL, 0, '39.57.34.94, 172.69.39.8', NULL),
(966, 'Abdiwali', 'Hirsijr@gmail.com', '$2y$10$R0UcxtNouU3.OkzxHOJaZu/NbfSCPM4UengAvLD3J9QioIGi.hDBe', NULL, 'Telegram', '+252615570430', 1, 0, 1, NULL, '2019-04-06 08:18:27', NULL, NULL, 0, '41.78.74.27, 162.158.155.178', NULL),
(967, 'ISMAIL JAUFAR', 'i.jaufar78@gmail.com', '$2y$10$xM/rwT815kZzPgFiikPHSebymKF6.E26GKeo3oJiP78SFZ/mwGggG', NULL, 'Telegram', '+9607352790', 1, 0, 1, NULL, '2019-04-06 08:55:55', NULL, NULL, 0, '209.212.203.150, 141.101.84.126', NULL),
(968, 'Sibghtullah', 'sibghatullahpitafikhan@gmail.com', '$2y$10$JHkBg5B0dGSg8grWKe5r.e.RKPECW3cUxLvH3GIynGqZ0g3iProG6', NULL, 'Telegram', '92+30442040142', 1, 0, 1, NULL, '2019-04-06 11:39:51', NULL, NULL, 0, '37.111.130.164, 172.69.39.6', NULL),
(969, 'Michael', 'endowed2bad@gmail.com', '$2y$10$xE1ceOuRPUc06SoGtvnjMOOUO8w8dXHzqf62919C9YP27g8TaUjWe', NULL, 'Telegram', '+2348136628424', 1, 0, 1, NULL, '2019-04-06 15:01:32', NULL, NULL, 0, '197.210.46.42, 172.69.14.18', NULL),
(970, 'Amine', 'aminemoussaoui16@gmail.com', '$2y$10$sP9P7MKj9XsaZ4USTjayAO83NmhTsiBW8IUKmCg.xtV8yDZMv2wNq', NULL, 'Telegram', '+212658599695', 1, 0, 1, NULL, '2019-04-06 17:31:06', NULL, NULL, 0, '105.159.188.72, 162.158.150.28', NULL),
(971, 'Joshua Sasita', 'sasitajoshua10@gmail.com', '$2y$10$ICqSW7nCWk7t9bdmDhRQZO6l7ihUTu/Amb0YesFXcs6dfbiNIb67i', NULL, 'Telegram', '+225752783655', 1, 0, 1, NULL, '2019-04-06 19:03:49', NULL, NULL, 0, '41.222.181.174, 197.234.242.174', NULL),
(972, 'Liile', 'matasaneliile@gmail.com', '$2y$10$zRyaUk8TkY2huyi7rakxHea6OETAUkY0qsLAB7I/YWVKtIrBkMj26', NULL, 'Telegram', '0026650861527', 1, 0, 1, NULL, '2019-04-06 19:10:15', NULL, NULL, 0, '197.254.137.232, 197.234.242.114', NULL),
(973, 'Ariful', 'joyraz4934@gmail.com', '$2y$10$gRtL4yz0GqVAh82cY.NHJ.llx2rX7I5NXwIRRUc7kW0q2pWXb8ioa', NULL, 'Telegram', '+8801856699091', 1, 0, 1, NULL, '2019-04-06 19:49:04', NULL, NULL, 0, '202.134.9.150, 141.101.98.168', NULL),
(974, 'James Selemani', 'selemani620@gmail.com', '$2y$10$gAX89MTRMwmKrffaeE0E6eq2AL/z/EZtjttymYL0Tz.VK3n7HGiJi', NULL, 'Telegram', '+255759741674', 1, 0, 1, NULL, '2019-04-06 19:54:25', NULL, NULL, 0, '196.249.97.125, 162.158.42.18', NULL),
(975, 'selvendra', 'selvendra1000@gmail.com', '$2y$10$H7q1e0QYranz.dLTRtbijemYl8kVKlnuLlgB3K3bgXBl8AlpHRb6u', NULL, 'Telegram', '+6581868079', 1, 0, 1, 'CYHhu6CzD57ckdIhIMwWDvdbBzrgiEun5JZPvGZb7q0mJDi2ouMwOkOTUW1b', '2019-04-06 23:35:18', '2019-04-09 12:39:36', NULL, 0, '116.89.56.198, 162.158.118.48', 1),
(976, 'Olayinka Julius', 'Jamesjulius85@gmail.com', '$2y$10$nBIrq3emoKSyFdvTifmuruTdkS2sFO3ZXYOEyMVWRDP2BTW.7TRQG', NULL, 'Telegram', '+2347064412535', 1, 0, 1, NULL, '2019-04-06 23:42:54', NULL, NULL, 0, '105.112.28.44, 172.69.14.18', NULL),
(977, 'Johnson Adeyeye', 'johnsongreats@yahoo.co.uk', '$2y$10$A7W61LwsYUijR2rG5KN4rOXEnjhpNTimnOylCt8cMlrlroSvxdViO', NULL, 'Telegram', '+2348069290418', 1, 0, 1, NULL, '2019-04-07 01:24:52', NULL, NULL, 0, '197.210.44.251, 172.69.14.12', NULL),
(978, 'René', 'cindyrvanwyk@gmail.com', '$2y$10$nnle5xbzA8nIzKBblLNijuhpIiEFXViYxH7mxUsRSxDpDGhHCovKq', NULL, 'Telegram', '0813528871', 1, 0, 1, NULL, '2019-04-07 01:47:45', NULL, NULL, 0, '41.182.91.108, 162.158.154.105', NULL),
(979, 'mohammed gamil mohammed alhaj', 'mohammed_alhag@yahoo.com', '$2y$10$SZCfWggf2vo/FrUInEju1eRKmvrh0HnsREUnFzfRFvCtwXB1cEUum', NULL, 'Telegram', '00967737909053', 1, 0, 1, NULL, '2019-04-07 04:27:02', NULL, NULL, 0, '63.168.169.84, 162.158.30.6', NULL),
(980, 'Rajib lochan baishnab', 'himadreerajib@gmail.com', '$2y$10$8JXF31/uazny3lfswXdVHOf1EgL1t8Ws6968ehrY7w2tuCumv4etG', NULL, 'Telegram', '+8801773136981', 1, 0, 1, NULL, '2019-04-07 07:41:49', NULL, NULL, 0, '103.230.106.28, 162.158.118.48', NULL),
(981, 'Nilkanth Tanti', 'nkfutureclub@hotmail.com', '$2y$10$AfczIWjvacx6JF3YjeIEAOIocPYxUtXECH9PKN8TxUohqN2BwYIVK', NULL, 'Telegram', '+917047267454', 1, 0, 1, NULL, '2019-04-07 08:17:37', NULL, NULL, 0, '106.207.46.199, 141.101.98.168', NULL),
(982, 'Abdul Diallo', 'abduldiallo72@gmail.com', '$2y$10$yaVUlwSVzCNNSLQxKtLq3u3qaZ4DKzu/LmILzUwSBXxCbQXFqzVHu', NULL, 'Telegram', '+258860777754', 1, 0, 1, NULL, '2019-04-07 08:37:07', NULL, NULL, 0, '197.218.93.29, 197.234.242.114', NULL),
(983, 'Alex Massawe', 'lexisbartson@gmail.com', '$2y$10$w.S9ky349/w.SPyEnmd3sOFUOKDc.Rj2UKhN82d8LToZRnQzvUmA2', NULL, 'Telegram', '+255687906869', 1, 0, 1, NULL, '2019-04-07 08:57:51', NULL, NULL, 0, '156.158.145.201, 162.158.42.38', NULL),
(984, 'Asif', 'mdasif.3942@gmail.com', '$2y$10$p3vIu5AnUbip5CvgK/oA7OGH1/OfAf8WQAwDuToKR9Fy./uRSR7tu', NULL, 'Telegram', '+919920659563', 1, 0, 1, NULL, '2019-04-07 10:13:18', NULL, NULL, 0, '1.186.186.142, 141.101.107.98', NULL),
(985, 'Oritseje sheriff', 'sheriffforex@outlook.com', '$2y$10$vGRqTYZhH8x71VWGSKNFxetQYo11Kk9Gdr453DIKdoF/4rrcNR9o.', NULL, 'Telegram', '+2348074469880', 1, 0, 1, NULL, '2019-04-07 10:30:55', NULL, NULL, 0, '105.112.104.46, 172.69.14.18', NULL),
(986, 'Tadiwanashe B Masina', 'tadiwabradley3@gmail.com', '$2y$10$O.1rtrlmmhqogzJf0EH2duqb91pgGgKDlakb9TBqxoBxV7Y5F/Kxq', NULL, 'Telegram', '+263784725448', 1, 0, 1, NULL, '2019-04-07 13:47:25', NULL, NULL, 0, '77.246.52.190, 197.234.242.114', NULL),
(987, 'Romario Michael', 'romarioemichael@gmail.com', '$2y$10$jvJ9CkJt0g5V38S/ZI95c.0sNS5No0OzaDlGVy.GSV7WJaVBY2Vj6', NULL, 'Telegram', '12687856486', 1, 0, 1, NULL, '2019-04-07 18:17:16', NULL, NULL, 0, '208.83.82.211, 172.68.78.60', NULL),
(988, 'INAMULLAH KHAN', 'uzmajamshaid0007@gmail.com', '$2y$10$8sKs/36aTSGmCXneNd0zAu0tPFcq5zGdDPZwAyYcYrGRLzi.OKVDK', NULL, 'Telegram', '+923092874129', 1, 0, 1, NULL, '2019-04-07 19:58:22', NULL, NULL, 0, '37.111.128.22, 172.69.225.30', NULL),
(989, 'Ndaba Machobane', 'ndaba.machobane@live.com', '$2y$10$/DCpyibJ5nyTh/.0SrqOc.1bB1MuYtRikSBTk.lvBxC6zN90dGM3S', NULL, 'Telegram', '00266 68388279', 1, 0, 1, NULL, '2019-04-07 21:23:08', NULL, NULL, 0, '129.232.82.243, 197.234.242.114', NULL),
(990, 'Dejan', 'dejangazdic@gmail.com', '$2y$10$0ciabczmSQOT382nEpUSR.fbbrKqsKFiOlFMsFnW0ShO88rldSl0W', NULL, 'Telegram', '+38163283003', 1, 0, 1, 'KdwzUWpuy5hSvEajbyg2GrvEKCQ2y4hKq4za7Q5lS8ZUkF71UbqRgPT26GKv', '2019-04-07 23:49:42', '2019-04-10 23:31:11', NULL, 0, '178.148.141.205, 172.68.154.30', 1),
(991, 'Abdullahi', 'abdalle.saleban@gmail.com', '$2y$10$ZUDM5nshoecZZ61x3D3IC.cEPXjY9CrRmUAPIPt.ny/pO9xZdNC2O', NULL, 'Telegram', '+252906460494', 1, 0, 1, NULL, '2019-04-08 01:18:45', NULL, NULL, 0, '197.231.203.118, 172.68.63.61', NULL),
(992, 'Kim Moi Siow', 'roselow_@hotmail.com', '$2y$10$SF9ps0bm6VHj5bmJkNghGeyH7R4lZ2g9SGTKda01rDy.yq5J861mG', NULL, 'Telegram', '+6591885490', 1, 0, 1, NULL, '2019-04-08 05:28:38', NULL, NULL, 0, '116.14.68.162, 172.68.189.195', NULL),
(993, 'Qasim Abdirahman', 'ksmabdirahman@gmail.com', '$2y$10$0NdG4akxoP6qVtMQ4YKV0uAw7xhlHNz97zwNRK3nCdUiT12pYyZZO', NULL, 'Telegram', '+252615644894', 1, 0, 1, NULL, '2019-04-08 07:42:07', NULL, NULL, 0, '41.78.72.131, 141.101.98.168', NULL),
(994, 'Nwafor', 'nijsline@gmail.com', '$2y$10$Ndmg4aQpgwxZc2bWqmhPGuRc4Wk7Sk4HIBpark/.pgcSN8QtUhBUm', NULL, 'Telegram', '07031934526', 1, 0, 1, NULL, '2019-04-08 10:12:25', NULL, NULL, 0, '105.112.98.11, 172.69.14.18', NULL),
(995, 'RAZA JAN', 'razaorakzaiboa@gmail.com', '$2y$10$74afT3Cxc7ftpWz3y3oTxOBZlXy3A3kYrFfUA.n8ilxgO9H7Oj1uy', NULL, 'Telegram', '+923156662298', 1, 0, 1, NULL, '2019-04-09 22:30:27', NULL, NULL, 0, '43.245.8.33, 172.69.225.18', NULL),
(996, 'Alexandru', 'georgealexandru09@gmail.com', '$2y$10$aWm/NH8st42ytkbL9McYcew0GOLmsCVYKrCId6J767NmNH7wUakJq', NULL, 'Telegram', '+440723755675', 1, 0, 1, NULL, '2019-04-15 17:09:13', NULL, NULL, 0, '82.137.8.59, 162.158.92.138', NULL),
(997, 'lily', 'lilyermina@yahoo.com', '$2y$10$m.RzKXDpHu5kcLnDhlL6nek37tbmTJGMFPiPlljbXa7OFxO.0NGVW', NULL, 'Telegram', '087808502898', 1, 0, 1, NULL, '2019-04-20 10:21:46', NULL, NULL, 0, '118.136.24.204, 162.158.167.32', NULL),
(998, 'asad', 'asad@smart-mail.info', '$2y$10$639z/ZQ5EJYaqO5UEYrbW.hQQOp9pS79esPWtSO5W8DbSSFfKUW82', NULL, 'Telegram', '+923005764545', 1, 0, 1, NULL, '2019-04-21 16:10:03', NULL, NULL, 0, '182.185.22.182, 172.69.225.18', NULL),
(999, 'Onyegili Ifeanyi Augustine', 'onyegili@gmail.com', '$2y$10$viGLjZNDHBBKJQwD0o8xdOWF/UgWZRDQbZBkzNwU2J3RRJmaUN5J6', NULL, 'Telegram', '+2348067884389', 1, 0, 1, NULL, '2019-04-24 16:29:26', NULL, NULL, 0, '197.210.227.172, 141.101.76.246', NULL),
(1000, 'Vitalie', 'vcociorva@gmail.com', '$2y$10$VMwqSKgF3uwYb/QUFECTd.AF8SmtUJhoEuaIt8p7JXYjKOc/faCVG', NULL, 'Telegram', '+34642631179', 1, 0, 1, NULL, '2019-04-27 01:01:11', NULL, NULL, 0, '46.37.82.181', NULL),
(1001, 'Omar Amer Ahmed', 'omaran68@gmail.com', '$2y$10$lKOCzAkzisHOcl.vTJBsJes8Nmo0Bkby8OWwnk.dXLdvdOjSf7SQu', NULL, 'Telegram', '+4915758462592', 1, 0, 1, 'Rh7hwdqX6IX8FCNW1pyGULFtppr7izmlafA7bbioohCDjsRJxDug68llqjdg', '2019-05-05 17:11:14', '2019-05-12 20:09:44', NULL, 0, '2001:16b8:2460:d00:9d93:82c0:532d:dd20', 1),
(1002, 'JUAN RUEDA', 'juanjoseruedabotello@gmail.com', '$2y$10$SuFdQ2eaygtA/LGMFYjZWeGQqJGGuioKvSD59F.BWZrkcPJgr2WRC', NULL, 'Telegram', '+573164262176', 1, 0, 1, NULL, '2019-05-06 01:33:54', NULL, NULL, 0, '179.33.76.188', NULL),
(1003, 'Eric Sackey', 'ricky123sack@gmail.com', '$2y$10$ucAA2Xx46hM/mU4xipM1WO6AiHjc/GTyiidxA77IpHsFenOifON7O', NULL, 'Telegram', '07904444623', 1, 0, 1, '4XC2IvWE82YVmIG58Yy1kDmzhOxuPkbkwodianvPxZ7jFWXZ66ZvwYQOkdWG', '2019-05-06 21:16:23', '2019-05-19 14:41:51', NULL, 0, '143.159.175.229', 1),
(1004, 'Linda Linda', 'ldsteranko@gmail.com', '$2y$10$EA20C385m0ulTP6HBr7OaeW0hwkCamczIXh.Bqq8C.0mQmWjYM6l2', NULL, 'Telegram', '7246129720', 1, 0, 1, 'DyjXirfMevEocW8kfXeHTipIRTIoVUBg8YHyL1AGNuX3NErvQwjxoIb384H2', '2019-05-08 04:32:12', '2019-05-09 06:41:43', NULL, 0, '24.112.207.17', 3),
(1005, 'Akeem', 'informationeed01@gmail.com', '$2y$10$oPgnZoguaioH4g5I9IpIEOmsHxjbRaUv5cOAQN.W0t5zvfT0EcBSG', NULL, 'Telegram', '+2348160839642', 1, 0, 1, NULL, '2019-05-13 13:26:59', NULL, NULL, 0, '197.210.29.3', NULL),
(1006, 'ziaullah', 'mustafazia1@gmail.com.com', '$2y$10$0mi5SoSCSXQq03my2P488eBNP1B0rVqxrtDAPczpyTS/YsNitGK2O', NULL, 'Telegram', '00923315993038', 1, 0, 1, NULL, '2019-05-17 12:51:01', '2019-05-17 12:58:05', NULL, 0, '94.237.64.251', 1),
(1007, 'Jennifer Sackey', 'jenni123sack@gmail.com', '$2y$10$wbZWXZ83.gd0h12DUfQipuDloYgjfuvewiwIT4hALK9BgBIuL1oP6', NULL, 'Telegram', '07366390176', 1, 0, 1, 'mRppGuXNl9GoQGzC4hvKvPEAhxlsDtbV2uD6048DUzAXhs4Fo3Tfme2ePtWK', '2019-05-19 14:33:55', '2019-05-24 02:03:51', NULL, 0, '143.159.175.156', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `broadcast_schedule`
--
ALTER TABLE `broadcast_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `channel`
--
ALTER TABLE `channel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equity`
--
ALTER TABLE `equity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investors`
--
ALTER TABLE `investors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `line_copier_ea`
--
ALTER TABLE `line_copier_ea`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_videos`
--
ALTER TABLE `live_videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_requests`
--
ALTER TABLE `password_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promos`
--
ALTER TABLE `promos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signals`
--
ALTER TABLE `signals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signal_counter`
--
ALTER TABLE `signal_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signal_dislikes`
--
ALTER TABLE `signal_dislikes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signal_likes`
--
ALTER TABLE `signal_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `broadcast_schedule`
--
ALTER TABLE `broadcast_schedule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `channel`
--
ALTER TABLE `channel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `equity`
--
ALTER TABLE `equity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `investors`
--
ALTER TABLE `investors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `line_copier_ea`
--
ALTER TABLE `line_copier_ea`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `live_videos`
--
ALTER TABLE `live_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `password_requests`
--
ALTER TABLE `password_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `promos`
--
ALTER TABLE `promos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `signals`
--
ALTER TABLE `signals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `signal_counter`
--
ALTER TABLE `signal_counter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `signal_dislikes`
--
ALTER TABLE `signal_dislikes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `signal_likes`
--
ALTER TABLE `signal_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=619;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1008;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
