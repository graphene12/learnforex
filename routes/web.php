<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@homePage')->name('landingpage');
Route::get('live-trade', 'PagesController@livetrade');
Route::get('managed-accounts', 'Access\PagesController@managedAccountForm')->name('managed_accounts');
Route::get('line-copier', 'Access\PagesController@lineCopier');
Route::get('contact-us', 'PagesController@contactPage')->name('contact');
Route::post('contact-us', 'LoadController@contactusAction');
Route::get('thanks', function(){
       return view('restricted.thank-you'); 
    });
    
Route::get('order-lot-size-indicator', 'LoadController@lotsizeIndicator');
Route::get('mentorship', 'LoadController@mentorship');
Route::group(['prefix' => 'auth', 'namespace' => 'Authentication'], function () {
    Route::group(['middleware' => 'check_auth'], function () {
        Route::get('login', 'PagesController@loginPage')->name('login_page');
        Route::get('register/{plan_id?}', 'PagesController@registerPage')->name('sign_up');
        Route::get('forgot-password', 'PagesController@forgotPasswordPage');
    });

    Route::get('logout', 'LoadController@logout');
    Route::post('login', 'LoadController@authenticate');
    Route::post('register', 'LoadController@createuser');
    Route::post('managed-account', 'LoadController@addinvestor');
    Route::post('line-copier', 'LoadController@addlinecopier');
    Route::post('forgot-password', 'LoadController@forgotpassword');
    Route::post('forgot-password/save/{uid}', 'LoadController@setnewpassword');
});

Route::group(['prefix' => 'dashboard', 'namespace' => 'Access'], function () {
    Route::get('/', 'PagesController@liveRoom')->middleware('auth')->name('home');
    Route::get('live-room', 'PagesController@liveRoom')->middleware('auth');
    Route::get('fetch-signals', 'PagesController@fetchSignals')->middleware('auth');
    Route::get('notification', 'PagesController@checknotification')->middleware('auth');
    Route::get('signals', 'PagesController@allsignals');
    Route::post('orderLot', 'PagesController@register')->middleware('auth');
    
    Route::group(['prefix' => 'signals', 'middleware' => 'auth'], function () {
        Route::get('like_signal', 'PagesController@likeSignal');
        Route::get('dislike_signal', 'PagesController@dislikeSignal');
    });
});

Route::group(['prefix' => 'message', 'namespace' => 'Message', 'middleware' => 'auth'], function () {
    Route::get('fetch', 'LoadController@fetch');
    Route::post('send', 'LoadController@send');
});

Route::group(['prefix' => 'order', 'namespace' => 'Order', 'middleware' => 'auth'], function () {
    Route::post('new', 'LoadController@orderlotsize');
    Route::post('subscription-save', 'LoadController@equityaccount');
});

Route::group(['prefix' => 'pricing', 'namespace' => 'Pricing'], function () {
    Route::get('/', 'LoadController@pricingPage')->name('pricing');
    Route::get('managed-account/subscription', 'LoadController@managedAccountPlans');
    Route::get('managed-account-activate-subscription', 'LoadController@activateInvestorsPlan');
    Route::get('activate/{plan_id}', 'LoadController@activatePlan')->middleware('auth');
});

Route::group(['prefix' => 'administrator', 'namespace' => 'Administrator', 'middleware' => 'auth'], function () {
    Route::get('/', 'PagesController@dashboard');
    Route::get('change_live_video','PagesController@changeVideoPage')->name('change_video');

    Route::group(['prefix' => 'signals', 'namespace' => 'Signal'], function () {
        Route::get('/', 'PagesController@fetchsignals');
        Route::get('add-new', 'PagesController@addnewsignal');
        Route::get('delete', 'LoadController@deletesignal');
        Route::get('{signal_id}', 'PagesController@fetchone');
        
        Route::post('add-new', 'LoadController@uploadsignal');
        Route::post('save-changes/{signal_id}', 'LoadController@savechanges');
    });

    Route::group(['prefix' => 'settings', 'namespace' => 'Setting'], function () {
        Route::post('save-broadcast', 'LoadController@saveSchedule');
        Route::post('switch-broadcast-channel', 'LoadController@switchChannel');
    });

    Route::group(['prefix' => 'users', 'namespace' => 'User'], function () {
        Route::group(['prefix' => 'line-copiers'], function () {
            Route::get('/', 'PagesController@fetchlinecopiers');
            Route::get('delete', 'LoadController@deletelinecopier');
        });

        Route::get('/', 'PagesController@fetchusers');
        Route::get('search', 'PagesController@fetchusers');
        Route::get('add-new', 'PagesController@addnewuser');
        Route::get('delete', 'LoadController@deleteuser');
        Route::get('approve-payment', 'LoadController@approvepayment');
        Route::get('deactivate-subscription', 'LoadController@deactivateSubscription');
        Route::get('make-admin', 'LoadController@makeadmin');
        Route::get('remove-admin', 'LoadController@removeadmin');
        Route::get('{user_id}', 'PagesController@fetchone');
        Route::post('add-new', 'LoadController@createuser');
    });

    Route::group(['prefix' => 'password-requests', 'namespace' => 'User'], function () {
        Route::get('/', 'PagesController@fetchPasswordRequests');
        Route::post('/{user_id}', 'LoadController@resetPassword');
    });

    Route::group(['prefix' => 'managed-accounts', 'namespace' => 'User'], function () {
        Route::get('/', 'PagesController@fetchinvestors');
        Route::get('delete', 'LoadController@deleteinvestor');
        Route::get('{investor_id}', 'PagesController@fetchinvestor');

        Route::post('{investor_id}/edit', 'LoadController@saveinvestor');
    });

    Route::group(['prefix' => 'lot-size-orders', 'namespace' => 'User'], function () {
        Route::get('/', 'PagesController@fetchorders');
        Route::get('{order_id}/completed', 'LoadController@completeorder');
    });

    Route::group(['prefix' => 'equity-accounts', 'namespace' => 'User'], function () {
        Route::get('/', 'PagesController@fetchequityaccounts');
        Route::get('delete', 'LoadController@deleteequityaccount');
    });

    Route::group(['prefix' => 'announcements', 'namespace' => 'Announcement'], function () {
        Route::get('/', 'PagesController@fetchall');
        Route::get('add-new', 'PagesController@new');
        Route::get('edit', 'PagesController@edit');
        Route::get("delete", 'LoadController@delete');

        Route::post('add-new', 'LoadController@new');
        Route::post('edit', 'LoadController@edit');
    });

    Route::group(['prefix' => 'videos', 'namespace' => 'Video'], function () {
        Route::post('create', 'LoadController@create');
    });
});

Route::group(['prefix' => 'settings', 'namespace' => 'User', 'middleware' => 'auth'], function () {
    Route::post('changeavatar', 'SettingsController@changeavatar');
});

Route::group(['prefix' => 'payment', 'namespace' => 'Payment', 'middleware' => 'auth'], function () {
    
    Route::post('ce', 'CashEnvoyController@standard');
    Route::post('subscription/ce', 'CashEnvoyController@subscription');
    Route::post('ce/return', 'CashEnvoyController@paymentResponse');
    Route::post('paydot', 'PayDotController@paydot');
    
});

