<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin')->nullable();
            $table->string('title')->nullable();
            $table->longText('content')->nullable();
            $table->integer('delay')->default(5);
            $table->integer('active')->default(1);
            $table->string('stop')->nullable();
            $table->string('callback_url')->nullable();
            $table->string('call_to_action')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}
