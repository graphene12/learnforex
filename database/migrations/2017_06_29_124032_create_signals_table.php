<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->string('setup_type')->nullable();
            $table->string('entry')->nullable();
            $table->string('stop_loss')->nullable();
            $table->string('target_1')->nullable();
            $table->string('target_2')->nullable();
            $table->string('target_3')->nullable();
            $table->string('target_4')->nullable();
            $table->string('high_impact_news_ahead')->nullable();
            $table->string('quality')->nullable();
            $table->string('duration')->nullable();
            $table->string('risk')->nullable();
            $table->string('image')->nullable();
            $table->string('update')->nullable();
            $table->integer('active')->default(1)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signals');
    }
}
