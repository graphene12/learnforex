<?php

use Illuminate\Database\Seeder;

class BroadcastChannelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('channel')->insert([
            'channel' => 'youtube',
            'created_at' => Carbon\Carbon::now()
        ]);
    }
}
