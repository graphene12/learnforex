<?php

use Illuminate\Database\Seeder;

class PromoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promos')->insert([
            'title' => '2 weeks free live trading room access',
            'days' => 14,
            'active' => 1
        ]);
    }
}
