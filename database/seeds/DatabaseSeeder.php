<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PricingTableSeeder::class);
        $this->call(BroadcastChannelTableSeeder::class);
        $this->call(PromoTableSeeder::class);
    }
}
