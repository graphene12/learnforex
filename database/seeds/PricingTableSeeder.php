<?php

use Illuminate\Database\Seeder;

class PricingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            'title' => 'trial',
            'description' => '',
            'price' => 10,
            'validity' => "30 Days",
            'days' => 30,
            'active' => 1
        ]);

        DB::table('plans')->insert([
            'title' => 'standard',
            'description' => '',
            'price' => 50,
            'validity' => "Month",
            'days' => 31,
            'active' => 1
        ]);

        DB::table('plans')->insert([
            'title' => 'premium',
            'description' => '',
            'price' => 120,
            'validity' => "3 Months",
            'days' => 93,
            'active' => 1
        ]);

        DB::table('plans')->insert([
            'title' => 'super premium',
            'description' => '',
            'price' => 250,
            'validity' => "6 Months",
            'days' => 186,
            'active' => 1
        ]);

        DB::table('plans')->insert([
            'title' => 'infinity',
            'description' => '',
            'price' => 500,
            'validity' => "Year",
            'days' => 365,
            'active' => 1
        ]);

        DB::table('plans')->insert([
            'title' => 'line copier ea',
            'description' => '',
            'price' => 20,
            'validity' => "Month",
            'days' => 31,
            'active' => 1
        ]);
    }
}
