<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Password extends Model
{
    protected $table = "password_requests";

    public function user()
    {
    	return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
