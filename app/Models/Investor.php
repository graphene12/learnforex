<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
    protected $table = "investors";

    public function subplan()
    {
    	return $this->hasOne('App\Models\Plan', 'id', 'plan');
    }
}
