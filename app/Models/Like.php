<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = "signal_likes";

    public function user()
    {
    	return $this->hasOne('App\Models\User',  'id', 'user_id');
    }

    public function signal()
    {
    	return $this->hasOne('App\Models\Signal', 'id', 'signal_id');
    }
}
