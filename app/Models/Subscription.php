<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = "subscriptions";

    public function plan()
    {
    	return $this->hasOne('App\Models\Plan', 'id', 'plan_id');
    }
}
