<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $table = "announcements";

    public function administrator()
    {
    	return $this->hasOne('App\Models\User', 'id', 'admin')->where('role', 'admin');
    }
}
