<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = "live_videos";

    public function admin()
    {
    	return $this->hasOne('App\User', 'id', 'admin_id');
    }
}
