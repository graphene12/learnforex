<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Signal extends Model
{
    protected $table = "signals";

    public function admin()
    {
    	return $this->hasOne('App\Models\User',  'id', 'admin_id');
    }

    public function likes()
    {
    	return $this->hasMany('App\Models\Like', 'signal_id');
    }

    public function dislikes()
    {
    	return $this->hasMany('App\Models\Dislike', 'signal_id');
    }
}
