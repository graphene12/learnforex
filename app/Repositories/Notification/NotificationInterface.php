<?php

namespace App\Repositories\Notification;

interface NotificationInterface {
	public function sendactivation($data);

	public function sendPasswordRecoveryLink($data);
}