<?php

namespace App\Repositories\Notification;
use App\Repositories\Notification\NotificationInterface as NotificationInterface;
use Illuminate\Http\Request;
use URL;
use Mail;
use Crypt;
use Session;
class NotificationRepository implements NotificationInterface {
    private $mail;
    private $notification;

    public function __construct(Mail $mail) {
        $this->mail = $mail;
    }

    public function sendactivation($data)
    {
        //try {
            $activate = URL('/')."/auth/activation/".Crypt::encrypt($data['id']);
            
            $data['message'] = "We are delighted to have you on board. \n please activate your email to continue.";
            $data['title'] = ucwords("Welcome Aboard");
            $data['salute'] = "Hi ".$data['name'];
            $data['button_title'] = "Activate Account";
            $data['url'] = $activate;

            Mail::send('emails.activation', ['data' => $data], function ($m) use ($data) {
                $m->from("me@iamtunde.com", 'Theresa from Learn Forex Live Signals');

                $m->to($data['email'], $data['name'])->subject('Account Activation Link!');
            });
            
        /*} catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }*/
    }

    public function sendPasswordRecoveryLink($data)
    {
        try {
            $activate = URL('/')."/auth/forgot-password?uid=".Crypt::encrypt($data['id']);

            //now send email
            $headers ='MIME-Version: 1.0' . "\r\n";
            $headers .= "Content-type: text/html; charset=utf-8\r\n";
            $headers .= 'From: Jacob Ashibi <support@learnforexlivesignals.com>' . "\r\n";
            $headers .= "To: ".$data['name']." <".$data['email'].">\r\n";
            $headers .= 'X-Mailer: PHP/' . phpversion();
            $to = $data['email'];
            $subject = "Recover Lost Password";

            $body = "<h2>Hi ".$data['name']."</h2>!<p>We heard you lost your password, don't worry here is a link to set a new one</p><br><a href=".$activate.">".$activate."</a>";
                  
              //mail function
            $mail = mail($to, $subject, $body, $headers);
            
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("An error occured while trying to send you a recovery link")->withInput();
        }
    }
}