<?php

namespace App\Repositories\Message;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Crypt;
use URL;
use Session;
use App\Models\User;
use App\Models\Message;
class MessageRepository implements MessageInterface {
    private $message;

    public function __construct(Message $message) {
        $this->message = $message;
    }

    public function send($data)
    {
        //dd($data);
        try {
            $data['created_at'] = Carbon::now();
            $data['updated_at'] = Carbon::now();
            $this->message->insert($data);
            return response()->json(['response' => 'message sent']);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function fetch()
    {
        try {
            $_messages = $this->message->where('active', 1)->select('sender_name', 'message', 'id', 'sender_id')->orderBy('id', 'ASC')->get();
            $messages = [];
            foreach ($_messages as $message) {
                $message_array['sender_name'] = $message->sender_name;
                $message_array['sender_id'] = $message->sender_id;
                $message_array['message'] = $message->message;
                array_push($messages, $message_array);
            }
            return response()->json(['messages' => $messages]);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function all()
    {
        try {
            $_messages = $this->message->where('active', 1)->select('sender_name', 'message', 'id', 'sender_id')->orderBy('id', 'ASC')->get();
            return $_messages;
        } catch (Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }
}