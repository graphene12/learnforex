<?php

namespace App\Repositories\Message;

interface MessageInterface {
	public function send($data);

	public function fetch();
}