<?php namespace App\Repositories\Message;
use Illuminate\Support\ServiceProvider;
class MessageRepoServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Message\MessageInterface', 'App\Repositories\Message\MessageRepository');
    }
}