<?php

namespace App\Repositories\Authentication;

use App\Repositories\Notification\NotificationInterface as NotificationInterface;
use App\Repositories\Subscription\SubscriptionInterface as SubscriptionInterface;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Crypt;
use Paginator;
use URL;
use Auth;
use Session;
use Mail;
use App\Models\User;
use App\Models\Investor;
use App\Models\Copier;
use App\Models\Password;

class AuthRepository implements AuthInterface
{
    private $user;
    private $notification;
    private $subscription;
    private $investor;
    private $copier;
    private $passwordrequest;

    public function __construct(User $user, NotificationInterface $notification, SubscriptionInterface $subscription, Investor $investor, Copier $copier, Password $passwordrequest)
    {
        $this->user = $user;
        $this->notification = $notification;
        $this->subscription = $subscription;
        $this->investor = $investor;
        $this->copier = $copier;
        $this->passwordrequest = $passwordrequest;
    }

    public function authenticate($data)
    {
        try {
            $userip = $data['ip_address'];
            unset($data['ip_address']);
            
            if (Auth::attempt($data, 1)) {
                $user = Auth::user();
                if ($user->ip_address === null) {
                    $this->user->where('id', $user->id)->update(['ip_address' => $userip]);
                }
                return redirect('dashboard');
            } else {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors("Incorrect login details")->withInput();
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(UserRL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function createnew($data)
    {
        try {
            $data['created_at'] = Carbon::now();
            $data['user_type'] = 1;
            $data['password'] = bcrypt($data['password']);
            $plan_id = isset($data['plan_id'])?$data['plan_id']:null;
            unset($data['plan_id']);

            $new_user = $this->user->insertGetId($data);
            
            //auto login after account creation.
            Auth::loginUsingId($new_user);

            if ($plan_id != null) {
                $params = [
                    'user_id'       => $new_user,
                    'plan_id'       => $plan_id
                ];

                $this->subscription->register($params);
                $url = $this->subscription->fetchplan($plan_id, 'url');
                $redirect_to = redirect($url->url);

                $this->user->where('id', $new_user)->update(['first_plan' => $plan_id]);
            } else {
                $redirect_to = redirect('pricing')->withErrors("Welcome, choose a pricing plan to conitnue");
            }
            //email content for user and send
            $params = [
                    'email' => $data['email'],
                    'name'  => $data['name'],
                    'id'    => $new_user
                ];

            //$this->notification->sendactivation($params);
            //ends...
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
        return $redirect_to;
    }

    public function investor($data)
    {
        try {
            $data['created_at'] = Carbon::now();
            try {
                $investor_id = $this->investor->insertGetId($data);
            } catch (\Exception $e) {
                $this->investor->where('email', $data['email'])->update($data);
                $investor_id = $this->investor->where('email', $data['email'])->first()->id;
            }
        } catch (Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
        Session::put('green', 1);
        return redirect("pricing/managed-account/subscription?investor_id=".$investor_id."&name=".$data['name']."&email=".$data['email']."&equity=".$data['trading_account_balance'])->withErrors("Your request has been submitted successfully.");
    }

    public function saveinvestor($userId, $data)
    {
        try {
            $this->investor->where('id', $userId)->update($data);
        } catch (Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }

    public function addlinecopier($data)
    {
        try {
            $redirect_to = $this->subscription->fetchplan($data['plan_id'], 'url');
            unset($data['plan_id']);
            $data['created_at'] = Carbon::now();
            $data['updated_at'] = Carbon::now();
            $this->copier->insert($data);
            return redirect($redirect_to->url);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function forgotPassword($data)
    {
        try {
            $user = $this->user->where('email', $data)->where('active', 1)->first();
            if ($user !== null) {
                $this->notification->sendPasswordRecoveryLink($user);
                /*$prev_req = $this->passwordrequest->where('user_id', $user->id)->where('active', 1)->first();
                if(!$prev_req)
                {
                   $params = ['user_id' => $user->id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
                   $this->passwordrequest->insert($params);
                } else {
                    Session::put('yellow', 1);
                    return redirect(URL::previous())->withErrors("You previously requested a new password, we shall get in touch with you via email.")->withInput();
                }*/
            } else {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors("The email you provided doesn't exist. Try registered email")->withInput();
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        Session::put('green', 1);
        return redirect(URL::previous())->withErrors("A password recovery will be sent to ".$user->email." shortly, please endeavour to check both inbox and spam box. Thanks");
    }

    public function setnewpassword($uid, $param)
    {
        try {
            $this->user->where('id', $uid)->update($param);

            //auto login after password changed
            Auth::loginUsingId($uid);

            Session::put('green', 1);
            return redirect('access/members-area');
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function logout()
    {
        try {
            Auth::logout();
            Session::flush();
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
        Session::put('blue', 1);
        return redirect()->route('login_page')->withErrors("You are now logged out");
    }
}
