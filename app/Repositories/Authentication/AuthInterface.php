<?php

namespace App\Repositories\Authentication;

interface AuthInterface {
	public function authenticate($request);

	public function createnew($data);

	public function investor($data);

	public function saveinvestor($userId, $data);

	public function addlinecopier($data);
	
	public function forgotPassword($data);

	public function logout();
}