<?php namespace App\Repositories\Authentication;
use Illuminate\Support\ServiceProvider;
class AuthRepoServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Authentication\AuthInterface', 'App\Repositories\Authentication\AuthRepository');
    }
}