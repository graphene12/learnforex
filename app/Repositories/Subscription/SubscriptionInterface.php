<?php

namespace App\Repositories\Subscription;

interface SubscriptionInterface {
	public function check($user_id);

	public function plan();

	public function register($params);
	
	public function storefirstplan($user_id, $col);
}