<?php

namespace App\Repositories\Subscription;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Crypt;
use URL;
use Session;
use Auth;
use App\Models\User;
use App\Models\Subscription;
use App\Models\Plan;
use App\Models\Promo;
class SubscriptionRepository implements SubscriptionInterface {
    private $subscription;
    private $plan;
    private $promo;
    private $auth;
    private $user;

    public function __construct(Subscription $subscription, Plan $plan, Promo $promo, Auth $auth, User $user) {
        $this->subscription = $subscription;
        $this->plan = $plan;
        $this->promo = $promo;
        $this->auth = $auth;
        $this->user = $user;
    }

    function get_deactivation($today, $days)
    {
        $date = date_create($today);
        date_add($date, date_interval_create_from_date_string($days.' days'));
        $deactivate_on = date_format($date, 'Y-m-d');

        return $deactivate_on;
    }

    public function check($user_id)
    {
        try {
            if(Auth::user()->role != "admin")
            {
                if(Auth::user()->user_type == 2) {
                    $check = ['check' => 1];
                } else {
                    $sub = $this->subscription->where('user_id', $user_id)->first();
                    if($sub !== NULL) {
                        if($sub->active == 1) {
                            $today = date("Y-m-d");
                            if($sub->deactivated_on >= $today)
                            {
                                $check = ['check' => 1, 'plan' => $sub->plan->id, 'url' => $sub->plan->url, 'plan_name' => $sub->plan->title];
                            } else {
                                $this->subscription->where('user_id', $user_id)->update(['active' => 0]);
                                $check = ['check' => 0, 'plan' => $sub->plan->id, 'url' => $sub->plan->url, 'plan_name' => $sub->plan->title];
                            }
                        } else {
                            $check = ['check' => 0, 'plan' => $sub->plan->id, 'url' => $sub->plan->url, 'plan_name' => $sub->plan->title];
                        }
                    } else {
                        $check = ['check' => 2];
                    }
                }
            } else {
                $check = ['check' => 1];
            }
            return $check;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function register($params)
    {
        try {
            $find_prev_sub = $this->subscription->where('user_id', $params['user_id'])->first();
            $today = date("Y-m-d");
            $params['updated_at'] = Carbon::now();
            $params['created_at'] = Carbon::now();
            $params['activated_on'] = $today;
            if($find_prev_sub !== NULL)
            {
                $params['deactivated_on'] = $this->get_deactivation($today, $find_prev_sub->plan->days);
                $this->subscription->where('user_id', $params['user_id'])->update($params);
            } else {
                $plan = $this->fetchplan($params['plan_id'], 'days');
                $params['deactivated_on'] = $this->get_deactivation($today, $plan->days);
                $this->subscription->insert($params);
            }

            if($params['plan_id'] == 6)
            {
                $redirect_to = $this->fetchplan($params['plan_id'], 'url');
                return $redirect_to->url;
            }
        } catch (Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }

    public function plan()
    {
        try {
            $plans = $this->plan->orderBy('id', 'Asc')->get();
            $data = [
                "plans"     => $plans,
                "title"     => "Pricing Plan"
            ];

            return $data;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }

    public function fetchplan($plan_id, $col)
    {
        try {
            $plan = $this->plan->where('id', $plan_id)->select($col)->first();
            return $plan;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }

    public function storefirstplan($user_id, $col)
    {
        try {
            //dd("hello");
            $plan = $this->subscription->where('user_id', $user_id)->value($col);
            if($plan != NULL)
            {
                $this->user->where('id', $user_id)->update(['first_plan' => $plan]);
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }
}