<?php namespace App\Repositories\Subscription;
use Illuminate\Support\ServiceProvider;
class SubscriptionRepoServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Subscription\SubscriptionInterface', 'App\Repositories\Subscription\SubscriptionRepository');
    }
}