<?php namespace App\Repositories\Announcement;
use Illuminate\Support\ServiceProvider;
class AnnouncementRepoServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Announcement\AnnouncementInterface', 'App\Repositories\Announcement\AnnouncementRepository');
    }
}