<?php

namespace App\Repositories\Announcement;
use App\Repositories\Announcement\AnnouncementInterface as AnnouncementInterface;
use Illuminate\Http\Request;
use Crypt;
use Session;
use Carbon\Carbon;
use App\Models\Announcement;
class AnnouncementRepository implements AnnouncementInterface {
    private $announcement;

    public function __construct(Announcement $announcement) {
        $this->announcement = $announcement;
    }

    public function fetchall($params)
    {
        $announcements = $this->announcement->where($params)->latest()->get();
        $data = [];
        foreach ($announcements as $announcement) {
            $arr = [
                'id'                    => $announcement->id,
                'title'                 => $announcement->title,
                'content'               => $announcement->content,
                'admin_name'            => $announcement->administrator?$announcement->administrator->name:NULL,
                'admin_avatar'          => $announcement->administrator?$announcement->administrator->avatar:NULL,
                'callback_url'          => $announcement->callback_url,
                'call_to_action'        => $announcement->call_to_action,
                'content'               => $announcement->content,
                'delay'                 => $announcement->delay,
                'created_at'            => $announcement->created_at,
                'stop'                  => $announcement->stop,
            ];

            array_push($data, $arr);
        }
        
        return $data;
    }

    public function fetchone($params = null)
    {
        if($params != null)
        {
            $announcement = $this->announcement->where($params)->first();
        } else {
            $today = date("m/d/Y");
            $announcement = $this->announcement->where('stop', '>=', $today)->first();
        }
        if($announcement != null)
        {
            $data = [
                'id'                    => $announcement->id,
                'title'                 => $announcement->title,
                'content'               => $announcement->content,
                'admin_name'            => $announcement->administrator?$announcement->administrator->name:NULL,
                'admin_avatar'          => $announcement->administrator?$announcement->administrator->avatar:NULL,
                'content'               => $announcement->content,
                'callback_url'          => $announcement->callback_url,
                'call_to_action'        => $announcement->call_to_action,
                'delay'                 => $announcement->delay,
                'created_at'            => $announcement->created_at,
                'stop'                  => $announcement->stop,
            ];
        } else {
            $data = null;
        }

        return $data;
    }

    public function create($data)
    {
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

        $created = $this->announcement->insertGetId($data);
        return $created;
    }

    public function save($params, $data)
    {
        $this->announcement->where($params)->update($data);
        return true;
    }

    public function delete($params)
    {
        $this->announcement->where($params)->delete();
        return true;
    }
}