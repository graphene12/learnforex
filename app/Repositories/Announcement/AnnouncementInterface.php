<?php

namespace App\Repositories\Announcement;

interface AnnouncementInterface {
	public function fetchall($params);

	public function fetchone($params = null);

	public function create($data);

	public function save($params, $data);

	public function delete($data);
}