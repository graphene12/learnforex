<?php

namespace App\Repositories\User;

interface UserInterface {
	public function fetchall($term = null);

	public function fetchone($params);

	public function fetchinvestors();

	public function fetchinvestor($param);

	public function lotsizeorders();

	public function fetchequity();

	public function line_copier_subscribers();

	public function admins($cond, $params = null);

	public function createnew($params);
	
	public function deleteuser($params);

	public function makeadmin($params);

	public function removeadmin($params);

	public function approvepayment($params);

	public function deleteCopier($param);

	public function deleteinvestor($param);

	public function savechanges($user_id, $params);

	public function passwordrequests($cond);

	public function savenewpassword($uid, $params);

	public function changeavatar($uid, $file);
}