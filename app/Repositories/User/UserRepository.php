<?php

namespace App\Repositories\User;
use Illuminate\Http\Request;
use App\Repositories\Subscription\SubscriptionInterface as SubscriptionInterface;
use Carbon\Carbon;
use Crypt;
use URL;
use Session;
use Paginator;
use App\Models\User;
use App\Models\Investor;
use App\Models\Order;
use App\Models\Equity;
use App\Models\Copier;
use App\Models\Password;
use App\Models\Subscription;
class UserRepository implements UserInterface {
    private $user;
    private $investor;
    private $order;
    private $copier;
    private $passwordrequest;
    private $subscription;

    public function __construct(User $user, Investor $investor, Order $order, Equity $equity, Copier $copier, Password $passwordrequest, Subscription $subscription) {
        $this->user = $user;
        $this->investor = $investor;
        $this->order = $order;
        $this->equity = $equity;
        $this->copier = $copier;
        $this->passwordrequest = $passwordrequest;
        $this->subscription = $subscription;
    }

    public function fetchall($term = null)
    {
        try {
            if($term !== NULL)
            {
                $users = $this->user->where('name', 'LIKE', '%%'.$term.'%%')->where('active', 1)->latest()->get();
            } else {
                $users = $this->user->where('active', 1)->latest()->get();
            }
            
            return $users;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function fetchone($params)
    {
        try {
            $user = $this->user->where($params)->first();
            return $user;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function fetchinvestors()
    {
        try {
            $investors = $this->investor->where('active', 1)->latest()->get();
            return $investors;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function fetchinvestor($param)
    {
        try {
            $investor = $this->investor->where($param)->first();
            return $investor;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }

    public function lotsizeorders()
    {
        try {
            $orders = $this->order->where('completed', 'no')->latest()->get();
            return $orders;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function fetchequity()
    {
        try {
            $equity = $this->equity->orderBy('id', 'Desc')->get();
            return $equity;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function line_copier_subscribers()
    {
        try {
            $line_copiers = $this->copier->where('active', 1)->latest()->get();
            return $line_copiers;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function deletecopier($param)
    {
        try {
            $this->copier->where($param)->update(['active' => 0]);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function deleteinvestor($param)
    {
        try {
            $this->investor->where($param)->update(['active' => 0]);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function admins($cond, $params = null)
    {
        try {
            if($params !== null)
            {
                $admins = $this->user->where($cond)->select($params)->latest()->get();
            } else {
                $admins = $this->user->where($cond)->latest()->get();
            }
            return $admins;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }

    public function createnew($params)
    {
        try {
            $params['password'] = bcrypt($params['password']);
            $this->user->insert($params);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function savechanges($user_id, $params)
    {
        $save_changes = $this->user->where('id', $user_id)->update($params);
        return $save_changes;
    }

    public function deleteuser($params)
    {
        try {
            $this->user->where($params)->delete();
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function passwordrequests($cond)
    {
        try {
            $requests = $this->passwordrequest->where($cond)->latest()->paginate(10);
            return $requests;
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function savenewpassword($uid, $params)
    {
        try {
            $this->user->where('id', $uid)->update($params);
            $this->passwordrequest->where('user_id', $uid)->update(['active' => 0]);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function makeadmin($param)
    {
        try {
            $this->user->where($param)->update(['role' => 'admin']);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function removeadmin($param)
    {
        try {
            $this->user->where($param)->update(['role' => NULL]);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function approvepayment($param)
    {
        try {
            $today = date("Y-m-d");
            $find_prev_sub = $this->subscription->where('user_id', $param['user_id'])->first();
            
            $this->subscription->where($param)->update(['active' => 1,'activated_on' => $today,'deactivated_on' => app('App\Repositories\Subscription\SubscriptionRepository')->get_deactivation($today,$find_prev_sub->plan->days)]);
            
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }
    
    public function deactivateSubscription($param)
    {
        try {
            $today = date("Y-m-d");
            $find_prev_sub = $this->subscription->where('user_id', $param['user_id'])->first();
            
            $this->subscription->where($param)->update(['active' => 0,'activated_on' => $today,'deactivated_on' => $today]);
            
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function changeavatar($uid, $file)
    {
        try {
            $extension = $file->getClientOriginalExtension();
            $destinationPath = public_path('assets/uploads/avatars/');
            
            switch ($extension) {
                case 'jpeg':
                    break;
                case 'jpg':
                    break;
                case 'png':
                    break;
                default:
                    return redirect(URL::previous())->withErrors("Image file format type must be - jpeg, jpg, png");
            }

            $filename = 'useravatar_'.str_random(30);
            $destinationFilename = $filename.'.'.$extension;
            $upload_success = $file->move($destinationPath, $destinationFilename);

            $avatar = $destinationFilename;

            $this->user->where('id', $uid)->update(['avatar' => $avatar]);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        Session::put('green', 1);
        return redirect(URL::previous())->withErrors("Profile photo changed successfully");
    }
}