<?php

namespace App\Repositories\Video;

interface VideoInterface {
	public function fetchone($params = null);

	public function create($data);

	public function save($params, $data);

	public function delete($data);
}