<?php

namespace App\Repositories\Video;
use App\Repositories\Video\VideoInterface as VideoInterface;
use Illuminate\Http\Request;
use Crypt;
use Session;
use Carbon\Carbon;
use App\Models\Video;
class VideoRepository implements VideoInterface {
    private $video;

    public function __construct(Video $video) {
        $this->video = $video;
    }

    public function fetchone($params = null)
    {
        $video = $this->video->orderBy('created_at', 'desc')->first();

        if($video != null)
        {
            $data = [
                'id'       => $video->id,
                'title'    => $video->title,
                'url'      => $video->url,
                'admin_id' => $video->admin_id,
            ];
        } else {
            $data = null;
        }

        return $data;
    }

    public function create($data)
    {
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

        $created = $this->video->insertGetId($data);
        return $created;
    }

    public function save($params, $data)
    {
        $this->video->where($params)->update($data);
        return true;
    }

    public function delete($params)
    {
        $this->video->where($params)->delete();
        return true;
    }
}