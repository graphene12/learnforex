<?php namespace App\Repositories\Video;
use Illuminate\Support\ServiceProvider;
class VideoRepoServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Video\VideoInterface', 'App\Repositories\Video\VideoRepository');
    }
}