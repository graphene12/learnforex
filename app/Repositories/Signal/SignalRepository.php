<?php

namespace App\Repositories\Signal;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Crypt;
use URL;
use Session;
use Auth;
use App\Models\Signal;
use App\Models\Counter;
use App\Models\Like;
use App\Models\Dislike;
use App\Models\User;
class SignalRepository implements SignalInterface {
    private $signal;
    private $signal_counter;
    private $like;
    private $dislike;
    private $user;

    public function __construct(Signal $signal, Counter $signal_counter, Like $like, Dislike $dislike, User $user) {
        $this->signal = $signal;
        $this->signal_counter = $signal_counter;
        $this->like = $like;
        $this->dislike = $dislike;
        $this->user = $user;
    }

    public function checknotification($uid)
    {
        try {
            $notify = $this->user->where('id', $uid)->value('notify');
            if($notify == 1)
            {
                $this->user->where('id', $uid)->update(['notify' => 0]);
                $indicator = 1;
            } else {
                $indicator = 0;
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
        return response()->json(['indicator' => $indicator]);
    }

    public function fetchall($page = null)
    {
        try {
            $_signals = $this->signal->where('active', 1);
            if(Session::has('admin') || (isset($page) && $page = "signal-page"))
            {
                return $_signals->latest()->paginate(10);
            } else {
                $get_signals = $_signals->latest()->take(10)->get();
                $signals = [];
                Session::forget('admin');
                
                $current_signal_count = $this->signal_counter->where('id', 1)->select('count')->first();
                if($current_signal_count)
                {
                    $new_signal_count = ($get_signals == NULL)?0:$_signals->count();
                    
                    if($new_signal_count > $current_signal_count->count)
                    {
                        $this->signal_counter->where('id', 1)->update(['count' => $new_signal_count]);
                        $this->user->where('active', 1)->update(['notify' => true]);
                    } elseif($new_signal_count == $current_signal_count->count) {
                        $this->user->where('active', 1)->update(['notify' => false]);
                    } elseif($new_signal_count < $current_signal_count) {
                        $this->signal_counter->where('id', 1)->update(['count' => $current_signal_count->count]);
                        $this->user->where('active', 1)->update(['notify' => true]);
                    } else {
                        $this->user->where('active', 1)->update(['notify' => false]);
                    }
                } else {
                    $params = ['count' => count($get_signals), 'created_at' => Carbon::now()];
                    $this->signal_counter->insert($params);
                    $indicator['ringbell'] = true;
                    $this->user->where('active', 1)->update(['notify' => true]);
                }

                foreach ($get_signals as $signal) {
                    $signal_array['admin'] = $signal->admin->name;
                    $signal_array['signal_info'] = $signal;
                    $signal_array['likes'] = (count($signal->likes) > 0)?count($signal->likes):0;
                    $signal_array['dislikes'] = (count($signal->dislikes) > 0)?count($signal->dislikes):0;
                    $signal_array['posted_time'] = Carbon::createFromTimeStamp(strtotime($signal->created_at))->diffForHumans();
                    if((URL::to('/') == "http://localhost:3000") || (URL::to('/') == "http://localhost:8000"))
                    {
                        $signal_array['signal_image'] = URL('/').'/assets/uploads/trade_signals/'.$signal->image;
                    } else {
                        $signal_array['signal_image'] = URL('/').'/core/public/assets/uploads/trade_signals/'.$signal->image;
                    }

                    if($signal->admin->avatar)
                    {
                        if((URL::to('/') == "http://localhost:3000") || (URL::to('/') == "http://localhost:8000"))
                        {
                            $signal_array['admin_image'] = URL('/').'/core/public/assets/uploads/avatars/'.$signal->admin->avatar;
                        } else {
                            $signal_array['signal_image'] = URL('/').'/core/public/assets/uploads/trade_signals/'.$signal->image;
                        }
                    } else {
                        $signal_array['admin_image'] = URL('/').'/assets/img/default-avatar.png';
                    }

                    array_push($signals, $signal_array);
                }
                return response()->json(['signals' => $signals]);
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function fetchone($params)
    {
        try {
            $signal = $this->signal->where($params)->first();
            return $signal;
        } catch (Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function uploadsignal($params)
    {
        try {
            if($params['image'])
            {
                $file = $params['image'];
                $image = $this->uploadsignalimage($file);

                $params['image'] = $image;
            } else {
                Session::put('red', 1);
                return redirect(URL::previous())->withErrors("You must upload a signal image")->withInput();
            }
            $params['created_at'] = Carbon::now();
            $params['updated_at'] = Carbon::now();
            $this->signal->insert($params);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function savesignal($signal_id, $params)
    {
        try {
            if(isset($params['image']))
            {
                $file = $params['image'];
                $image = $this->uploadsignalimage($file);

                $params['image'] = $image;
            }
            
            $this->signal->where('id', $signal_id)->update($params);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function uploadsignalimage($file)
    {
        try {
            $extension = $file->getClientOriginalExtension();
            $destinationPath = public_path('assets/uploads/trade_signals/');
            
            $filename = 'trade_signal_'.str_random(15);
            $destinationFilename = $filename.'.'.$extension;
            $upload_success = $file->move($destinationPath, $destinationFilename);

            $image = $destinationFilename;

            return $image;
        } catch (\Exception $e) {
            return response()->json(['message' => "oops, something went wrong, it wasn't your fault"], 500);
        }
    }

    public function likeSignal($params)
    {
        try {
            $find_signal = $this->signal->where('id', $params['signal_id'])->first();
            if($find_signal)
            {
                $prev_like = $this->like->where('signal_id', $params['signal_id'])->where('user_id', Auth::user()->id)->first();
                if($prev_like === null)
                {
                    $prev_dislike = $this->dislike->where('signal_id', $params['signal_id'])->where('user_id', Auth::user()->id)->first();
                    if($prev_dislike === null) {
                        
                        $data = [
                            'signal_id'     => $params['signal_id'], 
                            'user_id'       => Auth::user()->id,
                            'created_at'    => Carbon::now(),
                            'updated_at'    => Carbon::now()
                        ];
                        $this->like->insert($data);
                    } else {
                        return response()->json(['message' => 'already disliked signal', 'indicator' => false]);
                    }
                } else {
                    $prev_like = $this->like->where('signal_id', $params['signal_id'])->where('user_id', Auth::user()->id)->delete();

                    return response()->json(['message' => 'remove like', 'remove' => true]);
                }
            } else {
                return response()->json(['message' => 'signal not found']);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['message' => 'liked', 'indicator' => true]);
    }

    public function dislikeSignal($params)
    {
        try {
            $find_signal = $this->signal->where('id', $params['signal_id'])->first();
            if($find_signal)
            {
                $prev_dislike = $this->dislike->where('signal_id', $params['signal_id'])->where('user_id', Auth::user()->id)->first();
                if($prev_dislike === null)
                {
                    $prev_like = $this->like->where('signal_id', $params['signal_id'])->where('user_id', Auth::user()->id)->first();
                    if($prev_like == NULL)
                    {
                        $data = [
                            'signal_id'     => $params['signal_id'], 
                            'user_id'       => Auth::user()->id,
                            'created_at'    => Carbon::now(),
                            'updated_at'    => Carbon::now()
                        ];
                        $this->dislike->insert($data);
                    } else {
                        return response()->json(['message' => 'already disliked signal', 'indicator' => false]);
                    }
                } else {
                    $prev_dislike = $this->dislike->where('signal_id', $params['signal_id'])->where('user_id', Auth::user()->id)->delete();

                    return response()->json(['message' => 'remove dislike', 'remove' => true]);
                }
            } else {
                return response()->json(['message' => 'signal not found']);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }

        return response()->json(['message' => 'disliked', 'indicator' => true]);    
    }

    public function deleteSignal($param)
    {
        try {
            $this->signal->where($param)->delete();
            $this->signal_counter->where('id', 1)->decrement('count', 1);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }
}