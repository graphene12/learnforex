<?php

namespace App\Repositories\Signal;

interface SignalInterface {
	public function fetchall($page = null);

	public function fetchone($params);

	public function uploadsignal($params);

	public function savesignal($signal_id, $params);

	public function likeSignal($params);

	public function dislikeSignal($params);

	public function deleteSignal($param);

	public function checknotification($uid);
}