<?php namespace App\Repositories\Signal;
use Illuminate\Support\ServiceProvider;
class SignalRepoServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Signal\SignalInterface', 'App\Repositories\Signal\SignalRepository');
    }
}