<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Validator;
use URL;
class LoadController extends Controller
{
    public function contactusAction(Request $request)
    {
    	try {
    		$data = $request->except('_token');
	    	/*$data['message'] = "N";
	        $data['title'] = ucwords("Welcome Aboard");
	        $data['salute'] = "Hi ".$data['name'];
	        $data['url'] = $activate;

	        Mail::send('emails.customer', ['data' => $data], function ($m) use ($data) {
	            $m->from("info@learnforexlivesignals.com", $data['name']);
	            $m->to("support@learnforexlivesignals", "Site Master")->subject('Contact Us Message');
	        });*/

	        $to = 'yusuftunde001@yahoo.com';

			$subject = 'Website Change Request';

			$headers = "From: info@learnforexlivesignals.com \r\n";
			$headers .= "Reply-To: info@learnforexlivesignals.com \r\n";
			/*$headers .= "CC: susan@example.com\r\n";*/
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	        $message = '<html><body>';
			$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . strip_tags($data['name']) . "</td></tr>";
			$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($data['email']) . "</td></tr>";
			$message .= "<tr><td><strong>Message</strong> </td><td>" . strip_tags($data['message']) . "</td></tr>";
			$message .= "</table>";
			$message .= "</body></html>";

			mail($to, $subject, $message, $headers);	
    	} catch (\Exception $e) {
    		Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
    	}

        return redirect(URL::previous())->withErrors("Thank you for taking time to send us a message");
    }
    
    public function lotsizeIndicator(Request $request){
        return view('static.lot-size-indicator');
    }
    
    public function mentorship(Request $request){
        return view('static.mentorship');
    }
}
