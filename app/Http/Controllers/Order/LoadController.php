<?php

namespace App\Http\Controllers\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Validator;
use Carbon\Carbon;
use URL;
use App\Models\Order;
use App\Models\Equity;
class LoadController extends Controller
{
    private $order;
    private $equity;

    public function __construct(Order $order, Equity $equity)
    {
        $this->order = $order;
        $this->equity = $equity;
    }

    public function orderlotsizevalidator($data)
    {
        return Validator::make($data, [
            'name'  => 'required|max:200',
            'email' => 'required|max:256'
        ]);
    }

    public function equityvalidator($data)
    {
        return Validator::make($data, [
            'name'      => 'required|max:200',
            'email'     => 'required|max:256',
            'equity'    => 'required|numeric|min:5000'
        ]);
    }

    public function orderlotsize(Request $request)
    {
        try {
            $data = $request->except('_token');
            $validation = $this->orderlotsizevalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else{
                $find_prev_order = $this->order->where('email', $data['email'])->where('completed', 'no')->first();
                if($find_prev_order !== NULL)
                {
                    Session::put('yellow', 1);
                    return redirect(URL::previous())->withErrors("You can't resend this request, your previous order isn't completed yet!")->withInput();
                } else {
                    $data['created_at'] = Carbon::now();
                    $this->order->insert($data);
                }
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return redirect(URL::previous())->withErrors("Your request has been sent successfully. You will get a feedback from us ASAP. Please, do not resend this request!");
    }

    public function equityaccount(Request $request)
    {
        try {
            $data = $request->except('_token');
            $validation = $this->equityvalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else{
                $data['created_at'] = Carbon::now();
                $this->equity->insert($data);
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return redirect('managed-accounts/')->withErrors("Your request has been received. We will get back to you via email. Please, endeavour to check both spam and inbox. Thank you!");
    }
}