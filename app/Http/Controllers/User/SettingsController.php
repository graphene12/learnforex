<?php

namespace App\Http\Controllers\User;
use App\Repositories\User\UserInterface as UserInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use URL;

class SettingsController extends Controller
{
    private $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function changeavatar(Request $request)
    {
    	try {
    		$file = $request->file;
    		$uid = $request->uid;
    		return $this->user->changeavatar($uid, $file);
    	} catch (\Exception $e) {
    		Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
    	}
    }

    public function update(Request $request)
    {
        try {
            $params = $request->except('_token');
            $this->user->savechanges($params);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
        Session::put('green', 1);
        return redirect()->back()->withErrors("Changes successfully saved!");
    }
}
