<?php

namespace App\Http\Controllers\Pricing;

use App\Repositories\Subscription\SubscriptionInterface as SubscriptionInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use URL;
use Auth;
use Session;
use Crypt;
use App\Models\Investor;

class LoadController extends Controller
{
    private $subscription;
    private $auth;
    private $investor;
    public function __construct(SubscriptionInterface $subscription, Auth $auth, Investor $investor)
    {
        $this->subscription = $subscription;
        $this->auth = $auth;
        $this->investor = $investor;
    }

    public function pricingPage()
    {
        try {
            $data = $this->subscription->plan();
            return view('new_pages.pricing', $data);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }

    public function activatePlan($plan_id, Request $request)
    {
        try {
            $plan_id = Crypt::decrypt($plan_id);
            $params = [
                'user_id'    => Auth::user()->id,
                'plan_id'    => $plan_id
            ];
            $this->subscription->register($params);
            
            // Redirect to payment page
            return redirect($request->payment_url);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }

    public function managedAccountPlans(Request $request)
    {
        try {
            $data = [
                'title'     => "Managed Account Subscriptions",
                'name'      => $request->name,
                'email'     => $request->email,
                'equity'    => $request->equity,
                'id'        => $request->investor_id
            ];
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
        return view('new_pages.subscriptions', $data);
    }

    public function activateInvestorsPlan(Request $request)
    {
        try {
            $this->investor->where('id', $request->investor_id)->update(['plan' => $request->plan]);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }

        return redirect('managed-accounts/')->withErrors("Your request has been received. We will get back to you via email. Please, endeavour to check both spam and inbox. Thank you!");
    }
}
