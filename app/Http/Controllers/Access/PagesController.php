<?php

namespace App\Http\Controllers\Access;
use App\Repositories\Subscription\SubscriptionInterface as SubscriptionInterface;
use App\Repositories\User\UserInterface as UserInterface;
use App\Repositories\Signal\SignalInterface as SignalInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use URL;
use Auth;
use Session;
use Crypt;
use App\Models\Schedule;
use App\Models\Channel;
use App\Models\Order;
use App\Models\Video;
class PagesController extends Controller
{
	private $subscription;
    private $signal;
    private $schedule;
    private $channel;
    private $user;
    public function __construct(SubscriptionInterface $subscription, SignalInterface $signal, Schedule $schedule, Channel $channel, UserInterface $user)
    {
        $this->subscription = $subscription;
        $this->signal = $signal;
        $this->schedule = $schedule;
        $this->channel = $channel;
        $this->user = $user;
    }

    public function dashboard()
    {
        try {
            $user_id = Auth::user()->id;
            $params = ['id' => $user_id];
            $user = $this->user->fetchone($params);
            $data['title'] = "Dashboard";
            $data['user'] = $user;

        } catch (Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return view('restricted.dashboard', $data);
    }

    public function liveRoom(Request $request = null)
    {
    	try {
            if(Auth::check())
            {
        		$user = Auth::user();
        		$user_id = $user->id;
        		$subscription = $this->subscription->check($user_id);
                $schedule = $this->schedule->select('time')->first();
                $broadcast = $this->channel->select('channel')->first();
                
                if($user->firstplan === NULL)
                {
                    $plan = $this->subscription->storefirstplan($user->id, 'plan_id');
                }

                if($subscription['check'] !== 1)
                {
                    Session::put('blue', 1);
                    if(isset($subscription['url']))
                    {
                        
                        Session::put('red', 1);
                        return redirect()->route('pricing')->withErrors('Please pick a subscription Plan');
                    } else {
                        Session::put('red', 1);
                        return redirect()->route('pricing')->withErrors('Please pick a subscription Plan');
                    }
                } else {
        			$data = [
        				'title'			=> 'Members Area',
        				'user'			=> $user,
        				'sub_notice'	=> $subscription['check'],
                        'schedule'      => ($schedule !== NULL)?$schedule->time:NULL,
                        'broadcast'     => $broadcast,
                        'video' => Video::orderBy('created_at', 'desc')->first(),

        			];
                    
                    if(isset($subscription['url']))
                    {
                        $data['url'] = $subscription['url'];
                        $data['plan_id'] = $subscription['plan'];
                    } else {
                        $data['url'] = URL('/').'/pricing';
                        $data['plan_id'] = NULL;
                    }
                }
            } else {
                $data = [
                    'title'         => 'Members Area',
                    'user'          => NULL,
                    'video' => Video::orderBy('created_at', 'desc')->first(),
                ];

                if($request->plan_id)
                {
                    $data['plan_id'] = Crypt::decrypt($request->plan_id);
                } 
            }
            Session::put('signal-page', true);
    	} catch (\Exception $e) {
    		Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
    	}

    	return view('restricted.members_area', $data);
    }

    public function fetchSignals()
    {
        return $this->signal->fetchall();
    }

    public function allsignals()
    {
        Session::put('signal-page', true);
        $page = "signal-page";
        $signals = $this->signal->fetchall($page);
        $data = ['signals' => $signals];
        return view('restricted.dump_signals', $data);
    }

    public function checknotification()
    {
        $uid = Auth::user()->id;
        return $this->signal->checknotification($uid);
    }

    public function managedAccountForm()
    {
        try {
            $data = ['title' => 'Managed Account'];
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }

        return view('new_pages.managed_accounts', $data);
    }

    public function likeSignal(Request $request)
    {
        try {
            return $this->signal->likeSignal($request);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }

    public function dislikeSignal(Request $request)
    {
        try {
            return $this->signal->dislikeSignal($request);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }

    public function lineCopier(Request $request)
    {
        try {
            $plan_id = Crypt::decrypt($request->plan);
            $data = [
                'title'     => 'Line Copier',
                'plan_id'   => $plan_id
            ];
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }

        return view('restricted.linecopier', $data);
    }
    
      public function register(Request $request){
        $validatedData = $request->validate([
        'name' => 'required|alpha',
        'email' => 'required|unique:orders',
    ]);
        dd($validatedData);
        if($validatedData){
            $order = new Order();
            $order->name = ucfirst(strtolower($request['name']));
            $order->email = $request['email'];
            $order->save();
            return redirect()->route('home');
        }
    }
}