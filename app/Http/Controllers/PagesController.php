<?php
use App\Models\Announcement;
namespace App\Http\Controllers;
use App\Repositories\Announcement\AnnouncementInterface as Announcement;
use App\Repositories\Video\VideoInterface as Video;
use Illuminate\Http\Request;
class PagesController extends Controller
{
    private $announcement;
    private $video;

    public function __construct(Announcement $announcement, Video $video)
    {
        $this->announcement = $announcement;
        $this->video = $video;
    }

    public function homePage(Request $request)
    {
        //fetch any available announcement
        $announcement = $this->announcement->fetchone();
        //fetch live video from the DB
        
        $video = $this->video->fetchone();
    	$data = [
            'title'         => "Home",
            'video'         => $video,
            'announcement'  => ($announcement != null)?$announcement:NULL,
        ];
        
        
    	return view('new_pages.home', $data);
    }

    public function contactPage()
    {
    	$data = ['title' => "Contact Us"];
    	return view('new_pages.contact', $data);
    }

    public function livetrade()
    {
        //fetch live video from the DB
        $video = $this->video->fetchone();
        $data = ['title' => 'Live Trade', 'video' => $video];

        return view('static.live-trade', $data);
    }



    public function loginPage(Request $request){
        $data['title'] = 'Login';
        return view('new_pages.login',['data'=>$data]);
    }

    public function registerPage(Request $request){
        $data['title'] = 'Sign up';
        return view('new_pages.sign_up',['data' => $data]);
    }
    
  
}
