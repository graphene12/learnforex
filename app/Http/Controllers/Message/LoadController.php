<?php

namespace App\Http\Controllers\Message;
use App\Repositories\Message\MessageInterface as MessageInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use URL;
class LoadController extends Controller
{
    private $message;
    public function __construct(MessageInterface $message)
    {
        $this->message = $message;
    }

    public function messagevalidator($data)
    {
        return Validator::make($data, [
            'message'  => 'required'
        ]);
    }

    public function send(Request $request)
    {
    	try {
    		$data = $request->except('_token');
            return $this->message->send($data);
    	} catch (Exception $e) {
    		Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");	
    	}
    }

    public function fetch()
    {
        try {
            return $this->message->fetch();
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while"); 
        }
    }
}
