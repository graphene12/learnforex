<?php

namespace App\Http\Controllers\Payment;
use Illuminate\Support\Facades\Redirect;
use App\Models\Plan;
use Auth;
use Session;
use URL;
use \GuzzleHttp\Client as HTTPClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Subscription\SubscriptionInterface as SubscriptionInterface;

class PayDotController extends Controller
{

    public function paydot(Request $request, SubscriptionInterface $sub_interface){
        
        try {
            $params = [
                'user_id'    => Auth::user()->id,
                'plan_id'    => $request->plan_id
            ];
            
            $value = $sub_interface->register($params);
            
            
            
            switch($params['plan_id']){
                case 1:
                    $url = "https://paystack.com/pay/j-90swz7yy";
                    return Redirect::to($url);
                    break;
                case 2:
                    $url = "https://paystack.com/pay/xaduou0xqj";
                    return Redirect::to($url);
                    break;
                case 3:
                    $url = "https://paystack.com/pay/fyo1h0hff5";
                    return Redirect::to($url);
                    break;
                case 4:
                    $url = "https://paystack.com/pay/--ghzrsqti";
                    return Redirect::to($url);
                    break;
                case 5:
                    $url = "https://paystack.com/pay/8l1q980dcs";
                    return Redirect::to($url);
                    break;
                case 6:
                    $url = "https://paystack.com/pay/3duunb0jsu";
                    return Redirect::to($url);
                    break;
                
            
            }
            
        }catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }
}
