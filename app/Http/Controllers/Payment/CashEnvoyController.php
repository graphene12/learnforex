<?php

namespace App\Http\Controllers\Payment;

use App\Models\Plan;
use Auth;
use Session;
use URL;
use \GuzzleHttp\Client as HTTPClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Subscription\SubscriptionInterface as SubscriptionInterface;

class CashEnvoyController extends Controller
{
    private $key = '4be5a1ff839debf82f86fe1878a42c59';
    private $merchant_id = '4969';
    
    public function standard(Request $request, SubscriptionInterface $sub_interface)
    {   
        
        $forex_api_token = '01d8d93656d842ec8ae06bdc8ed87948';
        try {
            $params = [
                'user_id'    => Auth::user()->id,
                'plan_id'    => $request->plan_id
            ];
            $sub_interface->register($params);
            
            $plan = Plan::find($request->plan_id);
            $desc = 'Learn Forex Live Signals - '.ucwords($plan->title).' ('.ucwords($plan->validity).')';
            
            $ceamt = $plan->price;
            $tref = (new \DateTime('now'))->format('YmdHis');
            $client = new HTTPClient;
            $res = $client->request('GET', 'https://openexchangerates.org/api/latest.json?app_id='.$forex_api_token.'&base=USD');
            $ceamt = json_decode($res->getBody(), true)['rates']['NGN'] * $ceamt;
            
            $data = $this->key.$tref.$ceamt;
            
            $signature = hash_hmac('sha256', $data, $this->key, false);
            
            return view('payment.cashenvoy', [
                'sig' => $signature,
                'desc' => $desc,
                'amt' => $ceamt,
                'id' => Auth::user()->id,
                'tref' => $tref,
                'key' => $this->key,
                'merchant_id' => $this->merchant_id
            ]);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
        
    }
    
    public function paymentResponse(Request $request)
    {
        // {"ce_transref":"20180114101908","ce_amount":"17858.37","ce_customerid":"1","ce_memo":"Learn Forex Live Signals - Standard (Month)","ce_response":"A","ce_data":null}

        $transref = $request->input('ce_transref');
        $type = 'json';
        $cdata = $this->key.$transref.$this->merchant_id;
        $signature = hash_hmac('sha256', $cdata, $this->key, false);
        $response = $this->getStatus($transref, $this->merchant_id, $type, $signature);
        $response = json_decode($response, true);
        switch ($response['TransactionStatus']) {
            case 'C00':
                Session::put('green', 1);
                return redirect('/')->withErrors("Transaction Successful");
                break;
                
            case 'C01':
                Session::put('red', 1);
                return redirect('/')->withErrors("Transaction Canceled By You");
                break;
                
            case 'C02':
                Session::put('red', 1);
                return redirect('/')->withErrors("Transaction Canceled Due To Inactivity");
                break;
                
            case 'C03':
                Session::put('red', 1);
                return redirect('/');
                break;
                
            case 'C04':
                Session::put('red', 1);
                return redirect('/')->withErrors("Transaction Failed - Insufficient funds");
                break;
                
            case 'C05':
                Session::put('red', 1);
                return redirect('/')->withErrors("Transaction Failed - Contact Admin");
                break;
            
            default:
                Session::put('red', 1);
                return redirect('/')->withErrors("Oops, we did something wrong. try again after a while");
                break;
        }
        return $response;
    }
    
    public function getStatus($transref, $mertid, $type='', $sign)
    {
        //initialize the request variables
        $request = 'mertid='.$mertid.'&transref='.$transref.'&respformat='.$type.'&signature='.$sign;
        
        //this is the url of the gateway's test api
        // $url = 'https://www.cashenvoy.com/sandbox/?cmd=requery';
        //this is the url of the gateway's live api
        $url = 'https://www.cashenvoy.com/webservice/?cmd=requery';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    }
}
