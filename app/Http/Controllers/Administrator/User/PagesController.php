<?php

namespace App\Http\Controllers\Administrator\User;
use App\Repositories\Signal\SignalInterface as SignalInterface;
use App\Repositories\User\UserInterface as UserInterface;
use App\Repositories\Subscription\SubscriptionInterface as SubscriptionInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Crypt;
use URL;
class PagesController extends Controller
{
    private $signals;
    private $user;
    private $admin;
    private $subscription;

    public function __construct(UserInterface $user, SignalInterface $signal, Auth $admin, SubscriptionInterface $subscription)
    {
        $this->signals = $signal;
    	$this->user = $user;
        $this->admin = $admin;
        $this->subscription = $subscription;
    }

    public function fetchInfo()
    {
        $users = $this->user->fetchall();
        $signals = $this->signals->fetchall();
        $investors = $this->user->fetchinvestors();
        $lot_orders = $this->user->lotsizeorders();
        $line_copier = $this->user->line_copier_subscribers();
        $equities = $this->user->fetchequity();

        $data = [
            'users'             => $users,
            'signals'           => $signals,
            'investors'         => $investors,
            'lot_orders'        => $lot_orders,
            'equities'          => $equities,
            'line_copiers'      => $line_copier
        ];

        return $data;
    }

    public function fetchusers(Request $request = null)
    {
    	try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }
            Session::put('admin', 1);
            $data = $this->fetchInfo();

            $data['admin'] = Auth::user();
            if($request->term !== NULL)
            {
                $data['title'] = "Showing results for ".ucwords($request->term);
                $data['users'] = $this->user->fetchall($request->term);
            } else {
                $data['title'] = "Administrator Backend | Users";
            }

    	} catch (\Exception $e) {
    		Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
    	}

    	return view('administrator.user.home', $data);
    }

    public function fetchorders()
    {
        try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }
            Session::put('admin', 1);
            $data = $this->fetchInfo();

            $data['admin'] = Auth::user();
            $data['title'] = "Administrator Backend | Lot Size Orders";

        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return view('administrator.orders.home', $data);
    }

    public function fetchequityaccounts()
    {
        try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }
            Session::put('admin', 1);
            $data = $this->fetchInfo();

            $data['admin'] = Auth::user();
            $data['title'] = "Administrator Backend | Equity Accounts";

        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return view('administrator.user.equityaccounts', $data);
    }

    public function fetchlinecopiers()
    {
        try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }
            Session::put('admin', 1);
            $data = $this->fetchInfo();

            $data['admin'] = Auth::user();
            $data['title'] = "Administrator Backend | Line Copier EA";

        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return view('administrator.user.linecopiers', $data);
    }

    public function fetchinvestors()
    {
        try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }
            Session::put('admin', 1);
            $data = $this->fetchInfo();

            $data['admin'] = Auth::user();
            $data['title'] = "Administrator Backend | Managed Accounts";
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return view('administrator.user.managedaccounts', $data);
    }

    public function fetchinvestor($user_id, Request $request = null)
    {
        try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }

            Session::put('admin', 1);
            $param = ['id' => Crypt::decrypt($user_id)];
            $data = $this->fetchInfo();

            $data['admin'] = Auth::user();
            $data['investor'] = $this->user->fetchinvestor($param);
            $data['title'] = "Administrator Backend | Investor";
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
        if($request->edit == null)
        {
            return view('administrator.user.managedaccount', $data);
        } else {
            $getplans = $this->subscription->plan();
            $data['plans'] = $getplans['plans'];
            return view('administrator.user.editinvestor', $data);
        }
    }

    public function addnewuser()
    {
    	try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }
            Session::put('admin', 1);
    		$data = [
                'title'  			=> "Administrator Backend | Users",
                'remove_counter' 	=> 1,
                'admin'             => Auth::user(),
    		];
    	} catch (\Exception $e) {
    		Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
    	}

    	return view('administrator.user.new', $data);
    }

    public function fetchone($user_id, Request $request = null)
    {
        try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }

            Session::put('admin', 1);
            $user_id = Crypt::decrypt($user_id);
            $params = ['id' => $user_id];
            $user = $this->user->fetchone($params);

            $data = $this->fetchInfo();
            $data['admin'] = Auth::user();
            $data['title'] = "Administrator Backend | User Profile";
            $data['user'] = $user;
        } catch (Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return view('administrator.user.user', $data);
    }

    public function fetchPasswordRequests()
    {
        try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }

            Session::put('admin', 1);
            $cond = ['active' => 1];
            $requests = $this->user->passwordrequests($cond);

            $data = $this->fetchInfo();
            $data['admin'] = Auth::user();
            $data['title'] = "Administrator Backend | Password Requests";
            $data['requests'] = $requests;
        } catch (Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return view('administrator.user.password_requests', $data);
    }
}
