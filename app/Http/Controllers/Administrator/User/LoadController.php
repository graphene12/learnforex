<?php

namespace App\Http\Controllers\Administrator\User;
use App\Repositories\User\UserInterface as UserInterface;
use App\Repositories\Authentication\AuthInterface as AuthInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Equity;
use Carbon\Carbon;
use Validator;
use Session;
use URL;
use Crypt;
class LoadController extends Controller
{
    private $user;
    private $auth;
    private $order;
    private $equity;
    public function __construct(UserInterface $user, AuthInterface $auth, Order $order, Equity $equity)
    {
        $this->user = $user;
        $this->auth = $auth;
        $this->order = $order;
        $this->equity = $equity;
    }

    public function createuservalidator($data)
    {
        return Validator::make($data, [
            'name'              => 'required|max:200',
            'email'             => 'required|email|unique:users|max:256',
            'password'          => 'required',
            'password_confirm'  => 'required|same:password',
            'user_type'     	=> 'required',
            'phone'             => 'required'
        ]);
    }

    public function editinvestorvalidator($data)
    {
        return Validator::make($data, [
            'name'                      => 'required|max:200',
            'broker_name'               => 'required|max:200',
            'trading_account_balance'   => 'required|numeric|min:5000',
            'mt4_password'              => 'required',
            'mt4_account_number'        => 'required'
        ]);
    }

    public function newpasswordvalidator($data)
    {
        return Validator::make($data, [
            'password'          => 'required',
            'password_confirm'  => 'required|same:password'
        ]);
    }

    public function createuser(Request $request)
    {
        try {
            $data = $request->except('_token');
            $validation = $this->createuservalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else{
                unset($data['password_confirm']);
                $data['created_at'] = Carbon::now();
                $data['updated_at'] = Carbon::now();
                $data['signal_option'] = "Telegram";

                $this->user->createnew($data);
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return redirect('administrator/users')->withErrors("User successfully created");
    }

    public function saveuser(Request $request)
    {
        try {
            $data = $request->except('_token');
            $validation = $this->edituservalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else{
                $save_changes = $this->user->savechanges($user_id, $request);
                if($save_changes !== true)
                {
                    return redirect(URL::previous())->withErrors("Oops, unable to save changes try again later.");
                }
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return redirect(URL::previous())->withErrors("Changes saved!");
    }

    public function deleteuser(Request $request)
    {
        try {
            $params = ['id' => $request->user];
            $this->user->deleteuser($params);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
        Session::put('green', 1);
        return redirect('administrator/users')->withErrors("User successfully deleted");
    }

    public function deletelinecopier(Request $request)
    {
        try {
            $param = ['id' => $request->lineCopier];
            $this->user->deleteCopier($param);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        Session::put('green', 1);
        return redirect('administrator/users/line-copiers')->withErrors("Subscriber successfully deleted");
    }

    public function deleteinvestor(Request $request)
    {
        try {
            $param = ['id' => $request->investor];
            $this->user->deleteinvestor($param);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        Session::put('green', 1);
        return redirect('administrator/managed-accounts')->withErrors("Investor successfully deleted");
    }

    public function resetPassword($user_id, Request $request)
    {
        try {
            $data = $request->except('_token');
            $validation = $this->newpasswordvalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else{
                unset($data['password_confirm']);
                $data['password'] = bcrypt($data['password']);

                $uid = Crypt::decrypt($user_id);
                $this->user->savenewpassword($uid, $data);
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return redirect(URL::previous())->withErrors("Password succesfully set.");
    }

    public function saveinvestor($user_id, Request $request)
    {
        try {
            $data = $request->except('_token');
            $validation = $this->editinvestorvalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else{
                $userId = Crypt::decrypt($user_id);
                $this->auth->saveinvestor($userId, $data);
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }

        return redirect('administrator/managed-accounts/'.$user_id)->withErrors("Changes saved successfully.");
    }

    public function completeorder($order_id)
    {
        try {
            $orderId = Crypt::decrypt($order_id);
            $this->order->where('id', $orderId)->update(['completed' => 'yes']);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }

        return redirect('administrator/lot-size-orders')->withErrors("Order marked as completed");
    }

    public function deleteequityaccount(Request $request)
    {
        try {
            $accountId = $request->id;
            if($accountId)
            {
                $this->equity->where('id', $accountId)->delete();
            } else {
                Session::put('red', 1);
                return redirect(URL::previous())->withErrors("Account deletion failed, please try again");
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }

        return redirect(URL::previous())->withErrors("Account deleted successfully");
    }

    public function makeadmin(Request $request)
    {
        try {
            $params = ['id' => $request->user];
            $this->user->makeadmin($params);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
        Session::put('green', 1);
        return redirect(URL::previous())->withErrors("User has been given administrative access");
    }

    public function removeadmin(Request $request)
    {
        try {
            $params = ['id' => $request->user];
            $this->user->removeadmin($params);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
        Session::put('green', 1);
        return redirect(URL::previous())->withErrors("Administrative access revoked successfully");
    }

    public function approvepayment(Request $request)
    {
        try {
            $params = ['user_id' => $request->user];
            
            $this->user->approvepayment($params);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
        Session::put('green', 1);
        return redirect(URL::previous())->withErrors("User payment approved successfully");
    }
    
    public function deactivateSubscription(Request $request){
        try {
            $params = ['user_id' => $request->user];
            
            $this->user->deactivateSubscription($params);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
        Session::put('green', 1);
        return redirect(URL::previous())->withErrors("User Subscription deactivated successfully.");
    }
}