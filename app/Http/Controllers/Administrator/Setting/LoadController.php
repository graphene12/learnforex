<?php

namespace App\Http\Controllers\Administrator\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Schedule;
use App\Models\Channel;
use Carbon\Carbon;
use URL;
use Session;
class LoadController extends Controller
{
    private $schedule;
    private $channel;

	public function __construct(Schedule $schedule, Channel $channel)
	{
        $this->schedule = $schedule;
        $this->channel = $channel;
	}

	public function saveSchedule(Request $request)
    {
    	try {
            $data = $request->except('_token');
            $data['created_at'] = Carbon::now();
            $data['updated_at'] = Carbon::now();
            $check_schedule = $this->schedule->where('id', 1)->first();

            if($check_schedule !== NULL)
            {
                $this->schedule->where('id', 1)->update($data);
            } else {
                $this->schedule->insert($data);
            }
    	} catch (\Exception $e) {
    		Session::put("red", 1);
    		return redirect(URL::previous())->withErrors($e->getMessage());
    	}

    	return redirect(URL::previous())->withErrors("New schedule saved");
    }

    public function switchChannel(Request $request)
    {
        try {
            $this->channel->where('id', 1)->update(['channel' => $request->channel]);
        } catch (\Exception $e) {
            Session::put("red", 1);
            return redirect(URL::previous())->withErrors($e->getMessage());
        }

        return redirect(URL::previous())->withErrors("Stream channel switched to ".ucwords($request->channel));
    }
}