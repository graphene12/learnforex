<?php

namespace App\Http\Controllers\Administrator\Video;
use App\Repositories\Video\VideoInterface as VideoInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
class LoadController extends Controller
{
    private $video;
    public function __construct(VideoInterface $video)
    {
        $this->video = $video;
    }

    public function create(Request $request)
    {
    	try {
	    	$data = $request->except('_token');
            $this->video->create($data);
    	} catch (Exception $e) {
    		Session::put('red', 1);
	        return redirect()->back()->withErrors($e->getMessage())->withInput();
    	}

        return redirect()->back()->withErrors("Live video successfully added");
    }

}
