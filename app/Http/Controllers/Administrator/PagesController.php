<?php namespace App\Http\Controllers\Administrator;

use App\Repositories\User\UserInterface as UserInterface;
use App\Repositories\Signal\SignalInterface as SignalInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Models\Schedule;
class PagesController extends Controller
{
	private $user;
    private $signals;
    private $admin;
    private $schedule;

	public function __construct(UserInterface $user, SignalInterface $signal, Auth $admin, Schedule $schedule)
	{
        $this->user = $user;
        $this->signals = $signal;
        $this->admin = $admin;
        $this->schedule = $schedule;
    }
    
    public function changeVideoPage(Request $request){
        try{
            $data = [
                'title'             => "Administrator Backend | Change Live Video"
    		];

            return view('administrator.youtube',$data);
        }catch (\Exception $e) {
    		return view('errors.500');
    	}
    }

    public function dashboard()
    {
    	try {
            Session::put('admin', 1);
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }

    		$users = $this->user->fetchall();
    		$signals = $this->signals->fetchall();
            $investors = $this->user->fetchinvestors();
            $lot_orders = $this->user->lotsizeorders();
            $line_copier = $this->user->line_copier_subscribers();
            $equities = $this->user->fetchequity();

    		$data = [
    			'users'				=> $users,
                'signals'           => $signals,
                'investors'         => $investors,
                'lot_orders'        => $lot_orders,
                'equities'          => $equities,
                'line_copiers'      => $line_copier,
                'admin'             => Auth::user(),
                'title'             => "Administrator Backend | Dashboard"
    		];

            return view('administrator.dashboard', $data);
    	} catch (\Exception $e) {
    		return view('errors.500');
    	}
    }
}
