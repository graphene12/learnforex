<?php

namespace App\Http\Controllers\Administrator\Signal;
use App\Repositories\Signal\SignalInterface as SignalInterface;
use App\Repositories\User\UserInterface as UserInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Crypt;
class PagesController extends Controller
{
    private $signals;
    private $user;
    private $admin;

    public function __construct(UserInterface $user, SignalInterface $signal, Auth $admin)
    {
        $this->signals = $signal;
    	$this->user = $user;
        $this->admin = $admin;
    }

    public function fetchInfo()
    {
        $users = $this->user->fetchall();
        $signals = $this->signals->fetchall();
        $investors = $this->user->fetchinvestors();
        $lot_orders = $this->user->lotsizeorders();
        $line_copier = $this->user->line_copier_subscribers();
        $equities = $this->user->fetchequity();

        $data = [
            'users'             => $users,
            'signals'           => $signals,
            'investors'         => $investors,
            'lot_orders'        => $lot_orders,
            'equities'          => $equities,
            'line_copiers'      => $line_copier,
        ];

        return $data;
    }

    public function fetchsignals()
    {
    	try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }
            Session::put('admin', 1);
    		$data = $this->fetchInfo();

            $data['admin'] = Auth::user();
            $data['title'] = "Administrator Backend | Trade Signals";
    	} catch (\Exception $e) {
    		Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
    	}

    	return view('administrator.signal.home', $data);
    }

    public function addnewsignal()
    {
    	try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }
            Session::put('admin', 1);
            $params = ['id', 'name'];
            $cond = ['role' => 'admin', 'active' => 1];
    		$data = [
                'title'  			=> "Administrator Backend | Trade Signals",
                'remove_counter' 	=> 1,
                'admin'             => Auth::user(),
                'admins'            => $this->user->admins($cond, $params)
    		];
    	} catch (\Exception $e) {
    		Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
    	}

    	return view('administrator.signal.new', $data);
    }

    public function fetchone($id, Request $request = null)
    {
        try {
            if(Auth::check() && Auth::user()->role != "admin")
            {
                return redirect('access/members-area');
            }
            $signal_id = Crypt::decrypt($id);
            Session::put('admin', 1);
            $params = ['id', 'name'];
            $cond = ['role' => 'admin', 'active' => 1];
            $query = ['id' => $signal_id];

            $data = [
                'title'             => "Administrator Backend | Trade Signals",
                'remove_counter'    => 1,
                'admin'             => Auth::user(),
                'admins'            => $this->user->admins($cond, $params),
                'signal'            => $this->signals->fetchone($query)
            ];
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
        if($request->has('edit'))
        {
            return view('administrator.signal.edit', $data);
        } else {
            return view('administrator.signal.signal', $data);
        }
    }
}
