<?php

namespace App\Http\Controllers\Administrator\Signal;
use App\Repositories\Signal\SignalInterface as SignalInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use URL;
use Crypt;
class LoadController extends Controller
{
    private $signal;
    public function __construct(SignalInterface $signal)
    {
        $this->signal = $signal;
    }

    protected function signalvalidator(array $data)
    {
        return Validator::make($data, [
            'currency_pair'             => 'required',
            'setup_type' 				=> 'required',
            'entry'             		=> 'required',
            'stop_loss'         		=> 'required',
            'target_1'  				=> 'required',
            'target_2' 					=> 'required',
            'target_3'          		=> 'required',
            'high_impact_news_ahead' 	=> 'required',
            'quality'           	 	=> 'required',
            'duration'					=> 'required',
            'risk'           			=> 'required',
            'image'           			=> 'required'
        ]);
    }

    protected function editsignalvalidator(array $data)
    {
        return Validator::make($data, [
            'currency_pair'             => 'required',
            'setup_type'                => 'required',
            'entry'                     => 'required',
            'stop_loss'                 => 'required',
            'target_1'                  => 'required',
            'target_2'                  => 'required',
            'target_3'                  => 'required',
            'high_impact_news_ahead'    => 'required',
            'quality'                   => 'required',
            'duration'                  => 'required',
            'risk'                      => 'required'
        ]);
    }

    public function uploadsignal(Request $request)
    {
    	try {
    		$data = $request->except('_token');
    		$validation = $this->signalvalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else{
    			$this->signal->uploadsignal($data);
    		}
    	} catch (\Exception $e) {
    		Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();	
    	}
        Session::put('green', 1);
    	return redirect('administrator/signals')->withErrors("Signal successfully uploaded");
    }

    public function deletesignal(Request $request)
    {
        try {
            $params = ['id' => $request->signal];
            $this->signal->deleteSignal($params);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
        Session::put('green', 1);
        return redirect('administrator/signals')->withErrors("Signal successfully deleted");
    }

    public function savechanges($signal_id, Request $request)
    {
        try {
            $data = $request->except('_token');
            $validation = $this->editsignalvalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else{
                $signal_id = Crypt::decrypt($signal_id);
                $this->signal->savesignal($signal_id, $data);
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput(); 
        }

        Session::put('green', 1);
        return redirect('administrator/signals')->withErrors("Changes successfully updated");
    }
}
