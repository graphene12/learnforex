<?php

namespace App\Http\Controllers\Administrator\Announcement;
use App\Repositories\Announcement\AnnouncementInterface as AnnouncementInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use URL;
use Crypt;
class LoadController extends Controller
{
	private $announcement;
    public function __construct(AnnouncementInterface $announcement)
    {
        $this->announcement = $announcement;
    }

    public function announcementvalidator($data)
    {
        return Validator::make($data, [
            'title'  		    => 'required',
            'delay'  		    => 'required',
            'stop'  		    => 'required',
            'content'  		    => 'required',
            'callback_url'      => 'nullable',
            'call_to_action'    => 'nullable',
        ]);
    }

    public function new(Request $request)
    {
    	try {
	    	$data = $request->except('_token');
	    	$validation = $this->announcementvalidator($data);
	        if ($validation->fails()) {
	            Session::put('yellow', 1);
	            return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
	        } else{
	        	$split_date = explode(" ", $data['stop']);
	        	$data['stop'] = $split_date[0];
	            $this->announcement->create($data);
	        }
    	} catch (Exception $e) {
    		Session::put('red', 1);
	        return redirect(URL::previous())->withErrors($e->getMessage())->withInput();
    	}

        return redirect(URL::previous())->withErrors("Announcement created");
    }

    public function edit(Request $request)
    {
    	try {
	    	$data = $request->except('_token');
	    	$validation = $this->announcementvalidator($data);
	        if ($validation->fails()) {
	            Session::put('yellow', 1);
	            return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
	        } else{
	        	$split_date = explode(" ", $data['stop']);
	        	$data['stop'] = $split_date[0];

	        	$params = ['id' => $data['announcement_id']];
	        	unset($data['announcement_id']);
	            $this->announcement->save($params, $data);
	        }
    	} catch (Exception $e) {
    		Session::put('red', 1);
	        return redirect(URL::previous())->withErrors($e->getMessage())->withInput();
    	}

        return redirect(URL::previous())->withErrors("Changes successfully saved");
    }

    public function delete(Request $request)
    {
    	try {
    		$announcement_id = Crypt::decrypt($request->announcement_id);

    		//delete announcement from database
    		$params = ['id' => $announcement_id];
    		$this->announcement->delete($params);
    	} catch (\Exception $e) {
    		Session::put('red', 1);
	        return redirect(URL::previous())->withErrors($e->getMessage());
    	}

    	return redirect(URL::previous())->withErrors("Announcement successfully removed");
    }
}
