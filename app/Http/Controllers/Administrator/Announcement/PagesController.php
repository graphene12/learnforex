<?php

namespace App\Http\Controllers\Administrator\Announcement;
use App\Repositories\Announcement\AnnouncementInterface as AnnouncementInterface;
use App\Repositories\User\UserInterface as UserInterface;
use App\Repositories\Signal\SignalInterface as SignalInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Crypt;
class PagesController extends Controller
{
    private $announcement;
    private $user;
    private $auth;
    private $signals;

    public function __construct(AnnouncementInterface $announcement, UserInterface $user, SignalInterface $signals, Auth $auth)
    {
    	$this->announcement = $announcement;
    	$this->user = $user;
        $this->signals = $signals;
    	$this->auth = $auth;
    }

    private function stats()
    {
    	$users = $this->user->fetchall();
        $signals = $this->signals->fetchall();
        $investors = $this->user->fetchinvestors();
        $lot_orders = $this->user->lotsizeorders();
        $line_copier = $this->user->line_copier_subscribers();
        $equities = $this->user->fetchequity();

        $data = [
            'users'             => count($users),
            'signals'           => count($signals),
            'investors'         => count($investors),
            'lot_orders'        => count($lot_orders),
            'equities'          => count($equities),
            'line_copiers'      => count($line_copier),
        ];

        return $data;
    }

    public function fetchall()
    {
    	try {
    		$params = ['active' => 1];
    		$announcements = $this->announcement->fetchall($params);
    		$data = $this->stats();
            $data['announcements'] = $announcements;
            $data['title'] = "Announcements";
    	} catch (\Exception $e) {
    		return redirect()->back()->withErrors($e->getMessage());
    	}
    	
    	return view('administrator.announcements.all', $data);
    }

    public function new()
    {
        try {
            $data = [
                'title'             => 'New Announcement',
                'admin_id'          => $this->auth::user()->id,
                'remove_counter'    => true
            ];
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
        return view('administrator.announcements.new', $data);
    }

    public function edit(Request $request)
    {
        try {
            $announcement_id = Crypt::decrypt($request->announcement_id);
            $param = ['id' => $announcement_id];

            $announcement = $this->announcement->fetchone($param);
            $data = [
                'title'             => 'Edit announcement',
                'admin_id'          => $this->auth::user()->id,
                'announcement'      => $announcement,
                'remove_counter'    => true,
            ];
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        return view('administrator.announcements.edit', $data);
    }
}
