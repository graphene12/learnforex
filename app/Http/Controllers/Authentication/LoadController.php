<?php

namespace App\Http\Controllers\Authentication;
use App\Repositories\Authentication\AuthInterface as AuthInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Validator;
use URL;
use Crypt;

class LoadController extends Controller
{
    private $user;
    public function __construct(AuthInterface $user)
    {
        $this->user = $user;
    }

    public function authvalidator($data)
    {
        return Validator::make($data, [
            'email'     => 'required',
            'password'  => 'required'
        ]);
    }

    public function newpasswordvalidator($data)
    {
        return Validator::make($data, [
            'password'          => 'required',
            'password_confirm'  => 'required|same:password',
        ]);
    }

    public function createuservalidator($data)
    {
        return Validator::make($data, [
            'name'              => 'required|max:200',
            'email'             => 'required|email|unique:users|max:256',
            'password'          => 'required',
            'password_confirm'  => 'required|same:password',
            'signal_option'     => 'required',
            'phone'             => 'required|unique:users|min:6',
            'ip_address'        => 'unique:users'
        ]);
    }

    public function addinvestorvalidator($data)
    {
        return Validator::make($data, [
            'name'                      => 'required|max:200',
            'broker_name'               => 'required|max:200',
            'trading_account_balance'   => 'required|numeric|min:5000',
            'mt4_password'              => 'required',
            'mt4_password_confirm'      => 'required|same:mt4_password'
        ]);
    }

    private function getuserip()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    public function authenticate(Request $request)
    {
        try {
            $data = $request->except('_token');
            $data['ip_address'] = $this->getuserip();
            $validation = $this->authvalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else {
                return $this->user->authenticate($data);
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function createuser(Request $request)
    {
        try {
            $data = $request->except('_token');
            $data['ip_address'] = $this->getuserip();
            $validation = $this->createuservalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else {
                unset($data['password_confirm']);
                return $this->user->createnew($data);
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function addinvestor(Request $request)
    {
        try {
            $data = $request->except('_token');
            $validation = $this->addinvestorvalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else {
                unset($data['mt4_password_confirm']);
                // dd($this->user);
                return $this->user->investor($data);
                //return view('restricted.subscriptions');
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function addlinecopier(Request $request)
    {
        try {
            $data = $request->except('_token');
            return $this->user->addlinecopier($data);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function forgotpassword(Request $request)
    {
        try {
            $data = $request->except('_token');
            return $this->user->forgotPassword($data);
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function setnewpassword($uid, Request $request)
    {
        try {
            $data = $request->except('_token');
            $validation = $this->newpasswordvalidator($data);
            if ($validation->fails()) {
                Session::put('yellow', 1);
                return redirect(URL::previous())->withErrors($validation->getMessageBag())->withInput();
            } else {
                $userID = Crypt::decrypt($uid);
                $data['password'] = bcrypt($data['password']);
                unset($data['password_confirm']);

                return $this->user->setnewpassword($userID, $data);
            }
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while")->withInput();
        }
    }

    public function logout()
    {
        try {
            return $this->user->logout();
        } catch (\Exception $e) {
            Session::put('red', 1);
            return redirect(URL::previous())->withErrors("Oops, we did something wrong. try again after a while");
        }
    }
}
