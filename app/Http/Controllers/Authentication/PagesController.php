<?php

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserInterface as UserInterface;
use Crypt;
class PagesController extends Controller
{
    private $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function loginPage()
    {
    	try {
    		$data = ['title' => 'Login'];
    		return view('new_pages.login', $data);
    	} catch (\Exception $e) {
    		return redirect(URL::previous())->withErrors($e->getMessage);
    	}
    }

    public function registerPage($plan = null)
    {
    	try {
    		$data = ['title' => 'Register'];
            if($plan != NULL)
            {
                $data['plan_id'] = Crypt::decrypt($plan);
            }
    		return view('new_pages.sign_up', $data);
    	} catch (\Exception $e) {
    		return redirect(URL::previous())->withErrors($e->getMessage);
    	}
    }

    public function forgotPasswordPage(Request $request)
    {
    	try {
            if($request->has('uid'))
            {
                $userId = Crypt::decrypt($request->uid);
                $user = $this->user->fetchone(['id' => $userId]);

                $data = ['title' => 'Set New Password', 'user' => $user, 'setNewPassword' => true];
                return view('auth.forgot-password', $data);
            } else {
        		$data = ['title' => 'Recover Password'];
        		return view('auth.forgot-password', $data);
            }
    	} catch (\Exception $e) {
    		return redirect(URL::previous())->withErrors($e->getMessage);
    	}
    }
}
