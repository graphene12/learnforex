var $root = $('html, body');
$('a').click(function() {
    $root.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
});

$("a[href='#top']").click(function() {
    $root.animate({
    	scrollTop:($root).offset().top
    }, 500);
    return false;
});

function nextFields()
 {
 	document.getElementById("nextField").style.display = "block";
 	document.getElementById("activeField").style.display = "none";

 	document.getElementById("submitBtn").style.display = "block";
 	document.getElementById("continueBtn").style.display = "none";
 }