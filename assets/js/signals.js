function loadSignals() {
  var url = $("#signal_url").attr("url");
  var type = "GET";
  $.ajax({
    url: url,
    type: type,
    dataType: "html",
    cache: false,
    success: function(response) {
      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[0];
      $.each(JSON.parse(response).signals , function(i, val) {
        var admin = val.admin;
        var signal = val.signal_info;
        var signal_image = val.signal_image;
        var admin_image = val.admin_image;
        var posted_time = val.posted_time;
        var like_signal_url = baseUrl+"/access/signals/like_signal?signal_id="+signal['id'];
        var dislike_signal_url = baseUrl+"/access/signals/dislike_signal?signal_id="+signal['id'];
        
        $("#show-signals").append('<div class="col-md-6" id="showsignal"><div class="trade-signals-container"><div class="d-flex justify-content-between"><p class="mt-0"><img class="img-rounded" src="'+ admin_image +'" alt="Generic placeholder image" width="90px" height="90px">&nbsp;&nbsp;<strong>'+ admin +'</strong></p><span class="signal-posted-time">'+ posted_time +'</span></div><div class="media"><div class="media-left" style="width:50%;margin-right:-32%"><a href="javascript(void);" data-toggle="modal" data-target="#signalPop'+ signal['id'] +'"><img src="'+ signal_image +'" alt="" style="margin-right:" class="img-raised" width="50%"/></a></div><div class="media-body"><p><strong>Currency Pair: </strong>'+ signal['currency_pair'] +'<br><strong>Setup Type: </strong>'+ signal['setup_type'] +'<br><strong>Entry: </strong>'+ signal['entry'] +' <br><strong>Stop Loss: </strong>'+ signal['stop_loss'] +'<br><strong>Target 1: </strong>'+ signal['target_1'] +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Target 2: </strong>'+ signal['target_2'] +' <br><strong>Target 3: </strong>'+ signal['target_3'] +' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Target 4: </strong>'+ signal['target_4'] +' <br><strong>High Impact News Ahead: </strong>'+ signal['high_impact_news_ahead'] +' <br><strong>Quality: </strong>'+ signal['quality'] +' <br><strong>Duration: </strong>'+ signal['duration'] +' <br><strong>Risk: </strong>'+ signal['risk'] +' <br><strong>Update: </strong>'+ signal['update'] +'<br></p><!-- <span id="like_signal_url'+ signal['id'] +'" url="'+ like_signal_url +'"></span><a href="#like" onclick="likeSignal('+ signal['id'] +')" id="like_signal_button'+ signal['id'] +'" style="font-size:20px;"><i class="fa fa-thumbs-up"></i>&nbsp;<span id="likes_counter'+ signal['id'] +'">'+ val.likes +'</span></a> | --> <!--<a href="#dislike" onclick="dislikeSignal('+ signal['id'] +');" id="dislike_signal_button" style="font-size:20px;"><span id="dislike_signal_url'+ signal['id'] +'" url="'+ dislike_signal_url +'"></span><i class="fa fa-thumbs-down"></i>&nbsp;<span id="dislikes_counter'+ signal['id'] +'">'+ val.dislikes +'</span></a> --></div></div></div></div><div class="modal fade" id="signalPop'+signal['id']+'" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" style=""><a href="#" class="close-window" data-dismiss="modal" aria-hidden="true">close</a><form action="" method="POST"><div class="modal-content"><div class="modal-body"><div class="row"><div class="col-md-12"><img src="'+ signal_image +'" alt=""></div></div></div></div></form></div></div>');
      });
    }
  });
}

loadSignals(); // This will run on page load
checknotification(); //check for notification
setInterval(function(){
    $('#show-signals').html('');
    loadSignals() // this will run after every 60 seconds(i.e 1 minute)
    checknotification()
}, 60000);

function likeSignal(signalID) { 
  var url = $("#like_signal_url"+signalID).attr("url");
  var type = "GET"; 
  $.ajax({ 
    url: url, 
    type: type, 
    dataType: "html", 
    cache: false, 
    success: function(response, success) {
      if(JSON.parse(response).indicator == true)
      {
        var old_value = parseInt($("#likes_counter"+signalID).text()); 
        var new_value = old_value + 1; 
        $("#likes_counter"+signalID).text(new_value); 
      } else if (JSON.parse(response).remove == true) {
        var old_value = parseInt($("#likes_counter"+signalID).text());
        if(old_value == 0)
        {
          var new_value = 0;
        } else {
          var new_value = old_value - 1;
        }
        $("#likes_counter"+signalID).text(new_value);
      }
    }  
  }); 
} 

function checknotification() {
  var url = $("#notification_url").attr("url");
  var type = "GET";
  $.ajax({ 
    url: url, 
    type: type, 
    dataType: "html", 
    cache: false, 
    success: function(response) {
      var indicator = JSON.parse(response).indicator;
      var last_sync = new Date().toLocaleTimeString();
      if(indicator === 1) {
        var bell = document.getElementById("ringBell");
        bell.play();
        console.log("ring bell");
        console.log("last sync: " + last_sync);
        window.alert("New signal was posted!");
      } else {
        console.log("don't ring bell");
        console.log("last sync: " + last_sync);
      };
    }
  });
}

function dislikeSignal(signalID) { 
  var url = $("#dislike_signal_url"+signalID).attr("url");
  var type = "GET"; 
  $.ajax({ 
    url: url, 
    type: type, 
    dataType: "html", 
    cache: false, 
    success: function(response, success) {
      if(JSON.parse(response).indicator == true)
      {
        var old_value = parseInt($("#dislikes_counter"+signalID).text());
        var new_value = old_value + 1;
        $("#dislikes_counter"+signalID).text(new_value);
      } else if (JSON.parse(response).remove == true) {
        var old_value = parseInt($("#dislikes_counter"+signalID).text());
        if(old_value == 0)
        {
          var new_value = 0;
        } else {
          var new_value = old_value - 1;
        }
        $("#dislikes_counter"+signalID).text(new_value);
      }
    }
  });
}