$('#messageForm').on('submit', function(e) {
  var that = $(this),
  url = that.attr('action'),
  type = that.attr('method');
  var sender_id = $("#id").val();
  var sender_name = $("#name").val();
  var message = $("#message").val();

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    url: url,
    type: type,
    data: {sender_name: sender_name, message: message, sender_id: sender_id},
    success: function(msg) {
      $("#messages").append('<div class="message"><span id="sender_name">'+ sender_name +': </span><span id="sent_message">'+ message +'</span></div>');
      $('#message').val('');
      var objDiv = document.getElementById("messages-section");
      objDiv.scrollTop = objDiv.scrollHeight;
    }
  });

  return false;
});

function loadMessages() {
  var url = $("#url").attr("url");
  var type = "GET";

  $.ajax({
    url: url,
    type: type,
    dataType: "html",
    cache: false,
    success: function(response) {
      $.each(JSON.parse(response).messages , function(i, val) { 
        var sender = val.sender_name;
        var sender_message = val.message;
         $("#messages").append('<div class="message"><span id="sender_name">'+ sender +': </span><span id="sent_message">'+ sender_message +'</span></div>');
         var objDiv = document.getElementById("messages-section"); objDiv.scrollTop = objDiv.scrollHeight;
      });
    }
  });
}

loadMessages(); // This will run on page load
setInterval(function(){
    $('#messages').html('');
    loadMessages() // this will run after every 5 seconds
}, 120000);

window.onload=function () {
  var objDiv = document.getElementById("messages-section");
  objDiv.scrollTop = objDiv.scrollHeight;
}