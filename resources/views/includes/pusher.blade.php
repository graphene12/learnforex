<script src="https://js.pusher.com/4.0/pusher.min.js"></script>
<script>
  // Enable pusher logging - don't include this in production
  Pusher.logToConsole = true;

  var pusher = new Pusher('a8bc6e17a8e674c02d57', {
    cluster: 'eu',
    encrypted: true
  });

  var channel = pusher.subscribe('my-channel');
    channel.bind('my-event', function(data) {
    alert(data.message);
  });
</script>