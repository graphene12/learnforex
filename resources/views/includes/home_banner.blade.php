
@if(Request::is('/'))
    <div class="page-header">
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo URL('/'); ?>/assets/img/banner.jpeg');">
        </div>
        <div class="container">
            <div class="content-center" style="top: 50%;">
                <h1 class="title banner-title">Professional Forex Trading: A complete Practical series of Hector DeVille</h1>
                <div class="text-center">
                    <p class="small-title"><span class="underline">Learn and Earn</span> while trading our high quality signals.</p>
                    <a href="#services" class="btn btn-white btn-xl">Learn more</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    @if(!Auth::user())
                    <a  href = "{{ URL('/auth/register') }}" class="btn btn-blue btn-xl">sign up now</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class ="ticker">
   <script type="text/javascript">	
var w = '1000';
var s = '0';
var mbg = 'ff0000';
var bs = 'no';
var bc = '444138';
var f = 'verdana';
var fs = '9px';
var fc = 'ff0000';
var lc = 'ff0000';
var lhc = 'ff0000';
var vc = 'ff0000';


var ccHost = (("https:" == document.location.protocol) ? "https://www." : "http://www.");
document.write(unescape("%3Cscript src='" + ccHost + "currency.me.uk/remote/CUK-LFOREXRTICKER-1.php' type='text/javascript'%3E%3C/script%3E"));
</script>
</div>
@elseif(Request::is('managed-accounts'))
    <div class="page-header" id="managed-accounts-banner">
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo URL('/'); ?>/assets/img/banner2.jpg');">
        </div>
        <div class="container">
            <div class="content-center">
                <h5 class="title banner-title">Don't have time to the Forex Markets? <br> Do you lack consistent profitable strategies? <br> Do you lack discipline and always allow your emotions take control?</h5>
                <div class="text-center">
                    <p class="small-title" style="color: #cccccc; font-size: 20px; letter-spacing: normal;">If you answered "Yes" to any of these questions, our managed account option is the best solution for you.</p>
                    <br>
                    
                </div>
            </div>
        </div>
    </div>
@elseif(Request::is('contact-us'))
    <div class="page-header" id="simple-page-banner">
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo URL('/'); ?>/assets/img/contact-banner.jpg');">
        </div>
        <div class="container">
            <div class="content-center">
                <h1 class="title banner-title">Contact Us</h1>
                <div class="text-center">
                    <p class="small-title">Have questions, want to learn more about our service?<br> Drop us a message</p>
                </div>
            </div>
        </div>
    </div>
@endif
<div >
    
</div>