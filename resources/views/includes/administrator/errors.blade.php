@foreach($errors->all() as $error)
 	@if(Session::has('yellow'))
	  	<div class="alert alert-warning" alert-dismissible fade in" role="alert">
  	@elseif(Session::has('red'))
		<div class="alert alert-danger" alert-dismissible fade in" role="alert">
  	@elseif(Session::has('blue'))
  		<div class="alert alert-info" alert-dismissible fade in" role="alert">
  	@else
		<div class="alert alert-success" alert-dismissible fade in" role="alert">
  	@endif
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		{{ $error }}
	</div>
@endforeach
<?php Session::forget('yellow'); Session::forget('red'); Session::forget('blue');?>