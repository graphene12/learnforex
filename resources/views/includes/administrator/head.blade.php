<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Learn Forex Live Signals{{ isset($title)?" - ".$title:NULL }}</title>
    
    <!-- Bootstrap -->
    <link href="{{ URL::to('/') }}/assets/administrator/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ URL::to('/') }}/assets/administrator/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ URL::to('/') }}/assets/administrator/css/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ URL::to('/') }}/assets/administrator/css/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <!-- <link href="{{ URL::to('/') }}/assets/administrator/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> -->
    <!-- JQVMap -->
    <link href="{{ URL::to('/') }}/assets/administrator/css/jqvmap.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ URL::to('/') }}/assets/administrator/css/daterangepicker.css" rel="stylesheet">
    <link href="{{ URL::to('/') }}/assets/administrator/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ URL::to('/') }}/assets/administrator/css/custom.min.css" rel="stylesheet">
    <!-- <link href="{{ URL::to('/') }}/assets/administrator/css/style.css" rel="stylesheet"> -->

   
</script>
    
</head>