<?php $broadcast = App\Models\Channel::select('channel')->first(); ?>
<div class="modal fade" id="scheduleModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Broadcast Schedule</h4>
      </div>
      <form action="{{ URL('/administrator/settings/save-broadcast') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="admin_id" value="{{ Auth::user()->id }}">
        <div class="modal-body">
          <h4>Set a new Broadcast Schedule</h4>
          <div class="form-group">
            <div class="input-group date" id="myDatepicker">
              <input type="text" class="form-control" name="time" required="required">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save schedule</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="streamChannelModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Switch Broadcast Channel</h4>
      </div>
      <form action="{{ URL('/administrator/settings/switch-broadcast-channel') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="admin_id" value="{{ Auth::user()->id }}">
        <div class="modal-body">
          <div class="radio">
            <label class="">
              @if(isset($broadcast->channel) && $broadcast->channel === "youtube")
                <input type="radio" class="" checked="checked" onclick="document.getElementById('submitChannel').click()" name="channel" value="youtube"> Youtube
              @else
                <input type="radio" class="" onclick="document.getElementById('submitChannel').click()" name="channel" value="youtube"> Youtube
              @endif
            </label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <label class="">
              @if(isset($broadcast->channel) && $broadcast->channel === "joicaster")
                <input type="radio" class="" checked="checked" onclick="document.getElementById('submitChannel').click()" name="channel" value="joicaster"> Joicaster
              @else
                <input type="radio" class="" onclick="document.getElementById('submitChannel').click()" name="channel" value="joicaster"> Joicaster
              @endif
            </label>
          </div>
        </div>
        <div class="modal-footer" style="display: none;">
          <button type="submit" class="btn btn-primary" id="submitChannel">Save schedule</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="liveVideoForm" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="width: 400px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Add live video</h4>
      </div>
      <form action="{{ URL('/administrator/videos/create') }}" method="POST" autocomplete="off">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="admin_id" value="{{ Auth::user()->id }}">
        <div class="row modal-body">
          <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label">Video title</label>
            <input type="text" id="title" name="title" required="required" class="form-control" value="{{ old('title') }}">
          </div>

          <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label">Video URL <small>add the embed url from youtube "https://www.youtube.com/embed/ox7RsX1Ee34"</small></label>
            <input type="text" id="url" name="url" required="required" class="form-control" value="{{ old('url') }}">

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>