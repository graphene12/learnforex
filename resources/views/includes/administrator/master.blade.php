<!DOCTYPE html>
<html lang="en">
	@include("includes.administrator.head")
	<body class="nav-md">
    	<div class="container body">
      		<div class="main_container">
				<!-- Navbar -->
					@include("includes.administrator.menu")
				<!-- EndNavbar -->

				<!-- top navigation -->
					@include("includes.administrator.header")
				<!-- /top navigation -->
				@if((count($errors) > 0) || isset($sub_notice))
					<!-- include the errors file here -->
				@endif
				<div class="right_col" role="main">
					@if(!isset($remove_counter))
						@include("includes.administrator.counter")
					@endif
					@include("includes.administrator.errors")
					@yield("contents")
				</div>
				@include("includes.administrator.modals")
			    @include("includes.administrator.footer")
			<!-- div closed in the footer file -->
		<!-- div closed in the footer file -->
	</body>
</html>