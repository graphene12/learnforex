<?php $schedule = App\Models\Schedule::select('time')->first(); ?>
<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      @if(Request::is('administrator*'))
        <a href="{{ URL('/administrator') }}" class="site_title">
      @else
        <a href="{{ URL('/') }}" class="site_title">
      @endif
        <img src="{{ URL::to('/') }}/assets/img/logo-white.png" alt="..." class="logo" width="220px">
      </a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        @if(Auth::user()->avatar !== NULL)
          <img src="{{ URL::to('/') }}/core/public/assets/uploads/avatars/{{ Auth::user()->avatar }}" alt="..." class="img-circle profile_img img-responsive" width="200px" height="150px">
        @else
          <img src="{{ URL::to('/') }}/assets/img/default-avatar.png" alt="..." class="img-circle profile_img">
        @endif
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2>{{ Auth::user()->name }}</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->
    
    <br />
    @if(Request::is('administrator*'))
      <!-- sidebar menu -->
      <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
          <h3>Broadcast Schedule Time</h3>
          <ul class="nav side-menu">
            <span id="schedule-time" time="{{ ($schedule !== NULL)?$schedule->time:NULL }}"></span>
            <li><a><i class="fa fa-clock-o"></i> <span id="broadcast-shchedule"></span></a></li>
          </ul>
        </div>
        <div class="menu_section">
          <h3>General</h3>
          <ul class="nav side-menu">
            
            <li><a><i class="fa fa-users"></i> Users<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{ URL('administrator/users') }}">View Users</a></li>
                <li><a href="{{ URL('administrator/users/add-new') }}">Create User</a></li>
                <li><a href="{{ URL('administrator/users/line-copiers') }}">Line Copier Users</a></li>
              </ul>
            </li>
            <li><a><i class="fa fa-bar-chart-o"></i> Signals <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{ URL('administrator/signals') }}">View Signals</a></li>
                <li><a href="{{ URL('administrator/signals/add-new') }}">Upload Signal</a></li>
              </ul>
            </li>
            <li><a href="{{ URL('administrator/managed-accounts') }}"><i class="fa fa-user"></i> Managed Accounts</a></li>
            <!-- <li><a href="{{ URL('administrator/equity-accounts ') }}"><i class="fa fa-exchange"></i> Equity Accounts</a></li> -->
            <li><a href="{{ URL('administrator/lot-size-orders') }}"><i class="fa fa-shopping-basket"></i> LOT size Orders</a></li>
            <!-- <li>
              <a href="{{ URL('administrator/announcements') }}"><i class="fa fa-bullhorn"></i> Announcements</a>
            </li> -->
            <li>
              <a data-toggle="modal" data-target="#liveVideoForm"><i class="fa fa-file-video-o"></i> Add live video</a>
            </li>
            <!-- <li>
              <a href="{{ URL('administrator/password-requests') }}"><i class="fa fa-lock"></i> Password Reset Requests</a>
            </li> -->
            <li>
              <a data-toggle="modal" data-target="#scheduleModal"><i class="fa fa-calendar"></i> Schedule Broadcast</a>
            </li>
             <li><a href="{{ URL('administrator/announcements') }}"><i class="fa fa-calendar"></i>Announcements</a></li>
            <li>
              <a data-toggle="modal" data-target="#streamChannelModal"><i class="fa fa-video-camera"></i> Switch Stream Channel</a>
            </li>
            <li><a><i class="fa fa-cogs"></i> Settings <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="javascript:void(0);" onclick="document.getElementById('uploadavatar').click();">Change Avatar</a></li>
                <form action="{{ URL('settings/changeavatar') }}" method="POST" enctype="multipart/form-data" style="display: none;">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="uid" value="{{ Auth::user()->id }}">
                  <input type="file" name="file" id="uploadavatar" onchange="form.submit();">
                </form>
              </ul>
            </li>
          </ul>
        </div>
        <!-- <div class="menu_section">
          <h3>Live On</h3>
          <ul class="nav side-menu">
            <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="e_commerce.html">E-commerce</a></li>
                <li><a href="projects.html">Projects</a></li>
                <li><a href="project_detail.html">Project Detail</a></li>
                <li><a href="contacts.html">Contacts</a></li>
                <li><a href="profile.html">Profile</a></li>
              </ul>
            </li>
            <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="page_403.html">403 Error</a></li>
                <li><a href="page_404.html">404 Error</a></li>
                <li><a href="page_500.html">500 Error</a></li>
                <li><a href="plain_page.html">Plain Page</a></li>
                <li><a href="login.html">Login Page</a></li>
                <li><a href="pricing_tables.html">Pricing Tables</a></li>
              </ul>
            </li>
            <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                  <li><a href="#level1_1">Level One</a>
                  <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li class="sub_menu"><a href="level2.html">Level Two</a>
                      </li>
                      <li><a href="#level2_1">Level Two</a>
                      </li>
                      <li><a href="#level2_2">Level Two</a>
                      </li>
                    </ul>
                  </li>
                  <li><a href="#level1_2">Level One</a>
                  </li>
              </ul>
            </li>                  
            <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
          </ul>
        </div> -->

      </div>
      <!-- /sidebar menu -->
    @else
      <!-- sidebar menu -->
      <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
          <h3>General</h3>
          <ul class="nav side-menu">
            
            <li><a href="{{ URL('dashboard/live-room') }}"><i class="fa fa-video-camera"></i> Live Trading Room</a></li>
            
            
                <li>
                    <a class="nav-link" target="_blank" href="http://bit.ly/2sXUK6f">
                        <i class="fa fa-calendar-check-o "></i>
                        Forex Economic Calendar
                    </a>
                </li>
                <li class="nav-item dropdown">
                  <a href="#pablo" data-toggle="dropdown" id="navbarDropdownMenuLink1">
                      <i class="fa fa-cubes"></i>
                      Free Trading Strategies
                      <span class="fa fa-chevron-down"></span>
                  </a>
                  <ul class="nav child_menu">
                    <li><a target="_blank" href="http://bit.ly/1n2wOBY">The Trend Collapse Strategy</a></li>
                    <li><a target="_blank" href="http://bit.ly/1seYSW8">The BCC Pattern</a></li>
                    <li><a target="_blank" href="http://bit.ly/2rJ0T2L">The LOB strategy</a></li>
                    <li><a target="_blank" href="http://bit.ly/2sxqNcp">The 3SMA Strategy</a></li>
                    <li><a target="_blank" href="http://bit.ly/1DMnTzd">The Fake Breakout Strategy</a></li>
                    <li><a target="_blank" href="http://bit.ly/1KiGBjc">Money Management</a></li>
                    <li><a target="_blank" href="http://bit.ly/2t27CbZ">Part-Time Trading</a></li>
                    <li><a target="_blank" href="http://bit.ly/QUILzN">Intra-day Profit Targets</a></li>
                    <li><a target="_blank" href="http://bit.ly/1PjumUc">Golden Rules of Sensible Trading</a></li>
                    <li><a target="_blank" href="http://bit.ly/2sHXaDP">Intra-day Currency Pairs</a></li>
                  </ul>
                </li>
                
               
               <li>
                   <a class="nav-link" href="https://www.learnforexlivesignals.com/pricing" target="">
                      <i class="fa fa-money"></i>
                      Update Membership
                   </a>
               </li>
          </ul>
        </div>
      </div>
      <!-- /sidebar menu -->
    @endif

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <!-- <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a> -->
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ URL('auth/logout') }}">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>