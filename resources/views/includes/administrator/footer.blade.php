        <!-- footer content -->
            <footer style="background-color:black;color:#007FFF;font-size:13px;">
                <div class="pull-right" >
                     POWERED BY: JAYHEC EDUCATIONAL SERVICES
                </div>
                <div class="clearfix"></div>
            </footer>
        <!-- /footer content -->
        </div>
    </div>
    @if(Request::is('dashboard*'))
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- <script src="{{ URL('/') }}/assets/js/chat.js" type="text/javascript""></script> -->
        <script src="{{ URL('/') }}/assets/js/signals.js" type="text/javascript"></script>
        
        <script>
            $('#message').keypress(function (e) {
              if (e.which === 13) {
                $('#messageForm').submit();
                return false;
              }
            });
        </script>
    @endif
    <!-- jQuery -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/fastclick.js"></script>
    <!-- NProgress -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/skycons.js"></script>
    <!-- Flot -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.flot.js"></script>
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.flot.pie.js"></script>
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.flot.time.js"></script>
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.flot.stack.js"></script>
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.flot.orderBars.js"></script>
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.flot.spline.min.js"></script>
    <script src="{{ URL::to('/') }}/assets/administrator/js/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/date.js"></script>
    <!-- JQVMap -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.vmap.js"></script>
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.vmap.world.js"></script>
    <script src="{{ URL::to('/') }}/assets/administrator/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/moment.min.js"></script>
    <script src="{{ URL::to('/') }}/assets/administrator/js/daterangepicker.js"></script>
    <script src="{{ URL::to('/') }}/assets/administrator/js/datetimepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ URL::to('/') }}/assets/administrator/js/custom.min.js"></script>

    <!-- Schedule Timer -->
    <script>
        var getTime = $("#schedule-time").attr("time");
        var end = new Date(getTime);
        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {

                clearInterval(timer);
                document.getElementById('broadcast-shchedule').innerHTML = 'NOW!';

                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

            document.getElementById('broadcast-shchedule').innerHTML = days + 'days ';
            document.getElementById('broadcast-shchedule').innerHTML += hours + 'hrs ';
            document.getElementById('broadcast-shchedule').innerHTML += minutes + 'mins ';
            document.getElementById('broadcast-shchedule').innerHTML += seconds + 'secs';
        }

        timer = setInterval(showRemaining, 1000);
    
        var time = new Date().getTime();
        $(document.body).bind("mousemove keypress", function(e) {
            time = new Date().getTime();
        });

        function refresh() {
            if(new Date().getTime() - time >= 600000){
                window.location.reload(true);
            } else {
                setTimeout(refresh, 600000);
            }
        }
        setTimeout(refresh, 600000);
    </script>

    <script type="text/javascript">
        $(function () {
            $('#myDatepicker').datetimepicker();
        });
    </script>
    <!-- Ends -->
    <?php Session::forget('admin'); ?>