@if(Request::is('administrator'))
  <!-- top tiles -->
  <div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><!-- <i class="fa fa-users"></i> --> Total Users</span>
      <div class="count">{{ count($users) }}</div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><!-- <i class="fa fa-clock-o"></i> --> Signals</span>
      <div class="count">{{ count($signals) }}</div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><!-- <i class="fa fa-md"></i> --> Managed Accounts</span>
      <div class="count">{{ count($investors) }}</div>
    </div>
    <!-- <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-bar-chart-o"></i> Equity Accounts</span>
      <div class="count">{{-- count($equities) --}}</div>
    </div> -->
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><!-- <i class="fa fa-user"></i> --> LOT Size Orders</span>
      <div class="count">{{ count($lot_orders) }}</div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><!-- <i class="fa fa-user"></i> --> Line Copier EA</span>
      <div class="count">{{ count($line_copiers) }}</div>
    </div>
  </div>
  <!-- /top tiles -->
@endif