<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Learn Forex Live Signals{{ isset($title)?" - ".$title:NULL }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="{{ URL::to('/') }}/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ URL::to('/') }}/assets/css/now-ui-kit.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ URL::to('/') }}/assets/css/custom.css" rel="stylesheet" />
    <link href="{{ URL::to('/') }}/assets/css/demo.css" rel="stylesheet" />
    <!-- <script src="https://use.fontawesome.com/bbb8f665bd.js"></script> -->
    <link rel="shortcut icon" sizes="76x76" href="{{ URL::to('/') }}/assets/img/favicon.ico">
    <link rel="icon" type="image/png" href="{{ URL::to('/') }}/assets/img/favicon.ico">

    @if(Request::is('access/members-area'))
        <script type="text/javascript">
            (function(g,v,w,d,s,a,b){w['rumbleTalkMessageQueueName']=g;w[g]=w[g]||
            function(){(w[g].q=w[g].q||[]).push(arguments)};a=d.createElement(s);
            b=d.getElementsByTagName(s)[0];a.async=1;
            a.src='https://d1pfint8izqszg.cloudfront.net/api/'+v+'/sdk.js';
            b.parentNode.insertBefore(a,b);})('rtmq','v0.34',window,document,'script');
        </script>
    @endif
    <style type="text/css">
    
        .ticker{
            width:100%;
            background-color:white;
            height:50px;
            color:red;
            text-align:center;
            z-index:-5;
        }
        .bolder{
            color:black;
        }
         
    </style>

</head>