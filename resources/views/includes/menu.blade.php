
<nav class="navbar navbar-toggleable-md bg-white fixed-top">

    <div class="container">
       
        <div class="navbar-translate">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span>
                <!-- <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span> -->
            </button>
            <a class="navbar-brand" href="{{ URL('/') }}">
                <img src="{{ URL('/') }}/assets/img/logo.png" alt="">
            </a>
        </div>

        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
                @if(!Request::is('/'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('/') }}">
                            home
                        </a>
                    </li>
                @endif
                
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('/dashboard/live-room') }}">
                        live trading room
                    </a>
                </li>
                
                <li class="nav-item bolder">
                    <a class="nav-link" href="{{ URL('/managed-accounts') }}">
                        managed accounts
                    </a>
                </li>
                
                 {{-- I commented this because it has to be hidden temporaly 
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('line-copier?plan='.Crypt::encrypt(6)) }}">
                        line copier ea
                    </a>
                </li>
                --}}
                
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('pricing') }}">
                        pricing
                    </a>
                </li>
                @if(Auth::user())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('/auth/logout') }}">
                            logout
                        </a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('/auth/login') }}">
                            login
                        </a>
                    </li>
                @endif
                
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('/contact-us') }}">
                        contact us
                    </a>
                </li>
                @if(!Auth::user())
                 <li class="nav-item">
                    <a class="nav-link" href="{{ URL('/auth/register') }}">
                        Sign up
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>




<style>
    .wrapper{
        margin-top: -4.8% !important;
    }
    .forex{
        margin-left:0;
    }
</style>
 
@if(Request::is('access/members-area'))
    <div class="navbar navbar-toggleable-md secondary-menu">
        <div class="navbar-translate">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#secondary-navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-left" id="secondary-navigation" style="margin-top:15px !important;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a target="_blank" style="font-size: 10.4px !important;" class="nav-link" href="http://www.forexknights.com/store-advancedindicators_ja.php">
                        advanced indicators
                    </a>
                </li>
                
                <li class="nav-item">
                    <a style="font-size: 10.4px !important;" class="nav-link" href="http://bit.ly/2sXUK6f">
                        forex economic calendar
                    </a>
                </li>
                
                <!-- <li class="nav-item">
                    <a class="nav-link" href="#">
                        
                    </a>
                </li> -->
                <!--
                <li class="nav-item dropdown">
                    <a href="#pablo"  onmouseleave="document.getElementById('navbarDropdownMenuLink1').click();" onmouseover="document.getElementById('navbarDropdownMenuLink1').click();" class="dropdown-toggle add-hover-color" style="color: #FFF !important; text-decoration: none !important; line-height: 4; font-size: 10.4px !important;" data-toggle="dropdown" id="navbarDropdownMenuLink1">
                        Free Trading strategies
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink1" onmouseleave="document.getElementById('navbarDropdownMenuLink1').click();">
                        <a target="_blank" class="dropdown-item" href="http://bit.ly/1n2wOBY">The Trend Collapse Strategy</a>
                        <a target="_blank" class="dropdown-item" href="http://bit.ly/1seYSW8">The BCC Pattern</a>
                        <a target="_blank" class="dropdown-item" href="http://bit.ly/2rJ0T2L">The LOB strategy</a>
                        <a target="_blank" class="dropdown-item" href="http://bit.ly/2sxqNcp">The 3SMA Strategy</a>
                        <a target="_blank" class="dropdown-item" href="http://bit.ly/1DMnTzd">The Fake Breakout Strategy</a>
                        <a target="_blank" class="dropdown-item" href="http://bit.ly/1KiGBjc">Money Management</a>
                        <a target="_blank" class="dropdown-item" href="http://bit.ly/2t27CbZ">Part-Time Trading</a>
                        <a target="_blank" class="dropdown-item" href="http://bit.ly/QUILzN">Intra-day Profit Targets</a>
                        <a target="_blank" class="dropdown-item" href="http://bit.ly/1PjumUc">Golden Rules of Sensible Trading</a>
                        <a target="_blank" class="dropdown-item" href="http://bit.ly/2sHXaDP">Intra-day Currency pairs</a>
                    </ul>
                </li>-->
                
                <li class="nav-item">
                    <a target="_blank" style="font-size: 10.4px !important;" class="nav-link" href="http://www.forexknights.com/store-mentoringprogram_ja.php">
                        mentoring course
                    </a>
                </li>
                
                <li class="nav-item">
                        <a style="font-size: 10.4px !important;" class="nav-link" href="{{ URL('order-lot-size-indicator') }}">
                            order lot size indicators
                        </a>
                    </li>

                <li class="nav-item">
                   <a style="font-size: 10.4px !important;" class="nav-link" href="https://www.myfxbook.com/members/HectorDeVille/lfl-signals/2268192" >
                       performance
                   </a>
               </li>
            </ul>
        </div>
    </div>

@else
    <div class="navbar navbar-toggleable-md secondary-menu" style="z-index: 999;">
        <div class="container">
            <div class="navbar-translate">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#secondary-navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-center" id="secondary-navigation" style="margin-top:15px !important;">
                <ul class="navbar-nav">
                    
                    <li class="nav-item">
                        <a  style="font-size: 10.4px !important;" class="nav-link" href="http://www.forexknights.com/store-advancedindicators_ja.php">
                            advanced indicators
                        </a>
                    </li>
                    <!--
                    <li class="nav-item dropdown">
                        <a href="#pablo" onmouseover="document.getElementById('navbarDropdownMenuLink1').click();" class="dropdown-toggle add-hover-color" style="color: #FFF !important; text-decoration: none !important; line-height: 4; font-size: 10.4px !important;" data-toggle="dropdown" id="navbarDropdownMenuLink1">
                            Free Trading strategies
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink1">
                            <a target="_blank" class="dropdown-item" href="http://bit.ly/1n2wOBY">The Trend Collapse Strategy</a>
                            <a target="_blank" class="dropdown-item" href="http://bit.ly/1seYSW8">The BCC Pattern</a>
                            <a target="_blank" class="dropdown-item" href="http://bit.ly/2rJ0T2L">The LOB strategy</a>
                            <a target="_blank" class="dropdown-item" href="http://bit.ly/2sxqNcp">The 3SMA Strategy</a>
                            <a target="_blank" class="dropdown-item" href="http://bit.ly/1DMnTzd">The Fake Breakout Strategy</a>
                            <a target="_blank" class="dropdown-item" href="http://bit.ly/1KiGBjc">Money Management</a>
                            <a target="_blank" class="dropdown-item" href="http://bit.ly/2t27CbZ">Part-Time Trading</a>
                            <a target="_blank" class="dropdown-item" href="http://bit.ly/QUILzN">Intra-day Profit Targets</a>
                            <a target="_blank" class="dropdown-item" href="http://bit.ly/1PjumUc">Golden Rules of Sensible Trading</a>
                            <a target="_blank" class="dropdown-item" href="http://bit.ly/2sHXaDP">Intra-day Currency pairs</a>
                        </ul>
                    </li>
                    -->
                    
                    <li class="nav-item">
                        <a  style="font-size: 10.4px !important;" class="nav-link" href="http://www.forexknights.com/store-mentoringprogram_ja.php">
                            mentoring course
                        </a>
                    </li>
                        
                    <li class="nav-item">
                        <a style="font-size: 10.4px !important;" class="nav-link" href="{{ URL('mentorship') }}" >
                            one-on-one mentorship
                        </a>
                    </li>
                    <li class="nav-item">
                        <a style="font-size: 10.4px !important;" class="nav-link" href="{{ URL('order-lot-size-indicator') }}">
                            order lot size indicators
                        </a>
                    </li>
                    <li class="nav-item">
                   <a style="font-size: 10.4px !important;" class="nav-link" href="https://www.myfxbook.com/members/HectorDeVille/lfl-signals/2372259" >
                       performance History
                   </a>
               </li>
                </ul>
            </div>
        </div>
    </div>
    
  
@endif
