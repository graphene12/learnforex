<!DOCTYPE html>
<html lang="en">
    
	<body>

		@include("includes.head")
		<!-- Navbar -->
			@include("includes.menu")
		<!-- EndNavbar -->
		@if((count($errors) > 0) || isset($sub_notice))
			@include("includes.errors")
		@endif
		<div class="wrapper" style="margin-top: 3% !important;">
			@yield("contents")
			@unless(Request::is('order-lot-size-indicator') || Request::is('mentorship'))
			@include("includes.footer")
			@endif
		</div>
	</body>
</html>