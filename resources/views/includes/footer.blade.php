@if(!Request::is('auth*'))
    @if(Request::is('live-trade') || Request::is('access/members-area'))
    @else
        <div class="section pre-footer">
            @unless(Request::is('thanks'))
            <div class="backToTop">
                <a href="#top"><i class="now-ui-icons arrows-1_minimal-up"></i></a>
                <span class="back-to-top-indicator">Back to Top</span>
            </div>
            @endif
            <div class="container">
                <span class="social-icons">
                    <a target="_blank" href="https://www.facebook.com/learnforexlivesignals/"><i class="fa fa-facebook-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a target="_blank" href="https://twitter.com/forexknights"><i class="fa fa-twitter-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a target="_blank" href="https://www.youtube.com/channel/UC6sIo-PJlF3a7DFE4aKy4dQ"><i class="fa fa-youtube-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                <!-- <br><br> -->
                <img src="{{ URL('/') }}/assets/img/logo-white.png" alt="" class="footer-logo">
                <p class="footer-content">
                    <b>Risk Warning:</b> Trading the Forex Market is an extremely risky venture. It is not appropriate for individual(s) with limited capital or little or no expereince. Please ensure that you fully understand the risks involved, taking into consideration your investment objectives and level of experience before trading. If necessary seek independent advice from your licensed professional.
                    <br><br>
                    <b>Copyright &copy; <script>document.write(new Date().getFullYear())</script> Learn Forex Live Signals   |   All Rights Reserved</b>
                </p>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <nav>
                    <ul>
                        <li>
                            <a href="{{ URL('/') }}">
                                home
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL('pricing') }}">
                                pricing
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL('contact-us') }}">
                                contact us
                            </a>
                        </li>
                        <!-- <li>
                            <a href="#">
                                Blog
                            </a>
                        </li> -->
                        <li>
                            <a href="{{ URL('auth/login') }}">
                                login
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL('auth/register') }}">
                                get started
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>,&nbsp;&nbsp;POWERED BY:
                    <a > JAYHEC EDUCATIONAL SERVICES</a>.
                </div>
            </div>
        </footer>
    @endif
@endif


@if(Request::is('access/members-area'))
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- <script src="{{ URL('/') }}/assets/js/chat.js" type="text/javascript""></script> -->
    <script src="{{ URL('/') }}/assets/js/signals.js" type="text/javascript"></script>
    
    <script>
        $('#message').keypress(function (e) {
          if (e.which === 13) {
            $('#messageForm').submit();
            return false;
          }
        });
    </script>
    <!-- <script src="https://chatwee-api.com/v2/script/594dae4abd616d695f425245.js"></script> -->
    <!-- <script src="https://chatwee-api.com/v2/script/594a667bbd616d9c6d425245.js"></script> -->
@else
    <!--   Core JS Files   -->
    <script src="{{ URL('/') }}/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
@endif
<script src="{{ URL('/') }}/assets/js/core/tether.min.js" type="text/javascript"></script>
<script src="{{ URL('/') }}/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{ URL('/') }}/assets/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{ URL('/') }}/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="{{ URL('/') }}/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="{{ URL('/') }}/assets/js/now-ui-kit.js" type="text/javascript"></script>

<!-- Custom JS file -->
<script src="{{ URL('/') }}/assets/js/custom.js" type="text/javascript"></script>
<script src="{{ URL('/') }}/assets/js/plugins/countdown.min.js" type="text/javascript"></script>
<!-- <script src="{{ URL('/') }}/assets/js/chat.js" type="text/javascript"></script> -->

<!-- BEGIN JIVOSITE CODE {literal} -->
@if(!Request::is('access/members-area'))
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/593e6cecb3d02e11ecc696e5/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
@endif

@if(Request::is('/'))
    <script>
        var delay = $("#delay").val();
        $(window).on('load',function(){
            setTimeout(function(){
               $('#promoModal').modal('show');
            }, delay+'000');
        });
    </script>
@endif

@if(Request::is('/'))
    <script>
        var end = new Date('07/07/2017 12:00 AM');
        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {

                clearInterval(timer);
                document.getElementById('countdown').innerHTML = 'EXPIRED!';

                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

            document.getElementById('countdown').innerHTML = days + 'days ';
            document.getElementById('countdown').innerHTML += hours + 'hrs ';
            document.getElementById('countdown').innerHTML += minutes + 'mins ';
            document.getElementById('countdown').innerHTML += seconds + 'secs';
        }

        timer = setInterval(showRemaining, 1000);
    
        var time = new Date().getTime();
        $(document.body).bind("mousemove keypress", function(e) {
            time = new Date().getTime();
        });

        function refresh() {
            if(new Date().getTime() - time >= 600000){
                window.location.reload(true);
            } else {
                setTimeout(refresh, 600000);
            }
        }
        setTimeout(refresh, 600000);
    </script>
@elseif(Request::is('access/members-area'))
    <script>
        var getTime = $("#schedule-time").attr("time");
        var end = new Date(getTime);
        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var nowUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
            var distance = end - nowUTC;
            if (distance < 0) {

                clearInterval(timer);
                document.getElementById('live-stream-countdown').innerHTML = 'NOW!';

                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);

            document.getElementById('live-stream-countdown').innerHTML = days + 'days ';
            document.getElementById('live-stream-countdown').innerHTML += hours + 'hrs ';
            document.getElementById('live-stream-countdown').innerHTML += minutes + 'mins ';
        }

        timer = setInterval(showRemaining, 1000);
    
        var time = new Date().getTime();
        $(document.body).bind("mousemove keypress", function(e) {
            time = new Date().getTime();
        });

        function refresh() {
            if(new Date().getTime() - time >= 600000){
                window.location.reload(true);
            } else {
                setTimeout(refresh, 600000);
            }
        }
        setTimeout(refresh, 600000);
    </script>
@endif
<script>
    function displaySubmitBtn(){
        if(document.getElementById('i_agree').checked) {
            $("#submit_managed_account_form").show();
        } else {
            $("#submit_managed_account_form").hide();
        }
    }
</script>
<!-- {/literal} END JIVOSITE CODE -->