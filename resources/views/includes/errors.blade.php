@if(isset($sub_notice))
      @if($sub_notice == 0)
        <div class="error alert alert-info">
          <div class="container">
            <center>Your subscription plan has ended! <a href="{{ $url }}" class="error-link">Re-activate plan now!</a></center>
      @elseif($sub_notice == 2)
        <div class="error alert alert-warning">
          <div class="container">
            <center>You do not have an active subscription plan <a href="{{ $url }}" class="error-link">Subscribe now</a></center>
      @endif
    </div>
  </div>
@endif

@foreach($errors->all() as $error)
	  @if(Session::has('yellow'))
  	  <div class="error alert alert-warning" style="margin-bottom: -5% !important;">
	  @elseif(Session::has('red'))
  		<div class="error alert alert-danger" style="margin-bottom: -5% !important;">
	  @elseif(Session::has('blue'))
      <div class="error alert alert-info" style="margin-bottom: -5% !important;">
	  @else
  		<div class="error alert alert-success" style="margin-bottom: -5% !important;">
	  @endif
		<div class="container">
      <center>
        {{ $error }}
        @if(Session::has('sub_url'))
          "{{ ucwords(Session::get('plan_title')) }} Plan" <a href="{{ Session::get('sub_url') }}" class="error-link">activate now</a>&nbsp;&nbsp;or choose another.
        @endif
      </center>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">
          <i class="now-ui-icons ui-1_simple-remove"></i>
        </span>
      </button>
    </div>
	</div>
@endforeach
<?php Session::forget('yellow'); Session::forget('red'); Session::forget('blue'); Session::forget('plan_title'); Session::forget('sub_url'); ?>