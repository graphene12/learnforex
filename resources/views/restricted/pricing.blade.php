@extends("includes.master")
@section("contents")
	<div class="section pricing-plans">
		<div class="container">
			<div class="pricing-title">
				<h5>Pricing Plans</h5>
				<p style="font-size:17px;">To join the Live Trading Room and Signal Service, please select a subscription plan that is suitable for you. If you already have access to the Live Trading Room, wait for the expiration of your current subscription Plan before choosing a new one.</p>
				<p style="font-size:17px;">Subscription is not set to automatic renewal or recurring billing. You may also cancel at anytime by sending us an email to: <a href="mailto:support@learnforexlivesignals.com">support@learnforexlivesignals.com</a></p>
			</div>
			<div class="row" style="margin-bottom:-80px;">
				@if(Auth::check())
					<?php $first_plan = Auth::user()->first_plan; ?>
				@else
					<?php $first_plan = null; ?>
				@endif
				@foreach($plans as $plan)
					@if($first_plan !== NULL && $first_plan == 1 && $first_plan == $plan->id )
					@else
					        @if($plan->id != 7)
						<div class="col-md-2">
							@if($plan->id > 4 && $plan->id < 7)
		                  		<div class="pricing-table">
		                  	@else
								<div class="pricing-table">
		                  	@endif
		                  	    @if($plan->id == 1 )
		                    	<div class="table-contents text-center">
		                      		<div class="table-header brand-hover">
		                        		<div class="plan-title" style="font-size: 15px;">
		                          			<span>{{ $plan->title }}</span>
		                        		</div> 

		                        		<div class="plan-price">
		                        		    <span class="currency-symbol">$</span>
		                        		    <span class="price">{{ $plan->price }}</span>
		                          			<span class="currency-code">/{{ $plan->validity }}</span>
		                        		</div>
		                      		</div>
                                @else
                                    <div class="table-contents text-center">
		                      		<div class="table-header brand-hover">
		                      		    	
		                          			<div class="plan-title" style="font-size: 15px;">
		                          			<span>{{ $plan->title }}</span>
		                          			</div>
		                        		 

		                        		<div class="plan-price">
		                          			<span class="currency-symbol">$</span>
		                          			<span class="price">{{ $plan->price }}</span>
		                          			<span class="currency-code">/{{ $plan->validity }}</span>
		                        		</div>
		                      		</div>
                                @endif
		                      		<div class="table-info">
		                        	
		                      		</div>

		                      		<div class="table-footer" style="margin-top:-45px;">
		                        		<div class="purchase-button">
											<form action="/payment/paydot" method="POST">
												<input type="hidden" name="plan_id" value="{{ $plan->id }}">
												{{ csrf_field() }}
												<button type="submit" class="btn btn-sm btn-success"  >
													Subscribe Now
												</button>
											</form>	
		                        		
		                        		</div>
		                      		</div>
		                    	</div>
		                  	</div>
		                </div>
		                @endif
		         	@endif
				@endforeach
			</div>
			
		
		       
			</div>
			
		    </div>
		    
		    <div class="row">
                <img src="{{ URL('/') }}/assets/img/payment_channels.jpg" height="25%" width="25%" style="margin-left:auto; margin-right:auto;">
            </div>
		   </div>
	</div>
            
        
@stop
