@extends("includes.master")
@section("contents")
    <div class="section pricing-plans">
	<div class="container">
	    	<div class="pricing-title">
				<h5 style="font-family:Helvetica;color:green;">Thanks for your subscription!</h5>
				<p style="font-size:17px;font-weight:bold;"> You will recieve an acknowledgement email from us shortly. Please endeavour to check your spam and inbox. </p>
				<p style="font-size:17px;font-weight:bold;"> If you find problems anywhere, Please Contact us as soon as possible on <a href="mailto:support@learnforexlivesignals.com">support@learnforexlivesignals.com</a></p>
				@if(!Auth::user())
				<p style="font-size:25px;font-weight:bold;"> <a href ="{{ URL('auth/login') }}" >Click Here to Login</a> or <a href ="{{ URL('auth/register') }}">Create an Account</a></p>
				@endif
				
			</div>
	</div>
	</div>

@stop