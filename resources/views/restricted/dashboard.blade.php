@extends("includes.administrator.master")
@section("contents")
	<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $title }}</h2>
                    <div class="clearfix"></div>
                </div>
                <form action="{{ URL('settings/changeavatar') }}" method="POST" enctype="multipart/form-data" style="display: none;">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="uid" value="{{ Auth::user()->id }}">
                  <input type="file" name="file" id="uploadavatar" onchange="form.submit();">
                </form>
                <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      	<div class="profile_img">
                        	<div id="crop-avatar">
                          		<!-- Current avatar -->
                          		@if($user->avatar)
                          			<img class="img-responsive avatar-view" src="{{ URL::to('/') }}/core/public/assets/uploads/avatars/{{ $user->avatar }}" alt="Avatar" width="150px">
                          		@else
									              <img class="img-responsive avatar-view" src="{{ URL::to('/') }}/assets/img/default-avatar.png" alt="Avatar" width="150px">
                          		@endif
                        	</div>
                      	</div>
                      	<h3>{{ $user->name }}</h3>

                      	<ul class="list-unstyled user_data">
                        	<li><i class="fa fa-clock-o user-profile-icon"></i> Joined: {{ Carbon\Carbon::createFromTimeStamp(strtotime($user->created_at))->diffForHumans() }}</li>

                          <li><a class="btn btn-sm btn-primary flat" href="javascript:void(0);" onclick="document.getElementById('uploadavatar').click();"><i class="fa fa-picture-o"></i>&nbsp;&nbsp;Change Avatar</a></li>
                      	</ul>

                      	<!-- <a class="btn btn-primary" href="{{ URL('/administrator/users/'.Crypt::encrypt($user->id).'?edit=true') }}"><i class="fa fa-edit m-right-xs"></i> Edit</a> -->
                        @if($user->id !== Auth::user()->id)
                      	    <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteUserModal"><i class="fa fa-ban m-right-xs"></i> Delete User</a>
                            @if($user->role !== 'admin')
                              <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#makeAdminModal"><i class="fa fa-check m-right-xs"></i> Make Admin</a>
                            @else
                              <a class="btn btn-warning btn-sm" href="{{ URL('/administrator/users/remove-admin?user='.$user->id) }}"><i class="fa fa-times m-right-xs"></i> Remove Admin</a>
                            @endif

                            @if($user->subscription !== NULL)
                              @if($user->subscription->active == 0)
                                <a class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#approveUserModal"><i class="fa fa-thumbs-up m-right-xs"></i> Approve</a>
                              @endif
                            @endif
                        @endif
                    </div>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                    	<div class="profile_title">
                        	<div class="col-md-6">
                          		<h2>User Info</h2>
                        	</div>
                      	</div>
                      	<!-- start of user-activity-graph -->
                  		<table class="table table-striped">
                  			<tr>
                  				<td>Fullname</td>
                  				<td>{{ $user->name }}</td>
                  			</tr>
                  			<tr>
                  				<td>Email</td>
                  				<td>{{ $user->email }}</td>
                  			</tr>
                  			<tr>
                  				<td>Phone</td>
                  				<td>{{ $user->phone }}</td>
                  			</tr>
                  			<tr>
                  				<td>Signal Option</td>
                  				<td>{{ $user->signal_option }}</td>
                  			</tr>
                  		</table>
                      	<!-- end of user-activity-graph -->
                    
                    	<div class="profile_title">
                        	<div class="col-md-6">
                          		<h2>Subscriptions</h2>
                        	</div>
                      	</div>
                      	<!-- start of user-activity-graph -->
                      	@if($user->subscription !== NULL)
	                  		<table class="table table-striped">
	                  			<tr>
	                  				<td>Plan</td>
	                  				<td>{{ $user->subscription->plan->title }}</td>
	                  			</tr>
	                  			<tr>
	                  				<td>Plan Validity</td>
	                  				<td>{{ $user->subscription->plan->validity }}</td>
	                  			</tr>
	                  			<tr>
	                  				<td>Plan Price</td>
	                  				<td>${{ $user->subscription->plan->price }}</td>
	                  			</tr>
	                  			<tr>
	                  				<td>Activated On</td>
	                  				<td>{{ $user->subscription->activated_on }}</td>
	                  			</tr>
                          <tr>
                            <td>Deactivates On</td>
                            <td>{{ $user->subscription->deactivated_on }}</td>
                          </tr>
	                  		</table>
	                  	@else
          							<div class="alert alert-info" style="border-radius: 0% !important;">
          								<center>
          									<p>No subscription plan selected yet!</p>
          								</center>
          							</div>
	                  	@endif
                      	<!-- end of user-activity-graph -->
                    </div>
                </div>
            </div>
        </div>
    </div>
  
@stop