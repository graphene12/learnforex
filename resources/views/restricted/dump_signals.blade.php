@extends("includes.administrator.master")
@section("contents")
	<style>
		.pre-footer {
		    margin-top: 0%; 
		}
		.paginate{position: relative;}
		.pagination>li>a, .pagination>li>span {
		  padding: 6px 16px !important;
		  margin-left: 3px !important;
		  margin-right: 3px !important;
		  line-height: 1.42857143 !important;
		  color: #fff !important;
		  background-color: #211760 !important;
		  border: 0px !important;
		  font-size: 20px !important;
		}
		.right_col{ background-color:#171717 !important; }
    .trade-signals{
        min-height: 500px;
        background-color: #171717 !important;
        padding: 30px 0 !important;
    }
    .trade-signals-container{
        margin-top: 5%;
        border: 1px solid #d3d3d3;
        box-shadow:  rgba(80, 80, 80, 0.2) 0px 1px 3px, rgba(80, 80, 80, 0.14902) 0px 6px 10px !important;
        background-color: #FFF;
        padding:5px 5px 10px 5px;
        border-radius: 5px;
    }

    .signal-posted-time{
        margin: 5% 0% 5% 0%;
    }

    .interaction-icons{
        font-size: 20px;
    }
	</style>
	<div class="section trade-signals">
		<audio id="ringBell"><source src="{{ URL::to('/') }}/assets/media/bell.mp3" type="audio/mpeg"></audio>
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="color: #FFF; text-transform: uppercase; margin-top: -1%;">					
					<h5 class="underline">Trade Signals</h5>
				</div>
				<span id="signal_url" url="{{ URL('/access/fetch-signals') }}"></span>
			</div>
			<div class="row" id="show-signals">
				@foreach($signals as $signal)
					<?php
						if((URL::to('/') == "http://localhost:3000") || (URL::to('/') == "http://localhost:8000"))
	                    {
	                        $signal_image = URL('/').'/assets/uploads/trade_signals/'.$signal->image;
	                    } else {
	                        $signal_image = URL('/').'/core/public/assets/uploads/trade_signals/'.$signal->image;
	                    }

	                    if($signal->admin->avatar)
	                    {
	                        $admin_image = URL('/').'/core/public/assets/uploads/avatars/'.$signal->admin->avatar;
	                    } else {
	                        $admin_image = URL('/').'/assets/img/default-avatar.png';
	                    } 
					?>
					<div class="col-md-6" id="showsignal">
						<div class="trade-signals-container">
							<div class="d-flex justify-content-between">
								<p class="mt-0">
									<img class="img-rounded" src="{{ $admin_image }}" alt="Generic placeholder image" width="90px" height="90px">&nbsp;&nbsp;<strong>{{ $signal->admin->name }}</strong>
								</p>
								<span class="signal-posted-time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($signal->created_at))->diffForHumans() }}</span>
							</div>
							<div class="media">
								<div class="media-left" style="width:70%;margin-right:-32%">
									<a href="javascript(void);" data-toggle="modal" data-target="#signalPop{{ $signal->id }}">
										<img src="{{ $signal_image }}" alt="" style="margin-right:" class="img-raised" width="50%"/>
									</a>
								</div>
								<div class="media-body">
									<p>
										<strong>Currency Pair: </strong>{{ $signal->currency_pair }}<br>
										<strong>Setup Type: </strong>{{ $signal->setup_type }}<br>
										<strong>Entry: </strong>{{ $signal->entry }} <br>
										<strong>Stop Loss: </strong>{{ $signal->stop_loss }}<br>
										<strong>Target 1: </strong>{{ $signal->target_1 }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Target 2: </strong>{{ $signal->target_2 }} <br>
										<strong>Target 3: </strong>{{ $signal->target_3 }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Target 4: </strong>{{ $signal->target_4 }} <br>
										<strong>High Impact News Ahead: </strong>{{ $signal->high_impact_news_ahead }} <br>
										<strong>Quality: </strong>{{ $signal->quality }} <br>
										<strong>Duration: </strong>{{ $signal->duration }} <br>
										<strong>Risk: </strong>{{ $signal->risk }} <br>
										<strong>Update: </strong>{{ $signal->update }}<br>
									</p>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="signalPop{{ $signal->id }}" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg" style="">
							<a href="#" class="close-window" data-dismiss="modal" aria-hidden="true">close</a>
							<form action="" method="POST">
								<div class="modal-content">
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
												<img src="{{ $signal_image }}" alt="">
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				@endforeach
				<br><br>
				{!! $signals->render() !!}
			</div>
		</div>
	</div>
@stop