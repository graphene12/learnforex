@extends("includes.master")
@section("contents")
	<div class="section live-section">
		<div class="container theatre">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<iframe src="https://s3.amazonaws.com/fxk-videos/Line%20copier.mp4" frameborder="0" width="100%" height="405" style="margin-top: 2%;"></iframe>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>

			<div class="row" id="create-account">
				<div class="col-md-12" style="text-transform: uppercase; margin-top: -1%;">					
					<center><h5 class="underline">Create Account</h5></center>
				</div>
				<div class="col-md-2"></div>
				<div class="col-md-8"  style="margin-top: 5%">
					<form action="{{ URL('#') }}" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="plan_id" value="{{ isset($plan_id)?$plan_id:NULL }}">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group" style="margin-top: 18%;">
									<label for="name" class="form-element-label">Your Name:</label>
									<input type="name" id="name" name="name" value="{{ old('name') }}" class="form-control" required="required" placeholder="Fullname">
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label for="email" class="form-element-label">Your E-mail Address (we will communicate with you via this channel once your paymant is completed, Endeavour to check both spam and inbox within the next 24 hours):</label>
									<input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" required="required" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="float-right">
							<button class="btn btn-success login-btn" type="submit">Next</button>
						</div>
					</form>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
@stop