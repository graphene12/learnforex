@extends("includes.master")
@section("contents")
	@include("includes.home_banner")
	<div class="section service-section" id="services">
        <div class="container" style="margin-top: -5%;">
            <div class="row">
                <div class="col-md-6">
                    <ul class="benefits">
                        <li class="dots round"><p>No transfer of funds. Stay in total control of your capital.</p></li>
                        <li class="dots round"><p>Watch trading activities directly from your MT4.</p></li>
                        <li class="dots round"><p>3% Max. risk per trade.</p></li>
                        <li class="dots round"><p>Real-time stats on myfxbook.</p></li>
                        <li class="dots round"><p>100% account protection from news announcements.</p></li>
                        <li class="dots round"><p>Profit will be shared in the ratio of 60% (Investor) to 40% (LFL Signal)</p></li>
                        <li class="dots round"><p>Never lose your account.</p></li>
                        <li class="dots round"><p>Overall peace of mind.</p></li>
                        <li class="dots round"><p>Cancel at any time!</p></li>
                        <br>
                        <a href="#managed-account-form" class="btn btn-yellow">join us now</a>
                    </ul>
                </div>
                <div class="col-md-6">
                    <img src="{{ URL('/') }}/assets/img/desktop-screen-trading-forex.png" alt="">
                </div>
            </div>
        </div>
    </div>
   <!-- <div class="section pricing-plans" id="subscription-plans">
		<div class="container">
			<h5 class="benefits-title bold"><span class="underline">Commitment Fee</span></h5>
			<p class="testimonies-small-title">This is a one-time payment and will be recovered before the share of profit kicks in. <br>No further monthly subscription fee(s) will be required!"</p>
			<br>
			<div class="row">
				<div class="col-md-3">
		        </div>
		  
		        <div class="col-md-6">
		          	<div class="pricing-table">
		            	<div class="table-contents text-center">
		              		<div class="table-header brand-hover">

		                		<div class="plan-price">
		                  			<span class="currency-symbol">$</span>
		                  			<span class="price" size="20px">250</span>
		                		</div>
		              		</div>

		              		<div class="table-info">
		                	
		              		</div>

		              		<div class="table-footer">
		                		<div class="purchase-button">
		                			<a class="btn btn-lg btn-yellow" href="#managed-account-form">Start Now</a>
		                		</div>
		              		</div>
		            	</div>
		          	</div>
		        </div>
	</div>
		    
            
	    </div>
    </div> -->
    <div class="section testimonies-section" id="managed-account-form" style="background-color: #F8F8F8;">
        <div class="container">
            <h5 class="benefits-title bold"><span class="underline">investor's form</span></h5>
            <div class="row justify-content-center">
                <div class="col-8">
                	<div id="firstForm">
	            		<p class="testimonies-small-title"> Please take a few minutes to fill our investor's form and have us manage your account to success.</p>
	            		<form action="{{ URL('auth/managed-account') }}" method="POST" role="form">
	                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		                	<div class="row">
		                        <div class="col-md-6">
		                            <div class="form-group">
		                                <label for="name" class="form-element-label">Full Name:</label>
		                                <input type="name" id="name" name="name" value="{{ old('name') }}" class="form-control" required="required" placeholder="Fullname">
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                            <div class="form-group">
		                                <label for="broker_name" class="form-element-label">Broker's Name:</label>
		                                <input type="text" id="broker_name" name="broker_name" value="{{ old('broker_name') }}" class="form-control" required="required" placeholder="Broker's Name">
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                            <div class="form-group" style="margin-top: 6%;">
		                                <label for="mt4_account_number" class="form-element-label">MT4 Account Number:</label>
		                                <input type="number" id="mt4_account_number" name="mt4_account_number" value="{{ old('mt4_account_number') }}" class="form-control" required="required" placeholder="MT4 Account Number">
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                            <div class="form-group">
		                                <label for="trading_account_balance" class="form-element-label">What is your Trading Account Equity?  (Minimum of $5000):</label>
		                                <input type="number" id="trading_account_balance" name="trading_account_balance" value="{{ old('trading_account_balance') }}" min="5000" class="form-control" required="required" placeholder="What is your Trading Account Equity?  (Minimum of $5000)">
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                            <div class="form-group" style="margin-top: 6%;">
		                                <label for="mt4_password" class="form-element-label">MT4 Password:</label>
		                                <input type="password" id="mt4_password" name="mt4_password" class="form-control" required="required" placeholder="MT4 Password">
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                            <div class="form-group">
		                                <label for="email" class="form-element-label">Email Address (we shall communicate with you via this channel):</label>
		                                <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" required="required" placeholder="Email Address">
		                            </div>
		                        </div>

		                        <div class="col-md-12">
		                            <div class="form-group">
		                                <label for="mt4_password_confirm" class="form-element-label">Verify MT4 Password:</label>
		                                <input type="password" id="mt4_password_confirm" name="mt4_password_confirm" class="form-control" required="required" placeholder="Verify MT4 Password">
		                            </div>
		                        </div>

								
		                        <div class="col-md-6">
		                        	<div class="checkbox">
									    <input id="i_agree" type="checkbox" class="form-control" onclick="displaySubmitBtn()">
									    <label for="i_agree">
									        <b>Terms and Conditions:</b> I hereby take responsibility and authorize learnforexlivesignals to apply its trading strategies and money management principles in trading foreign currencies on my account.
									    </label>
									</div>
								</div>

		                        <div class="col-md-6">
									<button class="btn btn-blue login-btn float-right" onclick="switchForms()" id="submit_managed_account_form" style="display: none;" type="submit">Submit</button>
								</div>
		                    </div>
	            		</form>
	            	</div>
                </div>
            </div>
        </div>
    </div>
    <!-- Sart Modal -->
    <div class="modal fade" id="contactUsSubscription" tabindex="-1" role="dialog" aria-labelledby="contactUsSubscriptionLabel" aria-hidden="true">
        <div class="modal-dialog">
            <a href="javascript(void);" class="close-window" data-dismiss="modal" aria-hidden="true">close</a>
            <form action="{{ URL('order/subscription-save') }}" method="POST">
                <div class="modal-content">
                    <div class="modal-body">
                        <center><h5>Please fill this form to get started.</h5></center>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="form-element-label">Your Name:</label>
                                    <input type="name" id="name" name="name" value="{{ old('name') }}" class="form-control" required="required" placeholder="Fullname">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email" class="form-element-label">Your E-mail Address:</label>
                                    <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" required="required" placeholder="Email">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="equity" class="form-element-label">Equity Account:</label>
                                    <input type="number" id="equity" name="equity" value="{{ old('equity') }}" class="form-control" required="required" placeholder="Equity Account">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-blue btn-block" type="submit">continue</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--  End Modal -->
@stop