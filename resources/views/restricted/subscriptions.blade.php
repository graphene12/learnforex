@extends("includes.master")
@section("contents")
    
	<div class="section pricing-plans">
	        
		<div class="container">
			<div class="pricing-title">
				<div  style="color:green;text-align:center;font-weight:bold;text-transform:uppercase;margin-bottom:10px;display:block;"><i>Please proceed to pay the commitment fee with the link below</i>
            		</div>
            		
            		<div  style="color:black;text-align:center;font-weight:bold;text-transform:uppercase;margin:5px;font-size:13px;">(Once your Payment is recieved, we will contact you as soon as possible)
            		</div>
				<div class="section pricing-plans" id="subscription-plans">
		<div class="container">
			<h5 class="benefits-title bold"><span class="underline">Commitment Fee</span></h5>
			<p class="testimonies-small-title">This is a one-time payment and will be recovered before the share of profit kicks in. <br>No further monthly subscription fee(s) will be required.</p>
			<br>
			<div class="row">
				<div class="col-md-3">
		        </div>
		  
		        <div class="col-md-6">
		          	<div class="pricing-table">
		            	<div class="table-contents text-center">
		              		<div class="table-header brand-hover">

		                		<div class="plan-price">
		                  			<span class="currency-symbol">$</span>
		                  			<span class="price" size="20px">250</span>
		                		</div>
		              		</div>

		              		<div class="table-info">
		                		<!-- <ul><li>&nbsp;</li></ul> -->
		              		</div>

		              		<div class="table-footer">
		                		<div class="purchase-button">
		                		        
		                		        <a href="https://paystack.com/pay/60kn5mx4wi" class="btn btn-lg btn-yellow" >Pay</a>
		                		    
		                			
		                		</div>
		              		</div>
		            	</div>
		          	</div>
		        </div>
	</div>
		    
            
	    </div>
    </div>
			<!--		<div class="row">
				
				<div class="col-md-6">
					<table class="table" style="border-top: none !important;">
            			<tr>
            				<td><strong>ACCOUNT NAME</strong></td>
            				<td align="left">JAYHEC EDUCATIONAL SERVICES
            				</td>
            			</tr>
            			<tr>
            				<td><strong>ACCOUNT NUMBER</strong></td>
            				<td align="left">2032730771
            				</td>
            			</tr>
            			<tr>
            				<td><strong>BANK NAME</strong></td>
            				<td align="left">FIRST BANK 
            				</td>
            			</tr>
            			<tr>
            				<td><strong>BANK ADDRESS</strong></td>
            				<td align="left">CALABAR - NIGERIA
            				</td>
            			</tr>
            			<tr>
            				<td><strong>SWIFT CODE</strong></td>
            				<td align="left">FBNINGLA
            				</td>
            			</tr>
            			<tr>
            				<td><strong>SORT CODE</strong></td>
            				<td align="left">011150000
            				</td>
            			</tr>
            			<tr>
            				<td><strong>BRANCH</strong></td>
            				<td  align="left">CALABAR
            				</td>
            			</tr>
            			<tr>
            				<td><strong>AMOUNT</strong></td>
            				<td align="left">$250
            				</td>
            			</tr>
            		</table>
            		</div>
            		
            		
            		</div>-->
			</div>
		    
	</div>
	

	
@stop