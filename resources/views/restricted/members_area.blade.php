@extends("includes.administrator.master")
@section("contents")
	@if(Auth::check())
		@include("restricted.live_signal_page")
	@else
		@include('auth.live_introduction_form')
	@endif
@stop