<footer id="footer" style="bottom: -10px;position:relative;">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-6 col-md-6 footer-info offset-3">
            <img style="display:block;margin-left:auto;margin-right:auto;width:50%;" src="{{ URL('/') }}/assets/img/logo.png" >
            <p style="text-align:justify;font-size:18px;">Risk Warning: Trading the Forex Market is an extremely risky venture. It is not appropriate for individual(s) with limited capital or little or no expereince. Please ensure that you fully understand the risks involved, taking into consideration your investment objectives and level of experience before trading. If necessary seek independent advice from your licensed professional. </p>
            <center style="margin-top:20px;"><div class="social-links">
              <a href="tg://resolve?domain=lflsignals" class="telegram"><i  class="fa fa-telegram"></i></a>
              <a href="https://web.facebook.com/pg/learnforexlivesignals/posts/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://www.youtube.com/channel/UC6sIo-PJlF3a7DFE4aKy4dQ" class="youtube"><i class="fa fa-youtube"></i></a>
              <a href="https://twitter.com/forexknights" class="twitter"><i class="fa fa-twitter"></i></a>
            </div></center>
            
          </div>    
       

        </div>
      </div>
    </div>

    

    <div class="container">
      <div class="copyright">
        <?php $date = (int)  date('Y',time());?>
        Copyright © {{$date}} - {{$date+1}}. Learn Forex Live Signals | All Rights Reserved
      </div>
      
    </div>
  </footer><!-- #footer -->