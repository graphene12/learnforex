  <!-- JavaScript assets/new_home/libraries -->
  <script src="{{url('assets/new_home/lib/jquery/jquery.min.js')}}"></script>
  <script src="{{url('assets/new_home/lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{url('assets/new_home/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{url('assets/new_home/lib/easing/easing.min.js')}}"></script>
  <script src="{{url('assets/new_home/lib/superfish/hoverIntent.js')}}"></script>
  <script src="{{url('assets/new_home/lib/superfish/superfish.min.js')}}"></script>
  <script src="{{url('assets/new_home/lib/wow/wow.min.js')}}"></script>
  <script src="{{url('assets/new_home/lib/waypoints/waypoints.min.js')}}"></script>
  <script src="{{url('assets/new_home/lib/counterup/counterup.min.js')}}"></script>
  <script src="{{url('assets/new_home/lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{url('assets/new_home/lib/isotope/isotope.pkgd.min.js')}}"></script>
  <script src="{{url("assets/new_home/lib/lightbox/js/lightbox.min.js")}}"></script>
  <script src="{{url('assets/new_home/lib/touchSwipe/jquery.touchSwipe.min.js')}}"></script>


  <!-- Template Main Javascript File -->
  <script src="{{url('assets/new_home/js/main.js')}}"></script>
  @if(!Request::is('access/members-area'))
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/593e6cecb3d02e11ecc696e5/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
@endif

@if(Request::is('/'))
    <script>
        var delay = $("#delay").val();
        $(window).on('load',function(){
            setTimeout(function(){
               $('#promoModal').modal('show');
            }, delay+'000');
        });
    </script>
@endif

@if(Request::is('/'))
    <script>
        var end = new Date('07/07/2017 12:00 AM');
        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {

                clearInterval(timer);
                document.getElementById('countdown').innerHTML = 'EXPIRED!';

                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

            document.getElementById('countdown').innerHTML = days + 'days ';
            document.getElementById('countdown').innerHTML += hours + 'hrs ';
            document.getElementById('countdown').innerHTML += minutes + 'mins ';
            document.getElementById('countdown').innerHTML += seconds + 'secs';
        }

        timer = setInterval(showRemaining, 1000);
    
        var time = new Date().getTime();
        $(document.body).bind("mousemove keypress", function(e) {
            time = new Date().getTime();
        });

        function refresh() {
            if(new Date().getTime() - time >= 600000){
                window.location.reload(true);
            } else {
                setTimeout(refresh, 600000);
            }
        }
        setTimeout(refresh, 600000);
    </script>
@elseif(Request::is('access/members-area'))
    <script>
        var getTime = $("#schedule-time").attr("time");
        var end = new Date(getTime);
        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var nowUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
            var distance = end - nowUTC;
            if (distance < 0) {

                clearInterval(timer);
                document.getElementById('live-stream-countdown').innerHTML = 'NOW!';

                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);

            document.getElementById('live-stream-countdown').innerHTML = days + 'days ';
            document.getElementById('live-stream-countdown').innerHTML += hours + 'hrs ';
            document.getElementById('live-stream-countdown').innerHTML += minutes + 'mins ';
        }

        timer = setInterval(showRemaining, 1000);
    
        var time = new Date().getTime();
        $(document.body).bind("mousemove keypress", function(e) {
            time = new Date().getTime();
        });

        function refresh() {
            if(new Date().getTime() - time >= 600000){
                window.location.reload(true);
            } else {
                setTimeout(refresh, 600000);
            }
        }
        setTimeout(refresh, 600000);
    </script>
@endif
<script>
    function displaySubmitBtn(){
        if(document.getElementById('i_agree').checked) {
            $("#submit_managed_account_form").show();
        } else {
            $("#submit_managed_account_form").hide();
        }
    }
</script>