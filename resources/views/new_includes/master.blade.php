<!DOCTYPE html>
<html lang="en">
@include('new_includes.head')

<body>


@include('new_includes.navbar')
<main id="main">

@if((count($errors) > 0) || isset($sub_notice))
			@include("new_includes.errors")
		@endif

@yield('content')

  <!--==========================
    Footer
  ============================-->
  @include('new_includes.footer')

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
 <div id="preloader"></div>

@include('new_includes.script')
</main>

</body>
</html>
