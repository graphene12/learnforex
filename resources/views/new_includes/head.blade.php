<head>
    <meta charset="utf-8">
    <title>Learn Forex Live Signals {{ isset($title)?" - ".$title:NULL }}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
  
    <!-- Favicons --><link rel="icon" type="image/png" href="{{ url('assets/img/favicon.ico') }}">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Courgette&display=swap" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">
  
    <!-- Bootstrap CSS File -->
    <link href="https://fonts.googleapis.com/css?family=Amaranth&display=swap" rel="stylesheet">
    <link href="{{url('assets/new_home/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  
    <!-- assets/new_home/libraries CSS Files -->
    <link href="{{url('assets/new_home/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/new_home/lib/animate/animate.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/new_home/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/new_home/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/new_home/lib/lightbox/css/lightbox.min.css')}}" rel="stylesheet">
  
    <!-- Main Stylesheet File -->
    <link href="{{url('assets/new_home/css/styles.css')}}" rel="stylesheet">

    @if (Request::is('pricing*'))
    <link href="{{url('assets/new_home/css/pricing.css')}}" rel="stylesheet">
    <style type="text/css">
        .pricing-title{
            text-align: center;
        }
        .pricing-title h5{
            font-weight: bold;
            font-size: 2em;
            text-transform: uppercase;
        }
        
    </style>
    @endif
    
  
    <style type="text/css">
    .img-fluid {
    max-width: 100%;
    position: relative;
    width: 255px;
    height: 255px;
} 
.img-fluid2{
    max-width: 100%;
    position: relative;
    width: 859px;
    height: 430px;
}
.img-fluid3{
    max-width: 100%;
    position: relative;
    width: 819px;
    height: 437px;
}
.font-change{
    font-size:14px;
}
.auth-div {
    border: 2px solid #cbefcf;
    padding: 5%;
    background-color: #cbefcf;
    position: relative;
    /* top: 17%; */
    height: 110%;
    /* border-radius: 5px; */
}
</style>
  </head>