@if (Request::is('/') | Request::is('managed-accounts'))
<header id="header" style="position:fixed;">
@else
<header id="header" style="background-color:black;">
@endif
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <h1><a href="{{route('landingpage')}}" class="scrollto"><img src="{{ URL('/') }}/assets/img/logo.png" alt=""></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          @if(!Request::is('/'))
          <li class="menu-active"><a class="font-change" href="{{route('landingpage')}}">Home</a></li>
          @endif          
          @if(Auth::user())
          <li><a class="font-change" href="{{URL('/auth/logout')}}">Logout</a></li>
          @else 
          <li><a class="font-change" href="{{route('login_page')}}">Login</a></li>
          @endif
          @if(!Auth::user())
          <li><a class="font-change" href="{{route('sign_up')}}">Sign up</a></li>
          @endif
          <li><a class="font-change" href="{{route('managed_accounts')}}">Managed Accounts</a></li>
          <li><a class="font-change" href="{{route('pricing')}}">Pricing</a></li>
         
          <li><a class="font-change" href="{{route('contact')}}">Contact</a></li>
          <li><a class="font-change" href="{{ URL('/dashboard/live-room') }}">Live Trading Room</a></li>
          <li><a class="font-change" href="http://www.forexknights.com/store-mentoringprogram_ja.php">Mentoring Course</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->