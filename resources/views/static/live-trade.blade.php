@extends("includes.master")
@section("contents")
	<div class="page-banner">
		<!-- The video -->
        <div class="video-background">
            <div class="video-foreground">
                <iframe src="{{ $video['url'] }}?controls=0&showinfo=0&rel=0&autoplay=1&loop=1" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
            </div>
        </div>

		<!-- Optional: some overlay text to describe the video -->
		<div class="content">
			@if($video['title'])
		  		<h3>{{ $video['title'] }}</h3>
		  	@else
				<h3>No live video at this time, check back later.</h3>
		  	@endif
		  <!-- Use a button to pause/play the video with JavaScript -->
		  @if(Auth::check())
		  	<a class="btn btn-blue btn-md" href="{{ URL('/access/members-area') }}">member's area</a>
		  @else
			<a class="btn btn-blue btn-md">sign up now</a>
		  @endif
		</div>
	</div>
@stop