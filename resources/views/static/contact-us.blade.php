@extends("includes.master")
@section("contents")
	@include("includes.home_banner")
	<div class="section service-section" id="services" style="margin-bottom: 0%;">
        <div class="container" style="margin-top: -5%;">
            <div class="row justify-content-center">
            	<div class="col-md-4"></div>
                <div class="col-md-3">
            		<!-- <form action="{{ URL('contact-us') }}" method="POST" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
	                	<div class="row">
	                        <div class="col-md-6">
	                            <div class="form-group">
	                                <label for="name" class="form-element-label">Your Name:</label>
	                                <input type="name" id="name" name="name" value="{{ old('name') }}" class="form-control" required="required" placeholder="Fullname">
	                            </div>
	                        </div>

	                        <div class="col-md-6">
	                            <div class="form-group">
	                                <label for="email" class="form-element-label">Your Email:</label>
	                                <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" required="required" placeholder="Email">
	                            </div>
	                        </div>

	                        <div class="col-md-12">
	                            <div class="form-group">
	                                <label for="email" class="form-element-label">Your Message:</label>
	                                <textarea name="message" id="message" class="form-control form-textarea" required="required">{{ old('message') }}</textarea>
	                            </div>
	                        </div>

	                        <div class="col-md-12">
								<button class="btn btn-blue login-btn float-right" type="submit">send message</button>
							</div>
	                    </div>
            		</form> -->
            		<table class="table" style="border-top: none !important;">
            			<tr>
            				<td><strong>Support</strong></td>
            				<td>
            					<a href="mailto:support@learnforexlivesignals.com" style="font-size: 17px; cursor:pointer !important;">support@learnforexlivesignals.com</a>
            					<br>
            					<a href="mailto:learnforexlivesignals@gmail.com" style="font-size: 17px; cursor:pointer !important;">learnforexlivesignals@gmail.com</a>
            				</td>
            			</tr>
            		</table>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
	</div>
@stop