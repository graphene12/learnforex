@extends("includes.master")
@section("contents")
	@include("includes.home_banner")
	<script type="text/javascript" src="/jscolor/jscolor.js"></script>


    <div class="section service-section" id="services">
        <div class="container" style="margin-top: -5%;">
            <div class="row">
                <div class="col-md-12">
                    <center>
                        <p style="font-size: 17px;">
                            YOU MAY NEVER MAKE MONEY TRADING THE FOREX MARKET ALL BY YOURSELF!
                             Avoid losing your hard earned money!  Don't be scamed! Trade the FOREX markets with Professionals who will teach and guide you on how to take trade(s) and exit at the very right time. It's our job and we have decided to help many Traders  discover the most profitable ways of FOREX TRADING.
                        </p>
                    </center>
                </div>
            </div>
        </div>
    </div>


    <div class="section benefits-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ URL('/') }}/assets/img/signals.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <h5 class="benefits-title" style="text-align:left !important; margin-bottom: -1%;">Our <span class="underline">Services</span></h5>
                    <ul class="benefits">
                        <li class="dots"><p>24/7 Free Forex / MT4 technical support.</p></li>
                        <li class="dots"><p>2 Live Market Analysis Sessions daily (Intra-Day & Part-Time Live Market Analysis).</p></li>
                        <li class="dots"><p>High quality Forex Signals (with entries, stop loss & take profit levels) delivered directly to your mobile device.</p></li>
                        <li class="dots"><p>News Trading (i.e NFP, FOMC, etc.)</p></li>
                        <li class="dots"><p>Part-Time Trading (20 minutes a day) suitable for traders with limited screen time (D1, W1, & MN time frames).</p></li>
                        <li class="dots"><p>Intra-Day & Medium-Term Trading (M15, M30,H1, H4 time frames). No Scalping!</p></li>
                        <li class="dots"><p>Live chat with seasoned Traders.</p></li>
                        <br>
                        <a href="{{ URL('auth/register') }}" class="btn btn-yellow btn-lg">join us now</a>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="section mid-banner">
        <div class="convince-banner" data-parallax="true" style="background: url('<?php echo URL('/'); ?>/assets/img/candle-stick.jpg');">
            <div class="convince-banner-overlay">
                <div class="">
                    <p class="mid-banner-title">Take your forex trading career <br><span class="big-title">from zero to hero</span></p>
                    <p class="convince-banner-content">
                        Trade the FOREX markets by copying our profitable and easy to follow trading signals. We will make sure your subscription is worth your while. 
                        <br><br>
                        <a href="{{ URL('auth/register') }}" class="btn btn-yellow btn-lg make-white">Join us now</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    {{-- ----------------------------I had to do this because this has to be down temporary
    
    <div class="section mid-banner" style="margin-top: 5%; margin-bottom:5%;background-color: rgba(228, 225, 225, 0);">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="margin-top: 3%;">
                    <h5 class="benefits-title" style="text-align:left !important; margin-bottom: -1%;">Watch our <span class="underline">live trade</span></h5>
                    <ul class="benefits">
                        <li class="dots"><p>Follow us on our daily stream.</p></li>
                        <li class="dots"><p>Check out how we trade what we discuss.</p></li>
                    </ul>
                    <br>
                    <p style="font-size: 19px;">We'll be streaming to you live everyday showing you how we trade what we teach.</p>
                    <!-- <a href="{{ URL('auth/register') }}" class="btn btn-blue btn-lg">join us now</a> -->
                </div>
                <div class="col-md-6">
                    <iframe src="{{ $video['url'] }}?controls=1&showinfo=0&rel=0&autoplay=0&loop=1" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen style="width: inherit;height: 330px;"></iframe>
                </div>
            </div>
        </div>
    </div>
    --}}

    <div class="section testimonies-section" id="testimonies">
        <div class="container">
            <h5 class="benefits-title">Client <span class="underline">Testimonials</span></h5>
            <center><p class="testimonies-small-title">Hear what our clients are saying.</p></center>
            <div class="row justify-content-center">
                <div class="col-8">
                    <div id="carouselExampleControls" class="carousel slide" style="height: 320px !important;">
                        <div class="carousel-inner remove-shadow" role="listbox">
                            <div class="carousel-item active">
                                <center>
                                    <p>
                                        By far, this is the most honest and reliable forex resource site I have ever seen. I took the free offer, it got expired and was never billed automatically. Unlike most scammers that will take from my Bank account without my consent.  Thanks guys, I really appreciate. Staying on!
                                        <br><br>
                                        <img src="http://www.azur-autos.com/images/avatar.png" alt="" width="70px">
                                        <span class="client-name">
                                            Bill Viva&nbsp;&nbsp;-&nbsp;&nbsp;<span class="location small">Sydney, Australia</span>
                                        </span>
                                    </p>
                                </center>
                            </div>
                            <div class="carousel-item">
                                <center>
                                    <p>
                                        I came across learnforexlivesignals.com on Youtube and subscribed to your trading signals. I could not believe that such great performing signals were totally low-cost! I am grateful for this service and amazing support. Services as yours are really helpful for newbies like me. Highly recommended!
                                        <br><br>
                                        <img src="http://www.azur-autos.com/images/avatar.png" alt="" width="70px">
                                        <span class="client-name">
                                            Mr. Ogbebor Jonathan&nbsp;&nbsp;-&nbsp;&nbsp;<span class="location small">Lagos, Nigeria.</span>
                                        </span>
                                    </p>
                                </center>
                            </div>
                            <div class="carousel-item">
                                <center>
                                    <p>
                                        Thank you so much guys. Frankly, you have changed my FOREX orientation for the better. I am indeed grateful!
                                        <br><br>
                                        <img src="http://www.azur-autos.com/images/avatar.png" alt="" width="70px">
                                        <span class="client-name">
                                            Amelia George&nbsp;&nbsp;-&nbsp;&nbsp;<span class="location small">London, UK</span>
                                        </span>
                                    </p>
                                </center>
                            </div>
                            <div class="carousel-item">
                                <center>
                                    <p>
                                        Jacob, you are awesome! Your daily live sessions have helped me a lot and saved me from so many troubles. Keep up the good work.
                                        <br><br>
                                        <img src="http://www.azur-autos.com/images/avatar.png" alt="" width="70px">
                                        <span class="client-name">
                                            Michael Mason&nbsp;&nbsp;-&nbsp;&nbsp;<span class="location small">Detroit, USA</span>
                                        </span>
                                    </p>
                                </center>
                            </div>
                            <div class="carousel-item">
                                <center>
                                    <p>
                                        Thank God I signed up for your Managed Account. It's unbelieveable that my account is now doubled. Thanks again and again!
                                        <br><br>
                                        <img src="http://www.azur-autos.com/images/avatar.png" alt="" width="70px">
                                        <span class="client-name">
                                            Kim Sampath&nbsp;&nbsp;-&nbsp;&nbsp;<span class="location small">Atlanta, Georgia.</span>
                                        </span>
                                    </p>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    {{-- ---------------- This has to be hidden too
    
    <div class="section convinced-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <center>
                        <span class="convinced-text">
                            Learn more about forex trading by watching our LIVE TRADE VIDEOS
                        </span>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="{{ URL('live-trade') }}"  class="btn btn-convinced">watch now</a>
                    </center>
                </div>
            </div>
        </div>
    </div>
    
    --}}
    
    <div class="section mentors-section">
        <div class="container">
            <h4 class="mentors-title">Seasoned and <span class="underline">Professional Mentors</span></h4>
            <div class="row">
                <div class="col-lg-6 col-md-12 d-flex flex-lg-row mentors">
                    <!-- <div class="p-2">
                        
                    </div> -->
                    <div class="p-2">
                        <h5 class="mentor-name">Hector DeVille</h5>
                        <p>
                            <img src="{{ URL('/') }}/assets/img/hector.jpg" alt="" style="float:left; margin-right: 20px;" class="img-raised" width="135px" height="140px">
                            <p align="justify" style="font-size: 16px;">
                                Hector DeVille is an active Forex trader and educator. He began his FOREX career in 1998 and later worked in a Madrid based financial firm as a Senior Trader. Hector acquired his education in Madrid and Australia.
                                
                                He holds two advanced degrees in Finance and Statistics. As a seasoned trader, Hector's trading style is based purely on price action determined by his fundamental and technical analysis. 
                                
                                He has developed high quality trading strategies both for intraday and long term trading to include, the 3SMA trend riding strategy, Intra-day Market Flow, London Open Breakout (LOB), Trend Collapse, Fake Breakout, Breakout-Congestion-Continuation, etc.
                                
                                He is the Chief Strategist with a real and honest passion to educate others via Forex mentoring in other to help them become successful and profitable Traders.
                            </p>
                            <!-- <br>
                            <span class="social-icons">
                                <a href="#"><i class="fa fa-facebook-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-linkedin-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-twitter-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span> -->
                        </p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 d-flex flex-lg-row mentors">
                    <!-- <div class="p-2">
                        
                    </div> -->
                    <div class="p-2">
                        <h5 class="mentor-name">Jacob Ashibi</h5>
                        <p>
                            <img src="{{ URL('/') }}/assets/img/jacob.jpg" alt="" style="float:left; margin-right: 20px;" class="img-raised" width="145px" height="140px">
                            <p align="justify" style="font-size: 16px;">
                            Jacob Ashibi is a seasoned and professional Forex Trader with over 13 years of trading experience. With good chart reading skills, Jacob is an expert in Market reviews and Currency analysis.
                            
                            As a price action trader, he shares the same trading ideologies with Hector DeVille. As a long standing partner of Hector DeVille, he is now a Master Trader of all Hector DeVille's trading strategies.
                            
                            He is vast in financial economics and capital markets.
                            </p>
                            <!-- <br>
                            <span class="social-icons">
                                <a href="#"><i class="fa fa-facebook-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-linkedin-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-twitter-square"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span> -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Announcement Modal -->
    @if($announcement)
        <div class="modal fade" id="promoModal" tabindex="-1" role="dialog" aria-labelledby="promoModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <a href="javascript(void);" class="close-window" data-dismiss="modal" aria-hidden="true">close</a>
                <form action="#" autocomplete="off" method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <input id="delay" value="{{ $announcement['delay'] }}" style="display: none;">
                            <input id="stop" value="{{ $announcement['stop'] }}" style="display: none;">
                            <h4 class="home-auto-pop">
                                <center>{{ $announcement['title'] }}</center>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                {{ $announcement['content'] }}
                            </p>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
    @endif
    <!--  End Modal -->
@stop