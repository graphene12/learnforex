@extends('new_includes.master')

@section('content')
      <!--==========================
    Intro Section
  ============================-->
<style>
.membertext{
  font-weight:bold;
  color:white;
  font-family: 'Times New Roman', Times, serif;
}
.they{
  color: red;
  font-family:Verdana, Geneva, Tahoma, sans-serif, Times, serif;
  text-align: center;
  max-width: 400px;
}
.we{
  color: green;
  font-family:Verdana, Geneva, Tahoma, sans-serif, Times, serif;
  text-align:center;
  max-width: 400px;


}
</style>
  <section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel slide" data-ride="carousel">

        {{-- <ol class="carousel-indicators"></ol> --}}

        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active">
            <div class="carousel-background"><img src="{{url('assets/new_home/img/intro-carousel/2.jpg')}}" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 style="font-weight:900;text-transform:uppercase;font-family: Lato,'Helvetica Neue',Helvetica,Arial,sans-serif;">Professional Forex Trading</h2>
                <p style="font-size:2em;font-weight:bold;font-family: 'Courgette', cursive">... Changing Losers to Winners!</p>
                <a href="#clients" style="font-size:1.5em;" class="btn-get-started scrollto">Learn More</a>
                @if(!Auth::user())
                <a href="{{route('sign_up')}}" style="font-size:1.5em;" class="btn-get-started2 scrollto">Sign up</a>
                @endif
              </div>
            </div>
          </div>



        </div>

        {{-- <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>  --}}

      </div>
    </div>
  </section><!-- #intro -->

  <section id="clients" class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
    <div class="container">

      <header >
        <h3 style="color:black;font-weight:900;font-family:Verdana, Geneva, Tahoma, sans-serif;text-align:center;">
          INTRODUCING A BRAND NEW WAY OF TRADING THE FOREX MARKET. NO HYPE! NO SCAM!</h3>
      </header>


    </div>
  </section>

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container">

        <header class="section-header wow fadeInUp">
          <h3>Summary of what you will get</h3>
        </header>

        <div class="row">

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-android-contacts"></i></div>
            <h4 class="title"><a disable >Direct Technical Support</a></h4>
              <p>We are available to guide and set you up, Giving you professional trading advice. You'll enjoy the services of one of the best Forex Brokers in the world.</p>
             </div>

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
            <h4 class="title"><a disable>Live Market Analysis </a></h4>
            <p>You'll watch live videos and recieve updated Chart Analysis in real time. Your technical skills will improve positively.</p>
          </div>

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-paper-outline"></i></div>
            <h4 class="title"><a disable>High quality Signals</a></h4>
            <p>2-5 trades per day with a near 100% profitability. 500 - 1500 Pips a Month. Entry Signals with stop loss levels and profit targets directly on your mobile device. Trades are managed with real-time updates until they're closed.</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-time"></i></div>
            <h4 class="title"><a disable>News Protection</a></h4>
            <p>75% of trading accounts are blown out by News announcements. We'll keep you updated with every single fundamental event.</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-barcode-outline"></i></div>
            <h4 class="title"><a disable>Trading Tools</a></h4>
            <p>Access to most of the world's best expert advisors and indicators. This is priceless!</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-people-outline"></i></div>
            <h4 class="title"><a disable>mt4 Mastery</a></h4>
            <p>By watching our live market analysis videos, you will gain new skills in manipulating the MT4 platform to your advantage.</p>
          </div>
          

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action" class="wow fadeIn">
      <div class="container text-center">
        <h3>TAKE YOUR FOREX TRADING CAREER 
          FROM ZERO TO HERO</h3>
        <p> Trade the FOREX markets by copying our profitable and easy to follow trading signals. We will make sure your membership is worth your while.</p>
        @if(!Auth::user())
        <a style="border:2px yellow solid;color:yellow;font-family:Verdana, Geneva, Tahoma, sans-serif;font-weight:bold;padding:20px 50px;" class="btn btn-default btn-lg cta-btn" href="{{route('sign_up')}}">JOIN NOW!</a>
        @endif
      </div>
    </section><!-- #call-to-action -->



    <!--==========================
      Clients Section
    ============================-->
    <section id="testimonials" class="section-bg wow fadeInUp">
      <div class="container">

        <header class="section-header">
          <h3 style="color:black;">What some of our subscribers say</h3>
        </header>

        <div class="owl-carousel testimonials-carousel">

          <div class="testimonial-item">
            <h3>Bill Viva </h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              "By far, this is the most honest and reliable forex resource site I have ever seen. I took the free offer, it got expired and was never billed automatically. Unlike most scammers that will take from my Bank account without my consent.  Thanks guys, I really appreciate. Staying on!"
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
            <h3>Mr. Ogbebor Jonathan</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              "I came across learnforexlivesignals.com on Youtube and subscribed to your trading signals. I could not believe that such great performing signals were totally low-cost! I am grateful for this service and amazing support. Services as yours are really helpful for newbies like me. Highly recommended!"
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
            <h3>Mr. Eric Sackley.</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              "Thanks very much for all your help Jacob. Jared Martinez(FX Chief) has said that champion traders win their trade before taking it while losing traders take the trade first and then struggle to get the market go their way. With the little experience I've had with you, I can say you are a champion trader. Keep the good work going and let the winning fire keep burning. God bless."
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
            <h3>Mr. Mohammed Abdul.</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              "Keep up the Good work Guys. I`m happy to continue with you guys."<img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
            <h3>Mr. Frederick.</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              "I'm very Happy for you guys to keep managing my accounts, as i`m consistently in profit. Thanks to you guys"
            </p>
          </div>

          <div class="testimonial-item">
            <h3>Viny</h3>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
             "I`ve never worried since i have joined you, apart from on few occasions when you were away. I must be honest."
            </p>
          </div>

        </div>
      </div>
    </section><!-- #testimonials -->

    <!--==========================
      Team Section
    ============================-->
    <section id="team" >
      <div class="container">
        <div class="section-header wow fadeInUp">
          <h3 >Our Seasoned and Professional Mentors</h3>
        </div>

        <div class="row">

          <div class="col-lg-6 col-md-12 wow fadeInUp">          
            <div class="member">
              <img src="{{url('assets/img/hector.jpg')}}" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Hector DeVille</h4>
                 
                </div>
              </div>
            </div>
            <p class="membertext" >Hector DeVille is an active Forex trader and educator. He began his FOREX career in 1998 and later worked in a Madrid based financial firm as a Senior Trader. Hector acquired his education in Madrid and Australia. He holds two advanced degrees in Finance and Statistics. As a seasoned trader, Hector's trading style is based purely on price action determined by his fundamental and technical analysis. He has developed high quality trading strategies both for intraday and long term trading to include, the 3SMA trend riding strategy, Intra-day Market Flow, London Open Breakout (LOB), Trend Collapse, Fake Breakout, Breakout-Congestion-Continuation, etc. He is the Chief Strategist with a real and honest passion to educate others via Forex mentoring in other to help them become successful and profitable Traders.</p>

          </div>

          <div class="col-lg-6 col-md-12 wow fadeInUp">
            <div class="member">
              <img src="{{url('assets/img/jacob.jpeg')}}" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Jacob Ashibi</h4>
                 
                </div>
              </div>
            </div>
            <p class="membertext">Jacob Ashibi is a seasoned and professional Forex Trader with over 13 years of trading experience. With good chart reading skills, Jacob is an expert in Market reviews and Currency analysis. As a price action trader, he shares the same trading ideologies with Hector DeVille. As a long standing partner of Hector DeVille, he is now a Master Trader of all Hector DeVille's trading strategies. He is vast in financial economics and capital markets.</p>
          </div>

        </div>

      </div>
    </section><!-- #team -->
    <section id="about">
      <div class="container">
  
        <header class="section-header">
          <h3 style="color:black;">Managed Accounts</h3>
        </header>
        <div class="row about-cols">
  
            
          <div class="col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="about-col">
              <p style="color:black;font-size:20px;font-family:Montserrat, sans-serif;font-weight:500;margin-top:65px;text-align:justify;">
                <ul style="list-style:square;color:black;font-size:18px;font-family:Montserrat, sans-serif;margin-bottom:20px;">
                  <li>Don't have time to trade the Forex Markets?</li>
                  <li>Are you losing money consistently?</li>
                </ul>
              
                <span style="color:black;font-size:20px;font-family:Montserrat, sans-serif;font-weight:500;text-align:justify;">
                  Don't you worry, our managed account option is available for you. We will trade on your account with limited privileges. You will be in total (100%) control of your account.
                </span>
</p>
                 <center>
                  <a style="color:white;font-family:Verdana, Geneva, Tahoma, sans-serif;font-weight:bold;padding:20px 20px;margin:35px 0;" class="btn btn-success btn-lg cta-btn" href="{{route('managed_accounts')}}">INVEST NOW</a>
                 </center>
                 
            </div>
          </div>
  
          <div class="col-md-6 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="{{url('assets/new_home/2e259b1f-be42-4b71-a3ab-d49fdf5e4be4.jpeg')}}"  class="img-fluid2">
              </div>
              
            </div>
          </div>
  
        </div>
  
      </div>
    </section><!-- #about -->
    <section id="skills">
      <div class="container">

        <header class="section-header">
          <h3 style="color:black;">WHY WE STAND OUT FROM OTHERS</h3>
        </header>

        <div class="row about-cols col-lg-8 offset-lg-2">
          <table class="table table-bordered" style="text-align:center;">
            <thead>
              <tr>
                <th>OTHERS</th>
                <th>WE</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="they">
                  They throw the signals at you without explanations.
                </td>
                <td class="we">
                  We explain the reasons behind every trade before they`re taken.
                </td>
              </tr>
              <tr>
                <td class="they">
                  Their signals come without a pre-information.
                </td>
                <td class="we">
                  We give you at least 15 minutes pre-information/notification to get you prepared for the trade.
                </td>
              </tr>
              <tr>
                <td class="they">
                  They don`t manage their trades till the end, they`ll leave you to gamble the outcome all by yourself.
                </td>
                <td class="we">
                  We manage the trade with you till the end, notifying you of every single action taken.
                </td>
              </tr>
              <tr>
                <td class="they">
                  They are more interested in your short term subscription. Hence, they don`t care about your profitablity.
                </td>
                <td class="we">
                  We`re seriously interested in your long term subscription. We believe a profitable subscriber will be a long term subscribers.
                </td>
              </tr>
              <tr>
                <td class="they">
                  They leave you entirely in the hands of the newbies who only share junk signals that eventually blow your account.
                </td>
                <td class="we">
                  Our Signals are highly censored. They are strictly given by our seasoned Professionals.
                </td>
              </tr>
              <tr>
                <td class="they">
                  They are never profitable in the long run.
                </td>
                <td class="we">
                  We Guarantee your long term profitablity. Your account will grow steadily.
                </td>
              </tr>
              <tr>
                <td class="they">
                  They are full of deceit and Fraud.
                </td>
                <td class="we">
                  We are exceptionally transparent.
                </td>
              </tr>
              <tr>
                <td class="they">
                  They steal from your credit card via automatic renewal of subscription plan even when you are not satisfied.
                </td>
                <td class="we">
                  We ensure that all your financial transactions with us are authorised by you, Hence, no automatic renewal of your subscription plan.
                </td>
              </tr>
              <tr>
                <td class="they">
                  They ask you to cancel your subscription by writing to support. They delay support response and bill you automatically.
                </td>
                <td class="we">
                  We don`t require cancellation of subscription plan. If you`re not satisfied with our results, simply close our Website and that will be the end.
                </td>
              </tr>
              
            </tbody>
          </table>

        </div>

      </div>
    </section><!-- #about -->

    <section id="about">
      <div class="container">
  
        
        <div class="row about-cols">
  
          <div class="col-md-6 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="{{url('assets/new_home/df88744f-cf33-4d30-8c4f-2623368311fe.jpeg')}}"  class="img-fluid2">
              </div>
              
            </div>
          </div>
  
          <div class="col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="about-col">
              <p style="color:black;font-size:20px;font-family:Montserrat, sans-serif;margin-top:100px;text-align:justify;">
                Trade with us and regain your financial confidence. We'll help you recover all that you've lost and make you an independent professional. If you don't make it with us, we bet, you may never make it elsewhere. Take the bold step, sign up today and change your trading career for the better.
                 </p>
                 @if(!Auth::user())
                 <center>
                  <a style="color:white;font-family:Verdana, Geneva, Tahoma, sans-serif;font-weight:bold;padding:20px 20px;margin:35px 0;" class="btn btn-success btn-lg cta-btn" href="{{route('sign_up')}}">JOIN TRADING ROOM!</a>
                 </center>
                 @endif
            </div>
          </div>
  
         
  
        </div>
  
      </div>
    </section><!-- #about -->

  </main>
  @if($announcement)
  <div class="modal fade" id="promoModal" tabindex="-1" role="dialog" aria-labelledby="promoModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <a href="javascript(void);" class="close-window" data-dismiss="modal" aria-hidden="true">close</a>
          <form action="#" autocomplete="off" method="POST">
              <div class="modal-content">
                  <div class="modal-header">
                      <input id="delay" value="{{ $announcement['delay'] }}" style="display: none;">
                      <input id="stop" value="{{ $announcement['stop'] }}" style="display: none;">
                      <h4 class="home-auto-pop">
                          <center>{{ $announcement['title'] }}</center>
                      </h4>
                  </div>
                  <div class="modal-body">
                      <p>
                          {{ $announcement['content'] }}
                      </p>
                  </div>
                  
              </div>
          </form>
      </div>
  </div>
@endif
<!--  End Modal -->

@endsection