@extends('new_includes.master')

@section('content')
     
<section id="intro">
  <div class="intro-container">
    <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">


      <div class="carousel-inner" role="listbox">


        <div class="carousel-item active">
          <div class="carousel-background" style="opacity:inherit;"><img src="{{ URL('/') }}/assets/img/contact.jpg" alt=""></div>
          <div class="carousel-container">
            <div class="carousel-content">
              <div class="content-center">
                  <h5 class="title banner-title" style="font-weight:900;font-size:30px;color:white;">Contact Us</h5>
                  <hr style="border:2px solid white;">
                  <div class="text-center">
                      <p style="color:white;font-weight:600; font-size: 18px; ">Please send us an Email to any of the Email Address below
                        <br><a href="mailto:support@learnforexlivesignals.com">support@learnforexlivesignals.com</a>
                        <br><a href="mailto:learnforexlivesignals@gmail.com">learnforexlivesignals@gmail.com</a><br>
                        or Use the Live-Chat Widget @ the bottom right corner of your screen
                      </p>
                      
                      
                      
                  </div>
              </div>
          
            </div>
          </div>
        </div>

      </div>


    </div>
  </div>
</section>

  
   
@endsection