@extends('new_includes.master')

@section('content')

<?php
    $array = ['ultimite','silver','gold','mega','platinum','life'];
    $i = 0;
?>
    
        <section id="contact">
            <div class="section">
                <div class="container">		
                   
                        
                        <div class="pricing-title">
                            <h5 style="color:black;">Subscription Plans</h5>
                            <p style="font-size:17px;">To join the Live Trading Room and Signal Service, please select a subscription plan that is suitable for you. If you`re already a member of the Live Trading Room, wait for the expiration of your current subscription Plan before choosing a new one.</p>
                            <p style="font-size:17px;"><span style="color:green;font-weight:bold;">Subscription is not set to automatic renewal or recurring billing. If you are not satisfied with the services, simply walk away and you'll never be billed again.</span></a></p>
                            <p style="font-size:17px;">All Subscriptions Plan attract the same services. The difference is in the price. The higher the plan, the cheaper the cost of subscription.</p>
                        </div>

                        
                        <div id="price">
                            @if(Auth::check())
					<?php $first_plan = Auth::user()->first_plan; ?>
				@else
					<?php $first_plan = null; ?>
                @endif
                
                @foreach ($plans as $plan)
                @if($plan->id != '1')
                @if($first_plan !== NULL && $first_plan == 1 && $first_plan == $plan->id )
                @else
                @if($plan->id != 7)
                <div class="plan {{$array[$i]}}">
                    <?php $i++; ?>
                    <div class="plan-inner">
                      <div class="entry-title">
                        @if($i == 6)
                        <h3>{{$plan->title}} </h3>
                        @else
                        <h3>{{$plan->title}} - <span >{{$plan->validity}}</span></h3>
                        @endif
                        <div class="price">${{$plan->price}}
                        </div>
                      </div>
                      <div class="btn">
                        <form action="payment/paydot" method="POST">
                            <input type="hidden" name="plan_id" value="{{ $plan->id }}">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-sm btn-success"  >
                                Subscribe Now
                            </button>
                        </form>	
                      </div>
                    </div>
                  </div>
                <?php
                    if($i == count($array)){
                        $i = 0;
                    }
                ?>
                  @endif
                @endif
                @endif
                @endforeach
                        </div>

                        <div class="row">
                            <img src="{{ URL('/') }}/assets/img/payment_channels.jpg" height="25%" width="25%" style="margin-left:auto; margin-right:auto;">
                        </div>
            </div>
            </div>
<div class="row" style="margin-top:20px;">
  <div class="col-md-6 offset-md-3 card" style="color:black;text-align:justify;">
    <span style="font-weight:bold;color:black;text-align:center;">WHY YOUR PAYMENT MIGHT FAIL</span><br>
    <span style="color:red;font-weight:bold;">3D Authentication Failure:</span> 3D Secure protects the cardholder against unauthorised use of their card(s) for making online payments. When you use a MasterCard or VISA card that has 3D secure, it means that you get a token via SMS or e-mail to complete the payment you are making in order to prove it is you. Where your payment card is not 3D Authenticated, you will need to contact your Bank to activate 3D secure in it, otherwise, your transaction is likely to fail. We're highly regulated to accept payments only from 3D Secured Cards. As part of our transparency policy, this is to curb financial fraud on our website!
  </div>
</div>
            
        </section>
   
@endsection