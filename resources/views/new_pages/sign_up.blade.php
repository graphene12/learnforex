@extends('new_includes.master')

@section('content')
    
        <section id="contact">
            <div class="section	auth-section" style="margin-bottom: 50px;">
                <div class="container">		
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="auth-div" style="top: 5%">
                                <div class="form-header"><h4>Create an Account</h4></div>
                                <form action="{{ URL('auth/register') }}" autocomplete="off" method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="plan_id" value="{{ isset($plan_id)?$plan_id:NULL }}">
                                    <input type="hidden" name="signal_option" value="Telegram">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="form-element-label">Your Name:</label>
                                                <input type="name" id="name" name="name" value="{{ old('name') }}" class="form-control" required="required" placeholder="Fullname">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email" class="form-element-label">Your E-mail Address:</label>
                                                <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" required="required" placeholder="Email">
                                            </div>
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <label for="password" class="form-element-label">Your Password:</label>
                                        <input type="password" id="password" name="password" class="form-control" required="required" placeholder="Password">
                                    </div>
        
                                    <div class="form-group">
                                        <label for="password" class="form-element-label">Verify Password:</label>
                                        <input type="password" id="password" name="password_confirm" class="form-control" required="required" placeholder="Verify Password">
                                    </div>
                                    
                                        <div class="form-group">
                                            <label for="phone" class="form-element-label">Your Phone Number:</label><br>
                                            <span style="color:blue;">Please, download and install Telegram messenger on your device using your registered phone number below</span>
                                            <input type="phone" id="phone" name="phone" value="{{ old('phone') }}" class="form-control" required="required" placeholder="Example: (+44)7911123456">
                                        </div>
                                    
                                    <div class="float-left">
                                        <a href="{{ URL('/auth/login') }}">Have an account? Login here</a>
                                        <!-- <br>
                                        <a href="{{ URL('/auth/register') }}">Create account</a> -->
                                    </div>
        
                                    <div class="float-right">
                                        <button class="btn btn-success login-btn" type="submit">Next</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>

        </section>
   
@endsection