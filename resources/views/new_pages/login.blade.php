@extends('new_includes.master')

@section('content')
    
        <section id="contact">
            <div class="section auth-section">
                <div class="container">		
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <div class="auth-div">
                                <div class="form-header"><h4>Log into your account</h4></div>
                                <form action="{{ URL('auth/login') }}" method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label for="email" class="form-element-label">Your E-mail Address:</label>
                                        <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" required="required" placeholder="Email">
                                    </div>
        
                                    <div class="form-group">
                                        <label for="password" class="form-element-label">Your Password:</label>
                                        <input type="password" id="password" name="password" class="form-control" required="required" placeholder="Password">
                                    </div>
                                    <div class="float-left">
                                        <a href="{{ URL('/auth/forgot-password') }}">Forgot your password?</a>
                                        <br>
                                        <a href="{{ URL('/auth/register') }}">Create account</a>
                                    </div>
        
                                    <div class="float-right">
                                        <button class="btn btn-success login-btn" type="submit">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>

        </section>
   
@endsection