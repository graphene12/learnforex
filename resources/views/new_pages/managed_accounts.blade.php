@extends('new_includes.master')

@section('content')
<style>
.content-center {
    position: absolute;
    top: 50%;
    left: 50%;
    -ms-transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
    color: #FFFFFF;
    width: 100%;
    max-width: 880px;
}
.disable-a{
  font-family: "Montserrat", sans-serif;
}
.form-element-label{
  font-family: "Open Sans", sans-serif;
}

</style>
      <!--==========================
    Intro Section
  ============================-->
  <section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">


        <div class="carousel-inner" role="listbox">


          <div class="carousel-item active">
            <div class="carousel-background" style="opacity:inherit;"><img src="{{ URL('/') }}/assets/img/banner2.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <div class="content-center">
                    <h5 class="title banner-title" style="font-weight:900;font-size:30px;">Don't have time to the Forex Markets? <br> Do you lack consistent profitable strategies? <br> Do you lack discipline and always allow your emotions take control?</h5>
                    <hr style="border:2px solid white;">
                    <div class="text-center">
                        <p  style="color: #cccccc;font-weight:500; font-size: 20px; letter-spacing: normal;">If you answered "Yes" to any of these questions, our managed account option is the best solution for you.</p>
                        <br>
                        
                    </div>

                    <center><a class="btn btn-info btn-lg" href="#services">Get More Details</a></center>
                </div>
            
              </div>
            </div>
          </div>

        </div>


      </div>
    </div>
  </section><!-- #intro -->



    <!--==========================
      Services Section
    ============================-->
    <section id="services" style="background:#402b3f;">
      <div class="container">

        <header class="section-header wow fadeInUp">
          <h3>Why Managed Account is your best Solution</h3>
        </header>

        <div class="row">

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-disc"></i></div>
            <h4 class="title"><a disable >No transfer of funds. Stay in total control of your capital.</a></h4>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-disc"></i></div>
            <h4 class="title"><a disable>Watch trading activities directly from your MT4.</a></h4>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-disc"></i></div>
            <h4 class="title"><a disable>3% Max. risk per trade. </a></h4>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-disc"></i></div>
            <h4 class="title"><a disable>5% - 15% Monthly Returns on Investment.</a></h4>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-disc"></i></div>
            <h4 class="title"><a disable>100% account protection from news announcements.</a></h4>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-disc"></i></div>
            <h4 class="title"><a disable>Profit will be shared in the ratio of 60% (Investor) to 40% (LFL Signal)</a></h4>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-disc"></i></div>
            <h4 class="title"><a disable>Never lose your account.</a></h4>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-disc"></i></div>
            <h4 class="title"><a disable>Overall peace of mind.</a></h4>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-disc"></i></div>
            <h4 class="title"><a disable>Cancel at any time!</a></h4>
          </div>
          <div class="col-md-12 offset-md-5">
            <a href="#managed-account-form" class="btn btn-warning btn-lg">INVEST NOW</a>
          </div>
        </div>
        
      </div>
    </section><!-- #services -->

    <section id="managed-account-form" class="section-bg wow fadeInUp" style="padding-top:30px;padding-bottom:30px;">
        <div class="container">
  
          <div class="section-header">
            <h3 style="color:black;">Investor's Form</h3>
            <p>Please take a few minutes to fill our investor's form and have us manage your account to success.</p>
          </div>

  
          <div class="form col-lg-6 offset-lg-3">
            <form action="{{ URL('auth/managed-account') }}" method="POST" role="form" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="form-element-label">Full Name</label>
                            <input type="name" id="name" name="name" value="{{ old('name') }}" class="form-control" required="required" >
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="broker_name" class="form-element-label">Broker's Name</label>
                            <input type="text" id="broker_name" name="broker_name" value="{{ old('broker_name') }}" class="form-control" required="required" >
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group" >
                            <label for="mt4_account_number" class="form-element-label">MT4 Account Number</label>
                            <input type="number" id="mt4_account_number" name="mt4_account_number" value="{{ old('mt4_account_number') }}" class="form-control" required="required" >
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="trading_account_balance" class="form-element-label">What is your Trading Account Equity?  (Minimum of $5000)</label>
                            <input type="number" id="trading_account_balance" name="trading_account_balance" value="{{ old('trading_account_balance') }}" min="5000" class="form-control" required="required" >
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="mt4_password" class="form-element-label">MT4 Password</label>
                            <input type="password" id="mt4_password" name="mt4_password" class="form-control" required="required">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email" class="form-element-label">Email Address (we shall communicate with you via this channel)</label>
                            <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" required="required" >
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="mt4_password_confirm" class="form-element-label">Verify MT4 Password</label>
                            <input type="password" id="mt4_password_confirm" name="mt4_password_confirm" class="form-control" required="required" >
                        </div>
                    </div>

                    
                    <div class="col-md-12 form-row">
                        <div class="col-md-2">
                            <input id="i_agree" type="checkbox" class="form-control" onclick="displaySubmitBtn()">
                            
                        </div>
                        <div class="col-md-10">
                            <label for="i_agree" style="text-align:justify;font-weight:200;text-transform:initial">
                               I hereby take responsibility and authorize learnforexlivesignals to apply its trading strategies and money management principles in trading foreign currencies on my account.
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <button class="btn btn-primary login-btn float-right" onclick="switchForms()" id="submit_managed_account_form" style="display: none;" type="submit">Submit</button>
                    </div>
                </div>
            </form>
          </div>
  
        </div>
      </section><!-- #contact -->
      <div class="modal fade" id="contactUsSubscription" tabindex="-1" role="dialog" aria-labelledby="contactUsSubscriptionLabel" aria-hidden="true">
        <div class="modal-dialog">
            <a href="javascript(void);" class="close-window" data-dismiss="modal" aria-hidden="true">close</a>
            <form action="{{ URL('order/subscription-save') }}" method="POST">
                <div class="modal-content">
                    <div class="modal-body">
                        <center><h5>Please fill this form to get started.</h5></center>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="form-element-label">Your Name:</label>
                                    <input type="name" id="name" name="name" value="{{ old('name') }}" class="form-control" required="required" placeholder="Fullname">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email" class="form-element-label">Your E-mail Address:</label>
                                    <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" required="required" placeholder="Email">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="equity" class="form-element-label">Equity Account:</label>
                                    <input type="number" id="equity" name="equity" value="{{ old('equity') }}" class="form-control" required="required" placeholder="Equity Account">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-blue btn-block" type="submit">continue</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </main>

@endsection