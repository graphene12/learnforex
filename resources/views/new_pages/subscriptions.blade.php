@extends('new_includes.master')
@section('content')
<div id="contact">
	        
    <div class="section">
        <div class="container">
            <div  style="color:green;text-align:center;font-weight:bold;text-transform:uppercase;margin-bottom:10px;display:block;"><i>Please proceed to pay the commitment fee with the link below</i>
                </div>
                
                <div  style="color:black;text-align:center;font-weight:bold;text-transform:uppercase;margin:5px;font-size:13px;">(Once your Payment is recieved, we will contact you as soon as possible)
                </div>
    
        <div class="pricing-title" style="margin-top:30px;">
                <h5>Commitment Fee</h5>
                <p class="testimonies-small-title">This is a one-time payment and will be recovered before the share of profit kicks in. <br>No further monthly subscription fee(s) will be required.</p>
                <br>
        </div>
        
        <div class="row">
            <div class="col-md-3">
            </div>
      
            <div class="col-md-6" id="price">
                  <div class="plan ultimite">
                    <div class="plan-inner">
                          <div class="">

                            <div class="entry-title">
                                <h3>One time fee</h3>
                                <div class="price">
                                    $250
                                </div>
                                  
                            </div>
                          </div>


                          <div class="">
                                    
                                    <a href="https://paystack.com/pay/60kn5mx4wi" class="btn btn-lg btn-success"  >Pay</a>
                                
                          </div>
                    </div>
                  </div>
            </div>
</div>
        

       
        </div>
        
</div>

@endsection