@extends("includes.master")
@section("contents")
	@if(isset($setNewPassword))
		<div class="section auth-section">
		<div class="container">		
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<div class="auth-div">
						<div class="form-header"><h4>{{ $title }}</h4></div>
						<form action="{{ URL('auth/forgot-password/save/'.Crypt::encrypt($user->id)) }}" autocomplete="off" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label for="email" class="form-element-label">Your E-mail Address:</label>
								<input type="email" id="email" name="email" disabled="disabled" value="{{ $user->email }}" class="form-control" placeholder="Email">
							</div>

							<div class="form-group">
								<label for="password" class="form-element-label">New Password:</label>
								<input type="password" id="password" name="password" class="form-control" required="required" placeholder="New Password">
							</div>

							<div class="form-group">
								<label for="password" class="form-element-label">Confirm New Password:</label>
								<input type="password" id="password" name="password_confirm" class="form-control" required="required" placeholder="Confirm New Password">
							</div>

							<div class="float-right">
								<button class="btn btn-success login-btn" type="submit">Save new Password <i class="fa fa-arrow-right"></i></button>
							</div>
						</form>
					</div>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
	</div>
	@else
		<div class="section auth-section">
			<div class="container">		
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="auth-div">
							<div class="form-header"><h4>Recover lost Password</h4></div>
							<form action="{{ URL('auth/forgot-password') }}" autocomplete="off" method="POST">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="form-group">
									<label for="email" class="form-element-label">Your E-mail Address:</label>
									<input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" required="required" placeholder="Email">
								</div>

								<div class="float-left">
									<a href="{{ URL('/auth/login') }}">Login Here</a>
									<br>
									<a href="{{ URL('/auth/register') }}">Create account</a>
								</div>

								<div class="float-right">
									<button class="btn btn-success login-btn" type="submit">Recover</button>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
		</div>
	@endif
@stop