@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
        	<div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
                	<div class="x_panel">
                  		<div class="x_title">
                    		<h2>Upload Signal</h2>
                    		<div class="clearfix"></div>
                  		</div>
                  		<div class="x_content">
                    		<br>
                    		<form action="{{ URL('administrator/signals/add-new') }}" method="POST" id="demo-form2" data-parsley-validate="" class="form-horizontal" novalidate="" enctype="multipart/form-data">
                    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
                      			<div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">Currency Pair</label>
                              <input type="text" id="currency_pair" name="currency_pair" required="required" class="form-control" value="{{ old('currency_pair') }}">
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Setup Type</label>
                      				<input type="text" id="setup_type" name="setup_type" required="required" class="form-control" value="{{ old('setup_type') }}">
                      			</div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Entry</label>
                      				<input type="text" id="entry" name="entry" required="required" class="form-control" value="{{ old('entry') }}" />
                      			</div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label for="middle-name" class="control-label">Stop Loss</label>
                      				<input id="stop_loss" class="form-control" value="{{ old('stop_loss') }}" type="text" name="stop_loss" required="required" />
                      			</div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">Target 1</label>
                              <input id="target_1" class="form-control" value="{{ old('target_1') }}" type="text" name="target_1" required="required" />
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">Target 2</label>
                              <input id="target_2" class="form-control" value="{{ old('target_2') }}" type="text" name="target_2" required="required" />
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">Target 3</label>
                              <input id="target_3" class="form-control" value="{{ old('target_3') }}" type="text" name="target_3" required="required" />
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">Target 4</label>
                              <input id="target_4" class="form-control" value="{{ old('target_4') }}" type="text" name="target_4" required="required" />
                            </div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Any High Impact News Ahead?:</label>
                      				<input id="high_impact_news_ahead" class="form-control" value="{{ old('high_impact_news_ahead') }}" type="text" name="high_impact_news_ahead" required="rewuired" />
                      			</div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Quality:</label>
                      				<input id="quality" class="form-control" value="{{ old('quality') }}" type="text" name="quality" required="required" />
                      			</div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Duration:</label>
                      				<input id="duration" class="form-control" value="{{ old('duration') }}" type="text" name="duration" required="required" />
                      			</div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Risk:</label>
                      				<input id="risk" class="form-control" value="{{ old('risk') }}" type="text" name="risk" required="required" />
                      			</div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">Update:</label>
                              <input id="update" class="form-control" value="{{ old('update') }}" type="text" name="update" />
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                              <label class="control-label">Post As:</label>
                              <select name="admin_id" id="admin_id" class="form-control">
                                @foreach($admins as $admin)
                                  <option value="{{ $admin->id }}">{{ $admin->name }}</option>
                                @endforeach
                              </select>
                            </div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Signal Image:</label>
                      				<input id="image" class="form-control" type="file" name="image" required="required" />
                      			</div>
                      			<div class="clearfix"></div>
                      			<div class="form-group">
                        			<div class="pull-right">
                          				<a href="{{ URL('administrator/signals') }}" class="btn btn-primary">Cancel</a>
                          				<button type="submit" class="btn btn-success">Upload</button>
                        			</div>
                      			</div>
                    		</form>
                  		</div>
                	</div>
              	</div>
            </div>
        </div>
    </div>
@stop