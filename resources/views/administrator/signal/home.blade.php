@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
            <div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
				<!-- 
              	<div class="title_right">
                	<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  		<div class="input-group">
                    		<input type="text" class="form-control" placeholder="Search for...">
                    		<span class="input-group-btn">
                      			<button class="btn btn-default" type="button">Go!</button>
                    		</span>
                  		</div>
                	</div>
              	</div> -->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  	<div class="x_title">
	                    	<h2>Trade Signals</h2>
	                    	<ul class="nav navbar-right panel_toolbox">
                      			<li><a href="{{ URL('administrator/signals/add-new') }}" class="btn btn-sm btn-success">Upload Trade Signal</a></li>
                    		</ul>
	                    	<div class="clearfix"></div>
	                  	</div>
						@if(count($signals) > 0)
		                	<div class="x_content">
		                    	<div class="table-responsive">
		                      		<table class="table table-striped jambo_table bulk_action">
		                        		<thead>
				                          	<tr class="headings">
					                            <th class="column-title">Currency Pair </th>
					                            <th class="column-title">Setup Type </th>
					                            <th class="column-title">Entry </th>
					                            <th class="column-title">Stop Loss </th>
					                            <th class="column-title">Quality </th>
					                            <th class="column-title">Duration </th>
					                            <th class="column-title">Risk </th>
					                            <th class="column-title no-link last"><span class="nobr">Action</span></th>
				                          	</tr>
		                        		</thead>

		                        		<tbody>
		                        			@foreach($signals as $signal)
			                          			<tr class="even pointer">
			                            			<td class=" ">{{ $signal->currency_pair }}</td>
			                            			<td class=" ">{{ $signal->setup_type }}</td>
			                            			<td class=" ">{{ $signal->entry }} </td>
			                            			<td class=" ">{{ $signal->stop_loss }}</td>
			                            			<td class=" ">{{ $signal->quality }}</td>
			                            			<td class=" ">{{ $signal->duration }}</td>
			                            			<td class="a-right a-right ">{{ $signal->risk }}</td>
			                            			<td class=" last">
			                            				<a href="{{ URL('/administrator/signals/'.Crypt::encrypt($signal->id)) }}">View</a>
			                            				&nbsp;&nbsp;
			                            				<a href="{{ URL('/administrator/signals/'.Crypt::encrypt($signal->id).'?edit=true') }}">Edit</a>
														&nbsp;&nbsp;
			                            				<a href="#" data-toggle="modal" data-target="#deleteSignalModal{{ $signal->id }}">Delete</a>
			                            			</td>
			                          			</tr>

			                          			<div class="modal fade" id="deleteSignalModal{{ $signal->id }}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
											      	<div class="modal-dialog modal-sm">
											        	<div class="modal-content">
															<div class="modal-header">
											              		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
											              		<center><h4 class="modal-title" id="myModalLabel2">Are you sure?</h4></center>
											            	</div>
											          		<div class="modal-body">
											              		<center>
											                		<p>This action cannot be reversed.</p>
											                		<a href="{{ URL('/administrator/signals/delete?signal='.$signal->id) }}" class="btn btn-danger">Yes</a>
											                		<button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
											              		</center>
											          		</div>
											        	</div>
											      	</div>
											  	</div>
		                        			@endforeach
		                        		</tbody>
		                      		</table>
		                      		{!! $signals->render() !!}
		                    	</div>
							</div>
						@else
							<div class="x_content"></div>
						@endif
	                </div>
              	</div>
            </div>
        </div>
    </div>
    <?php Session::forget('admin'); ?>
@stop