@extends("includes.administrator.master")
@section("contents")
  <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $title }}</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                        <div class="profile_img">
                          <div id="crop-avatar">
                              <img class="img-responsive avatar-view" src="{{ URL::to('/') }}/core/public/assets/uploads/trade_signals/{{ $signal->image }}" alt="Avatar">
                          </div>
                        </div>
                        <br>
                        <ul class="list-unstyled user_data">
                          <li><i class="fa fa-clock-o user-profile-icon"></i> Posted: {{ Carbon\Carbon::createFromTimeStamp(strtotime($signal->created_at))->diffForHumans() }}</li>
                          <li>
                            <i class="fa fa-thumbs-up user-profile-icon"></i> Likes: {{ count($signal->likes) }}
                            <i class="fa fa-thumbs-down user-profile-icon"></i> Dis-likes: {{ count($signal->dislikes) }}
                          </li>
                        </ul>

                        <a class="btn btn-primary" href="{{ URL('/administrator/signals/'.Crypt::encrypt($signal->id).'?edit=true') }}"><i class="fa fa-edit m-right-xs"></i> Edit</a>
                        <a class="btn btn-danger" data-toggle="modal" data-target="#deleteSignalModal"><i class="fa fa-ban m-right-xs"></i> Delete Signal</a>
                    </div>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div class="profile_title">
                          <div class="col-md-6">
                              <h2>Signal Info</h2>
                          </div>
                        </div>
                        <!-- start of user-activity-graph -->
                      <table class="table table-striped">
                        <tr>
                          <td>Currency Pair</td>
                          <td>{{ $signal->currency_pair }}</td>
                        </tr>
                        <tr>
                          <td>Setup Type</td>
                          <td>{{ $signal->setup_type }}</td>
                        </tr>
                        <tr>
                          <td>Entry</td>
                          <td>{{ $signal->entry }}</td>
                        </tr>
                        <tr>
                          <td>Stop Loss</td>
                          <td>{{ $signal->stop_loss }}</td>
                        </tr>
                        <tr>
                          <td>Target 1</td>
                          <td>{{ $signal->target_1 }}</td>
                        </tr>
                        <tr>
                          <td>Target 2</td>
                          <td>{{ $signal->target_2 }}</td>
                        </tr>
                        <tr>
                          <td>Target 3</td>
                          <td>{{ $signal->target_3 }}</td>
                        </tr>
                        <tr>
                          <td>Target 4</td>
                          <td>{{ $signal->target_4 }}</td>
                        </tr>
                        <tr>
                          <td>High Impact News Ahead</td>
                          <td>{{ $signal->high_impact_news_ahead }}</td>
                        </tr>
                        <tr>
                          <td>Quality</td>
                          <td>{{ $signal->quality }}</td>
                        </tr>
                        <tr>
                          <td>Duration</td>
                          <td>{{ $signal->duration }}</td>
                        </tr>
                        <tr>
                          <td>Risk</td>
                          <td>{{ $signal->risk }}</td>
                        </tr>
                        <tr>
                          <td>Update</td>
                          <td>{{ $signal->update }}</td>
                        </tr>
                      </table>
                        <!-- end of user-activity-graph -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteSignalModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              <center><h4 class="modal-title" id="myModalLabel2">Are you sure?</h4></center>
            </div>
          <div class="modal-body">
              <center>
                <p>This action cannot be reversed.</p>
                <a href="{{ URL('/administrator/signals/delete?signal='.$signal->id) }}" class="btn btn-danger">Yes</a>
                <button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
              </center>
          </div>
        </div>
      </div>
  </div>
    <?php Session::forget('admin'); ?>
@stop