@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
        	<div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
                	<div class="x_panel">
                  		<div class="x_title">
                    		<h2>Create Live Video</h2>
                    		<div class="clearfix"></div>
                  		</div>
                  		<div class="x_content">
                    		<br>
                    		<form action="{{ URL('administrator/users/add-new') }}" method="POST" id="demo-form2" data-parsley-validate="" class="form-horizontal" novalidate="" enctype="multipart/form-data">
                    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Video Title</label>
                      				<input type="text" id="name" name="name" required="required" class="form-control" value="{{ old('title') }}">
                      			</div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Video URL</label>
                      				<input type="email" id="email" name="email" required="required" class="form-control" value="{{ old('email') }}" />
                      			</div>
                      			
                                                        
                      			<div class="clearfix"></div>
                      			<div class="form-group">
                        			<div class="pull-right">
                          				<button type="submit" class="btn btn-success">Create Video</button>
                        			</div>
                      			</div>
                    		</form>
                  		</div>
                	</div>
              	</div>
            </div>
        </div>
    </div>
@stop