@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
            <div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
				
              	<!-- <div class="title_right">
              	                	<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
              	                  		<div class="input-group">
              	                    		<input type="text" class="form-control" placeholder="Search for...">
              	                    		<span class="input-group-btn">
              	                      			<button class="btn btn-default" type="button">Go!</button>
              	                    		</span>
              	                  		</div>
              	                	</div>
              	</div> -->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  	<div class="x_title">
	                    	<h2>Orders</h2>
	                    	<div class="clearfix"></div>
	                  	</div>
						@if(count($lot_orders) > 0)
		                	<div class="x_content">
		                    	<div class="table-responsive">
		                      		<table class="table table-striped jambo_table bulk_action">
		                        		<thead>
				                          	<tr class="headings">
					                            <th class="column-title">Fullname </th>
					                            <th class="column-title">Email </th>
					                            <th class="column-title">Completed </th>
					                            <th class="column-title">Actions </th>
				                          	</tr>
		                        		</thead>

		                        		<tbody>
		                        			@foreach($lot_orders as $order)
			                          			<tr class="even pointer">
			                            			<td class=" ">{{ $order->name }}</td>
			                            			<td class=" ">{{ $order->email }} </td>
			                            			<td class=" ">
			                            				@if($order->completed == "no")
															<span class='label label-danger'>no</span>
			                            				@else
															<span class='label label-success'>yes</span>
			                            				@endif
			                            			</td>
			                            			<td class=" last">
			                            				<a href="#" data-toggle="modal" data-target="#deleteUserModal{{ $order->id }}">Complete Order</a>
			                            			</td>
			                          			</tr>

			                          			<div class="modal fade" id="deleteUserModal{{ $order->id }}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
											  		<div class="modal-dialog modal-sm">
											    		<div class="modal-content">

											      			<div class="modal-header">
											        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
											        			<center><h4 class="modal-title" id="myModalLabel2">Are you sure?</h4></center>
											      			</div>
											    			<div class="modal-body">
											      				<center>
											      					<p>This order is completed?.</p>
											      					<a href="{{ URL('/administrator/lot-size-orders/'.Crypt::encrypt($order->id).'/completed') }}" class="btn btn-danger">Yes</a>
											      					<button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
											      				</center>
											    			</div>
											    		</div>
											  		</div>
												</div>
		                        			@endforeach
		                        		</tbody>
		                      		</table>
		                    	</div>
							</div>
						@else
							<div class="x_content"></div>
						@endif
	                </div>
              	</div>
            </div>
        </div>
    </div>
    <?php Session::forget('admin'); ?>
@stop