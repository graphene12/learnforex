@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
            <div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  	<div class="x_title">
	                    	<h2>Investors</h2>
	                    	<div class="clearfix"></div>
	                  	</div>
						@if(count($investors) > 0)
		                	<div class="x_content">
		                    	<div class="table-responsive">
		                      		<table class="table table-striped jambo_table bulk_action">
		                        		<thead>
				                          	<tr class="headings">
					                            <th class="column-title">Fullname </th>
					                            <th class="column-title">Email </th>
					                            <th class="column-title">Broker Name </th>
					                            <th class="column-title">Trading Account Balance </th>
					                            <th class="column-title">Sbuscription </th>
					                            <th class="column-title">Actions </th>
				                          	</tr>
		                        		</thead>

		                        		<tbody>
		                        			@foreach($investors as $investor)
			                          			<tr class="even pointer">
			                            			<td class=" ">{{ $investor->name }}</td>
			                            			<td class=" ">{{ $investor->email }}</td>
			                            			<td class=" ">{{ $investor->broker_name }}</td>
			                            			<td class=" ">${{ $investor->trading_account_balance }}</td>
			                            			<td class=" ">{{ $investor->subplan?ucwords($investor->subplan->title).'&nbsp;-&nbsp;$'.$investor->subplan->price:"None selected" }}</td>
			                            			<td class=" last">
			                            				<a href="{{ URL('/administrator/managed-accounts/'.Crypt::encrypt($investor->id).'?edit=true') }}"> Edit</a>
			                            				&nbsp;&nbsp;&nbsp;
			                            				<a href="{{ URL('/administrator/managed-accounts/'.Crypt::encrypt($investor->id)) }}">View</a>
														&nbsp;&nbsp;&nbsp;
			                            				<a href="#" data-toggle="modal" data-target="#deleteInvestorModal{{ $investor->id }}">Delete</a>
			                            			</td>
			                          			</tr>

			                          			<div class="modal fade" id="deleteInvestorModal{{ $investor->id }}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
											  		<div class="modal-dialog modal-sm">
											    		<div class="modal-content">

											      			<div class="modal-header">
											        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
											        			<center><h4 class="modal-title" id="myModalLabel2">Are you sure?</h4></center>
											      			</div>
											    			<div class="modal-body">
											      				<center>
											      					<p>This action cannot be reversed.</p>
											      					<a href="{{ URL('/administrator/managed-accounts/delete?investor='.$investor->id) }}" class="btn btn-danger">Yes</a>
											      					<button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
											      				</center>
											    			</div>
											    		</div>
											  		</div>
												</div>
		                        			@endforeach
		                        		</tbody>
		                      		</table>
		                    	</div>
							</div>
						@else
							<div class="x_content"></div>
						@endif
	                </div>
              	</div>
            </div>
        </div>
    </div>
    <?php Session::forget('admin'); ?>
@stop