@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
            <div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
				<!-- 
              	<div class="title_right">
                	<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  		<div class="input-group">
                    		<input type="text" class="form-control" placeholder="Search for...">
                    		<span class="input-group-btn">
                      			<button class="btn btn-default" type="button">Go!</button>
                    		</span>
                  		</div>
                	</div>
              	</div> -->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  	<div class="x_title" style="border-bottom:none !important;">
	                    	<h2>Live Trading Room Subscribers</h2>
	                  	</div>
						@if(count($requests) > 0)
		                	<div class="x_content">
		                    	<div class="table-responsive">
		                      		<table class="table table-striped jambo_table bulk_action">
		                        		<thead>
				                          	<tr class="headings">
					                            <th class="column-title">Fullname </th>
					                            <th class="column-title">Email </th>
					                            <th class="column-title">Phone </th>
					                            <th class="column-title" width="30%">Actions </th>
				                          	</tr>
		                        		</thead>

		                        		<tbody>
		                        			@foreach($requests as $request)
			                          			<tr class="even pointer">
			                            			<td class=" ">{{ $request->user->name }}</td>
			                            			<td class=" ">{{ $request->user->email }} </td>
			                            			<td class=" ">{{ $request->user->phone }}</td>
			                            			<td class=" last">
			                            				<a style="cursor: pointer;" href="mailto:{{ $request->user->email }}">Contact User</a>
			                            				&nbsp;&nbsp;&nbsp;
			                            				<a style="cursor: pointer;" href="{{ URL('/administrator/users/'.Crypt::encrypt($request->user->id)) }}">View Profile</a>
			                            				&nbsp;&nbsp;&nbsp;
			                            				<a style="cursor: pointer;" href="#" data-toggle="modal" data-target="#setPasswordModal{{ $request->user->id }}">Set Password</a>
			                            			</td>
			                          			</tr>

			                          			<div class="modal fade" id="setPasswordModal{{ $request->user->id }}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
											  		<div class="modal-dialog modal-sm">
											  			<form action="{{ URL('administrator/password-requests/'.Crypt::encrypt($request->user->id)) }}" method="POST">
											  				<input type="hidden" name="_token" value="{{ csrf_token() }}">
												    		<div class="modal-content">
												      			<div class="modal-header">
												        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
												        			<center><h4 class="modal-title" id="myModalLabel2">Set Password</h4></center>
												      			</div>
												    			<div class="modal-body">
												      				<center>
												      					<p>Reset password for {{ ucwords($request->user->name) }}.</p>
												      				</center>
												      				<div class="form-group">
												      					<label for="password">Email:</label>
												      					<input type="email" disabled="disabled" value="{{ $request->user->email }}" class="form-control">
												      				</div>

												      				<div class="form-group">
												      					<label for="password">Choose new password:</label>
												      					<input type="password" placeholder="Choose new password" name="password" class="form-control" id="password" required="required">
												      				</div>

												      				<div class="form-group">
												      					<label for="password_confirm">Confirm new password:</label>
												      					<input type="password" name="password_confirm" placeholder="Confirm new password" class="form-control" id="password_confirm" required="required">
												      				</div>
												    			</div>
												    			<div class="modal-footer">
												      				<button type="submit" class="btn btn-default" data-dismiss="modal">Cancel</button>
												    				<button type="submit" class="btn btn-success">Set Password</button>
												    			</div>
												    		</div>
												    	</form>
											  		</div>
												</div>
		                        			@endforeach
		                        		</tbody>
		                      		</table>
		                    	</div>
							</div>
						@else
							<div class="x_content"></div>
						@endif
	                </div>
              	</div>
            </div>
        </div>
    </div>
    <?php Session::forget('admin'); ?>
@stop