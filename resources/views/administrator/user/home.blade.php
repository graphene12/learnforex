@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
            <div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
				
              	<div class="title_right">
                	<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                		<form action="{{ URL('administrator/users/search') }}" method="GET">
	                  		<div class="input-group">
	                    		<input type="text" name="term" class="form-control" placeholder="Search users...">
	                    		<span class="input-group-btn">
	                      			<button class="btn btn-default" type="submit">Go!</button>
	                    		</span>
	                  		</div>
                		</form>
                	</div>
              	</div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  	<div class="x_title">
	                    	<h2>Live Trading Room Subscribers</h2>
	                    	<ul class="nav navbar-right panel_toolbox">
                      			<li><a href="{{ URL('administrator/users/add-new') }}" class="btn btn-sm btn-success">Create User</a></li>
                    		</ul>
	                    	<div class="clearfix"></div>
	                  	</div>
						@if(count($users) > 0)
		                	<div class="x_content">
		                    	<div class="table-responsive">
                    				<div class="" role="tabpanel" data-example-id="togglable-tabs">
				                      	<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				                        	<li role="presentation" class="active"><a href="#approved" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Approved Subscribers</a></li>
				                        	<li role="presentation" class=""><a href="#unapproved" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Un-approved Subscribers</a></li>
				                        	<li role="presentation" class=""><a href="#non" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Non Subscribers</a></li>
				                      	</ul>

				                      	<div id="myTabContent" class="tab-content">
				                      		<div role="tabpanel" class="tab-pane fade active in" id="approved" aria-labelledby="home-tab">
					                      		<table class="table table-striped jambo_table bulk_action">
					                        		<thead>
							                          	<tr class="headings">
								                            <th class="column-title">Fullname </th>
								                            <th class="column-title">Email </th>
								                            <th class="column-title">Signal Option </th>
								                            <th class="column-title">Phone </th>
								                            <th class="column-title">Actions </th>
							                          	</tr>
					                        		</thead>

					                        		<tbody>
					                        			@foreach($users as $user)
					                        				@if($user->subscription !== NULL)
                              									@if($user->subscription->active != 0)
								                          			<tr class="even pointer">
								                            			<td class=" ">{{ $user->name }}</td>
								                            			<td class=" ">{{ $user->email }} </td>
								                            			<td class=" ">{{ $user->signal_option }}</td>
								                            			<td class=" ">{{ $user->phone }}</td>
								                            			<td class=" last">
								                            				<a href="{{ URL('/administrator/users/'.Crypt::encrypt($user->id)) }}">View</a>
								                            				&nbsp;&nbsp;&nbsp;&nbsp;
								                            				<a href="#" data-toggle="modal" data-target="#deleteUserModal{{ $user->id }}">Delete Account</a>
								                            			</td>
								                          			</tr>

								                          			<div class="modal fade" id="deleteUserModal{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
																  		<div class="modal-dialog modal-sm">
																    		<div class="modal-content">

																      			<div class="modal-header">
																        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
																        			<center><h4 class="modal-title" id="myModalLabel2">Are you sure?</h4></center>
																      			</div>
																    			<div class="modal-body">
																      				<center>
																      					<p>This action cannot be reversed.</p>
																      					<a href="{{ URL('/administrator/users/delete?user='.$user->id) }}" class="btn btn-danger">Yes</a>
																      					<button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
																      				</center>
																    			</div>
																    		</div>
																  		</div>
																	</div>
																@endif
															@endif
					                        			@endforeach
					                        		</tbody>
					                      		</table>
					                      	</div>

					                      	<div role="tabpanel" class="tab-pane fade in" id="unapproved" aria-labelledby="home-tab">
					                      		<table class="table table-striped jambo_table bulk_action">
					                        		<thead>
							                          	<tr class="headings">
								                            <th class="column-title">Fullname </th>
								                            <th class="column-title">Email </th>
								                            <th class="column-title">Signal Option </th>
								                            <th class="column-title">Phone </th>
								                            <th class="column-title">Actions </th>
							                          	</tr>
					                        		</thead>

					                        		<tbody>
					                        			@foreach($users as $user)
					                        				@if($user->subscription !== NULL)
					                        					@if($user->subscription->active == 0)
								                          			<tr class="even pointer">
								                            			<td class=" ">{{ $user->name }}</td>
								                            			<td class=" ">{{ $user->email }} </td>
								                            			<td class=" ">{{ $user->signal_option }}</td>
								                            			<td class=" ">{{ $user->phone }}</td>
								                            			<td class=" last">
								                            				<a href="{{ URL('/administrator/users/'.Crypt::encrypt($user->id)) }}">View</a>
								                            				&nbsp;&nbsp;&nbsp;&nbsp;
								                            				<a href="#" data-toggle="modal" data-target="#deleteUserModal{{ $user->id }}">Delete Account</a>
								                            			</td>
								                          			</tr>

								                          			<div class="modal fade" id="deleteUserModal{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
																  		<div class="modal-dialog modal-sm">
																    		<div class="modal-content">

																      			<div class="modal-header">
																        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
																        			<center><h4 class="modal-title" id="myModalLabel2">Are you sure?</h4></center>
																      			</div>
																    			<div class="modal-body">
																      				<center>
																      					<p>This action cannot be reversed.</p>
																      					<a href="{{ URL('/administrator/users/delete?user='.$user->id) }}" class="btn btn-danger">Yes</a>
																      					<button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
																      				</center>
																    			</div>
																    		</div>
																  		</div>
																	</div>
																@endif
															@endif
					                        			@endforeach
					                        		</tbody>
					                      		</table>
					                      	</div>

					                      	<div role="tabpanel" class="tab-pane fade in" id="non" aria-labelledby="home-tab">
					                      		<table class="table table-striped jambo_table bulk_action">
					                        		<thead>
							                          	<tr class="headings">
								                            <th class="column-title">Fullname </th>
								                            <th class="column-title">Email </th>
								                            <th class="column-title">Signal Option </th>
								                            <th class="column-title">Phone </th>
								                            <th class="column-title">Actions </th>
							                          	</tr>
					                        		</thead>

					                        		<tbody>
					                        			@foreach($users as $user)
					                        				@if($user->subscription == NULL)
							                          			<tr class="even pointer">
							                            			<td class=" ">{{ $user->name }}</td>
							                            			<td class=" ">{{ $user->email }} </td>
							                            			<td class=" ">{{ $user->signal_option }}</td>
							                            			<td class=" ">{{ $user->phone }}</td>
							                            			<td class=" last">
							                            				<a href="{{ URL('/administrator/users/'.Crypt::encrypt($user->id)) }}">View</a>
							                            				&nbsp;&nbsp;&nbsp;&nbsp;
							                            				<a href="#" data-toggle="modal" data-target="#deleteUserModal{{ $user->id }}">Delete Account</a>
							                            			</td>
							                          			</tr>

							                          			<div class="modal fade" id="deleteUserModal{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
															  		<div class="modal-dialog modal-sm">
															    		<div class="modal-content">

															      			<div class="modal-header">
															        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
															        			<center><h4 class="modal-title" id="myModalLabel2">Are you sure?</h4></center>
															      			</div>
															    			<div class="modal-body">
															      				<center>
															      					<p>This action cannot be reversed.</p>
															      					<a href="{{ URL('/administrator/users/delete?user='.$user->id) }}" class="btn btn-danger">Yes</a>
															      					<button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
															      				</center>
															    			</div>
															    		</div>
															  		</div>
																</div>
															@endif
					                        			@endforeach
					                        		</tbody>
					                      		</table>
					                      	</div>
				                      	</div>
			                      	</div>
		                    	</div>
							</div>
						@else
							<div class="x_content"></div>
						@endif
	                </div>
              	</div>
            </div>
        </div>
    </div>
    <?php Session::forget('admin'); ?>
@stop