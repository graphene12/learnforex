@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
        	<div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
                	<div class="x_panel">
                  		<div class="x_title">
                    		<h2>Create User</h2>
                    		<div class="clearfix"></div>
                  		</div>
                  		<div class="x_content">
                    		<br>
                    		<form action="{{ URL('administrator/users/add-new') }}" method="POST" id="demo-form2" data-parsley-validate="" class="form-horizontal" novalidate="" enctype="multipart/form-data">
                    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Fullname</label>
                      				<input type="text" id="name" name="name" required="required" class="form-control" value="{{ old('name') }}">
                      			</div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Email</label>
                      				<input type="email" id="email" name="email" required="required" class="form-control" value="{{ old('email') }}" />
                      			</div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label for="password" class="control-label">Password</label>
                      				<input id="password" class="form-control" value="{{ old('password') }}" type="password" name="password" required="required" />
                      			</div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">Confirm Password</label>
                              <input id="password_confirm" class="form-control" value="{{ old('password_confirm') }}" type="password" name="password_confirm" required="required" />
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">Phone Number</label>
                              <input id="phone" class="form-control" value="{{ old('phone') }}" type="text" name="phone" required="required" />
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">User Type</label>
                              <select name="user_type" id="user_type" class="form-control" required="required">
                                <option value=""></option>
                                <option value="1">Paid User</option>
                                <option value="2">Free User (no payment required)</option>
                              </select>
                            </div>
                            
                      			<div class="clearfix"></div>
                      			<div class="form-group">
                        			<div class="pull-right">
                          				<a href="{{ URL('administrator/users') }}" class="btn btn-primary">Cancel</a>
                          				<button type="submit" class="btn btn-success">Create User</button>
                        			</div>
                      			</div>
                    		</form>
                  		</div>
                	</div>
              	</div>
            </div>
        </div>
    </div>
@stop