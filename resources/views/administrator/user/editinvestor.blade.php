@extends("includes.administrator.master")
@section("contents")
	<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $title }}</h2>
                    <div class="clearfix"></div>
                </div>
                <form action="{{ URL('/administrator/managed-accounts/'.Crypt::encrypt($investor->id).'/edit') }}" method="POST">
                  <input type="hidden" value="{{ csrf_token() }}" name="_token">
                  <div class="x_content">
                      <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                        	<div class="profile_img">
                          	<div id="crop-avatar">
                            		<!-- Current avatar -->
                            		<img class="img-responsive avatar-view" src="{{ URL::to('/') }}/assets/img/default-avatar.png" alt="Avatar" width="150px">
                          	</div>
                        	</div>
                        	<h3>{{ $investor->name }}</h3>

                        	<ul class="list-unstyled user_data">
                          	<li><i class="fa fa-clock-o user-profile-icon"></i> {{ Carbon\Carbon::createFromTimeStamp(strtotime($investor->created_at))->diffForHumans() }}</li>
                        	</ul>

                        	<button type="submit" class="btn btn-primary" ><i class="fa fa-edit m-right-xs"></i> Save</button>
                        	<a class="btn btn-danger" href="{{ URL('/administrator/managed-accounts/'.Crypt::encrypt($investor->id)) }}"><i class="fa fa-ban m-right-xs"></i> Cancel</a>
                      </div>

                      <div class="col-md-9 col-sm-9 col-xs-12">
                      	<div class="profile_title">
                          	<div class="col-md-6">
                            		<h2>Investor Info</h2>
                          	</div>
                        	</div>
                        	<!-- start of user-activity-graph -->
                    		<table class="table table-striped">
                    			<tr>
                    				<td>Fullname</td>
                    				<td>
                              <input type="text" name="name" class="form-control" value="{{ $investor->name }}" required="" />
                            </td>
                    			</tr>
                    			<tr>
                    				<td>Email</td>
                            <td>
                              <input type="text" name="email" class="form-control" value="{{ $investor->email }}" required="" />
                            </td>
                    			</tr>
                    			<tr>
                    				<td>Broker Name</td>
                            <td>
                              <input type="text" name="broker_name" class="form-control" value="{{ $investor->broker_name }}" required="" />
                            </td>
                    			</tr>
                    			<tr>
                    				<td>MT4 Account Number</td>
                            <td>
                              <input type="text" name="mt4_account_number" class="form-control" value="{{ $investor->mt4_account_number }}" required="" />
                            </td>
                    			</tr>
                    			<tr>
                    				<td>Edit MT4 Account Password</td>
                            <td>
                              <input type="text" name="mt4_password" class="form-control" value="{{ $investor->mt4_password }}" required="" />
                            </td>
                    			</tr>
                    			<tr>
                    				<td>Trading Account Equity</td>
                            <td>
                              <input type="text" name="trading_account_balance" class="form-control" value="{{ $investor->trading_account_balance }}" required="" />
                            </td>
                    			</tr>
                    			<tr>
                    				<td>Subscription</td>
                            <td>
                              <select name="plan" id="plan" class="form-control">
                                <option value="{{ $investor->subplan?$investor->plan:'' }}">{{ $investor->subplan?ucwords($investor->subplan->title).'&nbsp;-&nbsp;$'.$investor->subplan->price:"None selected" }}</option>
                                @foreach($plans as $plan)
                                  <option value="{{ $plan->id }}">{{ ucwords($plan->title).'&nbsp;-&nbsp;$'.$plan->price }}</option>
                                @endforeach
                              </select>
                            </td>
                    			</tr>
                    		</table>
                        	<!-- end of user-activity-graph -->
                      </div>
                  </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  		<div class="modal-dialog modal-sm">
    		<div class="modal-content">

      			<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        			<center><h4 class="modal-title" id="myModalLabel2">Are you sure?</h4></center>
      			</div>
    			<div class="modal-body">
      				<center>
      					<p>This action cannot be reversed.</p>
      					<a href="{{ URL('/administrator/managed-accounts/delete?investor='.$investor->id) }}" class="btn btn-danger">Yes</a>
      					<button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
      				</center>
    			</div>
    		</div>
  		</div>
	</div>
    <?php Session::forget('admin'); ?>
@stop