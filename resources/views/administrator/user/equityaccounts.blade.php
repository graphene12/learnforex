@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
            <div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
				
              	<!-- <div class="title_right">
              	                	<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
              	                  		<div class="input-group">
              	                    		<input type="text" class="form-control" placeholder="Search for...">
              	                    		<span class="input-group-btn">
              	                      			<button class="btn btn-default" type="button">Go!</button>
              	                    		</span>
              	                  		</div>
              	                	</div>
              	</div> -->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  	<div class="x_title">
	                    	<h2>Equity Accounts</h2>
	                    	<div class="clearfix"></div>
	                  	</div>
						@if(count($equities) > 0)
		                	<div class="x_content">
		                    	<div class="table-responsive">
		                      		<table class="table table-striped jambo_table bulk_action">
		                        		<thead>
				                          	<tr class="headings">
					                            <th class="column-title">Fullname </th>
					                            <th class="column-title">Email </th>
					                            <th class="column-title">Equity Account </th>
					                            <th class="column-title">Actions </th>
				                          	</tr>
		                        		</thead>

		                        		<tbody>
		                        			@foreach($equities as $user)
			                          			<tr class="even pointer">
			                            			<td class=" ">{{ $user->name }}</td>
			                            			<td class=" ">{{ $user->email }} </td>
			                            			<td class=" ">${{ $user->equity }} </td>
			                            			<td class=" last">
			                            				<a href="#" data-toggle="modal" data-target="#deleteUserModal{{ $user->id }}">Delete Account</a>
			                            			</td>
			                          			</tr>

			                          			<div class="modal fade" id="deleteUserModal{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
											  		<div class="modal-dialog modal-sm">
											    		<div class="modal-content">

											      			<div class="modal-header">
											        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
											        			<center><h4 class="modal-title" id="myModalLabel2">Are you sure?</h4></center>
											      			</div>
											    			<div class="modal-body">
											      				<center>
											      					<p>This action cannot be reversed.</p>
											      					<a href="{{ URL('/administrator/equity-accounts/delete?id='.$user->id) }}" class="btn btn-danger">Yes</a>
											      					<button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
											      				</center>
											    			</div>
											    		</div>
											  		</div>
												</div>
		                        			@endforeach
		                        		</tbody>
		                      		</table>
		                    	</div>
							</div>
						@else
							<div class="x_content"></div>
						@endif
	                </div>
              	</div>
            </div>
        </div>
    </div>
    <?php Session::forget('admin'); ?>
@stop