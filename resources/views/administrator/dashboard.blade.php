@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
            <div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
             	<div class="col-md-6 col-sm-6 col-xs-12">
                	<div class="x_panel">
                  		<div class="x_title">
                    		<h2>Users <small>Live Trading Room Users</small></h2>
                    		<div class="clearfix"></div>
                  		</div>
                  		@if(count($users) > 0)
	                  		<div class="x_content">
	                    		<table class="table">
	                      			<thead>
	                        			<tr>
	                          				<th>Name</th>
	                          				<th>Email</th>
	                          				<th>Signal Option</th>
	                        			</tr>
	                      			</thead>
	                      			<tbody>
	                      				@foreach(array_slice($users->toArray(), 0, 5) as $user)
		                        			<tr>
		                          				<td>{{ $user['name'] }}</td>
		                          				<td>{{ $user['email'] }}</td>
		                          				<td>{{ $user['signal_option'] }}</td>
		                        			</tr>
	                      				@endforeach
	                      			</tbody>
	                    		</table>
	                  		</div>
	                  	@else
							<div class="x_content"></div>
						@endif
                	</div>
              	</div>

              	<div class="col-md-6 col-sm-6 col-xs-12">
                	<div class="x_panel">
                  		<div class="x_title">
                    		<h2>LOT Size Indicator Orders</h2>
                    		<div class="clearfix"></div>
                  		</div>
                  		@if(count($lot_orders) > 0)
	                  		<div class="x_content">
	                    		<table class="table">
	                      			<thead>
	                        			<tr>
	                          				<th>Name</th>
	                          				<th>Email</th>
	                          				<th>Date Ordered</th>
	                        			</tr>
	                      			</thead>
	                      			<tbody>
	                      				@foreach(array_slice($lot_orders->toArray(), 0, 5) as $order)
		                        			<tr>
		                          				<td>{{ $order['name'] }}</td>
		                          				<td>{{ $order['email'] }}</td>
		                          				<td>{{ Carbon\Carbon::createFromTimeStamp(strtotime($order['created_at']))->diffForHumans() }}</td>
		                        			</tr>
	                      				@endforeach
	                      			</tbody>
	                    		</table>
	                  		</div>
	                  	@else
							<div class="x_content"></div>
	                  	@endif
                	</div>
              	</div>
              	<div class="clearfix"></div>

            	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  	<div class="x_title">
	                    	<h2>Managed Accounts</h2>
	                    	<div class="clearfix"></div>
	                  	</div>
						@if(count($investors) > 0)
		                	<div class="x_content">
		                    	<div class="table-responsive">
		                      		<table class="table table-striped jambo_table bulk_action">
		                        		<thead>
				                          	<tr class="headings">
					                            <th class="column-title">Name </th>
					                            <th class="column-title">Email </th>
					                            <th class="column-title">Broker Name </th>
					                            <th class="column-title">MT4 Account Number </th>
					                            <th class="column-title">Trading Account Balance </th>
					                            <th class="column-title">Subscription Plan </th>
					                            <th class="column-title no-link last"><span class="nobr">Action</span></th>
				                          	</tr>
		                        		</thead>

		                        		<tbody>
		                        			@foreach(array_slice($investors->toArray(), 0, 10) as $investor)
			                          			<tr class="even pointer">
			                            			<td class=" ">{{ $investor['name'] }}</td>
			                            			<td class=" ">{{ $investor['email'] }} </td>
			                            			<td class=" ">{{ $investor['broker_name'] }}</td>
			                            			<td class=" ">{{ $investor['mt4_account_number'] }}</td>
			                            			<td class=" ">${{ $investor['trading_account_balance'] }}</td>
			                            			<td class="a-right a-right ">${{ $investor['plan']?$investor['plan']:0 }}</td>
			                            			<td class=" last"><a href="#">View</a></td>
			                          			</tr>
		                        			@endforeach
		                        		</tbody>
		                      		</table>
		                    	</div>
							</div>
						@else
							<div class="x_content"></div>
						@endif
	                </div>
              	</div>

              	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  	<div class="x_title">
	                    	<h2>Trade Signals</h2>
	                    	<div class="clearfix"></div>
	                  	</div>
						@if(count($signals) > 0)
		                	<div class="x_content">
		                    	<div class="table-responsive">
		                      		<table class="table table-striped jambo_table bulk_action">
		                        		<thead>
				                          	<tr class="headings">
					                            <th class="column-title">Setup Type </th>
					                            <th class="column-title">Entry </th>
					                            <th class="column-title">Stop Loss </th>
					                            <th class="column-title">Quality </th>
					                            <th class="column-title">Duration </th>
					                            <th class="column-title">Risk </th>
					                            <th class="column-title no-link last"><span class="nobr">Action</span></th>
				                          	</tr>
		                        		</thead>

		                        		<tbody>
		                        			@foreach($signals as $signal)
			                          			<tr class="even pointer">
			                            			<td class=" ">{{ $signal['setup_type'] }}</td>
			                            			<td class=" ">{{ $signal['entry'] }} </td>
			                            			<td class=" ">{{ $signal['stop_loss'] }}</td>
			                            			<td class=" ">{{ $signal['quality'] }}</td>
			                            			<td class=" ">{{ $signal['duration'] }}</td>
			                            			<td class="a-right a-right ">{{ $signal['risk'] }}</td>
			                            			<td class=" last">
			                            				<a href="{{ URL('/administrator/signals/'.Crypt::encrypt($signal['id'])) }}">View</a>
			                            				<a href="{{ URL('/administrator/signals/'.Crypt::encrypt($signal['id']).'/edit') }}">Edit</a>
			                            			</td>
			                          			</tr>
		                        			@endforeach
		                        		</tbody>
		                      		</table>
		                    	</div>
							</div>
						@else
							<div class="x_content"></div>
						@endif
	                </div>
              	</div>

              	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  	<div class="x_title">
	                    	<h2>Line Copier EA Registrations</h2>
	                    	<div class="clearfix"></div>
	                  	</div>
						@if(count($line_copiers) > 0)
		                	<div class="x_content">
		                    	<div class="table-responsive">
		                      		<table class="table table-striped jambo_table bulk_action">
		                        		<thead>
				                          	<tr class="headings">
					                            <th class="column-title">Name </th>
					                            <th class="column-title">Email </th>
					                            <th class="column-title">Date </th>
					                            <!-- <th class="column-title no-link last"><span class="nobr">Action</span></th> -->
				                          	</tr>
		                        		</thead>

		                        		<tbody>
		                        			@foreach($line_copiers as $copier)
			                          			<tr class="even pointer">
			                            			<td class=" ">{{ $copier['name'] }}</td>
			                            			<td class=" ">{{ $copier['email'] }} </td>
			                            			<td class=" ">{{ Carbon\Carbon::createFromTimeStamp(strtotime($copier['created_at']))->diffForHumans() }}</td>
			                          			</tr>
		                        			@endforeach
		                        		</tbody>
		                      		</table>
		                    	</div>
							</div>
						@else
							<div class="x_content"></div>
						@endif
	                </div>
              	</div>
            </div>
        </div>
    </div>
@stop