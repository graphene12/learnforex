@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
            <div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
						<form action="{{ URL::current() }}" method="POST" role="form">
		                  	<div class="x_title">
		                    	<h2>Edit</h2>
		                    	<a class="pull-right btn btn-sm btn-default" href="{{ URL('administrator/announcements') }}">Cancel</a>
		                    	<button type="submit" class="pull-right btn btn-sm btn-success">Save Changes</button>
		                    	<div class="clearfix"></div>
		                  	</div>
		                	<div class="x_content">
		                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="admin" value="{{ $admin_id }}">
		                    	<input type="hidden" name="announcement_id" value="{{ $announcement['id'] }}">
                      			<div class="form-group col-md-12 col-sm-12 col-xs-12">
		                            <label class="control-label">Title</label>
		                            <input type="text" id="title" name="title" required="required" class="form-control" value="{{ $announcement['title'] }}">
		                        </div>
                           		<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Trigger Time delay (in secs)</label>
                      				<input type="number" id="delay" name="delay" required="required" class="form-control" value="{{ $announcement['delay'] }}">
                      			</div>
                      			<div class="form-group col-md-6 col-sm-6 col-xs-12">
                    				<label class="control-label">Stop When</label>
                      				<div class="input-group date" id="myDatepicker">
						            	<input type="text" class="form-control" name="stop" required="required" value="{{ $announcement['stop'] }}">
						              	<span class="input-group-addon">
						                	<span class="glyphicon glyphicon-calendar"></span>
						              	</span>
						            </div>
                      			</div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">Button Label <small>this appears on the call to action button</small></label>
                              <input type="text" id="call_to_action" name="call_to_action" required="required" class="form-control" value="{{ $announcement['call_to_action'] }}">
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">Button URL <small>where you want the users to see after clicking the button</small></label>
                              <div class="input-group">
                                <span class="input-group-addon" style="border-radius: 0px;">
                                  <span>{{ URL('/') }}/</span>
                                </span>
                                <input type="text" id="callback_url" name="callback_url" required="required" class="form-control" value="{{ $announcement['callback_url'] }}">
                              </div>
                            </div>
                      			<div class="form-group col-md-12 col-sm-12 col-xs-12">
                    				<label class="control-label">Announcement</label>
                      				<textarea id="content" name="content" rows="5" style="resize:none;" required="required" class="form-control">{{ $announcement['content'] }}</textarea>
                      			</div>
							</div>
						</form>
	                </div>
              	</div>
            </div>
        </div>
    </div>
@stop