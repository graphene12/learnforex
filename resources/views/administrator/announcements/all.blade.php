@extends("includes.administrator.master")
@section("contents")
	<div class="" role="main">
        <div class="">
            <div class="page-title">
              	<div class="title_left">
                	<h3>{{ $title }}</h3>
              	</div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  	<div class="x_title">
	                    	<h2>Announcements</h2>
	                    	<a class="pull-right btn btn-sm btn-success" href="{{ URL('administrator/announcements/add-new') }}">New Announcement</a>
	                    	<div class="clearfix"></div>
	                  	</div>
	                	<div class="x_content">
	                    	<div class="table-responsive">
	                      		<table class="table table-striped jambo_table bulk_action">
	                        		<thead>
			                          	<tr class="headings">
				                            <th class="column-title">Title </th>
				                            <th class="column-title">Delay </th>
				                            <th class="column-title">Stop on</th>
				                            <th class="column-title">Date Added</th>
				                            <th class="column-title">Actions</th>
			                          	</tr>
	                        		</thead>

	                        		<tbody>
	                        			@foreach($announcements as $announcement)
		                          			<tr class="even pointer">
		                            			<td class="">{{ $announcement['title'] }} </td>
		                            			<td class="">{{ $announcement['delay'] }}s </td>
		                            			<td class="">{{ Carbon\Carbon::createFromTimeStamp(strtotime($announcement['stop']))->diffForHumans() }}</td>
		                            			<td class="">{{ Carbon\Carbon::createFromTimeStamp(strtotime($announcement['created_at']))->diffForHumans() }}</td>
		                            			<td class="last">
		                            				<a href="{{ URL::current().'/edit?announcement_id='.Crypt::encrypt($announcement['id']) }}" class="btn btn-xs btn-primary">Edit</a>
		                            				<a href="#" data-toggle="modal" data-target="#deleteAnnouncementModal{{ $announcement['id'] }}" class="btn btn-xs btn-danger">Delete</a>
		                            			</td>
		                          			</tr>

		                          			<div class="modal fade" id="deleteAnnouncementModal{{ $announcement['id'] }}" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
										      	<div class="modal-dialog modal-sm">
										        	<div class="modal-content">
														<div class="modal-header">
										              		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
										              		<center><h4 class="modal-title" id="myModalLabel2">Are you sure?</h4></center>
										            	</div>
										          		<div class="modal-body">
										              		<center>
										                		<p>This action cannot be reversed.</p>
										                		<a href="{{ URL::current().'/delete?announcement_id='.Crypt::encrypt($announcement['id']) }}" class="btn btn-danger">Yes</a>
										                		<button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
										              		</center>
										          		</div>
										        	</div>
										      	</div>
										  	</div>
	                        			@endforeach
	                        		</tbody>
	                      		</table>
	                    	</div>
						</div>
	                </div>
              	</div>
            </div>
        </div>
    </div>
@stop