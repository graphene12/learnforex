<body onLoad="document.submit2cepay_form.submit()">
<!-- 
Note: Replace https://www.cashenvoy.com/sandbox/?cmd=cepay with https://www.cashenvoy.com/webservice/?cmd=cepay once you have been switched to the live environment.
-->
<form method="post" name="submit2cepay_form" action="https://www.cashenvoy.com/webservice/?cmd=cepay" target="_self">
<input type="hidden" name="ce_merchantid" value="{{ $merchant_id }}"/>
<input type="hidden" name="ce_transref" value="{{ $tref }}"/>
<input type="hidden" name="ce_amount" value="{{ $amt }}"/>
<input type="hidden" name="ce_customerid" value="{{ $id }}"/>
<input type="hidden" name="ce_memo" value="{{ $desc }}"/>
<input type="hidden" name="ce_notifyurl" value="https://learnforexlivesignals.com/payment/ce/return"/>
<input type="hidden" name="ce_window" value="self"/><!-- self or parent -->
<input type="hidden" name="ce_signature" value="{{ $sig }}"/>
</form>
</body>